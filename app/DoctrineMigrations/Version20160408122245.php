<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160408122245 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');


        $this->addSql('ALTER TABLE liq_pay_transaction ADD case_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE liq_pay_transaction ADD CONSTRAINT FK_96948A38CF10D4F5 FOREIGN KEY (case_id) REFERENCES debt_case (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_96948A38CF10D4F5 ON liq_pay_transaction (case_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE liq_pay_transaction DROP FOREIGN KEY FK_96948A38CF10D4F5');
        $this->addSql('DROP INDEX IDX_96948A38CF10D4F5 ON liq_pay_transaction');
        $this->addSql('ALTER TABLE liq_pay_transaction DROP case_id');
    }
}

<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160405130617 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE liqpay_order (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, transaction_id INT NOT NULL, payment_id INT NOT NULL, status VARCHAR(100) NOT NULL, type VARCHAR(100) NOT NULL, paytype VARCHAR(100) NOT NULL, order_id VARCHAR(100) NOT NULL, description VARCHAR(255) NOT NULL, amount DOUBLE PRECISION NOT NULL, ip INT NOT NULL, currency VARCHAR(3) NOT NULL, sender_commission DOUBLE PRECISION NOT NULL, receiver_commission DOUBLE PRECISION NOT NULL, custom_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', liqpay_order_id VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_3F5E8952A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE liqpay_order ADD CONSTRAINT FK_3F5E8952A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE liqpay_order');
    }
}

<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160314133335 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE debt_case ADD user_id INT DEFAULT NULL, ADD safe_id VARCHAR(50) NOT NULL, DROP questionnaire, DROP user');
        $this->addSql('ALTER TABLE debt_case ADD CONSTRAINT FK_101866C0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_101866C0A76ED395 ON debt_case (user_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE debt_case DROP FOREIGN KEY FK_101866C0A76ED395');
        $this->addSql('DROP INDEX IDX_101866C0A76ED395 ON debt_case');
        $this->addSql('ALTER TABLE debt_case ADD questionnaire INT NOT NULL, ADD user INT NOT NULL, DROP user_id, DROP safe_id');
    }
}

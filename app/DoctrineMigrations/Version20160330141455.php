<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160330141455 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE autopay_order ADD user_id INT DEFAULT NULL, CHANGE id id INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE autopay_order ADD CONSTRAINT FK_552EE59EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_552EE59EA76ED395 ON autopay_order (user_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE autopay_order DROP FOREIGN KEY FK_552EE59EA76ED395');
        $this->addSql('DROP INDEX IDX_552EE59EA76ED395 ON autopay_order');
        $this->addSql('ALTER TABLE autopay_order DROP user_id, CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL');
    }
}

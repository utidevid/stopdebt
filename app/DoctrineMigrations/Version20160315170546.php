<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160315170546 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE strategy_item ADD case_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE strategy_item ADD CONSTRAINT FK_6409D843CF10D4F5 FOREIGN KEY (case_id) REFERENCES debt_case (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_6409D843CF10D4F5 ON strategy_item (case_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE strategy_item DROP FOREIGN KEY FK_6409D843CF10D4F5');
        $this->addSql('DROP INDEX IDX_6409D843CF10D4F5 ON strategy_item');
        $this->addSql('ALTER TABLE strategy_item DROP case_id');
    }
}

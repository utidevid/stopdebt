<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160315152507 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE debt_case ADD questionnaire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE debt_case ADD CONSTRAINT FK_101866C0CE07E8FF FOREIGN KEY (questionnaire_id) REFERENCES questionnaire (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_101866C0CE07E8FF ON debt_case (questionnaire_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE debt_case DROP FOREIGN KEY FK_101866C0CE07E8FF');
        $this->addSql('DROP INDEX UNIQ_101866C0CE07E8FF ON debt_case');
        $this->addSql('ALTER TABLE debt_case DROP questionnaire_id');
    }
}

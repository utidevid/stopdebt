<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160323141438 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE questionnaire CHANGE last_step last_step SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE reminder CHANGE start_at start_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE debt_case CHANGE tariff tariff VARCHAR(10) DEFAULT \'standard\' NOT NULL, CHANGE stage stage SMALLINT DEFAULT 1 NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE debt_case CHANGE tariff tariff VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, CHANGE stage stage SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE questionnaire CHANGE last_step last_step SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE reminder CHANGE start_at start_at DATETIME NOT NULL');
    }
}

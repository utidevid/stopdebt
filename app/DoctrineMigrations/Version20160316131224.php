<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160316131224 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE questionnaires_documents');
        $this->addSql('ALTER TABLE document ADD case_id INT DEFAULT NULL, ADD uploads LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', DROP filename, DROP is_uploaded_by_assistant');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76CF10D4F5 FOREIGN KEY (case_id) REFERENCES debt_case (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_D8698A76CF10D4F5 ON document (case_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE questionnaires_documents (questionnaire_id INT NOT NULL, document_id INT NOT NULL, INDEX IDX_945B0E1CCE07E8FF (questionnaire_id), INDEX IDX_945B0E1CC33F7837 (document_id), PRIMARY KEY(questionnaire_id, document_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE questionnaires_documents ADD CONSTRAINT FK_945B0E1CC33F7837 FOREIGN KEY (document_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE questionnaires_documents ADD CONSTRAINT FK_945B0E1CCE07E8FF FOREIGN KEY (questionnaire_id) REFERENCES questionnaire (id)');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76CF10D4F5');
        $this->addSql('DROP INDEX IDX_D8698A76CF10D4F5 ON document');
        $this->addSql('ALTER TABLE document ADD filename VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD is_uploaded_by_assistant SMALLINT NOT NULL, DROP case_id, DROP uploads');
    }
}

<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160302141951 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("CREATE TABLE `user` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `email_canonical` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `enabled` tinyint(1) NOT NULL,
              `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `last_login` datetime DEFAULT NULL,
              `locked` tinyint(1) NOT NULL,
              `expired` tinyint(1) NOT NULL,
              `expires_at` datetime DEFAULT NULL,
              `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `password_requested_at` datetime DEFAULT NULL,
              `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
              `credentials_expired` tinyint(1) NOT NULL,
              `credentials_expire_at` datetime DEFAULT NULL,
              `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `patronymic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `locality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

            CREATE TABLE `questionnaire` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `user_id` int(11) DEFAULT NULL,
                `goal_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)',
                `credit_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)',
                `credit_details` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)',
                `credit_extras` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)',
                `penalty_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)',
                `financial_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)',
                `resolution_attempts` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)',
                `status` smallint(6) NOT NULL,
                `custom_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)',
                `created_at` datetime NOT NULL,
                `updated_at` datetime NOT NULL,
                PRIMARY KEY (`id`),
                KEY `IDX_7A64DAFA76ED395` (`user_id`),
                CONSTRAINT `FK_7A64DAFA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
                ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
            ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DROP TABLE questionnaire; DROP TABLE user;');
    }
}

<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160323182829 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE reminder CHANGE start_at start_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE message ADD parent_message_id INT DEFAULT NULL, ADD top_level TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F14399779 FOREIGN KEY (parent_message_id) REFERENCES message (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_B6BD307F14399779 ON message (parent_message_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F14399779');
        $this->addSql('DROP INDEX IDX_B6BD307F14399779 ON message');
        $this->addSql('ALTER TABLE message DROP parent_message_id, DROP top_level');
        $this->addSql('ALTER TABLE reminder CHANGE start_at start_at DATETIME NOT NULL');
    }
}

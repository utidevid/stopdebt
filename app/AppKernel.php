<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
            new Slik\DompdfBundle\SlikDompdfBundle(),
            new Bmatzner\FontAwesomeBundle\BmatznerFontAwesomeBundle(),

            new \FOS\UserBundle\FOSUserBundle(),
            new SD\Bundle\QuestionnaireBundle\QuestionnaireBundle(),
            new SD\Bundle\UserBundle\UserBundle(),
            new SD\Bundle\DocumentsBundle\DocumentsBundle(),
            new SD\Bundle\AssistantBundle\AssistantBundle(),
            new SD\Bundle\StrategyBundle\StrategyBundle(),
            new SD\Bundle\CaseBundle\CaseBundle(),
            new SD\Bundle\UtilsBundle\UtilsBundle(),
            new SD\Bundle\RemindersBundle\RemindersBundle(),
            new SD\Bundle\MessagingBundle\MessagingBundle(),
            new SD\Bundle\PaymentsBundle\SDPaymentsBundle(),
            new SD\Bundle\DashboardBundle\DashboardBundle(),
            new SD\Bundle\ClientBundle\ClientBundle(),
            new SD\Bundle\NotificationsBundle\NotificationsBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Hautelook\AliceBundle\HautelookAliceBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
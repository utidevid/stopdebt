var browserify = require('browserify');
var gulp = require('gulp');
var sass = require('gulp-sass');
var stringify = require('stringify');
var source = require('vinyl-source-stream');
var concatCss = require('gulp-concat-css');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var cleanCSS = require('gulp-clean-css');
var gulpsync = require('gulp-sync')(gulp);

var source_file = {
    css: [
        'web/bundles/bmatznerfontawesome/css/font-awesome.min.css',
        'web/assets/vendor/jquery-ui/themes/base/jquery-ui.min.css',
        'web/assets/vendor/bootstrap/dist/css/bootstrap.min.css',
        'web/assets/vendor/toastr/toastr.min.css',
        'src/SD/Bundle/ClientBundle/Resources/scss/style.scss'
    ],
    js: {
        browserify: 'src/SD/Bundle/ClientBundle/Resources/js/questionnaire_module/app.js',
        concat: [
            'web/assets/vendor/jquery/dist/jquery.min.js',
            'web/assets/vendor/jquery-ui/jquery-ui.min.js',
            'web/assets/vendor/jquery.inputmask/dist/jquery.inputmask.bundle.js',
            'web/assets/vendor/underscore/underscore-min.js',
            'web/assets/vendor/backbone/backbone-min.js',
            'web/assets/vendor/phone-helper/bootstrap-formhelpers-phone.js',
            'web/assets/vendor/bootstrap/dist/js/bootstrap.min.js',
            'web/assets/vendor/toastr/toastr.min.js',
            'src/SD/Bundle/ClientBundle/Resources/js/general_modules/*.js',
            'web/assets/build/questionnaire_module.js'
        ]
    }
};

var dist_loc = {
    css: './web/assets/build/',
    js: {
        browserify: 'web/assets/build/',
        concat: 'web/assets/build/'
    }
};

var watch_files = {
    css: ['src/SD/Bundle/ClientBundle/Resources/scss/*', 'src/SD/Bundle/ClientBundle/Resources/scss/**/*'],
    js: ['src/SD/Bundle/ClientBundle/Resources/js/**/*', 'src/SD/Bundle/ClientBundle/Resources/js/**/**/*']
};

gulp.task('concat:dev', function() {
    return gulp.src(source_file.js.concat)
        .pipe(concat('main.js'))
        .pipe(gulp.dest(dist_loc.js.concat));
});

gulp.task('concat:production', function() {
    return gulp.src(source_file.js.concat)
        .pipe(concat('main.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest(dist_loc.js.concat));
});

gulp.task('browserify', function() {
    return browserify(source_file.js.browserify)
        .transform(stringify, {
            appliesTo: { includeExtensions: ['.ejs'] }
        })
        .bundle()
        .pipe(source('questionnaire_module.js'))
        .pipe(gulp.dest(dist_loc.js.browserify));
});


gulp.task('styles:dev', function() {
    return gulp.src(source_file.css)
        .pipe(sass().on('error', sass.logError))
        .pipe(concatCss("style.css", {rebaseUrls: false}))
        .pipe(gulp.dest(dist_loc.css));
});
gulp.task('styles:production', function() {
    return gulp.src(source_file.css)
        .pipe(sass({outputStyle: "compact"}).on('error', sass.logError))
        .pipe(concatCss("style.css", {rebaseUrls: false}))
        .pipe(cleanCSS())
        .pipe(gulp.dest(dist_loc.css));
});

gulp.task('watch', function() {
    gulp.watch(watch_files.css, ['styles:dev']);
    gulp.watch(watch_files.js, ['js:dev']);
});
gulp.task('js:dev', gulpsync.sync(['browserify', 'concat:dev']));
gulp.task('js:production', gulpsync.sync(['browserify', 'concat:production']));
gulp.task('build:dev', ['styles:dev', 'js:dev']);
gulp.task('build:production', ['styles:production', 'js:production']);
gulp.task('build', ['build:dev']);
gulp.task('default', ['build', 'watch']);
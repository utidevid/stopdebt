$(function() {
    $(document).on('click', 'a[data-method]', function(e) {
        e.preventDefault();
        var $form = $('<form action="' + $(this).attr('href') + '" method="'+ $(this).attr('data-method')  +'"></form>');
        $('body').append($form);
        $form.submit();
    });

    if ($('#notifications-page').length) {
        $.post('/notifications/viewed', {});
    }
});
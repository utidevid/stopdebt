var QUI_COMPONENTS = require('../view/questionnaire-ui-components');
var model = require('../model/Questions');

module.exports = {
    baseElem: '#questionnaire-container',
    init: function() {
        var qData = {};
        try {
            var data = $(this.baseElem).attr('q-data');
            qData = JSON.parse(data);
        } catch(e){
            console.error(e);
        }
        this.model = new model(qData);
        this.render();
    },
    render: function(){
        new QUI_COMPONENTS({
            el: this.baseElem,
            model: this.model
        });
    }
};
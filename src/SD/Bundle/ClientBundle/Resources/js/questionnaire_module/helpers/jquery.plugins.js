(function($) {
    $.fn.valueIfNotDisabled = function() {
        var elem = $(this);

        var disabled = elem.is(':disabled') || !elem.is(':visible');

        if (disabled) {
            if (elem.prop("tagName") == 'SELECT') {
                return 1;
            } else if (elem.prop("tagName") == 'INPUT') {
                if (elem.attr('type') == 'checkbox') {
                    return 0;
                } else if (elem.attr('type') == 'radio') {
                    return 1;
                } else {
                    return '';
                }
            } else if (elem.prop("tagName") == 'TEXTAREA') {
                return '';
            }
        }


        if (elem.prop('tagName') == 'INPUT' && elem.attr('type') == 'checkbox') {
            return +elem.is(':checked');
        }
        else {
            return elem.val();
        }
    };

    $.fn.valueOfCustomSelect = function() {
        var elem = $(this);
        return elem.find('label.active').attr('data-value');
    };
    $.fn.valueOfCustomRadio = function() {
        var elem = $(this);
        return elem.find('input:checked').val();
    };
    $.fn.valueOfCustomCheckbox = function() {
        var elem = $(this);
        var isChecked = elem.is(':checked');
        return isChecked ? 1 : 2;
    };
    $.fn.valueOfCustomInput = function() {
        var elem = $(this);
        var isDisabled = elem.is(':disabled');
        return isDisabled ? '' : elem.val();
    };
})(jQuery);
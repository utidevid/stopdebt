//load jQuery plugins
require('./helpers/jquery.plugins');

var QUI = require('./controller/questionnaire-ui');
var App = {
    initQUI: function(){
        $('#qui-loading').remove();
        $('#questionnaire-container').html('');
        QUI.init();
    }
};

if ($('#questionnaire-container').length) {
    App.initQUI();
}
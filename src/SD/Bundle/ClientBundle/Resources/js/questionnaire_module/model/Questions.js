var Questions = Backbone.Model.extend({
    idAttribute: 'questionnaireId',
    toJSON: function(){
        var res = this.attributes;
        res.showError = this.showError.bind(this);
        return res;
    },
    sync: function(method, model, options){
        var dataToSave = JSON.stringify(model.attributes);
        var status = model.get('status');

        $.post(model.get('send_url'), {
            data: dataToSave,
            status: status
        }, function(res) {
            model.set('validation', res.validation_data);
            options.success(res);
        });
    }
});

Questions.prototype.showError = function() {
    var curr = this.attributes;

    var args = Array.prototype.slice.call(arguments);

    args.unshift('validation');
    args.push('error');

    for (var i = 0; i < args.length; i++) {
        if (curr[args[i]] == undefined) {
            return null; // or ''
        }
        curr = curr[args[i]];
    }

    return curr;
};

module.exports = Questions;
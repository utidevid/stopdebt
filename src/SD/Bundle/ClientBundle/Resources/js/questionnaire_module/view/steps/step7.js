var enableElems = require('../../helpers').enableElems;
var disableElems = require('../../helpers').disableElems;

module.exports = Backbone.View.extend({
    events: {
        'change #qs7-income_type': 'baseIncomeChangeHandler',
        'change #qs7-income_data_kids': 'kidsChangeHandler',
        'change #qs7-income_data_ato': 'atoChangeHandler',
        'change #qs7-income_data_parents': 'parentsChangeHandler',
        'change #qs7-income_data_other': 'otherChangeHandler',
        'change #qs7-income_data_kids_edu': 'schoolChangeHandler',
        'change #qs7-income_data_kids_treatm': 'hospitalChangeHandler',
        'change #qs7-children_additional_expenses': 'childrenAdditionalExpensesHandler',
        'change #qs7-military_additional_expenses': 'militaryAdditionalExpensesHandler',
        'change #qs7-family_additional_expenses': 'familyAdditionalExpensesHandler',
        'change #qs7-family_additional_expenses-treatment': 'familyAdditionalExpensesTreatmentHandler',
        'change #qs7-sick_perps_additional_expenses': 'sickPerpsAdditionalExpensesHandler',
        'change #qs7-sick_perps_additional_expenses-treatment': 'sickPerpsAdditionalExpensesTreatmentHandler',
        'change #qs7-credit_at_different_bank': 'creditAtDifferentBankHandler',
        'change #qs7-personal_expenses': 'personalExpensesHandler',
        'change #qs7-personal_expenses-treatment': 'personalExpensesTreatmentHandler',
        'change #qs7-other_properties': 'otherPropertiesHandler',
        'change #qs7-other_properties-estate': 'otherPropertiesEstateHandler',
        'change #qs7-other_properties-vehicle': 'otherPropertiesVehicleHandler',
        'change #qs7-other_properties-other': 'otherPropertiesOtherHandler',
    },
    baseIncomeChangeHandler: function() {
        var self = this;
        var select = parseInt(this.$el.find('#qs7-income_type').val(), 10);
        _.each([1, 2, 3, 4], function(i) {
            if (i == select) {
                if (i == 4) {
                    disableElems(self.$el.find('#qs7-income_medium'));
                } else {
                    enableElems(self.$el.find('#qs7-income_medium'));
                }
            }
        });
    },
    kidsChangeHandler: function() {
        if (this.$el.find('#qs7-income_data_kids').is(':checked')) {
            enableElems(this.$el.find('#block-has_children'));
            this.childrenAdditionalExpensesHandler();
        } else {
            disableElems(this.$el.find('#block-has_children'));
        }
    },
    atoChangeHandler: function() {
        if (this.$el.find('#qs7-income_data_ato').is(':checked')) {
            enableElems(this.$el.find('#block-has_military'));
            this.militaryAdditionalExpensesHandler();
        } else {
            disableElems(this.$el.find('#block-has_military'));
        }
    },
    parentsChangeHandler: function() {
        if (this.$el.find('#qs7-income_data_parents').is(':checked')) {
            enableElems(this.$el.find('#block-has_family'));
            this.familyAdditionalExpensesHandler();
        } else {
            disableElems(this.$el.find('#block-has_family'));
        }
    },
    otherChangeHandler: function() {
        if (this.$el.find('#qs7-income_data_other').is(':checked')) {
            enableElems(this.$el.find('#block-has_sick_perps'));
            this.sickPerpsAdditionalExpensesHandler();
        } else {
            disableElems(this.$el.find('#block-has_sick_perps'));
        }
    },
    childrenAdditionalExpensesHandler: function() {
        if (this.$el.find('#qs7-children_additional_expenses').is(':checked')) {
            enableElems(this.$el.find('#block-children-additional_expenses'));
            this.schoolChangeHandler();
            this.hospitalChangeHandler();
        } else {
            disableElems(this.$el.find('#block-children-additional_expenses'));
        }
    },
    schoolChangeHandler: function() {
        if (this.$el.find('#qs7-income_data_kids_edu').is(':checked')) {
            enableElems(this.$el.find('#block-children_school'));
        } else {
            disableElems(this.$el.find('#block-children_school'));
        }
    },
    hospitalChangeHandler: function() {
        if (this.$el.find('#qs7-income_data_kids_treatm').is(':checked')) {
            enableElems(this.$el.find('#block-children_treatment'));
        } else {
            disableElems(this.$el.find('#block-children_treatment'));
        }
    },
    militaryAdditionalExpensesHandler: function() {
        if (this.$el.find('#qs7-military_additional_expenses').is(':checked')) {
            enableElems(this.$el.find('#block-military-additional_expenses'));
        } else {
            disableElems(this.$el.find('#block-military-additional_expenses'));
        }
    },
    familyAdditionalExpensesHandler: function() {
        if (this.$el.find('#qs7-family_additional_expenses').is(':checked')) {
            enableElems(this.$el.find('#block-family-additional_expenses'));
            this.familyAdditionalExpensesTreatmentHandler();
        } else {
            disableElems(this.$el.find('#block-family-additional_expenses'));
        }
    },
    familyAdditionalExpensesTreatmentHandler: function() {
        if (this.$el.find('#qs7-family_additional_expenses-treatment').is(':checked')) {
            enableElems(this.$el.find('#block-family_treatment'));
        } else {
            disableElems(this.$el.find('#block-family_treatment'));
        }
    },
    sickPerpsAdditionalExpensesHandler: function() {
        if (this.$el.find('#qs7-sick_perps_additional_expenses').is(':checked')) {
            enableElems(this.$el.find('#block-sick_perps-additional_expenses'));
            this.sickPerpsAdditionalExpensesTreatmentHandler();
        } else {
            disableElems(this.$el.find('#block-sick_perps-additional_expenses'));
        }
    },
    sickPerpsAdditionalExpensesTreatmentHandler: function() {
        if (this.$el.find('#qs7-sick_perps_additional_expenses-treatment').is(':checked')) {
            enableElems(this.$el.find('#block-sick_perps_treatment'));
        } else {
            disableElems(this.$el.find('#block-sick_perps_treatment'));
        }
    },
    creditAtDifferentBankHandler: function() {
        if (this.$el.find('#qs7-credit_at_different_bank').is(':checked')) {
            enableElems(this.$el.find('#block-credit_at_different_bank'));
        } else {
            disableElems(this.$el.find('#block-credit_at_different_bank'));
        }
    },
    personalExpensesHandler: function() {
        if (this.$el.find('#qs7-personal_expenses').is(':checked')) {
            enableElems(this.$el.find('#block-personal_expenses'));
            this.personalExpensesTreatmentHandler();
        } else {
            disableElems(this.$el.find('#block-personal_expenses'));
        }
    },
    personalExpensesTreatmentHandler: function() {
        if (this.$el.find('#qs7-personal_expenses-treatment').is(':checked')) {
            enableElems(this.$el.find('#block-personal_expenses-treatment_cost'));
        } else {
            disableElems(this.$el.find('#block-personal_expenses-treatment_cost'));
        }
    },
    otherPropertiesHandler: function() {
        if (this.$el.find('#qs7-other_properties').is(':checked')) {
            enableElems(this.$el.find('#block-other_properties'));
            this.otherPropertiesEstateHandler();
            this.otherPropertiesVehicleHandler();
            this.otherPropertiesOtherHandler();
        } else {
            disableElems(this.$el.find('#block-other_properties'));
        }
    },
    otherPropertiesEstateHandler: function() {
        if (this.$el.find('#qs7-other_properties-estate').is(':checked')) {
            enableElems(this.$el.find('#block-other_properties-estate'));
        } else {
            disableElems(this.$el.find('#block-other_properties-estate'));
        }
    },
    otherPropertiesVehicleHandler: function() {
        if (this.$el.find('#qs7-other_properties-vehicle').is(':checked')) {
            enableElems(this.$el.find('#block-other_properties-vehicle'));
        } else {
            disableElems(this.$el.find('#block-other_properties-vehicle'));
        }
    },
    otherPropertiesOtherHandler: function() {
        if (this.$el.find('#qs7-other_properties-other').is(':checked')) {
            enableElems(this.$el.find('#block-other_properties-other'));
        } else {
            disableElems(this.$el.find('#block-other_properties-other'));
        }
    },
    initialize: function(options) {
        this.qData = options.qData || {};
        this.QUI = options.parent;
        this.render();
    },
    saveToData: function() {
        this.qData.financial_data.regular_income = this.$el.find('#qs7-income_type').valueIfNotDisabled();;
        this.qData.financial_data.avg_monthly_income = this.$el.find('#qs7-income_medium').valueIfNotDisabled();;
        this.qData.financial_data.guarantor_avg_monthly_income = this.$el.find('#qs7-income_warr_medium').valueIfNotDisabled();;
        this.qData.financial_data.has_children = this.$el.find('#qs7-income_data_kids').valueIfNotDisabled();
        this.qData.financial_data.children_age = this.$el.find('#qs7-income_kids_ages').valueIfNotDisabled();;
        this.qData.financial_data.children_additional_expenses = this.$el.find('#qs7-children_additional_expenses').valueIfNotDisabled();
        this.qData.financial_data.children_additional_expenses_studies = this.$el.find('#qs7-income_data_kids_edu').valueIfNotDisabled();
        this.qData.financial_data.school_monthly_expenses = this.$el.find('#qs7-school_monthly_expenses').valueIfNotDisabled();;
        this.qData.financial_data.children_additional_expenses_treatment = this.$el.find('#qs7-income_data_kids_treatm').valueIfNotDisabled();
        this.qData.financial_data.hospital_monthly_expenses = this.$el.find('#qs7-hospital_monthly_expenses').valueIfNotDisabled();;
        this.qData.financial_data.has_military = this.$el.find('#qs7-income_data_ato').valueIfNotDisabled();
        this.qData.financial_data.military_additional_expenses = this.$el.find('#qs7-military_additional_expenses').valueIfNotDisabled();
        this.qData.financial_data.military_monthly_amount = this.$el.find('#qs7-income_data_ato_sum').valueIfNotDisabled();;
        this.qData.financial_data.has_family = this.$el.find('#qs7-income_data_parents').valueIfNotDisabled();
        this.qData.financial_data.family_age = this.$el.find('#qs7-family_age').valueIfNotDisabled();;
        this.qData.financial_data.family_additional_expenses = this.$el.find('#qs7-family_additional_expenses').valueIfNotDisabled();
        this.qData.financial_data.family_additional_expenses_treatment = this.$el.find('#qs7-family_additional_expenses-treatment').valueIfNotDisabled();
        this.qData.financial_data.family_monthly_amount = this.$el.find('#qs7-income_data_parents_sum').valueIfNotDisabled();;
        this.qData.financial_data.has_sick_perps = this.$el.find('#qs7-income_data_other').valueIfNotDisabled();
        this.qData.financial_data.sick_perps_age = this.$el.find('#qs7-income_data_other_ages').valueIfNotDisabled();;
        this.qData.financial_data.sick_perps_additional_expenses = this.$el.find('#qs7-sick_perps_additional_expenses').valueIfNotDisabled();
        this.qData.financial_data.sick_perps_additional_expenses_treatment = this.$el.find('#qs7-sick_perps_additional_expenses-treatment').valueIfNotDisabled();
        this.qData.financial_data.sick_perps_monthly_amount = this.$el.find('#qs7-income_data_other_sum').valueIfNotDisabled();;
        this.qData.financial_data.credit_at_different_bank = this.$el.find('#qs7-credit_at_different_bank').valueIfNotDisabled();
        this.qData.financial_data.credit_at_different_bank_monthly_pay = this.$el.find('#qs7-credit_at_different_bank-monthly_pay').valueIfNotDisabled();;
        this.qData.financial_data.credit_at_different_bank_status = this.$el.find('#qs7-credit_at_different_bank-status').valueIfNotDisabled();;
        this.qData.financial_data.personal_expenses = this.$el.find('#qs7-personal_expenses').valueIfNotDisabled();
        this.qData.financial_data.personal_expenses_treatment = this.$el.find('#qs7-personal_expenses-treatment').valueIfNotDisabled();
        this.qData.financial_data.personal_expenses_treatment_cost = this.$el.find('#qs7-personal_expenses-treatment_cost').valueIfNotDisabled();;
        this.qData.financial_data.other_properties = this.$el.find('#qs7-other_properties').valueIfNotDisabled();

        this.qData.financial_data.other_properties_estate = this.$el.find('#qs7-other_properties-estate').valueIfNotDisabled();
        this.qData.financial_data.other_properties_estate_in_pledge = this.$el.find('#qs7-other_properties-estate_in_pledge').valueIfNotDisabled();
        this.qData.financial_data.other_properties_vehicle = this.$el.find('#qs7-other_properties-vehicle').valueIfNotDisabled();
        this.qData.financial_data.other_properties_vehicle_in_pledge = this.$el.find('#qs7-other_properties-vehicle_in_pledge').valueIfNotDisabled();
        this.qData.financial_data.other_properties_other = this.$el.find('#qs7-other_properties-other').valueIfNotDisabled();
        this.qData.financial_data.other_properties_other_in_pledge = this.$el.find('#qs7-other_properties-other_pledge').valueIfNotDisabled();
    },
    render: function() {
        var self = this;
        var html = this.QUI.renderTemplate('step7', {
            data: this.qData
        }, function(html) {
            self.$el.html(html);

            self.baseIncomeChangeHandler();
            self.kidsChangeHandler();
            self.atoChangeHandler();
            self.parentsChangeHandler();
            self.otherChangeHandler();
            self.creditAtDifferentBankHandler();
            self.personalExpensesHandler();
            self.otherPropertiesHandler();
        });
    }
});
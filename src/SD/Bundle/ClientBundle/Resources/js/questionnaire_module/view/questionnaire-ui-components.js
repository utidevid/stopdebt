var Steps = require('./steps/index');
var compiledTemplates = require('../template/index');

module.exports = Backbone.View.extend({
    events: {
        'click .next-btn': 'nextStepBtnHandler',
        'click .prev-btn': 'prevStepBtnHandler',
        'click .go-to-step': 'goToStepBtnHandler'
    },
    saveToServer: function(status, cb) {

        this.model.set('last_step', this.activeStep);
        this.model.save({status: status}, {success: cb});
    },
    nextStepBtnHandler: function(e) {
        e.preventDefault();
        var self = this;

        if (this.stepView) {
            this.stepView.saveToData();
        }

        this.saveToServer(
            this.activeStep == this.totalSteps ? 2 : 1,
            function(model, response) {
                if (response.status == 'ok') {
                    self.activeStep += 1;

                    if (self.activeStep > self.totalSteps) {
                        self.onFinish.apply(self);
                        return;
                    }
                }
                self.render();
            }
        );
    },
    prevStepBtnHandler: function(e) {
        e.preventDefault();

        if (this.stepView) {
            this.stepView.saveToData();
        }

        this.activeStep -= 1;
        if (this.activeStep == 0) {
            this.activeStep = 1;
        }

        this.saveToServer(1);

        this.render();
    },
    goToStepBtnHandler: function(e) {
        e.preventDefault();

        if (this.stepView) {
            this.stepView.saveToData();
        }

        this.saveToServer(1);
        this.activeStep = parseInt($(e.currentTarget).attr('data-step'), 10);
        this.render();
    },
    initialize: function() {
        this.onFinish = function() {
            window.location = this.get('redirect_on_finish') || '/';
        }.bind(this.model);

        this.totalSteps = 8;
        this.activeStep = this.model.get('last_step');

        this.render();
    },
    render: function() {
        this.renderTemplate('base', {
            activeStep: this.activeStep,
            totalSteps: this.totalSteps
        }, function(html) {
            this.$el.html(html);

            var stepViewFnc = Steps[this.activeStep];

            if (this.stepView) {
                this.stepView.remove();
            }
            this.stepView = new stepViewFnc({
                qData: this.model.toJSON(),
                el: this.$el.selector + ' .step-container',
                parent: this
            });

        // todo:  scroll to Top if necessary
        }.bind(this));
    },
    renderTemplate: function(name, obj, cb) {
        obj = obj || {};
        cb = cb || function() {};
        cb(compiledTemplates[name](obj));
    }
});
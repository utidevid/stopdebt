module.exports = Backbone.View.extend({
    initialize: function(options) {
        this.qData = options.qData || {};
        this.QUI = options.parent;
        this.start_config();
        this.render();
    },
    start_config: function(){
        //null
    },
    switch_status: function(e){
        $(e.currentTarget).parents('.dibs').find('.dib label').removeClass('active');
        $(e.currentTarget).addClass('active');
    },
    render: function() {
        var self = this;
        this.QUI.renderTemplate(this.view_name, {
            data: this.qData
        }, function(html) {
            self.$el.html(html);
            self.after_render();
        });
    },
    after_render: function(){
        //null
    },
    saveToData: function(){
        _.each(this.fields, this.saveFields.bind(this));
    },
    saveFields: function(fields, section){
        _.each(fields, this.saveField.bind(this, section));
    },
    saveField: function(section, method, field){
        var element = this.$el.find('#qs-'+ field);
        if(element.length) {
            this.qData[section][field] = element[method]();
        }
    },
    controlDisableEnable: function(e){
        var element = $(e.currentTarget);
        var value = element.val();
        var method = value == 1 ? this.disableElement : this.enableElement;
        var control = element.parents('[data-control]');
        if(control.length){
            this.performDisableEnable(control.attr('data-control'), method);
        }
    },
    performDisableEnable: function(actionInfo, method){
        var ids = actionInfo.split(',');
        _.each(ids, function(id){
            var element = $('#qs-' + id);
            if(element.length) {
                method(element);
            }
        });
    },
    disableElement: function(element){
        var container = element.parents('.bordered');
        container.removeClass('disabled');
        container.find('input').prop('disabled', false);
    },
    enableElement: function(element){
        var container = element.parents('.bordered');
        container.addClass('disabled');
        container.find('input').prop('disabled', true);
    },
    fields: {}
});
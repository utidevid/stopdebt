var QUI_AUTOCOMPLETE = require('../../helpers').QUI_AUTOCOMPLETE;
var step_template = require('./step_template');

module.exports = step_template.extend({
    events: {
        'click .dib label:not(.active)': "switch_status"
    },
    view_name: 'step1',
    fields: {
        user: {
            last_name: "valueOfCustomInput",
            first_name: "valueOfCustomInput",
            patronymic: "valueOfCustomInput",
            locality: "valueOfCustomInput",
            phone_number: "valueOfCustomInput",
            passport: "valueOfCustomInput",
            identification_code: "valueOfCustomInput",
            birthdate: "valueOfCustomInput"
        },
        goal: {
            client_status: "valueOfCustomSelect"
        }
    },
    after_render: function() {
        this.$el.find('#qs-birthdate').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true
        });

        this.$el.find('#qs-phone_number').inputmask({
            mask: '+38(999) 999-99-99'
        });

        this.$el.find('#qs-locality').autocomplete({
            source: QUI_AUTOCOMPLETE.cities,
            minLength: 2
        });
    }
});
module.exports = {
    1: require('./step1'),
    2: require('./step2'),
    3: require('./step3'),
    4: require('./step4'),
    5: require('./step5'),
    6: require('./step6'),
    7: require('./step7'),
    8: require('./step8')
};
var QUI_AUTOCOMPLETE = require('../../helpers').QUI_AUTOCOMPLETE;
var step_template = require('./step_template');

module.exports = step_template.extend({
    events: {
        'change #qs-is_delayed input': 'controlDisableEnable'
    },
    start_config: function(){
        this.view_name = this.qData.caseTariff == 'standard' ? 'step4_simple' : 'step4';
    },
    fields: {
        credit_details: {
            credit_left_amount: 'valueOfCustomInput',
            credit_body_amount: 'valueOfCustomInput',
            credit_left_rate: 'valueOfCustomInput',
            credit_fines: 'valueOfCustomInput',
            is_delayed: 'valueOfCustomRadio',
            delay_days: 'valueOfCustomInput',
            delay_amount: 'valueOfCustomInput',
            paid_to_bank_amount: 'valueOfCustomInput',
            bank_name: 'valueOfCustomInput',
            agreement_id: 'valueOfCustomInput',
            credit_issue_date: 'valueOfCustomInput',
            credit_close_date: 'valueOfCustomInput',
            issued_credit_amount: 'valueOfCustomInput',
            credit_rate: 'valueOfCustomInput',
            monthly_pay: 'valueOfCustomInput'
        }
    },
    after_render: function() {

        this.$el.find('#qs-bank_name').autocomplete({
            source: QUI_AUTOCOMPLETE.banks,
            minLength: 2
        });

        this.$el.find('#qs-credit_issue_date').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true
        });

        this.$el.find('#qs-credit_close_date').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true
        });
    }
});
var enableElems = require('../../helpers').enableElems;
var disableElems = require('../../helpers').disableElems;

module.exports = Backbone.View.extend({
    events: {
        'click #qs-main_goal div label': 'changeGoal',
        'change [name="qs1-goal2-a"]': 'goal2TypeChangeHandler',
        'change #qs1-goal2-b': 'goal2SelectBChangeHandler',
        'change #qs1-goal2-e': 'goal2SelectEChangeHandler',
        'change [name="qs1-goal4-a"]': 'goal4TypeChangeHandler',
        'change #qs1-goal4-b': 'goal4SelectBChangeHandler'
    },
    changeGoal: function(e){
        $(e.currentTarget).parents('.goals').find('div label').removeClass('active');
        $(e.currentTarget).addClass('active');
        this.goalChangeHandler();
    },
    goalChangeHandler: function() {
        var self = this;
        var goalVal = parseInt(this.$el.find('#qs-main_goal label.active').attr('data-value'), 10);
        _.each([1, 2, 3, 4], function(i) {
            if (i == goalVal) {
                enableElems(self.$el.find('#qs-goal-' + i + '-set'));

                if (i == 2) {
                    self.goal2TypeChangeHandler();
                } else if (i == 4) {
                    self.goal4TypeChangeHandler();
                }
            } else {
                disableElems(self.$el.find('#qs-goal-' + i + '-set'));
            }
        });
    },
    goal2TypeChangeHandler: function() {
        var self = this;
        var goalVal = parseInt(this.$el.find('[name="qs1-goal2-a"]:checked').val(), 10);

        _.each([1, 2, 3], function(i) {
            if (i == goalVal) {
                enableElems(self.$el.find('#qs-goal-2-set-' + i));

                if (i == 1) {
                    self.goal2SelectBChangeHandler();
                } else if (i == 3) {
                    self.goal2SelectEChangeHandler();
                }
            } else {
                disableElems(self.$el.find('#qs-goal-2-set-' + i));
            }
        });
    },
    goal2SelectBChangeHandler: function() {
        var fVal = this.$el.find('#qs1-goal2-b').val();

        if (fVal == 1) {
            this.$el.find('#qs1-goal2-c').parent().show();
            this.$el.find('#qs1-goal2-c2').hide();
        } else {
            this.$el.find('#qs1-goal2-c').parent().hide();
            this.$el.find('#qs1-goal2-c2').show();
        }
    },
    goal2SelectEChangeHandler: function() {
        var fVal = this.$el.find('#qs1-goal2-e').val();

        if (fVal == 1) {
            this.$el.find('#qs1-goal2-f').parent().show();
            this.$el.find('#qs1-goal2-f2').hide();
        } else {
            this.$el.find('#qs1-goal2-f').parent().hide();
            this.$el.find('#qs1-goal2-f2').show();
        }
    },
    goal4TypeChangeHandler: function() {
        var self = this;
        var goalVal = parseInt(this.$el.find('[name="qs1-goal4-a"]:checked').val(), 10);

        _.each([1, 2], function(i) {
            if (i == goalVal) {
                enableElems(self.$el.find('#qs-goal-4-set-' + i));

                if (i == 1) {
                    self.goal4SelectBChangeHandler();
                }
            } else {
                disableElems(self.$el.find('#qs-goal-4-set-' + i));
            }
        });
    },
    goal4SelectBChangeHandler: function() {
        var fVal = this.$el.find('#qs1-goal4-b').val();

        if (fVal == 1) {
            this.$el.find('#qs1-goal4-c').parent().show();
            this.$el.find('#qs1-goal4-c2').hide();
        } else {
            this.$el.find('#qs1-goal4-c').parent().hide();
            this.$el.find('#qs1-goal4-c2').show();
        }
    },
    saveToData: function() {
        this.qData.goal.main_goal = this.$el.find('#qs-main_goal').valueOfCustomSelect();
        this.qData.goal.exchange_to_uah = this.$el.find('#qs1-goal1-exchange').valueIfNotDisabled();
        this.qData.goal.monthly_pay = this.$el.find('#qs1-goal1-amount').valueIfNotDisabled();
        this.qData.goal.property_type = this.$el.find('[name="qs1-goal2-a"]:checked').valueIfNotDisabled();
        this.qData.goal.property_type_1_pay_with_discount = this.$el.find('#qs1-goal2-b').valueIfNotDisabled();
        this.qData.goal.property_type_1_amount = this.$el.find('#qs1-goal2-c').valueIfNotDisabled();
        this.qData.goal.property_type_2_relocation = this.$el.find('#qs1-goal2-d').valueIfNotDisabled();
        this.qData.goal.property_type_3_pay_with_discount = this.$el.find('#qs1-goal2-e').valueIfNotDisabled();
        this.qData.goal.property_type_3_amount = this.$el.find('#qs1-goal2-f').valueIfNotDisabled();
        this.qData.goal.bankruptcy_type = this.$el.find('[name="qs1-goal4-a"]:checked').valueIfNotDisabled();
        this.qData.goal.bankruptcy_pay_with_discount = this.$el.find('#qs1-goal4-b').valueIfNotDisabled();
        this.qData.goal.bankruptcy_amount = this.$el.find('#qs1-goal4-c').valueIfNotDisabled();
    },
    initialize: function(options) {
        this.qData = options.qData || {};
        this.QUI = options.parent;
        this.render();
    },
    render: function() {
        var self = this;
        var html = this.QUI.renderTemplate('step2', {
            data: this.qData
        }, function(html) {
            self.$el.html(html);
            self.goalChangeHandler();
        });
    }
});
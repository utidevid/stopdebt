var enableElems = require('../../helpers').enableElems;
var disableElems = require('../../helpers').disableElems;
var QUI_AUTOCOMPLETE = require('../../helpers').QUI_AUTOCOMPLETE;

module.exports = Backbone.View.extend({
    events: {
        'change #qs5-credit_extras-h-mortgage': 'enableMortgage',
        //'change #qs5-credit_extras-mortgage-h-apartment': 'enableMortgageApartment',
        'change #qs5-credit_extras-mortgage-apartment4': 'enableMortgageApartmentPerps',
        //'change #qs5-credit_extras-mortgage-h-house': 'enableMortgageHouse',
        'change #qs5-credit_extras-mortgage-house4': 'enableMortgageHousePerps',
        //'change #qs5-credit_extras-mortgage-h-estate': 'enableMortgageEstate',
        'change #qs5-credit_extras-mortgage-estate4': 'enableMortgageEstateProfit',
        //'change #qs5-credit_extras-mortgage-h-area': 'enableMortgageArea',
        //'change #qs5-credit_extras-mortgage-h-other': 'enableMortgageOther',
        'change #qs5-credit_extras-h-surety': 'enableSurety',
        'change #qs5-credit_extras-h-pledge': 'enablePledge',
        'change #qs5-credit_extras-pledge-h-car': 'enablePledgeCar',
        'change #qs5-credit_extras-pledge-h-movables': 'enablePledgeMovables',
        'change #qs5-credit_extras-pledge-h-securities': 'enablePledgeSecurities',
        'change #qs5-credit_extras-pledge-h-corporate_rights': 'enablePledgeCorporateRights',
        'change #qs5-credit_extras-pledge-corporate_rights1': 'enablePledgeCorporateRightsOperated',
        'change #qs5-credit_extras-pledge-h-other': 'enablePledgeOther',
        'change #qs5-credit_extras-h-nothing': 'enableNothing',
        'change [name="qs5-credit_extras-mortgage-h-option"]': 'mortgageOptionHandler'
    },
    mortgageOptionHandler: function() {
        var self = this;
        var mortgageOption = parseInt(this.$el.find('[name="qs5-credit_extras-mortgage-h-option"]:checked').val(), 10);

        _.each([1,2,3,4,5], function(i) {
            if (i == mortgageOption) {
                if (i == 1) {
                    // enable apartment
                    enableElems(self.$el.find('#block-credit_extras-mortgage-apartment'));
                    self.enableMortgageApartmentPerps();

                    // disable others
                    disableElems(self.$el.find('#block-credit_extras-mortgage-house'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-estate'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-area'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-other'));
                }
                else if (i == 2) {
                    // enable house
                    enableElems(self.$el.find('#block-credit_extras-mortgage-house'));
                    self.enableMortgageHousePerps();

                    // disable others
                    disableElems(self.$el.find('#block-credit_extras-mortgage-apartment'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-estate'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-area'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-other'));
                }
                else if (i == 3) {
                    // enable estate
                    enableElems(self.$el.find('#block-credit_extras-mortgage-estate'));
                    self.enableMortgageEstateProfit();

                    // disable others
                    disableElems(self.$el.find('#block-credit_extras-mortgage-apartment'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-house'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-area'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-other'));
                }
                else if (i == 4) {
                    // enable area
                    enableElems(self.$el.find('#block-credit_extras-mortgage-area'));

                    // disable others
                    disableElems(self.$el.find('#block-credit_extras-mortgage-apartment'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-house'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-estate'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-other'));
                }
                else if (i == 5) {
                    // enable other
                    enableElems(self.$el.find('#block-credit_extras-mortgage-other'));

                    // disable others
                    disableElems(self.$el.find('#block-credit_extras-mortgage-apartment'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-house'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-estate'));
                    disableElems(self.$el.find('#block-credit_extras-mortgage-area'));
                }
            }
        });
    },
    enableMortgage: function() {

        if (this.$el.find('#qs5-credit_extras-h-mortgage').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-mortgage'));

            this.mortgageOptionHandler();
            /*this.enableMortgageApartment();
             this.enableMortgageHouse();
             this.enableMortgageEstate();
             this.enableMortgageArea();
             this.enableMortgageOther();*/

            // Disable nothing section
            this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', false);
            this.enableNothing();
        } else {
            disableElems(this.$el.find('#block-credit_extras-mortgage'));

            if (!this.$el.find('#qs5-credit_extras-h-pledge').is(':checked') && !this.$el.find('#qs5-credit_extras-h-surety').is(':checked')) {
                this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', true);
                this.enableNothing();
            }
        }
    },
    enableMortgageApartment: function() {
        if (this.$el.find('#qs5-credit_extras-mortgage-h-apartment').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-mortgage-apartment'));
            this.enableMortgageApartmentPerps();
        } else {
            disableElems(this.$el.find('#block-credit_extras-mortgage-apartment'));
        }
    },
    enableMortgageApartmentPerps: function() {
        var self = this;
        var apartmentPerpsVal = parseInt(this.$el.find('#qs5-credit_extras-mortgage-apartment4').val(), 10);
        _.each([1, 2], function(i) {
            if (i == apartmentPerpsVal) {
                if (i == 1) {
                    enableElems(self.$el.find('#qs5-credit_extras-mortgage-apartment5'));
                } else if (i == 2) {
                    disableElems(self.$el.find('#qs5-credit_extras-mortgage-apartment5'));
                }
            }
        });
    },
    enableMortgageHouse: function() {
        if (this.$el.find('#qs5-credit_extras-mortgage-h-house').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-mortgage-house'));
            this.enableMortgageHousePerps();
        } else {
            disableElems(this.$el.find('#block-credit_extras-mortgage-house'));
        }
    },
    enableMortgageHousePerps: function() {
        var self = this;
        var housePerpsVal = parseInt(this.$el.find('#qs5-credit_extras-mortgage-house4').val(), 10);
        _.each([1, 2], function(i) {
            if (i == housePerpsVal) {
                if (i == 1) {
                    enableElems(self.$el.find('#qs5-credit_extras-mortgage-house5'));
                } else if (i == 2) {
                    disableElems(self.$el.find('#qs5-credit_extras-mortgage-house5'));
                }
            }
        });
    },
    enableMortgageEstate: function() {
        if (this.$el.find('#qs5-credit_extras-mortgage-h-estate').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-mortgage-estate'));
            this.enableMortgageEstateProfit();
        } else {
            disableElems(this.$el.find('#block-credit_extras-mortgage-estate'));
        }
    },
    enableMortgageEstateProfit: function() {
        var self = this;
        var estateOperatedVal = parseInt(this.$el.find('#qs5-credit_extras-mortgage-estate4').val(), 10);
        _.each([1, 2], function(i) {
            if (i == estateOperatedVal) {
                if (i == 1) {
                    enableElems(self.$el.find('#qs5-credit_extras-mortgage-estate5'));
                } else if (i == 2) {
                    disableElems(self.$el.find('#qs5-credit_extras-mortgage-estate5'));
                }
            }
        });
    },
    enableMortgageArea: function() {
        if (this.$el.find('#qs5-credit_extras-mortgage-h-area').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-mortgage-area'));

        } else {
            disableElems(this.$el.find('#block-credit_extras-mortgage-area'));
        }
    },
    enableMortgageOther: function() {
        if (this.$el.find('#qs5-credit_extras-mortgage-h-other').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-mortgage-other'));

        } else {
            disableElems(this.$el.find('#block-credit_extras-mortgage-other'));
        }
    },
    enableSurety: function() {
        if (this.$el.find('#qs5-credit_extras-h-surety').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-surety'));

            // Disable nothing section
            this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', false);
            this.enableNothing();
        } else {
            disableElems(this.$el.find('#block-credit_extras-surety'));

            if (!this.$el.find('#qs5-credit_extras-h-mortgage').is(':checked') && !this.$el.find('#qs5-credit_extras-h-pledge').is(':checked')) {
                this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', true);
                this.enableNothing();
            }
        }
    },
    enablePledge: function() {
        if (this.$el.find('#qs5-credit_extras-h-pledge').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-pledge'));

            this.enablePledgeCar();
            this.enablePledgeMovables();
            this.enablePledgeSecurities();
            this.enablePledgeCorporateRights();
            this.enablePledgeOther();

            // Disable nothing section
            this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', false);
            this.enableNothing();

        } else {
            disableElems(this.$el.find('#block-credit_extras-pledge'));

            if (!this.$el.find('#qs5-credit_extras-h-mortgage').is(':checked') && !this.$el.find('#qs5-credit_extras-h-surety').is(':checked')) {
                this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', true);
                this.enableNothing();
            }
        }
    },
    enablePledgeCar: function() {
        if (this.$el.find('#qs5-credit_extras-pledge-h-car').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-pledge-car'));
        } else {
            disableElems(this.$el.find('#block-credit_extras-pledge-car'));
        }
    },
    enablePledgeMovables: function() {
        if (this.$el.find('#qs5-credit_extras-pledge-h-movables').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-pledge-movables'));
        } else {
            disableElems(this.$el.find('#block-credit_extras-pledge-movables'));
        }
    },
    enablePledgeSecurities: function() {
        if (this.$el.find('#qs5-credit_extras-pledge-h-securities').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-pledge-securities'));
        } else {
            disableElems(this.$el.find('#block-credit_extras-pledge-securities'));
        }
    },
    enablePledgeCorporateRights: function() {
        if (this.$el.find('#qs5-credit_extras-pledge-h-corporate_rights').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-pledge-corporate_rights'));
            this.enablePledgeCorporateRightsOperated();
        } else {
            disableElems(this.$el.find('#block-credit_extras-pledge-corporate_rights'));
        }
    },
    enablePledgeCorporateRightsOperated: function() {
        var self = this;
        var corporateFactoryOperated = parseInt(this.$el.find('#qs5-credit_extras-pledge-corporate_rights1').val(), 10);
        _.each([1, 2], function(i) {
            if (i == corporateFactoryOperated) {
                if (i == 1) {
                    enableElems(self.$el.find('#qs5-credit_extras-pledge-corporate_rights2'));
                } else if (i == 2) {
                    disableElems(self.$el.find('#qs5-credit_extras-pledge-corporate_rights2'));
                }
            }
        });
    },
    enablePledgeOther: function() {
        if (this.$el.find('#qs5-credit_extras-pledge-h-other').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-pledge-other'));
        } else {
            disableElems(this.$el.find('#block-credit_extras-pledge-other'));
        }
    },
    enableNothing: function() {
        if (this.$el.find('#qs5-credit_extras-h-nothing').is(':checked')) {
            enableElems(this.$el.find('#block-credit_extras-nothing'));

            this.$el.find('#qs5-credit_extras-h-mortgage').prop('checked', false);
            disableElems(this.$el.find('#block-credit_extras-mortgage'));
            this.$el.find('#qs5-credit_extras-h-surety').prop('checked', false);
            disableElems(this.$el.find('#block-credit_extras-surety'));
            this.$el.find('#qs5-credit_extras-h-pledge').prop('checked', false);
            disableElems(this.$el.find('#block-credit_extras-pledge'));

        } else {
            if (this.$el.find('#qs5-credit_extras-h-mortgage').is(':checked') ||
                this.$el.find('#qs5-credit_extras-h-surety').is(':checked') ||
                this.$el.find('#qs5-credit_extras-h-pledge').is(':checked')) {

                disableElems(this.$el.find('#block-credit_extras-nothing'));
            }
            else {
                this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', true);
            }
        }
    },
    initialize: function(options) {
        this.qData = options.qData || {};
        this.QUI = options.parent;
        this.render();
    },
    saveToData: function() {
        this.qData.credit_extras.mortgage = this.$el.find('#qs5-credit_extras-h-mortgage').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_option = this.$el.find('[name="qs5-credit_extras-mortgage-h-option"]:checked').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_city = this.$el.find('#qs5-credit_extras-mortgage1').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_county = this.$el.find('#qs5-credit_extras-mortgage2').valueIfNotDisabled();
        //this.qData.credit_extras.mortgage_apartment = this.$el.find('#qs5-credit_extras-mortgage-h-apartment').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_apartment_area = this.$el.find('#qs5-credit_extras-mortgage-apartment1').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_apartment_state = this.$el.find('#qs5-credit_extras-mortgage-apartment2').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_apartment_cost = this.$el.find('#qs5-credit_extras-mortgage-apartment3').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_apartment_is_registered_perps = this.$el.find('#qs5-credit_extras-mortgage-apartment4').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_apartment_perps = this.$el.find('#qs5-credit_extras-mortgage-apartment5').valueIfNotDisabled();
        //this.qData.credit_extras.mortgage_house = this.$el.find('#qs5-credit_extras-mortgage-h-house').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_house_area = this.$el.find('#qs5-credit_extras-mortgage-house1').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_house_state = this.$el.find('#qs5-credit_extras-mortgage-house2').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_house_cost = this.$el.find('#qs5-credit_extras-mortgage-house3').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_house_is_registered_perps = this.$el.find('#qs5-credit_extras-mortgage-house4').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_house_perps = this.$el.find('#qs5-credit_extras-mortgage-house5').valueIfNotDisabled();
        //this.qData.credit_extras.mortgage_estate = this.$el.find('#qs5-credit_extras-mortgage-h-estate').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_estate_area = this.$el.find('#qs5-credit_extras-mortgage-estate1').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_estate_state = this.$el.find('#qs5-credit_extras-mortgage-estate2').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_estate_cost = this.$el.find('#qs5-credit_extras-mortgage-estate3').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_estate_operated = this.$el.find('#qs5-credit_extras-mortgage-estate4').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_estate_profit = this.$el.find('#qs5-credit_extras-mortgage-estate5').valueIfNotDisabled();
        //this.qData.credit_extras.mortgage_area = this.$el.find('#qs5-credit_extras-mortgage-h-area').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_area_area = this.$el.find('#qs5-credit_extras-mortgage-area1').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_area_purpose = this.$el.find('#qs5-credit_extras-mortgage-area2').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_area_cost = this.$el.find('#qs5-credit_extras-mortgage-area3').valueIfNotDisabled();
        //this.qData.credit_extras.mortgage_other = this.$el.find('#qs5-credit_extras-mortgage-h-other').valueIfNotDisabled();
        this.qData.credit_extras.mortgage_other_cost = this.$el.find('#qs5-credit_extras-mortgage-other1').valueIfNotDisabled();

        if (this.qData.caseTariff != 'standard') {
            this.qData.credit_extras.surety = this.$el.find('#qs5-credit_extras-h-surety').valueIfNotDisabled();
            this.qData.credit_extras.surety_person_fullname = this.$el.find('#qs5-credit_extras-surety1').valueIfNotDisabled();
            this.qData.credit_extras.surety_person_phone = this.$el.find('#qs5-credit_extras-surety2').valueIfNotDisabled();
            this.qData.credit_extras.surety_person_email = this.$el.find('#qs5-credit_extras-surety3').valueIfNotDisabled();
            this.qData.credit_extras.surety_person_contact_details = this.$el.find('#qs5-credit_extras-surety4').valueIfNotDisabled();
        }

        this.qData.credit_extras.pledge = this.$el.find('#qs5-credit_extras-h-pledge').valueIfNotDisabled();
        this.qData.credit_extras.pledge_car = this.$el.find('#qs5-credit_extras-pledge-h-car').valueIfNotDisabled();
        this.qData.credit_extras.pledge_car_brand = this.$el.find('#qs5-credit_extras-pledge-car1').valueIfNotDisabled();
        this.qData.credit_extras.pledge_car_manufacture_year = this.$el.find('#qs5-credit_extras-pledge-car2').valueIfNotDisabled();
        this.qData.credit_extras.pledge_car_cost = this.$el.find('#qs5-credit_extras-pledge-car3').valueIfNotDisabled();
        this.qData.credit_extras.pledge_car_registration_year = this.$el.find('#qs5-credit_extras-pledge-car4').valueIfNotDisabled();
        this.qData.credit_extras.pledge_movables = this.$el.find('#qs5-credit_extras-pledge-h-movables').valueIfNotDisabled();
        this.qData.credit_extras.pledge_movables_name = this.$el.find('#qs5-credit_extras-pledge-movables1').valueIfNotDisabled();
        this.qData.credit_extras.pledge_movables_manufacture_year = this.$el.find('#qs5-credit_extras-pledge-movables2').valueIfNotDisabled();
        this.qData.credit_extras.pledge_movables_cost = this.$el.find('#qs5-credit_extras-pledge-movables3').valueIfNotDisabled();
        this.qData.credit_extras.pledge_movables_registration_year = this.$el.find('#qs5-credit_extras-pledge-movables4').valueIfNotDisabled();
        this.qData.credit_extras.pledge_securities = this.$el.find('#qs5-credit_extras-pledge-h-securities').valueIfNotDisabled();
        this.qData.credit_extras.pledge_securities_cost = this.$el.find('#qs5-credit_extras-pledge-securities1').valueIfNotDisabled();
        this.qData.credit_extras.pledge_corporate_rights = this.$el.find('#qs5-credit_extras-pledge-h-corporate_rights').valueIfNotDisabled();
        this.qData.credit_extras.pledge_corporate_rights_operated = this.$el.find('#qs5-credit_extras-pledge-corporate_rights1').valueIfNotDisabled();
        this.qData.credit_extras.pledge_corporate_rights_owned = this.$el.find('#qs5-credit_extras-pledge-corporate_rights2').valueIfNotDisabled();
        this.qData.credit_extras.pledge_other = this.$el.find('#qs5-credit_extras-pledge-h-other').valueIfNotDisabled();
        this.qData.credit_extras.pledge_other_cost = this.$el.find('#qs5-credit_extras-pledge-other1').valueIfNotDisabled();
        this.qData.credit_extras.nothing = this.$el.find('#qs5-credit_extras-h-nothing').valueIfNotDisabled();
    },
    render: function() {
        var self = this;
        var html = this.QUI.renderTemplate(this.qData.caseTariff == 'standard' ? 'step5_simple' : 'step5', {
            data: this.qData
        }, function(html) {
            self.$el.html(html);
            self.enableMortgage();
            self.enableSurety();
            self.enablePledge();
            self.enableNothing();

            self.$el.find('#qs5-credit_extras-mortgage1').autocomplete({
                source: QUI_AUTOCOMPLETE.cities,
                minLength: 2
            });

            self.$el.find('#qs5-credit_extras-pledge-car4').autocomplete({
                source: QUI_AUTOCOMPLETE.cities,
                minLength: 2
            });

            self.$el.find('#qs5-credit_extras-pledge-movables4').autocomplete({
                source: QUI_AUTOCOMPLETE.cities,
                minLength: 2
            });
        });
    }
});
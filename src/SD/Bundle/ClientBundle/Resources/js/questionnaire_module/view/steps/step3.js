var step_template = require('./step_template');

module.exports = step_template.extend({
    events: {
        'click .dib label:not(.active)': "switch_status"
    },
    view_name: 'step3',
    fields: {
        credit: {
            status: "valueOfCustomRadio",
            currency: "valueOfCustomSelect",
            is_entrepreneur: "valueOfCustomCheckbox",
            distributed_moratorium: "valueOfCustomCheckbox"
        }
    }
});
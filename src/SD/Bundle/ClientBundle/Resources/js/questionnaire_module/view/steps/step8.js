var enableElems = require('../../helpers').enableElems;
var disableElems = require('../../helpers').disableElems;

module.exports = Backbone.View.extend({
    events: {
        'change #qs5-resolution_attempts-h-no_action': 'enableNoAction',
        'change #qs5-resolution_attempts-h-restructuring': 'enableRestructuring',
        'change #qs5-resolution_attempts-h-discount': 'enableDiscount',
        'change #qs5-resolution_attempts-h-foreclosure_sale': 'enableForeclosureSale',
        'change #qs5-resolution_attempts-h-voluntary_property_transfer': 'enableVoluntaryPropertyTransfer',
        'change #qs5-resolution_attempts-restructuring1': 'enableRestructuringHappened',
        'change #qs5-resolution_attempts-restructuring3': 'enableRestructuringConditions',
        'change #qs5-resolution_attempts-discount1': 'enableDiscountConditions',
        'change #qs5-resolution_attempts-foreclosure_sale1': 'enableForeclosureSaleConditions',
    },
    initialize: function(options) {
        this.qData = options.qData || {};
        this.QUI = options.parent;
        this.render();
    },
    enableNoAction: function() {
        if (this.$el.find('#qs5-resolution_attempts-h-no_action').is(':checked')) {
            enableElems(this.$el.find('#block-resolution_attempts-no_action'));

            // disable fields
            this.$el.find('#qs5-resolution_attempts-h-restructuring').prop('checked', false);
            disableElems(this.$el.find('#block-resolution_attempts-restructuring'));
            this.$el.find('#qs5-resolution_attempts-h-discount').prop('checked', false);
            disableElems(this.$el.find('#block-resolution_attempts-discount'));
            this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').prop('checked', false);
            disableElems(this.$el.find('#block-resolution_attempts-foreclosure_sale'));
            this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').prop('checked', false);
            disableElems(this.$el.find('#block-resolution_attempts-voluntary_property_transfer'));

        } else {
            if (this.$el.find('#qs5-resolution_attempts-h-restructuring').is(':checked') ||
                this.$el.find('#qs5-resolution_attempts-h-discount').is(':checked') ||
                this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').is(':checked') ||
                this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').is(':checked')) {

                disableElems(this.$el.find('#block-resolution_attempts-no_action'));
            }
            else {
                this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', true);
            }
        }
    },
    enableRestructuring: function() {
        if (this.$el.find('#qs5-resolution_attempts-h-restructuring').is(':checked')) {
            enableElems(this.$el.find('#block-resolution_attempts-restructuring'));
            this.enableRestructuringHappened();
            this.enableRestructuringConditions();

            // Disable no action section
            this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', false);
            this.enableNoAction();

        } else {
            disableElems(this.$el.find('#block-resolution_attempts-restructuring'));

            if (!this.$el.find('#qs5-resolution_attempts-h-discount').is(':checked') &&
                !this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').is(':checked') &&
                !this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').is(':checked')) {

                this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', true);
                this.enableNoAction();
            }
        }
    },
    enableRestructuringHappened: function() {
        var self = this;
        var select = parseInt(this.$el.find('#qs5-resolution_attempts-restructuring1').val(), 10);
        _.each([1, 2], function(i) {
            if (i == select) {
                if (i == 1) {
                    enableElems(self.$el.find('#qs5-resolution_attempts-restructuring2'));
                    disableElems(self.$el.find('#block-resolution_attempts-no_restructuring_happened'));
                } else if (i == 2) {
                    disableElems(self.$el.find('#qs5-resolution_attempts-restructuring2'));
                    enableElems(self.$el.find('#block-resolution_attempts-no_restructuring_happened'));
                }
            }
        });
    },
    enableRestructuringConditions: function() {
        var self = this;
        var select = parseInt(this.$el.find('#qs5-resolution_attempts-restructuring3').val(), 10);
        _.each([1, 2], function(i) {
            if (i == select) {
                if (i == 1) {
                    disableElems(self.$el.find('#qs5-resolution_attempts-restructuring4'));
                } else if (i == 2) {
                    enableElems(self.$el.find('#qs5-resolution_attempts-restructuring4'));
                }
            }
        });
    },
    enableDiscount: function() {
        if (this.$el.find('#qs5-resolution_attempts-h-discount').is(':checked')) {
            enableElems(this.$el.find('#block-resolution_attempts-discount'));
            this.enableDiscountConditions();

            // Disable no action section
            this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', false);
            this.enableNoAction();
        } else {
            disableElems(this.$el.find('#block-resolution_attempts-discount'));

            if (!this.$el.find('#qs5-resolution_attempts-h-restructuring').is(':checked') &&
                !this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').is(':checked') &&
                !this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').is(':checked')) {

                this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', true);
                this.enableNoAction();
            }
        }
    },
    enableDiscountConditions: function() {
        var self = this;
        var select = parseInt(this.$el.find('#qs5-resolution_attempts-discount1').val(), 10);
        _.each([1, 2, 3], function(i) {
            if (i == select) {
                if (i == 1) {
                    disableElems(self.$el.find('#block-resolution_attempts-discount_by_bank'));
                    disableElems(self.$el.find('#block-resolution_attempts-discount_by_you'));
                } else if (i == 2) {
                    enableElems(self.$el.find('#block-resolution_attempts-discount_by_bank'));
                    disableElems(self.$el.find('#block-resolution_attempts-discount_by_you'));
                } else if (i == 3) {
                    disableElems(self.$el.find('#block-resolution_attempts-discount_by_bank'));
                    enableElems(self.$el.find('#block-resolution_attempts-discount_by_you'));
                }
            }
        });
    },
    enableForeclosureSale: function() {
        if (this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').is(':checked')) {
            enableElems(this.$el.find('#block-resolution_attempts-foreclosure_sale'));
            this.enableForeclosureSaleConditions();

            // Disable no action section
            this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', false);
            this.enableNoAction();
        } else {
            disableElems(this.$el.find('#block-resolution_attempts-foreclosure_sale'));

            if (!this.$el.find('#qs5-resolution_attempts-h-restructuring').is(':checked') &&
                !this.$el.find('#qs5-resolution_attempts-h-discount').is(':checked') &&
                !this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').is(':checked')) {

                this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', true);
                this.enableNoAction();
            }
        }
    },
    enableForeclosureSaleConditions: function() {
        var self = this;
        var select = parseInt(this.$el.find('#qs5-resolution_attempts-foreclosure_sale1').val(), 10);
        _.each([1, 2, 3], function(i) {
            if (i == select) {
                if (i == 1) {
                    disableElems(self.$el.find('#block-resolution_attempts-foreclosure_sale_by_bank'));
                    disableElems(self.$el.find('#block-resolution_attempts-foreclosure_sale_by_you'));
                } else if (i == 2) {
                    enableElems(self.$el.find('#block-resolution_attempts-foreclosure_sale_by_bank'));
                    disableElems(self.$el.find('#block-resolution_attempts-foreclosure_sale_by_you'));
                } else if (i == 3) {
                    disableElems(self.$el.find('#block-resolution_attempts-foreclosure_sale_by_bank'));
                    enableElems(self.$el.find('#block-resolution_attempts-foreclosure_sale_by_you'));
                }
            }
        });
    },
    enableVoluntaryPropertyTransfer: function() {
        if (this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').is(':checked')) {
            enableElems(this.$el.find('#block-resolution_attempts-voluntary_property_transfer'));

            // Disable no action section
            this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', false);
            this.enableNoAction();
        } else {
            disableElems(this.$el.find('#block-resolution_attempts-voluntary_property_transfer'));

            if (!this.$el.find('#qs5-resolution_attempts-h-restructuring').is(':checked') &&
                !this.$el.find('#qs5-resolution_attempts-h-discount').is(':checked') &&
                !this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').is(':checked')) {

                this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', true);
                this.enableNoAction();
            }
        }
    },
    saveToData: function() {
        this.qData.resolution_attempts.no_action = this.$el.find('#qs5-resolution_attempts-h-no_action').valueIfNotDisabled();
        this.qData.resolution_attempts.restructuring = this.$el.find('#qs5-resolution_attempts-h-restructuring').valueIfNotDisabled();
        this.qData.resolution_attempts.restructuring_happened = this.$el.find('#qs5-resolution_attempts-restructuring1').valueIfNotDisabled();;
        this.qData.resolution_attempts.restructuring_date = this.$el.find('#qs5-resolution_attempts-restructuring2').valueIfNotDisabled();;
        this.qData.resolution_attempts.restructuring_conditions = this.$el.find('#qs5-resolution_attempts-restructuring3').valueIfNotDisabled();;
        this.qData.resolution_attempts.restructuring_condition_details = this.$el.find('#qs5-resolution_attempts-restructuring4').valueIfNotDisabled();;
        this.qData.resolution_attempts.discount = this.$el.find('#qs5-resolution_attempts-h-discount').valueIfNotDisabled();
        this.qData.resolution_attempts.discount_conditions = this.$el.find('#qs5-resolution_attempts-discount1').valueIfNotDisabled();;
        this.qData.resolution_attempts.discount_amount_by_bank = this.$el.find('#qs5-resolution_attempts-discount2').valueIfNotDisabled();;
        this.qData.resolution_attempts.discount_date_by_bank = this.$el.find('#qs5-resolution_attempts-discount3').valueIfNotDisabled();;
        this.qData.resolution_attempts.discount_amount_by_you = this.$el.find('#qs5-resolution_attempts-discount4').valueIfNotDisabled();;
        this.qData.resolution_attempts.discount_date_by_you = this.$el.find('#qs5-resolution_attempts-discount5').valueIfNotDisabled();;
        this.qData.resolution_attempts.foreclosure_sale = this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').valueIfNotDisabled();
        this.qData.resolution_attempts.foreclosure_sale_conditions = this.$el.find('#qs5-resolution_attempts-foreclosure_sale1').valueIfNotDisabled();;
        this.qData.resolution_attempts.foreclosure_sale_date_by_bank = this.$el.find('#qs5-resolution_attempts-foreclosure_sale2').valueIfNotDisabled();;
        this.qData.resolution_attempts.foreclosure_sale_bank_dept_conditions = this.$el.find('#qs5-resolution_attempts-foreclosure_sale3').valueIfNotDisabled();;
        this.qData.resolution_attempts.foreclosure_sale_date_by_you = this.$el.find('#qs5-resolution_attempts-foreclosure_sale4').valueIfNotDisabled();;
        this.qData.resolution_attempts.foreclosure_sale_you_dept_conditions = this.$el.find('#qs5-resolution_attempts-foreclosure_sale5').valueIfNotDisabled();;
        this.qData.resolution_attempts.voluntary_property_transfer = this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').valueIfNotDisabled();

    },
    render: function() {
        var self = this;
        var html = this.QUI.renderTemplate('step8', {
            data: this.qData
        }, function(html) {
            self.$el.html(html);

            self.enableNoAction();
            self.enableRestructuring();
            self.enableDiscount();
            self.enableForeclosureSale();
            self.enableVoluntaryPropertyTransfer();

            self.$el.find('#qs5-resolution_attempts-restructuring2').datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
            });

            self.$el.find('#qs5-resolution_attempts-discount3').datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
            });

            self.$el.find('#qs5-resolution_attempts-discount5').datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
            });
        });
    }
});
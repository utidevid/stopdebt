var enableElems = require('../../helpers').enableElems;
var disableElems = require('../../helpers').disableElems;
var QUI_AUTOCOMPLETE = require('../../helpers').QUI_AUTOCOMPLETE;

module.exports = Backbone.View.extend({
    events: {
        'change #qs6-stage': 'stageChangeHandler',
        'change #qs6-stage1-d': 'stage1DChangeHandler',
        'change #qs6-stage2-a': 'stage2AChangeHandler',
        'change #qs6-stage2-f': 'stage2FChangeHandler',
        'change #qs6-stage2-g': 'stage2GChangeHandler',
        'change #qs6-stage2-g2': 'stage2G2ChangeHandler',
        'change #qs6-stage2-g2-set3-a': 'stage2G23AChangeHandler',
        'change #qs6-stage2-i': 'stage2IChangeHandler',
        'change #qs6-stage2-j': 'stage2JChangeHandler',
        'change #qs6-stage3-e': 'stage3EChangeHandler',
    },
    stageChangeHandler: function() {
        var self = this;
        var goalVal = parseInt(this.$el.find('#qs6-stage').val(), 10);
        _.each([1, 2, 3, 4], function(i) {
            if (i == goalVal) {
                enableElems(self.$el.find('#qs6-stage-' + i + '-set'));

                if (i == 1) {
                    self.stage1DChangeHandler();
                } else if (i == 2) {
                    self.stage2AChangeHandler();
                    self.stage2FChangeHandler();
                    self.stage2GChangeHandler();
                    self.stage2IChangeHandler();
                    self.stage2JChangeHandler();
                } else if (i == 3) {
                    self.stage3EChangeHandler();
                }
            } else {
                disableElems(self.$el.find('#qs6-stage-' + i + '-set'));
            }
        });
    },
    stage1DChangeHandler: function() {
        if (this.$el.find('#qs6-stage1-d').is(':checked')) {
            enableElems(this.$el.find('#qs6-stage1-d2').parent());
        } else {
            disableElems(this.$el.find('#qs6-stage1-d2').parent());
        }
    },
    stage2AChangeHandler: function() {
        if (this.$el.find('#qs6-stage2-a').val() == 2) {
            enableElems(this.$el.find('#qs6-stage2-a2'));
        } else {
            disableElems(this.$el.find('#qs6-stage2-a2'));
        }
    },
    stage2FChangeHandler: function() {
        if (this.$el.find('#qs6-stage2-f').is(':checked')) {
            enableElems(this.$el.find('#qs6-stage2-f1').parent());
        } else {
            disableElems(this.$el.find('#qs6-stage2-f1').parent());
        }
    },
    stage2GChangeHandler: function() {
        if (this.$el.find('#qs6-stage2-g').val() == 1) {
            enableElems(this.$el.find('#qs6-stage2-g-set'));
            this.stage2G2ChangeHandler();
        } else {
            disableElems(this.$el.find('#qs6-stage2-g-set'));
        }
    },
    stage2G2ChangeHandler: function() {
        this.$el.find('#qs6-stage2-g2-set2').hide();
        this.$el.find('#qs6-stage2-g2-set3').hide();

        var cVal = this.$el.find('#qs6-stage2-g2').val();
        this.$el.find('#qs6-stage2-g2-set' + cVal).show();

        if (cVal == 3) {
            this.stage2G23AChangeHandler();
        }
    },
    stage2G23AChangeHandler: function() {
        if (this.$el.find('#qs6-stage2-g2-set3-a').val() == 2) {
            enableElems(this.$el.find('#qs6-stage2-g2-set3-b'));
        } else {
            disableElems(this.$el.find('#qs6-stage2-g2-set3-b'));
        }
    },
    stage2IChangeHandler: function() {
        if (this.$el.find('#qs6-stage2-i').val() == 2) {
            enableElems(this.$el.find('#qs6-stage2-i2-set'));
        } else {
            disableElems(this.$el.find('#qs6-stage2-i2-set'));
        }
    },
    stage2JChangeHandler: function() {
        if (this.$el.find('#qs6-stage2-j').val() == 1) {
            enableElems(this.$el.find('#qs6-stage2-k'));
        } else {
            disableElems(this.$el.find('#qs6-stage2-k'));
        }
    },
    stage3EChangeHandler: function() {
        if (this.$el.find('#qs6-stage3-e').val() == 6) {
            enableElems(this.$el.find('#qs6-stage3-e6-set'));
        } else {
            disableElems(this.$el.find('#qs6-stage3-e6-set'));
        }
    },
    saveToData: function() {
        this.qData.penalty.city = this.$el.find('#qs6-city').valueIfNotDisabled();;
        this.qData.penalty.creditor_type = this.$el.find('#qs6-creditor').valueIfNotDisabled();;
        this.qData.penalty.creditor_name = this.$el.find('#qs6-creditor-name').valueIfNotDisabled();;
        this.qData.penalty.stage = this.$el.find('#qs6-stage').valueIfNotDisabled();;
        this.qData.penalty.stage1_calls = this.$el.find('#qs6-stage1-a').valueIfNotDisabled();
        this.qData.penalty.stage1_mails = this.$el.find('#qs6-stage1-b').valueIfNotDisabled();
        this.qData.penalty.satge1_visits = this.$el.find('#qs6-stage1-c').valueIfNotDisabled();
        this.qData.penalty.stage1_else = this.$el.find('#qs6-stage1-d').valueIfNotDisabled();
        this.qData.penalty.stage1_else_details = this.$el.find('#qs6-stage1-d2').valueIfNotDisabled();;
        this.qData.penalty.stage2_participation = this.$el.find('#qs6-stage2-a').valueIfNotDisabled();;
        this.qData.penalty.stage2_fio = this.$el.find('#qs6-stage2-b').valueIfNotDisabled();;
        this.qData.penalty.stage2_phone = this.$el.find('#qs6-stage2-c').valueIfNotDisabled();;
        this.qData.penalty.stage2_email = this.$el.find('#qs6-stage2-d').valueIfNotDisabled();;
        this.qData.penalty.stage2_else = this.$el.find('#qs6-stage2-e').valueIfNotDisabled();;
        this.qData.penalty.stage2_type1 = this.$el.find('#qs6-stage2-f').valueIfNotDisabled();
        this.qData.penalty.stage2_type1_amount = this.$el.find('#qs6-stage2-f1').valueIfNotDisabled();;
        this.qData.penalty.stage2_type2 = this.$el.find('#qs6-stage2-f2').valueIfNotDisabled();
        this.qData.penalty.stage2_type3 = this.$el.find('#qs6-stage2-f3').valueIfNotDisabled();
        this.qData.penalty.stage2_type4 = this.$el.find('#qs6-stage2-f4').valueIfNotDisabled();
        this.qData.penalty.stage2_court_type = this.$el.find('#qs6-stage2-g').valueIfNotDisabled();;
        this.qData.penalty.stage2_court_fio = this.$el.find('#qs6-stage2-g1').valueIfNotDisabled();;
        this.qData.penalty.stage2_court_stage = this.$el.find('#qs6-stage2-g2').valueIfNotDisabled();;
        this.qData.penalty.stage2_court_stage2_amount1 = this.$el.find('#qs6-stage2-g2-set2-a').valueIfNotDisabled();;
        this.qData.penalty.stage2_court_stage2_amount2 = this.$el.find('#qs6-stage2-g2-set2-b').valueIfNotDisabled();;
        this.qData.penalty.stage2_court_stage2_date = this.$el.find('#qs6-stage2-g2-set2-c').valueIfNotDisabled();;
        this.qData.penalty.stage2_court_stage3_state = this.$el.find('#qs6-stage2-g2-set3-a').valueIfNotDisabled();;
        this.qData.penalty.stage2_court_stage3_date = this.$el.find('#qs6-stage2-g2-set3-b1').valueIfNotDisabled();;
        this.qData.penalty.stage2_court_stage3_amount = this.$el.find('#qs6-stage2-g2-set3-b2').valueIfNotDisabled();;
        this.qData.penalty.stage2_court_stage3_type = this.$el.find('#qs6-stage2-g2-set3-b3').valueIfNotDisabled();;
        this.qData.penalty.stage2_appeal_type = this.$el.find('#qs6-stage2-h').valueIfNotDisabled();;
        this.qData.penalty.stage2_appeal_stage = this.$el.find('#qs6-stage2-i').valueIfNotDisabled();;
        this.qData.penalty.stage2_appeal_stage2_amount1 = this.$el.find('#qs6-stage2-i2-set-a').valueIfNotDisabled();;
        this.qData.penalty.stage2_appeal_stage2_amount2 = this.$el.find('#qs6-stage2-i2-set-b').valueIfNotDisabled();;
        this.qData.penalty.stage2_appeal_stage2_date = this.$el.find('#qs6-stage2-i2-set-c').valueIfNotDisabled();;
        this.qData.penalty.stage2_cassation_type = this.$el.find('#qs6-stage2-j').valueIfNotDisabled();;
        this.qData.penalty.stage2_cassation_type2 = this.$el.find('#qs6-stage2-k').valueIfNotDisabled();;
        this.qData.penalty.stage3_name = this.$el.find('#qs6-stage3-a').valueIfNotDisabled();;
        this.qData.penalty.stage3_fio = this.$el.find('#qs6-stage3-b').valueIfNotDisabled();;
        this.qData.penalty.stage3_date = this.$el.find('#qs6-stage3-c').valueIfNotDisabled();;
        this.qData.penalty.stage3_amount = this.$el.find('#qs6-stage3-d').valueIfNotDisabled();;
        this.qData.penalty.stage3_stage = this.$el.find('#qs6-stage3-e').valueIfNotDisabled();;
        this.qData.penalty.stage3_writeoff = this.$el.find('#qs6-stage3-e6-set-a').valueIfNotDisabled();;
        this.qData.penalty.stage3_queue = this.$el.find('#qs6-stage3-e6-set-b').valueIfNotDisabled();;
    },
    initialize: function(options) {
        this.qData = options.qData || {};
        this.QUI = options.parent;
        this.render();
    },
    render: function() {
        var self = this;
        var html = this.QUI.renderTemplate('step6', {
            data: this.qData
        }, function(html) {
            self.$el.html(html);
            self.stageChangeHandler();

            self.$el.find('#qs6-stage2-i2-set-c').datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
            });

            self.$el.find('#qs6-stage3-c').datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
            });

            self.$el.find('#qs6-city').autocomplete({
                source: QUI_AUTOCOMPLETE.cities,
                minLength: 2
            });
        });
    }
});
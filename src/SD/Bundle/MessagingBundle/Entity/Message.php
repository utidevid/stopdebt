<?php

namespace SD\Bundle\MessagingBundle\Entity;

/**
 * Message
 */
class Message
{

    const LEVEL_GENERAL = 1;
    const LEVEL_SPECIAL = 2;

    const THREAD_STATUS_ACTIVE = 0;
    const THREAD_STATUS_DONE = 1;

    const STATUS_UNREAD = 0;
    const STATUS_READ = 1;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var bool
     */
    private $userMessage;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Message
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set userMessage
     *
     * @param boolean $userMessage
     *
     * @return Message
     */
    public function setUserMessage($userMessage)
    {
        $this->userMessage = $userMessage;

        return $this;
    }

    /**
     * Get userMessage
     *
     * @return bool
     */
    public function getUserMessage()
    {
        return $this->userMessage;
    }
    /**
     * @var \SD\Bundle\UserBundle\Entity\User
     */
    private $related_user;

    /**
     * @var \SD\Bundle\UserBundle\Entity\User
     */
    private $author;


    /**
     * Set relatedUser
     *
     * @param \SD\Bundle\UserBundle\Entity\User $relatedUser
     *
     * @return Message
     */
    public function setRelatedUser(\SD\Bundle\UserBundle\Entity\User $relatedUser = null)
    {
        $this->related_user = $relatedUser;

        return $this;
    }

    /**
     * Get relatedUser
     *
     * @return \SD\Bundle\UserBundle\Entity\User
     */
    public function getRelatedUser()
    {
        return $this->related_user;
    }

    /**
     * Set author
     *
     * @param \SD\Bundle\UserBundle\Entity\User $author
     *
     * @return Message
     */
    public function setAuthor(\SD\Bundle\UserBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \SD\Bundle\UserBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }


    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $threadMessages;

    /**
     * @var \SD\Bundle\MessagingBundle\Entity\Message
     */
    private $parentMessage;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->threadMessages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add threadMessage
     *
     * @param \SD\Bundle\MessagingBundle\Entity\Message $threadMessage
     *
     * @return Message
     */
    public function addThreadMessage(\SD\Bundle\MessagingBundle\Entity\Message $threadMessage)
    {
        $this->threadMessages[] = $threadMessage;

        return $this;
    }

    /**
     * Remove threadMessage
     *
     * @param \SD\Bundle\MessagingBundle\Entity\Message $threadMessage
     */
    public function removeThreadMessage(\SD\Bundle\MessagingBundle\Entity\Message $threadMessage)
    {
        $this->threadMessages->removeElement($threadMessage);
    }

    /**
     * Get threadMessages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThreadMessages()
    {
        return $this->threadMessages;
    }

    /**
     * Set parentMessage
     *
     * @param \SD\Bundle\MessagingBundle\Entity\Message $parentMessage
     *
     * @return Message
     */
    public function setParentMessage(\SD\Bundle\MessagingBundle\Entity\Message $parentMessage = null)
    {
        $this->parentMessage = $parentMessage;

        return $this;
    }

    /**
     * Get parentMessage
     *
     * @return \SD\Bundle\MessagingBundle\Entity\Message
     */
    public function getParentMessage()
    {
        return $this->parentMessage;
    }
    /**
     * @var integer
     */
    private $level = 1;

    /**
     * @var \SD\Bundle\UserBundle\Entity\User
     */
    private $assigned_assistant;


    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Message
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set assignedAssistant
     *
     * @param \SD\Bundle\UserBundle\Entity\User $assignedAssistant
     *
     * @return Message
     */
    public function setAssignedAssistant(\SD\Bundle\UserBundle\Entity\User $assignedAssistant = null)
    {
        $this->assigned_assistant = $assignedAssistant;

        return $this;
    }

    /**
     * Get assignedAssistant
     *
     * @return \SD\Bundle\UserBundle\Entity\User
     */
    public function getAssignedAssistant()
    {
        return $this->assigned_assistant;
    }
    /**
     * @var boolean
     */
    private $isTopMessage;

    /**
     * @var integer
     */
    private $threadStatus;


    /**
     * Set isTopMessage
     *
     * @param boolean $isTopMessage
     *
     * @return Message
     */
    public function setIsTopMessage($isTopMessage)
    {
        $this->isTopMessage = $isTopMessage;

        return $this;
    }

    /**
     * Get isTopMessage
     *
     * @return boolean
     */
    public function getIsTopMessage()
    {
        return $this->isTopMessage;
    }

    /**
     * Set threadStatus
     *
     * @param integer $threadStatus
     *
     * @return Message
     */
    public function setThreadStatus($threadStatus)
    {
        $this->threadStatus = $threadStatus;

        return $this;
    }

    /**
     * Get threadStatus
     *
     * @return integer
     */
    public function getThreadStatus()
    {
        return $this->threadStatus;
    }
    /**
     * @var integer
     */
    private $status;


    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Message
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}

<?php

namespace SD\Bundle\MessagingBundle\Service;


use Doctrine\ORM\EntityManager;
use SD\Bundle\UserBundle\Entity\User;

class MessagingService
{
    /** @var EntityManager */
    protected $em;

    public function __construct(EntityManager $manager)
    {
        $this->em = $manager;
    }

    protected function getGenericUnreadCountQuery()
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select($qb->expr()->count('m'))
            ->from('SD\Bundle\MessagingBundle\Entity\Message', 'm')
            ->where('m.status = :status')
            ->setParameter('status', 0);

        return $qb;
    }

    /**
     * Gets amount of unread messages for user
     * @param User $user
     * @return int messages count
     */
    public function getUserUnreadCount($user)
    {
        return $this->getGenericUnreadCountQuery()
            ->andWhere('m.related_user = :user')
            ->andWhere('m.userMessage = 0')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Get's amount of all unread messages
     * @return int messages count
     */
    public function getAssistantUnreadCount()
    {
        return $this->getGenericUnreadCountQuery()->andWhere('m.userMessage = 1')->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Generic for the user/assistant
     * @param User $user
     * @return int unread messages count
     */
    public function getUnreadMessagesCount(User $user) {
        if ($user->hasRole('ROLE_ASSISTANT')) {
            $count = $this->getAssistantUnreadCount();
        } else {
            $count = $this->getUserUnreadCount($user);
        }

        return $count;
    }
}
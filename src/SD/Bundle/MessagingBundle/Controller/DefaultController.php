<?php

namespace SD\Bundle\MessagingBundle\Controller;

use SD\Bundle\MessagingBundle\Repository\MessageRepository;
use SD\Bundle\UtilsBundle\Controller\AbstractUserController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use SD\Bundle\MessagingBundle\Entity\Message;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class DefaultController extends AbstractUserController
{

    const SECTION_NAME = 'messaging';

    public function indexAction()
    {
        $this->loadCaseFromSession();

        $repository = $this->getDoctrine()->getRepository('MessagingBundle:Message');

        $messages = $repository->createQueryBuilder('p')
            ->select('p as message', 'count(m.id) as cnt')
            ->where('p.isTopMessage = 1 AND p.author = ?1')
            ->leftJoin('p.threadMessages', 'm', 'WITH', 'm.status = 0 and m.userMessage = 0')
            ->orderBy('cnt', 'DESC')
            ->addOrderBy('p.threadStatus', 'ASC')
            ->addOrderBy('p.createdAt', 'DESC')
            ->groupBy('p.id')
            ->setParameter(1, $this->getUser())
            ->getQuery()->getResult();

        $form = $this->createFormBuilder()->setAction($this->generateUrl('messaging_push'))
            ->add('message', TextareaType::class)
            ->add('add', SubmitType::class, ['label' => 'Отправить', 'attr' => ['class' => 'btn btn-primary']])->getForm();

        return $this->render('MessagingBundle:Default:index.html.twig', [
            'form' => $form->createView(),
            'messages' => $messages
        ]);
    }

    public function threadAction($messageId)
    {
        $this->loadCaseFromSession();

        $repository = $this->getDoctrine()->getRepository('MessagingBundle:Message');
        $mainMessage = $repository->findOneBy([
            'id' => $messageId,
            'author' => $this->getUser()
        ]);

        $messages = array_merge([$mainMessage], $mainMessage->getThreadMessages()->toArray());

        $form = $this->createFormBuilder()->setAction($this->generateUrl('messaging_thread_push', ['messageId' => $messageId]))
            ->add('message', TextareaType::class)
            ->add('add', SubmitType::class, ['label' => 'Отправить', 'attr' => ['class' => 'btn btn-primary']])->getForm();

        return $this->render('MessagingBundle:Default:thread.html.twig', [
            'form' => $form->createView(),
            'messages' => $messages,
            'mainMessage' => $mainMessage
        ]);
    }

    public function pushAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $message = new Message();

        $form = $this->createFormBuilder()->add('message', TextareaType::class)->add('add', SubmitType::class)->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $message->setMessage($form['message']->getData());
                $message->setAuthor($this->getUser());
                $message->setRelatedUser($this->getUser());
                $message->setUserMessage(1);
                $message->setIsTopMessage(1);
                $message->setStatus(Message::STATUS_UNREAD);
                $message->setThreadStatus(Message::THREAD_STATUS_ACTIVE);
                $message->setLevel(Message::LEVEL_GENERAL);
                $em->persist($message);
                $em->flush();
            }
        }

        return $this->redirectToRoute('messaging_thread', [
            'messageId' => $message->getId()
        ]);
    }

    public function threadPushAction($messageId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $message = new Message();

        $form = $this->createFormBuilder()->add('message', TextareaType::class)->add('add', SubmitType::class)->getForm();

        $parentMessage =  NULL;
        foreach ($this->getUser()->getRelatedMessages()->toArray() as $m) {
            if ($m->getId() == $messageId) {
                $parentMessage = $m;
            }
        }

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $message->setMessage($form['message']->getData());
                $message->setAuthor($this->getUser());
                $message->setRelatedUser($this->getUser());
                $message->setUserMessage(1);
                $message->setIsTopMessage(0);
                $message->setThreadStatus(0);
                $message->setLevel(0);
                $message->setStatus(Message::STATUS_UNREAD);
                $message->setParentMessage($parentMessage);

                $parentMessage->setThreadStatus(Message::THREAD_STATUS_ACTIVE);

                $em->persist($message);
                $em->flush();
            }
        }

        return $this->redirectToRoute('messaging_thread', ['messageId' => $messageId]);
    }


    /**
     * @param Message $mainMessage
     * @return JsonResponse
     * @ParamConverter("mainMessage", class="MessagingBundle:Message")
     */
    public function setUserThreadReadAction(Message $mainMessage)
    {
        $this->denyAccessUnlessGranted('user_read', $mainMessage);

        /** @var MessageRepository $repository */
        $repository = $this->getDoctrine()->getRepository('MessagingBundle:Message');

        $repository->createQueryBuilder('m')
            ->update()->set('m.status', Message::STATUS_READ)
            ->where('m.parentMessage = :message')
            ->andWhere('m.userMessage = 0')
            ->orWhere('m = :message')
            ->setParameter('message', $mainMessage)
            ->getQuery()->execute();

        return new JsonResponse(['status' => true]);
    }
}

<?php

namespace SD\Bundle\MessagingBundle\Tests\Controller;

use SD\Bundle\UtilsBundle\Tests\WebTestCase;

class DefaultControllerTest extends WebTestCase
{

    public function testIndex()
    {
        $this->client->loginRealUser();
        $crawler = $this->client->request('GET', '/messaging');
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

}

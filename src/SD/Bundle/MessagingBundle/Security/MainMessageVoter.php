<?php
namespace SD\Bundle\MessagingBundle\Security;

use SD\Bundle\MessagingBundle\Entity\Message;
use SD\Bundle\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use \Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class MessageVoter
 * @package SD\Bundle\MessagingBundle\Security
 */
class MainMessageVoter extends Voter
{
    protected $allowedAttributes = [
        self::USER_READ,
        self::ASSISTANT_READ,
        self::VIEW,
        self::POST_TO_THREAD
    ];

    const USER_READ = 'user_read';
    const ASSISTANT_READ = 'assistant_read';
    const VIEW = 'view';
    const POST_TO_THREAD = 'post_to_thread';

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, $this->allowedAttributes)) {
            return false;
        }

        if (!$subject instanceof Message || !$subject->getIsTopMessage()) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param Message|mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if ($attribute == self::USER_READ) {
            return $user == $subject->getAuthor();
        } else {
            if ($user->hasRole('ROLE_ASSISTANT')) {
                return true;
            }

            if ($attribute != self::ASSISTANT_READ) {
                return $user == $subject->getAuthor();
            }
        }

        return false;
    }
} 
<?php

namespace SD\Bundle\UtilsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CourtsHearingsAllCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('courts:hearings:all')
            ->setDescription('Download all court hearings from all courts to database')
            ->addArgument('from', InputArgument::REQUIRED, 'Start court_id')
            ->addArgument('to', InputArgument::REQUIRED, 'End court_id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $from = intval($input->getArgument('from'));
        $to = intval($input->getArgument('to'));

        if ($from > $to) {
            return;
        }

        $service = $this->getContainer()->get('utils.court_hearings');

        for ($i=$from; $i <= $to; $i++) {
            $courtId = sprintf('%04d', $i);
            $hearings = $service->processCourt($courtId);

            $count = count($hearings);
            $output->writeln('Court: '.$courtId.'. Added: '.$count);
        }

        $output->writeln('Done.');
    }

}

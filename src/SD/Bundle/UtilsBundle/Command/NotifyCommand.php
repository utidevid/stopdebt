<?php
namespace SD\Bundle\UtilsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotifyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('utils:notify')->setDescription('Send un-viewed notifications to users.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $repo = $doctrine->getRepository('UtilsBundle:Notification');

        $list = $repo->createQueryBuilder('p')->where('p.viewed = 0 AND p.createdAt >= ?1')->setParameter(1, new \DateTime('15 minutes ago'))->getQuery()->getResult();

        foreach ($list as $notification) {
            $this->getContainer()->get('utils.mailer')->sendNotification($notification->getUser(), $notification->getMessage(), $notification->getLink());
            $output->writeln("Processed notification #".$notification->getId());
        }

        $output->writeln("Done.");
    }
}
<?php
namespace SD\Bundle\UtilsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class MakeRegularUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('users:make-regular-user')->setDescription('Makes user a regular one')
            ->setDefinition(array(
                new InputArgument('email', InputArgument::REQUIRED, 'Email of user'),
            ));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $email = $input->getArgument('email');

        $userManager = $this->getContainer()->get('fos_user.user_manager');
        $user = $userManager->findUserBy([
            'email' => $email
        ]);

        if (is_null($user)) {
            $output->writeln('No user with such email...');
            return;
        }

        $user->setRoles(['ROLE_USER']);
        $userManager->updateUser($user);

        $doctrine->getManager()->flush($user);

        $output->writeln('User '.$user->getEmail().'(#'.$user->getId().') is a regular user now.');
    }
}
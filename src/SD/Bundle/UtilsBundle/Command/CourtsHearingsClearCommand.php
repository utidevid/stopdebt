<?php

namespace SD\Bundle\UtilsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SD\Bundle\UtilsBundle\Entity\CourtHearing;

class CourtsHearingsClearCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('courts:hearings:clear')
            ->setDescription('Delete all court hearings from database');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->createQuery('DELETE FROM UtilsBundle:CourtHearing')->execute();
        $output->writeln('Done. Court hearings was deleted.');
    }

}

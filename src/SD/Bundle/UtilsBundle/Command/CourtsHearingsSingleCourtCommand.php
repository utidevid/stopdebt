<?php

namespace SD\Bundle\UtilsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SD\Bundle\UtilsBundle\Entity\CourtHearing;

class CourtsHearingsSingleCourtCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('courts:hearings:single-court')
            ->setDescription('Download all court hearings from single court to database')
            ->addArgument('court_id', InputArgument::REQUIRED, 'ID of the court')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $court_id = $input->getArgument('court_id');
        $service = $this->getContainer()->get('utils.court_hearings');

        $hearings = $service->processCourt($court_id);

        foreach ($hearings as $hearing) {
            $output->writeln('#'.$hearing->getId().': '.$hearing->getCaseNumber());
        }

        $output->writeln('');
        $output->writeln('Done.');
    }

}

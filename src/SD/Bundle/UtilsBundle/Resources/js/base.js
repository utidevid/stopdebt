$(function() {
    if ($('#activeCaseSelect').length) {
        $('#activeCaseSelect').change(function() {
            var val = $(this).val();
            if (val != '') {
                window.location = val;
            }
        });
    }

    $(document).on('click', 'a[data-method]', function(e) {
        e.preventDefault();
        var $form = $('<form action="' + $(this).attr('href') + '" method="'+ $(this).attr('data-method')  +'"></form>');
        $('body').append($form);
        $form.submit();
    });
});
<?php

namespace SD\Bundle\UtilsBundle\Entity;

/**
 * LandingVisitLog
 */
class LandingVisitLog
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $params;

    /**
     * @var string
     */
    private $addr;

    /**
     * @var array
     */
    private $headers;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set params
     *
     * @param string $params
     *
     * @return LandingVisitLog
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Get params
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set addr
     *
     * @param string $addr
     *
     * @return LandingVisitLog
     */
    public function setAddr($addr)
    {
        $this->addr = $addr;

        return $this;
    }

    /**
     * Get addr
     *
     * @return string
     */
    public function getAddr()
    {
        return $this->addr;
    }

    /**
     * Set headers
     *
     * @param array $headers
     *
     * @return LandingVisitLog
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Get headers
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return LandingVisitLog
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}


<?php

namespace SD\Bundle\UtilsBundle\Entity;

/**
 * CourtHearing
 */
class CourtHearing
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $courtId;

    /**
     * @var string
     */
    private $caseNumber;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $forma;

    /**
     * @var string
     */
    private $involved;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $judge;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set courtId
     *
     * @param string $courtId
     *
     * @return CourtHearing
     */
    public function setCourtId($courtId)
    {
        $this->courtId = $courtId;

        return $this;
    }

    /**
     * Get courtId
     *
     * @return string
     */
    public function getCourtId()
    {
        return $this->courtId;
    }

    /**
     * Set caseNumber
     *
     * @param string $caseNumber
     *
     * @return CourtHearing
     */
    public function setCaseNumber($caseNumber)
    {
        $this->caseNumber = $caseNumber;

        return $this;
    }

    /**
     * Get caseNumber
     *
     * @return string
     */
    public function getCaseNumber()
    {
        return $this->caseNumber;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return CourtHearing
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return CourtHearing
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set forma
     *
     * @param string $forma
     *
     * @return CourtHearing
     */
    public function setForma($forma)
    {
        $this->forma = $forma;

        return $this;
    }

    /**
     * Get forma
     *
     * @return string
     */
    public function getForma()
    {
        return $this->forma;
    }

    /**
     * Set involved
     *
     * @param string $involved
     *
     * @return CourtHearing
     */
    public function setInvolved($involved)
    {
        $this->involved = $involved;

        return $this;
    }

    /**
     * Get involved
     *
     * @return string
     */
    public function getInvolved()
    {
        return $this->involved;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CourtHearing
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set judge
     *
     * @param string $judge
     *
     * @return CourtHearing
     */
    public function setJudge($judge)
    {
        $this->judge = $judge;

        return $this;
    }

    /**
     * Get judge
     *
     * @return string
     */
    public function getJudge()
    {
        return $this->judge;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CourtHearing
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}

<?php
namespace SD\Bundle\UtilsBundle\Services;

use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserInterface;
use Mailgun\Mailgun;
use SD\Bundle\UserBundle\Entity\User;

class Mailer extends Mailgun implements MailerInterface
{

    private $twig;
    private $router;
    private $parameters;

    public function __construct(\Twig_Environment $twig, $router, $parameters, $env)
    {
        $this->parameters = $parameters;
        $this->twig = $twig;
        $this->router = $router;
        $this->env = $env;

        parent::__construct($parameters['api_key']);
    }

    public function sendNewResponseFromAssistant(User $user, $message)
    {
        $this->sendToUser($user, 'Новый ответ в обратной связи!', 'new_response_from_assistant.html.twig', [
           'user' => $user,
            'message' => $message
        ]);
    }

    public function sendNotification(User $user, $message, $link)
    {
        $this->sendToUser($user, 'Новое уведомление от СтопДолг', 'notification.html.twig', [
           'user' => $user,
           'message' => $message,
           'link' => $link
        ]);
    }

    public function sendWelcomeMessage(User $user, $password)
    {
        $this->sendToUser($user, 'Добро пожаловать в СтопДолг!', 'welcome.html.twig', [
           'user' => $user,
           'loginLink' => $this->generateLoginToPayLink($user, $this->router->generate('user_homepage', [], true)),
           'password' => $password
        ]);
    }

    public function sendInviteWelcomeMessage(User $user, $password)
    {
        $this->sendToUser($user, 'Добро пожаловать в СтопДолг!', 'invite_welcome.html.twig', [
           'user' => $user,
           'loginLink' => $this->generateLoginToPayLink($user, $this->router->generate('user_homepage', [], true)),
           'password' => $password
        ]);
    }

    public function sendInvoiceMessage(User $user, $case)
    {
        $payUrl = $this->generateLoginToPayLink($user, $this->router->generate('payments_pay_liqpay', [
            'caseId' => $case->getSafeId()
        ], true));

        $this->sendToUser($user, 'Счёт по кейсу '.$case->getSafeId(), 'invoice.html.twig', [
            'user' => $user,
            'case' => $case,
            'payUrl' => $payUrl
        ]);
    }

    public function sendServiceInvoiceMessage(User $user, $service)
    {
        $payUrl = $this->generateLoginToPayLink($user, $this->router->generate('payments_pay_service_liqpay', [
            'serviceId' => $service->getSafeId()
        ], true));

        $this->sendToUser($user, 'Счёт по услуге '.$service->getSafeId(), 'service_invoice.html.twig', [
            'user' => $user,
            'service' => $service,
            'payUrl' => $payUrl
        ]);
    }

    public function sendCasePaidMessage(User $user, $case)
    {
        $this->sendToUser($user, 'Кейс '.$case->getSafeId().' оплачен', 'case_paid.html.twig', [
            'user' => $user,
            'case' => $case
        ]);
    }

    public function sendServicePaidMessage(User $user, $service)
    {
        $this->sendToUser($user, 'Услуга '.$service->getSafeId().' оплачена', 'service_paid.html.twig', [
            'user' => $user,
            'service' => $service
        ]);
    }

    public function sendPasswordChangedMessage(User $user)
    {
        $this->sendToUser($user, 'Пароль в системе СтопДолг изменен', 'password_changed.html.twig', [
            'user' => $user,
        ]);
    }

    protected function sendToUser($user, $subject, $template, $params = []) {
        $this->sendMessage($this->parameters['domain'], [
            'from' => $this->parameters['robot_email'],
            'to' => $this->getReceiver($user),
            'subject' => $subject,
            'html' => $this->twig->render('UtilsBundle:Mailer:'.$template, $params)
        ]);
    }

    protected function getReceiver(User $user)
    {
        return ($this->env == 'dev' && !empty($this->parameters['dev_receiver'])) ?
                $this->parameters['dev_receiver'] : $user->getEmail();
    }

    protected function generateLoginToPayLink($user, $url) {
        $secret = 'f6f28a9a-07ac-11e6-b512-3e1d05defe78';
        $lastLoginTime = $user->getLastLogin() ? $user->getLastLogin()->format('dMY') : 0;
        $obj = [
            'url' => $url,
            'email' => $user->getEmail(),
            'sign' => md5($secret.$url.$user->getEmail().$lastLoginTime)
        ];
        $q = base64_encode(json_encode($obj));

        return $this->router->generate('login_to_payment', [
            'q' => $q
        ], true);
    }

    public function sendConfirmationEmailMessage(UserInterface $user)
    {
    }

    public function sendResettingEmailMessage(UserInterface $user)
    {

    }
}
<?php
namespace SD\Bundle\UtilsBundle\Services;

use SD\Bundle\UtilsBundle\Entity\CourtHearing;

class CourtHearingsService
{
    private $doctrine;
    private $em;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
        $this->em = $doctrine->getManager();
    }

    public function processCourt($courtId) {
        $data = $this->getRawData($courtId);
        if (empty($data) || !is_array($data)) {
            return [];
        }

        $list = [];

        foreach ($data as $item) {
            $obj = $this->createCourtHearingFromDataItem($courtId, $item);
            $this->em->persist($obj);

            $list[] = $obj;
        }

        $this->em->flush();
        return $list;
    }

    protected function createCourtHearingFromDataItem($courtId, $dataItem) {
        $obj = new CourtHearing();
        $obj->setCourtId($courtId);

        $obj->setCaseNumber($dataItem['number']);
        $obj->setDate(\DateTime::createFromFormat('d.m.Y H:i', $dataItem['date']));
        $obj->setAddress($dataItem['add_address']);
        $obj->setForma($dataItem['forma']);
        $obj->setInvolved($dataItem['involved']);
        $obj->setDescription($dataItem['description']);
        $obj->setJudge($dataItem['judge']);

        return $obj;
    }

    protected function getRawData($courtId) {
        $data_string = "q_court_id=".$courtId;

        $ch = curl_init('http://if.arbitr.gov.ua/new.php');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-Requested-With: XMLHttpRequest',
            'Origin: http://if.arbitr.gov.ua',
            'Referer: http://if.arbitr.gov.ua/sud5010/700/csz/',
            'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

}
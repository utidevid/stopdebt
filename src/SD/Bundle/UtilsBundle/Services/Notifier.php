<?php
namespace SD\Bundle\UtilsBundle\Services;
use SD\Bundle\UtilsBundle\Entity\Notification;

class Notifier {

    protected $doctrine;
    protected $em;
    protected $mailer;

    public function __construct($doctrine, $mailer) {
        $this->doctrine = $doctrine;
        $this->em = $doctrine->getManager();
        $this->mailer = $mailer;
    }

    public function pushNotification($user, $message, $link = '', $sendEmail = true) {
        $notification = new Notification();
        $notification->setUser($user);
        $notification->setMessage($message);
        $notification->setLink($link);
        $notification->setViewed(0);
        $notification->setCreatedAt(new \DateTime());

        if ($sendEmail) {
            $this->mailer->sendNotification($user, $message, $link);
        }

        $this->em->persist($notification);
        $this->em->flush();

        return $notification;
    }

}
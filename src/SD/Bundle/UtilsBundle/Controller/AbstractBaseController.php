<?php

namespace SD\Bundle\UtilsBundle\Controller;

use SD\Bundle\CaseBundle\Entity\DebtCase;
use SD\Bundle\QuestionnaireBundle\Entity\Questionnaire;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class AbstractBaseController extends Controller
{
    const SECTION_NAME = null;

    /**
     * Loads and returns Questionnaire entity
     * @param int $questionnaireId
     * @return Questionnaire
     */
    protected function loadQuestionnaireBySafeId($questionnaireId)
    {
        $repo = $this->getDoctrine()->getRepository('QuestionnaireBundle:Questionnaire');

        $questionnaire = $repo->findOneBy([
            'safeId' => $questionnaireId
        ]);

        if (empty($questionnaire)) {
            throw new NotFoundHttpException("Анкета №" . $questionnaireId . " не найдена!");
        }

        return $questionnaire;
    }

    /**
     * Loads and returns DebtCase entity
     * @param $caseId
     * @return DebtCase
     */
    protected function loadCaseBySafeId($caseId)
    {
        $repo = $this->getDoctrine()->getRepository('CaseBundle:DebtCase');

        $case = $repo->findOneBy([
            'safeId' => $caseId
        ]);

        if (empty($case)) {
            throw new NotFoundHttpException("Кейс №" . $caseId . " не найден!");
        }

        return $case;
    }

    protected function loadServiceBySafeId($serviceId)
    {
        $repo = $this->getDoctrine()->getRepository('CaseBundle:CustomService');

        $service = $repo->findOneBy([
            'safeId' => $serviceId
        ]);

        if (empty($service)) {
            throw new NotFoundHttpException("Дополнительная услуга №" . $serviceId . " не найден!");
        }

        return $service;
    }

    protected function loadReminderById($reminderId)
    {
        $repo = $this->getDoctrine()->getRepository('AssistantBundle:Reminder');

        $reminder = $repo->findOneBy([
            'id' => $reminderId
        ]);

        if (empty($reminder)) {
            throw new NotFoundHttpException("Событие №" . $reminderId . " не найдено!");
        }

        return $reminder;
    }

    protected function loadDocumentBySafeId($documentId)
    {
        $repo = $this->getDoctrine()->getRepository('DocumentsBundle:Document');

        $document = $repo->findOneBy([
            'safeId' => $documentId
        ]);

        if (empty($document)) {
            throw new NotFoundHttpException("Документ №" . $documentId . " не найден!");
        }

        return $document;
    }

    /**
     * Renders a view.
     *
     * @param string $view The view name
     * @param array $parameters An array of parameters to pass to the view
     * @param Response $response A response instance
     *
     * @return Response A Response instance
     */
    public function render($view, array $parameters = array(), Response $response = null)
    {
        $parameters = array_merge([
            'section' => $this->getSectionName(),
            'current_user' => $this->getUser(),
            'sd_unread_messages' => $this->get('messaging.service')->getUnreadMessagesCount($this->getUser()),
            'sd_unviewed_notifications' => $this->get('notifications.service')->getUserNotViewedCount($this->getUser())
        ], $parameters);

        return parent::render($view, $parameters, $response);
    }

    /**
     * Section name to display in the navigation
     * @return null
     */
    protected function getSectionName()
    {
        return static::SECTION_NAME;
    }
}
<?php
namespace SD\Bundle\UtilsBundle\Controller;

/**
 * Should be extended whenever authenticated assistant involved
 * Class AbstractAssistantController
 * @package SD\Bundle\UtilsBundle\Controller
 */
class AbstractAssistantController extends AbstractBaseController
{

} 
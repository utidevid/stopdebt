<?php
namespace SD\Bundle\UtilsBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Should be extended whenever authenticated user involved
 * Class AbstractUserController
 * @package SD\Bundle\UtilsBundle\Controller
 */
abstract class AbstractUserController extends AbstractBaseController
{
    protected $activeCase = NULL;

    protected function getActiveCase()
    {
        return $this->activeCase;
    }

    /**
     * {@inheritdoc}
     */
    protected function loadQuestionnaireBySafeId($questionnaireId)
    {
        $questionnaire = parent::loadQuestionnaireBySafeId($questionnaireId);

        if ($this->getUser()->getId() != $questionnaire->getUser()) {
            #consider 404
            throw new AccessDeniedHttpException("Анкета принадлежит другому пользователю!");
        }

        return $questionnaire;
    }

    protected function loadCaseFromSession() {
        $activeCaseId = $this->get('session')->get('activeCaseId');
        if ($activeCaseId) {
            $repo = $this->getDoctrine()->getRepository('CaseBundle:DebtCase');
            $this->activeCase = $repo->findOneBy([
                'safeId' => $activeCaseId
            ]);
        } else {
            $this->activeCase = $this->getUser()->getCases()->first();
        }
    }

    protected function loadCaseBySafeId($caseId)
    {
        $case = parent::loadCaseBySafeId($caseId);

        if ($this->getUser()->getId() != $case->getUser()->getId()) {
            throw new AccessDeniedHttpException("You don't have access to this case");
        }

        $this->activeCase = $case;
        $this->get('session')->set('activeCaseId', $case->getSafeId());

        return $case;
    }

    public function render($view, array $parameters = array(), Response $response = null)
    {

        $parameters = array_merge([
            'activeCase' => $this->getActiveCase()
        ], $parameters);

        return parent::render($view, $parameters, $response);
    }
}
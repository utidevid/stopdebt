<?php

namespace SD\Bundle\UtilsBundle\Tests\Traits;

use Doctrine\ORM\EntityManager;
use Hautelook\AliceBundle\Doctrine\Command\LoadDataFixturesCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

trait SDDatabaseTestCaseTrait
{
    protected function purgeDatabaseAndRollFixtures($kernel = null)
    {
        $app = new Application($this->getKernel($kernel));
        /** @var LoadDataFixturesCommand $commandService */
        $commandService = $this->getKernel()->getContainer()->get('hautelook_alice.doctrine.command.load_command');

        $app->add($commandService);

        $output = new ConsoleOutput(OutputInterface::VERBOSITY_VERBOSE);
        $input = new ArrayInput([]);
        $input->setInteractive(false);

        $command = $app->find("hautelook_alice:doctrine:fixtures:load");
        return $command->run($input, $output);
    }


    /**
     * @param null $kernel
     * @return EntityManager
     */
    protected function getEntityManager($kernel = null)
    {
        return $this->getKernel($kernel)->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @param \AppKernel|null $kernel
     * @return \AppKernel
     */
    protected function getKernel($kernel = null)
    {
        if (!is_null(self::$kernel)) {
            return self::$kernel;
        }

        if (is_null($kernel)) {
            return new \AppKernel('test', true);
        }

        return $kernel;
    }
}
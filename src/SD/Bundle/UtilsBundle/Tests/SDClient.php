<?php
namespace SD\Bundle\UtilsBundle\Tests;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\User;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use \Symfony\Bundle\FrameworkBundle\Client;

class SDClient extends Client
{
    public function loginRealUser($email = 'test@test.com')
    {
        /** @var User $user */
        $user = $this->getEntityManager()->getRepository('UserBundle:User')->findOneBy(['email' => $email]);

        $this->tokenLogin($user);
    }

    public function loginAssistant()
    {
        $this->loginRealUser('assistant@test.com');
    }

    protected function tokenLogin(User $user)
    {
        $session = $this->getContainer()->get('session');
        $token = new UsernamePasswordToken($user, 'test', 'fos_userbundle', $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $this->getCookieJar()->set($cookie);
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }
} 
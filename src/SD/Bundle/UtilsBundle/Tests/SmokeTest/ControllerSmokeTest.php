<?php
namespace SD\Bundle\UtilsBundle\Tests\SmokeTest;

use SD\Bundle\UtilsBundle\Tests\WebTestCase;

class ControllerSmokeTest extends WebTestCase
{

    public function testMainRoutesProvider()
    {
        return [
            ['/login'],
            ['/signup']
        ];
    }

    /**
     * @param $url
     * @dataProvider testMainRoutesProvider
     */
    public function testMainRoutes($url)
    {
        $this->client->request('GET', $url);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    /**
     * Tests to make sure closed pages are redirecting to login
     */
    public function testRedirects()
    {
        $this->client->request('GET', '/');
        $this->assertRegExp('/http(s?):\/\/[0-9a-z_\-\.]+\/login/i',
            $this->client->getResponse()->headers->get('Location'), "non logged in users isn't redirected to login!");
    }
}
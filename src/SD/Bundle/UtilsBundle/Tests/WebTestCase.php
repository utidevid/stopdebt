<?php

namespace SD\Bundle\UtilsBundle\Tests;

class WebTestCase extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase
{
    /** @var SDClient  */
    public $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }
}

<?php
namespace SD\Bundle\QuestionnaireBundle\Tests\Model;

use SD\Bundle\QuestionnaireBundle\Entity\Questionnaire;

class QuestionnaireTest extends \PHPUnit_Framework_TestCase
{
    public function testSetGoalDataProvider()
    {
        return [
            [json_decode('{"client_status":"1","main_goal":"2","exchange_to_uah":"1","monthly_pay":"123",
                "property_type":"2","property_type_1_pay_with_discount":"1","property_type_1_amount":"",
                "property_type_2_relocation":"2","property_type_3_pay_with_discount":"1","property_type_3_amount":"",
                "bankruptcy_type":"1","bankruptcy_pay_with_discount":"1","bankruptcy_amount":""}', true),

                ['client_status' => 1, 'main_goal' => 2, 'property_type' => 2, 'property_type_2_relocation' => 2]
            ]
        ];
    }

    /**
     * #todo cover more cases
     * @param $data
     * @param $expected
     * @dataProvider testSetGoalDataProvider
     */
    public function testSetGoalData($data, $expected)
    {
        $questionnaire = new Questionnaire();

        $questionnaire->setGoalData($data);

        $actualData = $questionnaire->getGoalData();

        $this->assertEquals($expected, $actualData, 'filtered data doesn\'t match');
    }
} 
<?php
namespace SD\Bundle\QuestionnaireBundle\Tests\Model;

use SD\Bundle\QuestionnaireBundle\Entity\Questionnaire;
use SD\Bundle\QuestionnaireBundle\Model\QuestionnaireModel;

class QuestionnaireModelTest extends \PHPUnit_Framework_TestCase
{
    public function calculateWeightsDataProvider()
    {
        return [
            [[
                'credit_extras' => '{"mortgage":1,"mortgage_city":"\u041a\u0418\u0435\u0432",
                    "mortgage_county":"123123","mortgage_apartment":1,"mortgage_apartment_area":"23",
                    "mortgage_apartment_state":"1","mortgage_apartment_cost":"12312",
                    "mortgage_apartment_is_registered_perps":"1","mortgage_apartment_perps":"123123",
                    "mortgage_house":0,"mortgage_house_area":"","mortgage_house_state":"1","mortgage_house_cost":"",
                    "mortgage_house_is_registered_perps":"1","mortgage_house_perps":"","mortgage_estate":0,
                    "mortgage_estate_area":"","mortgage_estate_state":"1","mortgage_estate_cost":"",
                    "mortgage_estate_operated":"1","mortgage_estate_profit":"","mortgage_area":0,
                    "mortgage_area_area":"","mortgage_area_purpose":"1","mortgage_area_cost":"","mortgage_other":0,
                    "mortgage_other_cost":"","surety":0,"surety_person_fullname":"","surety_person_phone":"",
                    "surety_person_email":"","surety_person_contact_details":"","pledge":0,"pledge_car":0,
                    "pledge_car_brand":"","pledge_car_manufacture_year":"","pledge_car_cost":"",
                    "pledge_car_registration_year":"","pledge_movables":0,"pledge_movables_name":"",
                    "pledge_movables_manufacture_year":"","pledge_movables_cost":"",
                    "pledge_movables_registration_year":"","pledge_securities":0,"pledge_securities_cost":"",
                    "pledge_corporate_rights":0,"pledge_corporate_rights_operated":"1",
                    "pledge_corporate_rights_owned":"1","pledge_other":0,"pledge_other_cost":"","nothing":0}',
                'credit_details' => '{"bank_name":"123","agreement_id":"123","credit_issue_date":"1123",
                    "credit_close_date":"123",
                    "issued_credit_amount":"123","credit_rate":"123","monthly_pay":"123","credit_left_amount":"12",
                    "credit_body_amount":"32","credit_left_rate":"23","credit_fines":"123","is_delayed":"1",
                    "delay_days":"12","delay_amount":"123","paid_to_bank_amount":"123123"}',
                'goal_data' => '{"client_status":"1","main_goal":"2","exchange_to_uah":"1","monthly_pay":"123",
                    "property_type":"2","property_type_1_pay_with_discount":"1","property_type_1_amount":"",
                    "property_type_2_relocation":"2","property_type_3_pay_with_discount":"1",
                    "property_type_3_amount":"","bankruptcy_type":"1","bankruptcy_pay_with_discount":"1","bankruptcy_amount":""}',
                'credit_data' => '{"status":"1","currency":"1","is_entrepreneur":"1","distributed_moratorium":"1"}',
                'penalty_data' => '{"city":"\u041a\u0418\u0435\u0432","creditor_type":"1","creditor_name":"123",
                    "stage":"1","stage1_calls":1,"stage1_mails":1,"satge1_visits":1,"stage1_else":0,
                    "stage1_else_details":"","stage2_participation":"1","stage2_fio":"","stage2_phone":"","stage2_email":"","stage2_else":"","stage2_type1":0,"stage2_type1_amount":"","stage2_type2":0,"stage2_type3":0,"stage2_type4":0,"stage2_court_type":"1","stage2_court_fio":"","stage2_court_stage2_amount1":"","stage2_court_stage2_amount2":"","stage2_court_stage2_date":"","stage2_court_stage3_state":"1","stage2_court_stage3_date":"","stage2_court_stage3_amount":"","stage2_court_stage3_type":"1","stage2_appeal_type":"1","stage2_appeal_stage":"1","stage2_appeal_stage2_amount1":"","stage2_appeal_stage2_amount2":"","stage2_appeal_stage2_date":"","stage2_cassation_type":"1","stage2_cassation_type2":"1","stage3_name":"","stage3_fio":"","stage3_date":"","stage3_amount":"","stage3_stage":"1","stage3_writeoff":"1","stage3_queue":"3"}',
                'financial_data' => '{"sick_perps_monthly_amount":"0","regular_income":"1",
                    "avg_monthly_income":"123","guarantor_avg_monthly_income":"123","has_children":1,
                    "children_age":"14","children_monthly_amount":"232","children_studies":1,"children_treatment":1,
                    "has_military":1,"military_monthly_amount":"23","has_family":1,"family_monthly_amount":"32",
                    "has_sick_perps":0,"sick_perps_age":"","sick_perps_monthly_amount":"",
                    "credit_at_different_bank":0, "personal_expenses":0, "other_properties": 0}',
                'resolution_attempts' => '{"no_action":0,"restructuring":1,"restructuring_happened":"2",
                    "restructuring_date":"","restructuring_conditions":"1","restructuring_condition_details":"",
                    "discount":1,"discount_conditions":"1","discount_amount_by_bank":"","discount_date_by_bank":"",
                    "discount_amount_by_you":"","discount_date_by_you":"","foreclosure_sale":1,
                    "foreclosure_sale_conditions":"3","foreclosure_sale_date_by_bank":"",
                    "foreclosure_sale_bank_dept_conditions":"1","foreclosure_sale_date_by_you":"123",
                    "foreclosure_sale_you_dept_conditions":"1","voluntary_property_transfer":0}'
            ]]
        ];
    }

    /**
     * @param $data
     * @dataProvider calculateWeightsDataProvider
     */
    public function testCalculateWeights($data)
    {
        $qModel = new Questionnaire();
        $model = new QuestionnaireModel();

        $qModel->setCreditData(json_decode($data['credit_data'], true));
        $qModel->setCreditExtras(json_decode($data['credit_extras'], true));
        $qModel->setCreditDetails(json_decode($data['credit_details'], true));
        $qModel->setGoalData(json_decode($data['goal_data'], true));
        $qModel->setPenaltyData(json_decode($data['penalty_data'], true));
        $qModel->setFinancialData(json_decode($data['financial_data'], true));
        $qModel->setResolutionAttempts(json_decode($data['resolution_attempts'], true));


        $result = $model->calculateWeights($qModel);

        $this->assertArrayHasKey('total', $result, "Calculated weights results don't have `total` key!");
        $this->assertArrayHasKey('separate', $result, "Calculated weights results don't have `separate` key!");
        $this->assertArrayHasKey('goal', $result['separate'], "Separate weights don't have `goal` key!");
        $this->assertArrayHasKey('credit', $result['separate'], "Separate weights don't have `credit` key!");
        $this->assertArrayHasKey('credit_details', $result['separate'], "Separate weights don't have `credit_details` key!");
        $this->assertArrayHasKey('financial_data', $result['separate'], "Separate weights don't have `financial_data` key!");
    }
}
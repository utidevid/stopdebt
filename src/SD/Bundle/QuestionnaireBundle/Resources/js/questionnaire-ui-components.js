var QUI_COMPONENTS = (function() {

    function enableElems(elems) {
        elems.removeAttr('disabled');
        elems.find('input, select, textarea').removeAttr('disabled');
        elems.css('opacity', 1);
    }
    function disableElems(elems) {
        elems.attr('disabled', 'disabled');
        elems.find('input, select, textarea').attr('disabled', 'disabled');
        elems.css('opacity', 0.3);
    }

    var BaseCmp = Backbone.View.extend({
        events: {
            'click .next-btn': 'nextStepBtnHandler',
            'click .prev-btn': 'prevStepBtnHandler',
            'click .go-to-step': 'goToStepBtnHandler'
        },
        saveToServer: function(status, cb) {
            var self = this;

            self.qData.last_step = this.activeStep;

            var dataToSave = JSON.stringify(self.qData);
            console.log(dataToSave);
            console.log('Status', status);
            console.log('Sending to server...');

            $.post(self.qData.send_url, {
                data: dataToSave,
                'status': status
            }, function(res) {
                self.qData.validation = res.validation_data;

                cb(res.status == 'ok');
            });
        },
        nextStepBtnHandler: function(e) {
            e.preventDefault();
            var self = this;

            if (this.stepView) {
                this.stepView.saveToData();
            }

            this.saveToServer(
                this.activeStep == this.totalSteps ? 2 : 1,
                function(valid) {
                    if (valid) {
                        self.activeStep += 1;

                        if (self.activeStep > self.totalSteps) {
                            self.onFinish.apply(self);
                            return;
                        }
                    }
                    self.render();
                }
            );
        },
        prevStepBtnHandler: function(e) {
            e.preventDefault();

            if (this.stepView) {
                this.stepView.saveToData();
            }

            this.activeStep -= 1;
            if (this.activeStep == 0) {
                this.activeStep = 1;
            }

            this.saveToServer(1, function() {});

            this.render();
        },
        goToStepBtnHandler: function(e) {
            e.preventDefault();

            if (this.stepView) {
                this.stepView.saveToData();
            }

            this.activeStep = parseInt($(e.currentTarget).attr('data-step'), 10);
            this.saveToServer(1, function() {});
            this.render();
        },
        initialize: function(options) {
            this.onFinish = options.onFinish || function() {};
            this.qData = options.qData || {};

            this.totalSteps = 8;
            this.activeStep = this.qData.last_step;

            this.steps = {
                1: Step1Cmp,
                2: Step2Cmp,
                3: Step3Cmp,
                4: Step4Cmp,
                5: Step5Cmp,
                6: Step6Cmp,
                7: Step7Cmp,
                8: Step8Cmp
            };

            this.render();
        },
        render: function() {
            var self = this;
            var html = QUI.renderTemplate('base', {
                activeStep: this.activeStep,
                totalSteps: this.totalSteps
            }, function(html) {
                self.$el.html(html);

                var stepViewFnc = self.steps[self.activeStep];

                if (self.stepView) {
                    self.stepView.remove();
                    delete self.stepView;
                }
                self.stepView = new stepViewFnc({
                    qData: self.qData
                });

                self.$el.find('.step-container').append(self.stepView.$el);

            // todo:  scroll to Top if necessary
            });
        }
    });

    var Step1Cmp = Backbone.View.extend({
        events: {},
        initialize: function(options) {
            this.qData = options.qData || {};
            this.render();
        },
        saveToData: function() {
            this.qData.user.last_name = this.$el.find('#qs1-lastname').valueIfNotDisabled();
            this.qData.user.first_name = this.$el.find('#qs1-name').valueIfNotDisabled();
            this.qData.user.patronymic = this.$el.find('#qs1-patronymic').valueIfNotDisabled();
            this.qData.user.locality = this.$el.find('#qs1-city').valueIfNotDisabled();
            this.qData.user.phone_number = this.$el.find('#qs1-phone').valueIfNotDisabled();
            this.qData.goal.client_status = this.$el.find('#qs1-client-status').valueIfNotDisabled();
            this.qData.user.passport = this.$el.find('#qs1-passport').valueIfNotDisabled();
            this.qData.user.identification_code = this.$el.find('#qs1-identification_code').valueIfNotDisabled();
            this.qData.user.birthdate = this.$el.find('#qs1-birthdate').valueIfNotDisabled();
        },
        render: function() {
            var self = this;
            var html = QUI.renderTemplate('step1', {
                data: this.qData
            }, function(html) {
                self.$el.html(html);

                self.$el.find('#qs1-birthdate').datepicker({
                    dateFormat: "dd-mm-yy",
                    changeMonth: true,
                    changeYear: true
                });

                self.$el.find('#qs1-city').autocomplete({
                    source: QUI_AUTOCOMPLETE.cities,
                    minLength: 2
                });
            });
        }
    });

    var Step2Cmp = Backbone.View.extend({
        events: {
            'change #qs1-goal': 'goalChangeHandler',
            'change [name="qs1-goal2-a"]': 'goal2TypeChangeHandler',
            'change #qs1-goal2-b': 'goal2SelectBChangeHandler',
            'change #qs1-goal2-e': 'goal2SelectEChangeHandler',
            'change [name="qs1-goal4-a"]': 'goal4TypeChangeHandler',
            'change #qs1-goal4-b': 'goal4SelectBChangeHandler'
        },
        goalChangeHandler: function() {
            var self = this;
            var goalVal = parseInt(this.$el.find('#qs1-goal').val(), 10);
            _.each([1, 2, 3, 4], function(i) {
                if (i == goalVal) {
                    enableElems(self.$el.find('#qs-goal-' + i + '-set'));

                    if (i == 2) {
                        self.goal2TypeChangeHandler();
                    } else if (i == 4) {
                        self.goal4TypeChangeHandler();
                    }
                } else {
                    disableElems(self.$el.find('#qs-goal-' + i + '-set'));
                }
            });
        },
        goal2TypeChangeHandler: function() {
            var self = this;
            var goalVal = parseInt(this.$el.find('[name="qs1-goal2-a"]:checked').val(), 10);

            _.each([1, 2, 3], function(i) {
                if (i == goalVal) {
                    enableElems(self.$el.find('#qs-goal-2-set-' + i));

                    if (i == 1) {
                        self.goal2SelectBChangeHandler();
                    } else if (i == 3) {
                        self.goal2SelectEChangeHandler();
                    }
                } else {
                    disableElems(self.$el.find('#qs-goal-2-set-' + i));
                }
            });
        },
        goal2SelectBChangeHandler: function() {
            var fVal = this.$el.find('#qs1-goal2-b').val();

            if (fVal == 1) {
                this.$el.find('#qs1-goal2-c').parent().show();
                this.$el.find('#qs1-goal2-c2').hide();
            } else {
                this.$el.find('#qs1-goal2-c').parent().hide();
                this.$el.find('#qs1-goal2-c2').show();
            }
        },
        goal2SelectEChangeHandler: function() {
            var fVal = this.$el.find('#qs1-goal2-e').val();

            if (fVal == 1) {
                this.$el.find('#qs1-goal2-f').parent().show();
                this.$el.find('#qs1-goal2-f2').hide();
            } else {
                this.$el.find('#qs1-goal2-f').parent().hide();
                this.$el.find('#qs1-goal2-f2').show();
            }
        },
        goal4TypeChangeHandler: function() {
            var self = this;
            var goalVal = parseInt(this.$el.find('[name="qs1-goal4-a"]:checked').val(), 10);

            _.each([1, 2], function(i) {
                if (i == goalVal) {
                    enableElems(self.$el.find('#qs-goal-4-set-' + i));

                    if (i == 1) {
                        self.goal4SelectBChangeHandler();
                    }
                } else {
                    disableElems(self.$el.find('#qs-goal-4-set-' + i));
                }
            });
        },
        goal4SelectBChangeHandler: function() {
            var fVal = this.$el.find('#qs1-goal4-b').val();

            if (fVal == 1) {
                this.$el.find('#qs1-goal4-c').parent().show();
                this.$el.find('#qs1-goal4-c2').hide();
            } else {
                this.$el.find('#qs1-goal4-c').parent().hide();
                this.$el.find('#qs1-goal4-c2').show();
            }
        },
        saveToData: function() {
            this.qData.goal.main_goal = this.$el.find('#qs1-goal').valueIfNotDisabled();
            this.qData.goal.exchange_to_uah = this.$el.find('#qs1-goal1-exchange').valueIfNotDisabled();;
            this.qData.goal.monthly_pay = this.$el.find('#qs1-goal1-amount').valueIfNotDisabled();;
            this.qData.goal.property_type = this.$el.find('[name="qs1-goal2-a"]:checked').valueIfNotDisabled();;
            this.qData.goal.property_type_1_pay_with_discount = this.$el.find('#qs1-goal2-b').valueIfNotDisabled();;
            this.qData.goal.property_type_1_amount = this.$el.find('#qs1-goal2-c').valueIfNotDisabled();;
            this.qData.goal.property_type_2_relocation = this.$el.find('#qs1-goal2-d').valueIfNotDisabled();;
            this.qData.goal.property_type_3_pay_with_discount = this.$el.find('#qs1-goal2-e').valueIfNotDisabled();;
            this.qData.goal.property_type_3_amount = this.$el.find('#qs1-goal2-f').valueIfNotDisabled();;
            this.qData.goal.bankruptcy_type = this.$el.find('[name="qs1-goal4-a"]:checked').valueIfNotDisabled();;
            this.qData.goal.bankruptcy_pay_with_discount = this.$el.find('#qs1-goal4-b').valueIfNotDisabled();;
            this.qData.goal.bankruptcy_amount = this.$el.find('#qs1-goal4-c').valueIfNotDisabled();;
        },
        initialize: function(options) {
            this.qData = options.qData || {};
            this.render();
        },
        render: function() {
            var self = this;
            var html = QUI.renderTemplate('step2', {
                data: this.qData
            }, function(html) {
                self.$el.html(html);
                self.goalChangeHandler();
            });
        }
    });

    var Step3Cmp = Backbone.View.extend({
        events: {},
        initialize: function(options) {
            this.qData = options.qData || {};
            this.render();
        },
        saveToData: function() {
            this.qData.credit.status = this.$el.find('#qs3-credit_data1').valueIfNotDisabled();;
            this.qData.credit.currency = this.$el.find('#qs3-credit_data2').valueIfNotDisabled();;
            this.qData.credit.is_entrepreneur = this.$el.find('#qs3-credit_data3').valueIfNotDisabled();;
            this.qData.credit.distributed_moratorium = this.$el.find('#qs3-credit_data4').valueIfNotDisabled();;
        },
        render: function() {
            var self = this;
            var html = QUI.renderTemplate('step3', {
                data: this.qData
            }, function(html) {
                self.$el.html(html);
            });
        }
    });

    var Step4Cmp = Backbone.View.extend({
        events: {
            'change #qs4-credit_details11': 'creditDetails11SelectChangeHandler',
        },
        creditDetails11SelectChangeHandler: function() {
            var fVal = this.$el.find('#qs4-credit_details11').val();

            if (fVal == 1) {
                enableElems(this.$el.find('#qs4-credit_details12').parent());
                enableElems(this.$el.find('#qs4-credit_details13').parent());
            } else {
                disableElems(this.$el.find('#qs4-credit_details12').parent());
                disableElems(this.$el.find('#qs4-credit_details13').parent());
            }
        },
        initialize: function(options) {
            this.qData = options.qData || {};
            this.render();
        },
        saveToData: function() {
            if (this.qData.caseTariff != 'standard') {
                this.qData.credit_details.bank_name = this.$el.find('#qs4-credit_details0').valueIfNotDisabled();
                this.qData.credit_details.agreement_id = this.$el.find('#qs4-credit_details1').valueIfNotDisabled();
                this.qData.credit_details.credit_issue_date = this.$el.find('#qs4-credit_details2').valueIfNotDisabled();
                this.qData.credit_details.credit_close_date = this.$el.find('#qs4-credit_details3').valueIfNotDisabled();
                this.qData.credit_details.issued_credit_amount = this.$el.find('#qs4-credit_details4').valueIfNotDisabled();
                this.qData.credit_details.credit_rate = this.$el.find('#qs4-credit_details5').valueIfNotDisabled();
                this.qData.credit_details.monthly_pay = this.$el.find('#qs4-credit_details6').valueIfNotDisabled();
            }

            this.qData.credit_details.credit_left_amount = this.$el.find('#qs4-credit_details7').valueIfNotDisabled();
            this.qData.credit_details.credit_body_amount = this.$el.find('#qs4-credit_details8').valueIfNotDisabled();
            this.qData.credit_details.credit_left_rate = this.$el.find('#qs4-credit_details9').valueIfNotDisabled();
            this.qData.credit_details.credit_fines = this.$el.find('#qs4-credit_details10').valueIfNotDisabled();
            this.qData.credit_details.is_delayed = this.$el.find('#qs4-credit_details11').valueIfNotDisabled();
            this.qData.credit_details.delay_days = this.$el.find('#qs4-credit_details12').valueIfNotDisabled();
            this.qData.credit_details.delay_amount = this.$el.find('#qs4-credit_details13').valueIfNotDisabled();
            this.qData.credit_details.paid_to_bank_amount = this.$el.find('#qs4-credit_details14').valueIfNotDisabled();

        },
        render: function() {
            var self = this;
            var html = QUI.renderTemplate(this.qData.caseTariff == 'standard' ? 'step4_simple' : 'step4', {
                data: this.qData
            }, function(html) {
                self.$el.html(html);
                self.creditDetails11SelectChangeHandler();

                self.$el.find('#qs4-credit_details0').autocomplete({
                    source: QUI_AUTOCOMPLETE.banks,
                    minLength: 2
                });
            });
        }
    });

    var Step5Cmp = Backbone.View.extend({
        events: {
            'change #qs5-credit_extras-h-mortgage': 'enableMortgage',
            //'change #qs5-credit_extras-mortgage-h-apartment': 'enableMortgageApartment',
            'change #qs5-credit_extras-mortgage-apartment4': 'enableMortgageApartmentPerps',
            //'change #qs5-credit_extras-mortgage-h-house': 'enableMortgageHouse',
            'change #qs5-credit_extras-mortgage-house4': 'enableMortgageHousePerps',
            //'change #qs5-credit_extras-mortgage-h-estate': 'enableMortgageEstate',
            'change #qs5-credit_extras-mortgage-estate4': 'enableMortgageEstateProfit',
            //'change #qs5-credit_extras-mortgage-h-area': 'enableMortgageArea',
            //'change #qs5-credit_extras-mortgage-h-other': 'enableMortgageOther',
            'change #qs5-credit_extras-h-surety': 'enableSurety',
            'change #qs5-credit_extras-h-pledge': 'enablePledge',
            'change #qs5-credit_extras-pledge-h-car': 'enablePledgeCar',
            'change #qs5-credit_extras-pledge-h-movables': 'enablePledgeMovables',
            'change #qs5-credit_extras-pledge-h-securities': 'enablePledgeSecurities',
            'change #qs5-credit_extras-pledge-h-corporate_rights': 'enablePledgeCorporateRights',
            'change #qs5-credit_extras-pledge-corporate_rights1': 'enablePledgeCorporateRightsOperated',
            'change #qs5-credit_extras-pledge-h-other': 'enablePledgeOther',
            'change #qs5-credit_extras-h-nothing': 'enableNothing',
            'change [name="qs5-credit_extras-mortgage-h-option"]': 'mortgageOptionHandler'
        },
        mortgageOptionHandler: function() {
            var self = this;
            var mortgageOption = parseInt(this.$el.find('[name="qs5-credit_extras-mortgage-h-option"]:checked').val(), 10);

            _.each([1,2,3,4,5], function(i) {
                if (i == mortgageOption) {
                    if (i == 1) {
                        // enable apartment
                        enableElems(self.$el.find('#block-credit_extras-mortgage-apartment'));
                        self.enableMortgageApartmentPerps();

                        // disable others
                        disableElems(self.$el.find('#block-credit_extras-mortgage-house'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-estate'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-area'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-other'));
                    }
                    else if (i == 2) {
                        // enable house
                        enableElems(self.$el.find('#block-credit_extras-mortgage-house'));
                        self.enableMortgageHousePerps();

                        // disable others
                        disableElems(self.$el.find('#block-credit_extras-mortgage-apartment'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-estate'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-area'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-other'));
                    }
                    else if (i == 3) {
                        // enable estate
                        enableElems(self.$el.find('#block-credit_extras-mortgage-estate'));
                        self.enableMortgageEstateProfit();

                        // disable others
                        disableElems(self.$el.find('#block-credit_extras-mortgage-apartment'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-house'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-area'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-other'));
                    }
                    else if (i == 4) {
                        // enable area
                        enableElems(self.$el.find('#block-credit_extras-mortgage-area'));

                        // disable others
                        disableElems(self.$el.find('#block-credit_extras-mortgage-apartment'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-house'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-estate'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-other'));
                    }
                    else if (i == 5) {
                        // enable other
                        enableElems(self.$el.find('#block-credit_extras-mortgage-other'));

                        // disable others
                        disableElems(self.$el.find('#block-credit_extras-mortgage-apartment'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-house'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-estate'));
                        disableElems(self.$el.find('#block-credit_extras-mortgage-area'));
                    }
                }
            });
        },
        enableMortgage: function() {

            if (this.$el.find('#qs5-credit_extras-h-mortgage').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-mortgage'));

                this.mortgageOptionHandler();
                /*this.enableMortgageApartment();
                this.enableMortgageHouse();
                this.enableMortgageEstate();
                this.enableMortgageArea();
                this.enableMortgageOther();*/

                // Disable nothing section
                this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', false);
                this.enableNothing();
            } else {
                disableElems(this.$el.find('#block-credit_extras-mortgage'));

                if (!this.$el.find('#qs5-credit_extras-h-pledge').is(':checked') && !this.$el.find('#qs5-credit_extras-h-surety').is(':checked')) {
                    this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', true);
                    this.enableNothing();
                }
            }
        },
        enableMortgageApartment: function() {
            if (this.$el.find('#qs5-credit_extras-mortgage-h-apartment').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-mortgage-apartment'));
                this.enableMortgageApartmentPerps();
            } else {
                disableElems(this.$el.find('#block-credit_extras-mortgage-apartment'));
            }
        },
        enableMortgageApartmentPerps: function() {
            var self = this;
            var apartmentPerpsVal = parseInt(this.$el.find('#qs5-credit_extras-mortgage-apartment4').val(), 10);
            _.each([1, 2], function(i) {
                if (i == apartmentPerpsVal) {
                    if (i == 1) {
                        enableElems(self.$el.find('#qs5-credit_extras-mortgage-apartment5'));
                    } else if (i == 2) {
                        disableElems(self.$el.find('#qs5-credit_extras-mortgage-apartment5'));
                    }
                }
            });
        },
        enableMortgageHouse: function() {
            if (this.$el.find('#qs5-credit_extras-mortgage-h-house').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-mortgage-house'));
                this.enableMortgageHousePerps();
            } else {
                disableElems(this.$el.find('#block-credit_extras-mortgage-house'));
            }
        },
        enableMortgageHousePerps: function() {
            var self = this;
            var housePerpsVal = parseInt(this.$el.find('#qs5-credit_extras-mortgage-house4').val(), 10);
            _.each([1, 2], function(i) {
                if (i == housePerpsVal) {
                    if (i == 1) {
                        enableElems(self.$el.find('#qs5-credit_extras-mortgage-house5'));
                    } else if (i == 2) {
                        disableElems(self.$el.find('#qs5-credit_extras-mortgage-house5'));
                    }
                }
            });
        },
        enableMortgageEstate: function() {
            if (this.$el.find('#qs5-credit_extras-mortgage-h-estate').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-mortgage-estate'));
                this.enableMortgageEstateProfit();
            } else {
                disableElems(this.$el.find('#block-credit_extras-mortgage-estate'));
            }
        },
        enableMortgageEstateProfit: function() {
            var self = this;
            var estateOperatedVal = parseInt(this.$el.find('#qs5-credit_extras-mortgage-estate4').val(), 10);
            _.each([1, 2], function(i) {
                if (i == estateOperatedVal) {
                    if (i == 1) {
                        enableElems(self.$el.find('#qs5-credit_extras-mortgage-estate5'));
                    } else if (i == 2) {
                        disableElems(self.$el.find('#qs5-credit_extras-mortgage-estate5'));
                    }
                }
            });
        },
        enableMortgageArea: function() {
            if (this.$el.find('#qs5-credit_extras-mortgage-h-area').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-mortgage-area'));

            } else {
                disableElems(this.$el.find('#block-credit_extras-mortgage-area'));
            }
        },
        enableMortgageOther: function() {
            if (this.$el.find('#qs5-credit_extras-mortgage-h-other').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-mortgage-other'));

            } else {
                disableElems(this.$el.find('#block-credit_extras-mortgage-other'));
            }
        },
        enableSurety: function() {
            if (this.$el.find('#qs5-credit_extras-h-surety').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-surety'));

                // Disable nothing section
                this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', false);
                this.enableNothing();
            } else {
                disableElems(this.$el.find('#block-credit_extras-surety'));

                if (!this.$el.find('#qs5-credit_extras-h-mortgage').is(':checked') && !this.$el.find('#qs5-credit_extras-h-pledge').is(':checked')) {
                    this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', true);
                    this.enableNothing();
                }
            }
        },
        enablePledge: function() {
            if (this.$el.find('#qs5-credit_extras-h-pledge').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-pledge'));

                this.enablePledgeCar();
                this.enablePledgeMovables();
                this.enablePledgeSecurities();
                this.enablePledgeCorporateRights();
                this.enablePledgeOther();

                // Disable nothing section
                this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', false);
                this.enableNothing();

            } else {
                disableElems(this.$el.find('#block-credit_extras-pledge'));

                if (!this.$el.find('#qs5-credit_extras-h-mortgage').is(':checked') && !this.$el.find('#qs5-credit_extras-h-surety').is(':checked')) {
                    this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', true);
                    this.enableNothing();
                }
            }
        },
        enablePledgeCar: function() {
            if (this.$el.find('#qs5-credit_extras-pledge-h-car').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-pledge-car'));
            } else {
                disableElems(this.$el.find('#block-credit_extras-pledge-car'));
            }
        },
        enablePledgeMovables: function() {
            if (this.$el.find('#qs5-credit_extras-pledge-h-movables').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-pledge-movables'));
            } else {
                disableElems(this.$el.find('#block-credit_extras-pledge-movables'));
            }
        },
        enablePledgeSecurities: function() {
            if (this.$el.find('#qs5-credit_extras-pledge-h-securities').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-pledge-securities'));
            } else {
                disableElems(this.$el.find('#block-credit_extras-pledge-securities'));
            }
        },
        enablePledgeCorporateRights: function() {
            if (this.$el.find('#qs5-credit_extras-pledge-h-corporate_rights').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-pledge-corporate_rights'));
                this.enablePledgeCorporateRightsOperated();
            } else {
                disableElems(this.$el.find('#block-credit_extras-pledge-corporate_rights'));
            }
        },
        enablePledgeCorporateRightsOperated: function() {
            var self = this;
            var corporateFactoryOperated = parseInt(this.$el.find('#qs5-credit_extras-pledge-corporate_rights1').val(), 10);
            _.each([1, 2], function(i) {
                if (i == corporateFactoryOperated) {
                    if (i == 1) {
                        enableElems(self.$el.find('#qs5-credit_extras-pledge-corporate_rights2'));
                    } else if (i == 2) {
                        disableElems(self.$el.find('#qs5-credit_extras-pledge-corporate_rights2'));
                    }
                }
            });
        },
        enablePledgeOther: function() {
            if (this.$el.find('#qs5-credit_extras-pledge-h-other').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-pledge-other'));
            } else {
                disableElems(this.$el.find('#block-credit_extras-pledge-other'));
            }
        },
        enableNothing: function() {
            if (this.$el.find('#qs5-credit_extras-h-nothing').is(':checked')) {
                enableElems(this.$el.find('#block-credit_extras-nothing'));

                this.$el.find('#qs5-credit_extras-h-mortgage').prop('checked', false);
                disableElems(this.$el.find('#block-credit_extras-mortgage'));
                this.$el.find('#qs5-credit_extras-h-surety').prop('checked', false);
                disableElems(this.$el.find('#block-credit_extras-surety'));
                this.$el.find('#qs5-credit_extras-h-pledge').prop('checked', false);
                disableElems(this.$el.find('#block-credit_extras-pledge'));

            } else {
                if (this.$el.find('#qs5-credit_extras-h-mortgage').is(':checked') ||
                    this.$el.find('#qs5-credit_extras-h-surety').is(':checked') ||
                    this.$el.find('#qs5-credit_extras-h-pledge').is(':checked')) {

                    disableElems(this.$el.find('#block-credit_extras-nothing'));
                }
                else {
                    this.$el.find('#qs5-credit_extras-h-nothing').prop('checked', true);
                }
            }
        },
        initialize: function(options) {
            this.qData = options.qData || {};
            this.render();
        },
        saveToData: function() {
            this.qData.credit_extras.mortgage = this.$el.find('#qs5-credit_extras-h-mortgage').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_option = this.$el.find('[name="qs5-credit_extras-mortgage-h-option"]:checked').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_city = this.$el.find('#qs5-credit_extras-mortgage1').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_county = this.$el.find('#qs5-credit_extras-mortgage2').valueIfNotDisabled();
            //this.qData.credit_extras.mortgage_apartment = this.$el.find('#qs5-credit_extras-mortgage-h-apartment').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_apartment_area = this.$el.find('#qs5-credit_extras-mortgage-apartment1').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_apartment_state = this.$el.find('#qs5-credit_extras-mortgage-apartment2').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_apartment_cost = this.$el.find('#qs5-credit_extras-mortgage-apartment3').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_apartment_is_registered_perps = this.$el.find('#qs5-credit_extras-mortgage-apartment4').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_apartment_perps = this.$el.find('#qs5-credit_extras-mortgage-apartment5').valueIfNotDisabled();
            //this.qData.credit_extras.mortgage_house = this.$el.find('#qs5-credit_extras-mortgage-h-house').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_house_area = this.$el.find('#qs5-credit_extras-mortgage-house1').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_house_state = this.$el.find('#qs5-credit_extras-mortgage-house2').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_house_cost = this.$el.find('#qs5-credit_extras-mortgage-house3').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_house_is_registered_perps = this.$el.find('#qs5-credit_extras-mortgage-house4').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_house_perps = this.$el.find('#qs5-credit_extras-mortgage-house5').valueIfNotDisabled();
            //this.qData.credit_extras.mortgage_estate = this.$el.find('#qs5-credit_extras-mortgage-h-estate').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_estate_area = this.$el.find('#qs5-credit_extras-mortgage-estate1').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_estate_state = this.$el.find('#qs5-credit_extras-mortgage-estate2').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_estate_cost = this.$el.find('#qs5-credit_extras-mortgage-estate3').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_estate_operated = this.$el.find('#qs5-credit_extras-mortgage-estate4').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_estate_profit = this.$el.find('#qs5-credit_extras-mortgage-estate5').valueIfNotDisabled();
            //this.qData.credit_extras.mortgage_area = this.$el.find('#qs5-credit_extras-mortgage-h-area').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_area_area = this.$el.find('#qs5-credit_extras-mortgage-area1').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_area_purpose = this.$el.find('#qs5-credit_extras-mortgage-area2').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_area_cost = this.$el.find('#qs5-credit_extras-mortgage-area3').valueIfNotDisabled();
            //this.qData.credit_extras.mortgage_other = this.$el.find('#qs5-credit_extras-mortgage-h-other').valueIfNotDisabled();
            this.qData.credit_extras.mortgage_other_cost = this.$el.find('#qs5-credit_extras-mortgage-other1').valueIfNotDisabled();

            if (this.qData.caseTariff != 'standard') {
                this.qData.credit_extras.surety = this.$el.find('#qs5-credit_extras-h-surety').valueIfNotDisabled();
                this.qData.credit_extras.surety_person_fullname = this.$el.find('#qs5-credit_extras-surety1').valueIfNotDisabled();
                this.qData.credit_extras.surety_person_phone = this.$el.find('#qs5-credit_extras-surety2').valueIfNotDisabled();
                this.qData.credit_extras.surety_person_email = this.$el.find('#qs5-credit_extras-surety3').valueIfNotDisabled();
                this.qData.credit_extras.surety_person_contact_details = this.$el.find('#qs5-credit_extras-surety4').valueIfNotDisabled();
            }

            this.qData.credit_extras.pledge = this.$el.find('#qs5-credit_extras-h-pledge').valueIfNotDisabled();
            this.qData.credit_extras.pledge_car = this.$el.find('#qs5-credit_extras-pledge-h-car').valueIfNotDisabled();
            this.qData.credit_extras.pledge_car_brand = this.$el.find('#qs5-credit_extras-pledge-car1').valueIfNotDisabled();
            this.qData.credit_extras.pledge_car_manufacture_year = this.$el.find('#qs5-credit_extras-pledge-car2').valueIfNotDisabled();
            this.qData.credit_extras.pledge_car_cost = this.$el.find('#qs5-credit_extras-pledge-car3').valueIfNotDisabled();
            this.qData.credit_extras.pledge_car_registration_year = this.$el.find('#qs5-credit_extras-pledge-car4').valueIfNotDisabled();
            this.qData.credit_extras.pledge_movables = this.$el.find('#qs5-credit_extras-pledge-h-movables').valueIfNotDisabled();
            this.qData.credit_extras.pledge_movables_name = this.$el.find('#qs5-credit_extras-pledge-movables1').valueIfNotDisabled();
            this.qData.credit_extras.pledge_movables_manufacture_year = this.$el.find('#qs5-credit_extras-pledge-movables2').valueIfNotDisabled();
            this.qData.credit_extras.pledge_movables_cost = this.$el.find('#qs5-credit_extras-pledge-movables3').valueIfNotDisabled();
            this.qData.credit_extras.pledge_movables_registration_year = this.$el.find('#qs5-credit_extras-pledge-movables4').valueIfNotDisabled();
            this.qData.credit_extras.pledge_securities = this.$el.find('#qs5-credit_extras-pledge-h-securities').valueIfNotDisabled();
            this.qData.credit_extras.pledge_securities_cost = this.$el.find('#qs5-credit_extras-pledge-securities1').valueIfNotDisabled();
            this.qData.credit_extras.pledge_corporate_rights = this.$el.find('#qs5-credit_extras-pledge-h-corporate_rights').valueIfNotDisabled();
            this.qData.credit_extras.pledge_corporate_rights_operated = this.$el.find('#qs5-credit_extras-pledge-corporate_rights1').valueIfNotDisabled();
            this.qData.credit_extras.pledge_corporate_rights_owned = this.$el.find('#qs5-credit_extras-pledge-corporate_rights2').valueIfNotDisabled();
            this.qData.credit_extras.pledge_other = this.$el.find('#qs5-credit_extras-pledge-h-other').valueIfNotDisabled();
            this.qData.credit_extras.pledge_other_cost = this.$el.find('#qs5-credit_extras-pledge-other1').valueIfNotDisabled();
            this.qData.credit_extras.nothing = this.$el.find('#qs5-credit_extras-h-nothing').valueIfNotDisabled();
        },
        render: function() {
            var self = this;
            var html = QUI.renderTemplate(this.qData.caseTariff == 'standard' ? 'step5_simple' : 'step5', {
                data: this.qData
            }, function(html) {
                self.$el.html(html);
                self.enableMortgage();
                self.enableSurety();
                self.enablePledge();
                self.enableNothing();

                self.$el.find('#qs5-credit_extras-mortgage1').autocomplete({
                    source: QUI_AUTOCOMPLETE.cities,
                    minLength: 2
                });

                self.$el.find('#qs5-credit_extras-pledge-car4').autocomplete({
                    source: QUI_AUTOCOMPLETE.cities,
                    minLength: 2
                });

                self.$el.find('#qs5-credit_extras-pledge-movables4').autocomplete({
                    source: QUI_AUTOCOMPLETE.cities,
                    minLength: 2
                });
            });
        }
    });

    var Step6Cmp = Backbone.View.extend({
        events: {
            'change #qs6-stage': 'stageChangeHandler',
            'change #qs6-stage1-d': 'stage1DChangeHandler',
            'change #qs6-stage2-a': 'stage2AChangeHandler',
            'change #qs6-stage2-f': 'stage2FChangeHandler',
            'change #qs6-stage2-g': 'stage2GChangeHandler',
            'change #qs6-stage2-g2': 'stage2G2ChangeHandler',
            'change #qs6-stage2-g2-set3-a': 'stage2G23AChangeHandler',
            'change #qs6-stage2-i': 'stage2IChangeHandler',
            'change #qs6-stage2-j': 'stage2JChangeHandler',
            'change #qs6-stage3-e': 'stage3EChangeHandler',
        },
        stageChangeHandler: function() {
            var self = this;
            var goalVal = parseInt(this.$el.find('#qs6-stage').val(), 10);
            _.each([1, 2, 3, 4], function(i) {
                if (i == goalVal) {
                    enableElems(self.$el.find('#qs6-stage-' + i + '-set'));

                    if (i == 1) {
                        self.stage1DChangeHandler();
                    } else if (i == 2) {
                        self.stage2AChangeHandler();
                        self.stage2FChangeHandler();
                        self.stage2GChangeHandler();
                        self.stage2IChangeHandler();
                        self.stage2JChangeHandler();
                    } else if (i == 3) {
                        self.stage3EChangeHandler();
                    }
                } else {
                    disableElems(self.$el.find('#qs6-stage-' + i + '-set'));
                }
            });
        },
        stage1DChangeHandler: function() {
            if (this.$el.find('#qs6-stage1-d').is(':checked')) {
                enableElems(this.$el.find('#qs6-stage1-d2').parent());
            } else {
                disableElems(this.$el.find('#qs6-stage1-d2').parent());
            }
        },
        stage2AChangeHandler: function() {
            if (this.$el.find('#qs6-stage2-a').val() == 2) {
                enableElems(this.$el.find('#qs6-stage2-a2'));
            } else {
                disableElems(this.$el.find('#qs6-stage2-a2'));
            }
        },
        stage2FChangeHandler: function() {
            if (this.$el.find('#qs6-stage2-f').is(':checked')) {
                enableElems(this.$el.find('#qs6-stage2-f1').parent());
            } else {
                disableElems(this.$el.find('#qs6-stage2-f1').parent());
            }
        },
        stage2GChangeHandler: function() {
            if (this.$el.find('#qs6-stage2-g').val() == 1) {
                enableElems(this.$el.find('#qs6-stage2-g-set'));
                this.stage2G2ChangeHandler();
            } else {
                disableElems(this.$el.find('#qs6-stage2-g-set'));
            }
        },
        stage2G2ChangeHandler: function() {
            this.$el.find('#qs6-stage2-g2-set2').hide();
            this.$el.find('#qs6-stage2-g2-set3').hide();

            var cVal = this.$el.find('#qs6-stage2-g2').val();
            this.$el.find('#qs6-stage2-g2-set' + cVal).show();

            if (cVal == 3) {
                this.stage2G23AChangeHandler();
            }
        },
        stage2G23AChangeHandler: function() {
            if (this.$el.find('#qs6-stage2-g2-set3-a').val() == 2) {
                enableElems(this.$el.find('#qs6-stage2-g2-set3-b'));
            } else {
                disableElems(this.$el.find('#qs6-stage2-g2-set3-b'));
            }
        },
        stage2IChangeHandler: function() {
            if (this.$el.find('#qs6-stage2-i').val() == 2) {
                enableElems(this.$el.find('#qs6-stage2-i2-set'));
            } else {
                disableElems(this.$el.find('#qs6-stage2-i2-set'));
            }
        },
        stage2JChangeHandler: function() {
            if (this.$el.find('#qs6-stage2-j').val() == 1) {
                enableElems(this.$el.find('#qs6-stage2-k'));
            } else {
                disableElems(this.$el.find('#qs6-stage2-k'));
            }
        },
        stage3EChangeHandler: function() {
            if (this.$el.find('#qs6-stage3-e').val() == 6) {
                enableElems(this.$el.find('#qs6-stage3-e6-set'));
            } else {
                disableElems(this.$el.find('#qs6-stage3-e6-set'));
            }
        },
        saveToData: function() {
            this.qData.penalty.city = this.$el.find('#qs6-city').valueIfNotDisabled();;
            this.qData.penalty.creditor_type = this.$el.find('#qs6-creditor').valueIfNotDisabled();;
            this.qData.penalty.creditor_name = this.$el.find('#qs6-creditor-name').valueIfNotDisabled();;
            this.qData.penalty.stage = this.$el.find('#qs6-stage').valueIfNotDisabled();;
            this.qData.penalty.stage1_calls = this.$el.find('#qs6-stage1-a').valueIfNotDisabled();
            this.qData.penalty.stage1_mails = this.$el.find('#qs6-stage1-b').valueIfNotDisabled();
            this.qData.penalty.satge1_visits = this.$el.find('#qs6-stage1-c').valueIfNotDisabled();
            this.qData.penalty.stage1_else = this.$el.find('#qs6-stage1-d').valueIfNotDisabled();
            this.qData.penalty.stage1_else_details = this.$el.find('#qs6-stage1-d2').valueIfNotDisabled();;
            this.qData.penalty.stage2_participation = this.$el.find('#qs6-stage2-a').valueIfNotDisabled();;
            this.qData.penalty.stage2_fio = this.$el.find('#qs6-stage2-b').valueIfNotDisabled();;
            this.qData.penalty.stage2_phone = this.$el.find('#qs6-stage2-c').valueIfNotDisabled();;
            this.qData.penalty.stage2_email = this.$el.find('#qs6-stage2-d').valueIfNotDisabled();;
            this.qData.penalty.stage2_else = this.$el.find('#qs6-stage2-e').valueIfNotDisabled();;
            this.qData.penalty.stage2_type1 = this.$el.find('#qs6-stage2-f').valueIfNotDisabled();
            this.qData.penalty.stage2_type1_amount = this.$el.find('#qs6-stage2-f1').valueIfNotDisabled();;
            this.qData.penalty.stage2_type2 = this.$el.find('#qs6-stage2-f2').valueIfNotDisabled();
            this.qData.penalty.stage2_type3 = this.$el.find('#qs6-stage2-f3').valueIfNotDisabled();
            this.qData.penalty.stage2_type4 = this.$el.find('#qs6-stage2-f4').valueIfNotDisabled();
            this.qData.penalty.stage2_court_type = this.$el.find('#qs6-stage2-g').valueIfNotDisabled();;
            this.qData.penalty.stage2_court_fio = this.$el.find('#qs6-stage2-g1').valueIfNotDisabled();;
            this.qData.penalty.stage2_court_stage = this.$el.find('#qs6-stage2-g2').valueIfNotDisabled();;
            this.qData.penalty.stage2_court_stage2_amount1 = this.$el.find('#qs6-stage2-g2-set2-a').valueIfNotDisabled();;
            this.qData.penalty.stage2_court_stage2_amount2 = this.$el.find('#qs6-stage2-g2-set2-b').valueIfNotDisabled();;
            this.qData.penalty.stage2_court_stage2_date = this.$el.find('#qs6-stage2-g2-set2-c').valueIfNotDisabled();;
            this.qData.penalty.stage2_court_stage3_state = this.$el.find('#qs6-stage2-g2-set3-a').valueIfNotDisabled();;
            this.qData.penalty.stage2_court_stage3_date = this.$el.find('#qs6-stage2-g2-set3-b1').valueIfNotDisabled();;
            this.qData.penalty.stage2_court_stage3_amount = this.$el.find('#qs6-stage2-g2-set3-b2').valueIfNotDisabled();;
            this.qData.penalty.stage2_court_stage3_type = this.$el.find('#qs6-stage2-g2-set3-b3').valueIfNotDisabled();;
            this.qData.penalty.stage2_appeal_type = this.$el.find('#qs6-stage2-h').valueIfNotDisabled();;
            this.qData.penalty.stage2_appeal_stage = this.$el.find('#qs6-stage2-i').valueIfNotDisabled();;
            this.qData.penalty.stage2_appeal_stage2_amount1 = this.$el.find('#qs6-stage2-i2-set-a').valueIfNotDisabled();;
            this.qData.penalty.stage2_appeal_stage2_amount2 = this.$el.find('#qs6-stage2-i2-set-b').valueIfNotDisabled();;
            this.qData.penalty.stage2_appeal_stage2_date = this.$el.find('#qs6-stage2-i2-set-c').valueIfNotDisabled();;
            this.qData.penalty.stage2_cassation_type = this.$el.find('#qs6-stage2-j').valueIfNotDisabled();;
            this.qData.penalty.stage2_cassation_type2 = this.$el.find('#qs6-stage2-k').valueIfNotDisabled();;
            this.qData.penalty.stage3_name = this.$el.find('#qs6-stage3-a').valueIfNotDisabled();;
            this.qData.penalty.stage3_fio = this.$el.find('#qs6-stage3-b').valueIfNotDisabled();;
            this.qData.penalty.stage3_date = this.$el.find('#qs6-stage3-c').valueIfNotDisabled();;
            this.qData.penalty.stage3_amount = this.$el.find('#qs6-stage3-d').valueIfNotDisabled();;
            this.qData.penalty.stage3_stage = this.$el.find('#qs6-stage3-e').valueIfNotDisabled();;
            this.qData.penalty.stage3_writeoff = this.$el.find('#qs6-stage3-e6-set-a').valueIfNotDisabled();;
            this.qData.penalty.stage3_queue = this.$el.find('#qs6-stage3-e6-set-b').valueIfNotDisabled();;
        },
        initialize: function(options) {
            this.qData = options.qData || {};
            this.render();
        },
        render: function() {
            var self = this;
            var html = QUI.renderTemplate('step6', {
                data: this.qData
            }, function(html) {
                self.$el.html(html);
                self.stageChangeHandler();

                self.$el.find('#qs6-stage2-i2-set-c').datepicker({
                    dateFormat: "dd-mm-yy",
                    changeMonth: true,
                    changeYear: true
                });

                self.$el.find('#qs6-stage3-c').datepicker({
                    dateFormat: "dd-mm-yy",
                    changeMonth: true,
                    changeYear: true
                });

                self.$el.find('#qs6-city').autocomplete({
                    source: QUI_AUTOCOMPLETE.cities,
                    minLength: 2
                });
            });
        }
    });

    var Step7Cmp = Backbone.View.extend({
        events: {
            'change #qs7-income_type': 'baseIncomeChangeHandler',
            'change #qs7-income_data_kids': 'kidsChangeHandler',
            'change #qs7-income_data_ato': 'atoChangeHandler',
            'change #qs7-income_data_parents': 'parentsChangeHandler',
            'change #qs7-income_data_other': 'otherChangeHandler',
            'change #qs7-income_data_kids_edu': 'schoolChangeHandler',
            'change #qs7-income_data_kids_treatm': 'hospitalChangeHandler',
            'change #qs7-children_additional_expenses': 'childrenAdditionalExpensesHandler',
            'change #qs7-military_additional_expenses': 'militaryAdditionalExpensesHandler',
            'change #qs7-family_additional_expenses': 'familyAdditionalExpensesHandler',
            'change #qs7-family_additional_expenses-treatment': 'familyAdditionalExpensesTreatmentHandler',
            'change #qs7-sick_perps_additional_expenses': 'sickPerpsAdditionalExpensesHandler',
            'change #qs7-sick_perps_additional_expenses-treatment': 'sickPerpsAdditionalExpensesTreatmentHandler',
            'change #qs7-credit_at_different_bank': 'creditAtDifferentBankHandler',
            'change #qs7-personal_expenses': 'personalExpensesHandler',
            'change #qs7-personal_expenses-treatment': 'personalExpensesTreatmentHandler',
            'change #qs7-other_properties': 'otherPropertiesHandler',
            'change #qs7-other_properties-estate': 'otherPropertiesEstateHandler',
            'change #qs7-other_properties-vehicle': 'otherPropertiesVehicleHandler',
            'change #qs7-other_properties-other': 'otherPropertiesOtherHandler',
        },
        baseIncomeChangeHandler: function() {
            var self = this;
            var select = parseInt(this.$el.find('#qs7-income_type').val(), 10);
            _.each([1, 2, 3, 4], function(i) {
                if (i == select) {
                    if (i == 4) {
                        disableElems(self.$el.find('#qs7-income_medium'));
                    } else {
                        enableElems(self.$el.find('#qs7-income_medium'));
                    }
                }
            });
        },
        kidsChangeHandler: function() {
            if (this.$el.find('#qs7-income_data_kids').is(':checked')) {
                enableElems(this.$el.find('#block-has_children'));
                this.childrenAdditionalExpensesHandler();
            } else {
                disableElems(this.$el.find('#block-has_children'));
            }
        },
        atoChangeHandler: function() {
            if (this.$el.find('#qs7-income_data_ato').is(':checked')) {
                enableElems(this.$el.find('#block-has_military'));
                this.militaryAdditionalExpensesHandler();
            } else {
                disableElems(this.$el.find('#block-has_military'));
            }
        },
        parentsChangeHandler: function() {
            if (this.$el.find('#qs7-income_data_parents').is(':checked')) {
                enableElems(this.$el.find('#block-has_family'));
                this.familyAdditionalExpensesHandler();
            } else {
                disableElems(this.$el.find('#block-has_family'));
            }
        },
        otherChangeHandler: function() {
            if (this.$el.find('#qs7-income_data_other').is(':checked')) {
                enableElems(this.$el.find('#block-has_sick_perps'));
                this.sickPerpsAdditionalExpensesHandler();
            } else {
                disableElems(this.$el.find('#block-has_sick_perps'));
            }
        },
        childrenAdditionalExpensesHandler: function() {
            if (this.$el.find('#qs7-children_additional_expenses').is(':checked')) {
                enableElems(this.$el.find('#block-children-additional_expenses'));
                this.schoolChangeHandler();
                this.hospitalChangeHandler();
            } else {
                disableElems(this.$el.find('#block-children-additional_expenses'));
            }
        },
        schoolChangeHandler: function() {
            if (this.$el.find('#qs7-income_data_kids_edu').is(':checked')) {
                enableElems(this.$el.find('#block-children_school'));
            } else {
                disableElems(this.$el.find('#block-children_school'));
            }
        },
        hospitalChangeHandler: function() {
            if (this.$el.find('#qs7-income_data_kids_treatm').is(':checked')) {
                enableElems(this.$el.find('#block-children_treatment'));
            } else {
                disableElems(this.$el.find('#block-children_treatment'));
            }
        },
        militaryAdditionalExpensesHandler: function() {
            if (this.$el.find('#qs7-military_additional_expenses').is(':checked')) {
                enableElems(this.$el.find('#block-military-additional_expenses'));
            } else {
                disableElems(this.$el.find('#block-military-additional_expenses'));
            }
        },
        familyAdditionalExpensesHandler: function() {
            if (this.$el.find('#qs7-family_additional_expenses').is(':checked')) {
                enableElems(this.$el.find('#block-family-additional_expenses'));
                this.familyAdditionalExpensesTreatmentHandler();
            } else {
                disableElems(this.$el.find('#block-family-additional_expenses'));
            }
        },
        familyAdditionalExpensesTreatmentHandler: function() {
            if (this.$el.find('#qs7-family_additional_expenses-treatment').is(':checked')) {
                enableElems(this.$el.find('#block-family_treatment'));
            } else {
                disableElems(this.$el.find('#block-family_treatment'));
            }
        },
        sickPerpsAdditionalExpensesHandler: function() {
            if (this.$el.find('#qs7-sick_perps_additional_expenses').is(':checked')) {
                enableElems(this.$el.find('#block-sick_perps-additional_expenses'));
                this.sickPerpsAdditionalExpensesTreatmentHandler();
            } else {
                disableElems(this.$el.find('#block-sick_perps-additional_expenses'));
            }
        },
        sickPerpsAdditionalExpensesTreatmentHandler: function() {
            if (this.$el.find('#qs7-sick_perps_additional_expenses-treatment').is(':checked')) {
                enableElems(this.$el.find('#block-sick_perps_treatment'));
            } else {
                disableElems(this.$el.find('#block-sick_perps_treatment'));
            }
        },
        creditAtDifferentBankHandler: function() {
            if (this.$el.find('#qs7-credit_at_different_bank').is(':checked')) {
                enableElems(this.$el.find('#block-credit_at_different_bank'));
            } else {
                disableElems(this.$el.find('#block-credit_at_different_bank'));
            }
        },
        personalExpensesHandler: function() {
            if (this.$el.find('#qs7-personal_expenses').is(':checked')) {
                enableElems(this.$el.find('#block-personal_expenses'));
                this.personalExpensesTreatmentHandler();
            } else {
                disableElems(this.$el.find('#block-personal_expenses'));
            }
        },
        personalExpensesTreatmentHandler: function() {
            if (this.$el.find('#qs7-personal_expenses-treatment').is(':checked')) {
                enableElems(this.$el.find('#block-personal_expenses-treatment_cost'));
            } else {
                disableElems(this.$el.find('#block-personal_expenses-treatment_cost'));
            }
        },
        otherPropertiesHandler: function() {
            if (this.$el.find('#qs7-other_properties').is(':checked')) {
                enableElems(this.$el.find('#block-other_properties'));
                this.otherPropertiesEstateHandler();
                this.otherPropertiesVehicleHandler();
                this.otherPropertiesOtherHandler();
            } else {
                disableElems(this.$el.find('#block-other_properties'));
            }
        },
        otherPropertiesEstateHandler: function() {
            if (this.$el.find('#qs7-other_properties-estate').is(':checked')) {
                enableElems(this.$el.find('#block-other_properties-estate'));
            } else {
                disableElems(this.$el.find('#block-other_properties-estate'));
            }
        },
        otherPropertiesVehicleHandler: function() {
            if (this.$el.find('#qs7-other_properties-vehicle').is(':checked')) {
                enableElems(this.$el.find('#block-other_properties-vehicle'));
            } else {
                disableElems(this.$el.find('#block-other_properties-vehicle'));
            }
        },
        otherPropertiesOtherHandler: function() {
            if (this.$el.find('#qs7-other_properties-other').is(':checked')) {
                enableElems(this.$el.find('#block-other_properties-other'));
            } else {
                disableElems(this.$el.find('#block-other_properties-other'));
            }
        },
        initialize: function(options) {
            this.qData = options.qData || {};
            this.render();
        },
        saveToData: function() {
            this.qData.financial_data.regular_income = this.$el.find('#qs7-income_type').valueIfNotDisabled();;
            this.qData.financial_data.avg_monthly_income = this.$el.find('#qs7-income_medium').valueIfNotDisabled();;
            this.qData.financial_data.guarantor_avg_monthly_income = this.$el.find('#qs7-income_warr_medium').valueIfNotDisabled();;
            this.qData.financial_data.has_children = this.$el.find('#qs7-income_data_kids').valueIfNotDisabled();
            this.qData.financial_data.children_age = this.$el.find('#qs7-income_kids_ages').valueIfNotDisabled();;
            this.qData.financial_data.children_additional_expenses = this.$el.find('#qs7-children_additional_expenses').valueIfNotDisabled();
            this.qData.financial_data.children_additional_expenses_studies = this.$el.find('#qs7-income_data_kids_edu').valueIfNotDisabled();
            this.qData.financial_data.school_monthly_expenses = this.$el.find('#qs7-school_monthly_expenses').valueIfNotDisabled();;
            this.qData.financial_data.children_additional_expenses_treatment = this.$el.find('#qs7-income_data_kids_treatm').valueIfNotDisabled();
            this.qData.financial_data.hospital_monthly_expenses = this.$el.find('#qs7-hospital_monthly_expenses').valueIfNotDisabled();;
            this.qData.financial_data.has_military = this.$el.find('#qs7-income_data_ato').valueIfNotDisabled();
            this.qData.financial_data.military_additional_expenses = this.$el.find('#qs7-military_additional_expenses').valueIfNotDisabled();
            this.qData.financial_data.military_monthly_amount = this.$el.find('#qs7-income_data_ato_sum').valueIfNotDisabled();;
            this.qData.financial_data.has_family = this.$el.find('#qs7-income_data_parents').valueIfNotDisabled();
            this.qData.financial_data.family_age = this.$el.find('#qs7-family_age').valueIfNotDisabled();;
            this.qData.financial_data.family_additional_expenses = this.$el.find('#qs7-family_additional_expenses').valueIfNotDisabled();
            this.qData.financial_data.family_additional_expenses_treatment = this.$el.find('#qs7-family_additional_expenses-treatment').valueIfNotDisabled();
            this.qData.financial_data.family_monthly_amount = this.$el.find('#qs7-income_data_parents_sum').valueIfNotDisabled();;
            this.qData.financial_data.has_sick_perps = this.$el.find('#qs7-income_data_other').valueIfNotDisabled();
            this.qData.financial_data.sick_perps_age = this.$el.find('#qs7-income_data_other_ages').valueIfNotDisabled();;
            this.qData.financial_data.sick_perps_additional_expenses = this.$el.find('#qs7-sick_perps_additional_expenses').valueIfNotDisabled();
            this.qData.financial_data.sick_perps_additional_expenses_treatment = this.$el.find('#qs7-sick_perps_additional_expenses-treatment').valueIfNotDisabled();
            this.qData.financial_data.sick_perps_monthly_amount = this.$el.find('#qs7-income_data_other_sum').valueIfNotDisabled();;
            this.qData.financial_data.credit_at_different_bank = this.$el.find('#qs7-credit_at_different_bank').valueIfNotDisabled();
            this.qData.financial_data.credit_at_different_bank_monthly_pay = this.$el.find('#qs7-credit_at_different_bank-monthly_pay').valueIfNotDisabled();;
            this.qData.financial_data.credit_at_different_bank_status = this.$el.find('#qs7-credit_at_different_bank-status').valueIfNotDisabled();;
            this.qData.financial_data.personal_expenses = this.$el.find('#qs7-personal_expenses').valueIfNotDisabled();
            this.qData.financial_data.personal_expenses_treatment = this.$el.find('#qs7-personal_expenses-treatment').valueIfNotDisabled();
            this.qData.financial_data.personal_expenses_treatment_cost = this.$el.find('#qs7-personal_expenses-treatment_cost').valueIfNotDisabled();;
            this.qData.financial_data.other_properties = this.$el.find('#qs7-other_properties').valueIfNotDisabled();

            this.qData.financial_data.other_properties_estate = this.$el.find('#qs7-other_properties-estate').valueIfNotDisabled();
            this.qData.financial_data.other_properties_estate_in_pledge = this.$el.find('#qs7-other_properties-estate_in_pledge').valueIfNotDisabled();
            this.qData.financial_data.other_properties_vehicle = this.$el.find('#qs7-other_properties-vehicle').valueIfNotDisabled();
            this.qData.financial_data.other_properties_vehicle_in_pledge = this.$el.find('#qs7-other_properties-vehicle_in_pledge').valueIfNotDisabled();
            this.qData.financial_data.other_properties_other = this.$el.find('#qs7-other_properties-other').valueIfNotDisabled();
            this.qData.financial_data.other_properties_other_in_pledge = this.$el.find('#qs7-other_properties-other_pledge').valueIfNotDisabled();
        },
        render: function() {
            var self = this;
            var html = QUI.renderTemplate('step7', {
                data: this.qData
            }, function(html) {
                self.$el.html(html);

                self.baseIncomeChangeHandler();
                self.kidsChangeHandler();
                self.atoChangeHandler();
                self.parentsChangeHandler();
                self.otherChangeHandler();
                self.creditAtDifferentBankHandler();
                self.personalExpensesHandler();
                self.otherPropertiesHandler();
            });
        }
    });

    var Step8Cmp = Backbone.View.extend({
        events: {
            'change #qs5-resolution_attempts-h-no_action': 'enableNoAction',
            'change #qs5-resolution_attempts-h-restructuring': 'enableRestructuring',
            'change #qs5-resolution_attempts-h-discount': 'enableDiscount',
            'change #qs5-resolution_attempts-h-foreclosure_sale': 'enableForeclosureSale',
            'change #qs5-resolution_attempts-h-voluntary_property_transfer': 'enableVoluntaryPropertyTransfer',
            'change #qs5-resolution_attempts-restructuring1': 'enableRestructuringHappened',
            'change #qs5-resolution_attempts-restructuring3': 'enableRestructuringConditions',
            'change #qs5-resolution_attempts-discount1': 'enableDiscountConditions',
            'change #qs5-resolution_attempts-foreclosure_sale1': 'enableForeclosureSaleConditions',
        },
        initialize: function(options) {
            this.qData = options.qData || {};
            this.render();
        },
        enableNoAction: function() {
            if (this.$el.find('#qs5-resolution_attempts-h-no_action').is(':checked')) {
                enableElems(this.$el.find('#block-resolution_attempts-no_action'));

                // disable fields
                this.$el.find('#qs5-resolution_attempts-h-restructuring').prop('checked', false);
                disableElems(this.$el.find('#block-resolution_attempts-restructuring'));
                this.$el.find('#qs5-resolution_attempts-h-discount').prop('checked', false);
                disableElems(this.$el.find('#block-resolution_attempts-discount'));
                this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').prop('checked', false);
                disableElems(this.$el.find('#block-resolution_attempts-foreclosure_sale'));
                this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').prop('checked', false);
                disableElems(this.$el.find('#block-resolution_attempts-voluntary_property_transfer'));

            } else {
                if (this.$el.find('#qs5-resolution_attempts-h-restructuring').is(':checked') ||
                    this.$el.find('#qs5-resolution_attempts-h-discount').is(':checked') ||
                    this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').is(':checked') ||
                    this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').is(':checked')) {

                    disableElems(this.$el.find('#block-resolution_attempts-no_action'));
                }
                else {
                    this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', true);
                }
            }
        },
        enableRestructuring: function() {
            if (this.$el.find('#qs5-resolution_attempts-h-restructuring').is(':checked')) {
                enableElems(this.$el.find('#block-resolution_attempts-restructuring'));
                this.enableRestructuringHappened();
                this.enableRestructuringConditions();

                // Disable no action section
                this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', false);
                this.enableNoAction();

            } else {
                disableElems(this.$el.find('#block-resolution_attempts-restructuring'));

                if (!this.$el.find('#qs5-resolution_attempts-h-discount').is(':checked') &&
                    !this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').is(':checked') &&
                    !this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').is(':checked')) {

                    this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', true);
                    this.enableNoAction();
                }
            }
        },
        enableRestructuringHappened: function() {
            var self = this;
            var select = parseInt(this.$el.find('#qs5-resolution_attempts-restructuring1').val(), 10);
            _.each([1, 2], function(i) {
                if (i == select) {
                    if (i == 1) {
                        enableElems(self.$el.find('#qs5-resolution_attempts-restructuring2'));
                        disableElems(self.$el.find('#block-resolution_attempts-no_restructuring_happened'));
                    } else if (i == 2) {
                        disableElems(self.$el.find('#qs5-resolution_attempts-restructuring2'));
                        enableElems(self.$el.find('#block-resolution_attempts-no_restructuring_happened'));
                    }
                }
            });
        },
        enableRestructuringConditions: function() {
            var self = this;
            var select = parseInt(this.$el.find('#qs5-resolution_attempts-restructuring3').val(), 10);
            _.each([1, 2], function(i) {
                if (i == select) {
                    if (i == 1) {
                        disableElems(self.$el.find('#qs5-resolution_attempts-restructuring4'));
                    } else if (i == 2) {
                        enableElems(self.$el.find('#qs5-resolution_attempts-restructuring4'));
                    }
                }
            });
        },
        enableDiscount: function() {
            if (this.$el.find('#qs5-resolution_attempts-h-discount').is(':checked')) {
                enableElems(this.$el.find('#block-resolution_attempts-discount'));
                this.enableDiscountConditions();

                // Disable no action section
                this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', false);
                this.enableNoAction();
            } else {
                disableElems(this.$el.find('#block-resolution_attempts-discount'));

                if (!this.$el.find('#qs5-resolution_attempts-h-restructuring').is(':checked') &&
                    !this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').is(':checked') &&
                    !this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').is(':checked')) {

                    this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', true);
                    this.enableNoAction();
                }
            }
        },
        enableDiscountConditions: function() {
            var self = this;
            var select = parseInt(this.$el.find('#qs5-resolution_attempts-discount1').val(), 10);
            _.each([1, 2, 3], function(i) {
                if (i == select) {
                    if (i == 1) {
                        disableElems(self.$el.find('#block-resolution_attempts-discount_by_bank'));
                        disableElems(self.$el.find('#block-resolution_attempts-discount_by_you'));
                    } else if (i == 2) {
                        enableElems(self.$el.find('#block-resolution_attempts-discount_by_bank'));
                        disableElems(self.$el.find('#block-resolution_attempts-discount_by_you'));
                    } else if (i == 3) {
                        disableElems(self.$el.find('#block-resolution_attempts-discount_by_bank'));
                        enableElems(self.$el.find('#block-resolution_attempts-discount_by_you'));
                    }
                }
            });
        },
        enableForeclosureSale: function() {
            if (this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').is(':checked')) {
                enableElems(this.$el.find('#block-resolution_attempts-foreclosure_sale'));
                this.enableForeclosureSaleConditions();

                // Disable no action section
                this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', false);
                this.enableNoAction();
            } else {
                disableElems(this.$el.find('#block-resolution_attempts-foreclosure_sale'));

                if (!this.$el.find('#qs5-resolution_attempts-h-restructuring').is(':checked') &&
                    !this.$el.find('#qs5-resolution_attempts-h-discount').is(':checked') &&
                    !this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').is(':checked')) {

                    this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', true);
                    this.enableNoAction();
                }
            }
        },
        enableForeclosureSaleConditions: function() {
            var self = this;
            var select = parseInt(this.$el.find('#qs5-resolution_attempts-foreclosure_sale1').val(), 10);
            _.each([1, 2, 3], function(i) {
                if (i == select) {
                    if (i == 1) {
                        disableElems(self.$el.find('#block-resolution_attempts-foreclosure_sale_by_bank'));
                        disableElems(self.$el.find('#block-resolution_attempts-foreclosure_sale_by_you'));
                    } else if (i == 2) {
                        enableElems(self.$el.find('#block-resolution_attempts-foreclosure_sale_by_bank'));
                        disableElems(self.$el.find('#block-resolution_attempts-foreclosure_sale_by_you'));
                    } else if (i == 3) {
                        disableElems(self.$el.find('#block-resolution_attempts-foreclosure_sale_by_bank'));
                        enableElems(self.$el.find('#block-resolution_attempts-foreclosure_sale_by_you'));
                    }
                }
            });
        },
        enableVoluntaryPropertyTransfer: function() {
            if (this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').is(':checked')) {
                enableElems(this.$el.find('#block-resolution_attempts-voluntary_property_transfer'));

                // Disable no action section
                this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', false);
                this.enableNoAction();
            } else {
                disableElems(this.$el.find('#block-resolution_attempts-voluntary_property_transfer'));

                if (!this.$el.find('#qs5-resolution_attempts-h-restructuring').is(':checked') &&
                    !this.$el.find('#qs5-resolution_attempts-h-discount').is(':checked') &&
                    !this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').is(':checked')) {

                    this.$el.find('#qs5-resolution_attempts-h-no_action').prop('checked', true);
                    this.enableNoAction();
                }
            }
        },
        saveToData: function() {
            this.qData.resolution_attempts.no_action = this.$el.find('#qs5-resolution_attempts-h-no_action').valueIfNotDisabled();
            this.qData.resolution_attempts.restructuring = this.$el.find('#qs5-resolution_attempts-h-restructuring').valueIfNotDisabled();
            this.qData.resolution_attempts.restructuring_happened = this.$el.find('#qs5-resolution_attempts-restructuring1').valueIfNotDisabled();;
            this.qData.resolution_attempts.restructuring_date = this.$el.find('#qs5-resolution_attempts-restructuring2').valueIfNotDisabled();;
            this.qData.resolution_attempts.restructuring_conditions = this.$el.find('#qs5-resolution_attempts-restructuring3').valueIfNotDisabled();;
            this.qData.resolution_attempts.restructuring_condition_details = this.$el.find('#qs5-resolution_attempts-restructuring4').valueIfNotDisabled();;
            this.qData.resolution_attempts.discount = this.$el.find('#qs5-resolution_attempts-h-discount').valueIfNotDisabled();
            this.qData.resolution_attempts.discount_conditions = this.$el.find('#qs5-resolution_attempts-discount1').valueIfNotDisabled();;
            this.qData.resolution_attempts.discount_amount_by_bank = this.$el.find('#qs5-resolution_attempts-discount2').valueIfNotDisabled();;
            this.qData.resolution_attempts.discount_date_by_bank = this.$el.find('#qs5-resolution_attempts-discount3').valueIfNotDisabled();;
            this.qData.resolution_attempts.discount_amount_by_you = this.$el.find('#qs5-resolution_attempts-discount4').valueIfNotDisabled();;
            this.qData.resolution_attempts.discount_date_by_you = this.$el.find('#qs5-resolution_attempts-discount5').valueIfNotDisabled();;
            this.qData.resolution_attempts.foreclosure_sale = this.$el.find('#qs5-resolution_attempts-h-foreclosure_sale').valueIfNotDisabled();
            this.qData.resolution_attempts.foreclosure_sale_conditions = this.$el.find('#qs5-resolution_attempts-foreclosure_sale1').valueIfNotDisabled();;
            this.qData.resolution_attempts.foreclosure_sale_date_by_bank = this.$el.find('#qs5-resolution_attempts-foreclosure_sale2').valueIfNotDisabled();;
            this.qData.resolution_attempts.foreclosure_sale_bank_dept_conditions = this.$el.find('#qs5-resolution_attempts-foreclosure_sale3').valueIfNotDisabled();;
            this.qData.resolution_attempts.foreclosure_sale_date_by_you = this.$el.find('#qs5-resolution_attempts-foreclosure_sale4').valueIfNotDisabled();;
            this.qData.resolution_attempts.foreclosure_sale_you_dept_conditions = this.$el.find('#qs5-resolution_attempts-foreclosure_sale5').valueIfNotDisabled();;
            this.qData.resolution_attempts.voluntary_property_transfer = this.$el.find('#qs5-resolution_attempts-h-voluntary_property_transfer').valueIfNotDisabled();

        },
        render: function() {
            var self = this;
            var html = QUI.renderTemplate('step8', {
                data: this.qData
            }, function(html) {
                self.$el.html(html);

                self.enableNoAction();
                self.enableRestructuring();
                self.enableDiscount();
                self.enableForeclosureSale();
                self.enableVoluntaryPropertyTransfer();

                self.$el.find('#qs5-resolution_attempts-restructuring2').datepicker({
                    dateFormat: "dd-mm-yy",
                    changeMonth: true,
                    changeYear: true
                });

                self.$el.find('#qs5-resolution_attempts-discount3').datepicker({
                    dateFormat: "dd-mm-yy",
                    changeMonth: true,
                    changeYear: true
                });

                self.$el.find('#qs5-resolution_attempts-discount5').datepicker({
                    dateFormat: "dd-mm-yy",
                    changeMonth: true,
                    changeYear: true
                });
            });
        }
    });

    return {
        BaseCmp: BaseCmp
    };
}());
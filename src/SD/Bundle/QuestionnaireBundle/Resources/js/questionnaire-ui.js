var QUI = (function() {
    var compiledTemplates = {};

    function renderTemplate(name, obj, cb) {
        obj = obj || {};
        cb = cb || function(html) {};

        if (compiledTemplates[name]) {
            cb(compiledTemplates[name](obj));
            return;
        }
        $.get('/questionnaires/template/' + name, {}, function(tmpl) {
            compiledTemplates[name] = _.template(tmpl);
            cb(compiledTemplates[name](obj));
        });
    }

    return {
        preloadTemplates: function(cb) {
            renderTemplate('base', {
                activeStep: 0,
                totalSteps: 0
            }, cb);
        },
        showFinishedState: function(baseElem) {
            var finishCmp = new QUI_COMPONENTS.FinishCmp();
            baseElem.append(finishCmp.$el);
        },
        init: function(baseElem) {
            var qData = JSON.parse(baseElem.attr('q-data')) || {};
            qData.showError = function() {
                var curr = qData;

                var args = Array.prototype.slice.call(arguments);

                args.unshift('validation');
                args.push('error');

                for (var i = 0; i < args.length; i++) {
                    if (curr[args[i]] == undefined) {
                        return null; // or ''
                    }
                    curr = curr[args[i]];
                }

                return curr;
            };

            var baseCmp = new QUI_COMPONENTS.BaseCmp({
                qData: qData,
                onFinish: function() {
                    window.location = qData.redirect_on_finish ? qData.redirect_on_finish : '/';
                }
            });
            baseElem.append(baseCmp.$el);
        },
        renderTemplate: renderTemplate
    }
}());

(function($) {
    $.fn.valueIfNotDisabled = function() {
        var elem = $(this);

        var disabled = elem.attr('disabled') == 'disabled' ? true : false;

        disabled = disabled || !elem.is(':visible');

        if (disabled) {
            if (elem.prop("tagName") == 'SELECT') {
                return 1;
            } else if (elem.prop("tagName") == 'INPUT') {
                if (elem.attr('type') == 'checkbox') {
                    return 0;
                } else if (elem.attr('type') == 'radio') {
                    return 1;
                } else {
                    return '';
                }
            } else if (elem.prop("tagName") == 'TEXTAREA') {
                return '';
            }
        }


        if (elem.prop('tagName') == 'INPUT' && elem.attr('type') == 'checkbox') {
            return elem.is(':checked') ? 1 : 0;
        }
        else {
            return elem.val();
        }
    };

})(jQuery);

$(function() {
    if ($('#questionnaire-container').length) {
        QUI.preloadTemplates(function() {
            $('#qui-loading').remove();
            $('#questionnaire-container').html('');
            QUI.init($('#questionnaire-container'));
        });
    }

});
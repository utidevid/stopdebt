<?php

namespace SD\Bundle\QuestionnaireBundle\Entity;

/**
 * Questionnaire
 */
class Questionnaire
{
    // JSON data pieces
    const USER_DATA_KEY = 'user';
    const GOAL_DATA_KEY = 'goal';
    const CREDIT_DATA_KEY = 'credit';
    const CREDIT_DETAILS_KEY = 'credit_details';
    const PENALTY_DATA_KEY = 'penalty';
    const CREDIT_EXTRAS_KEY = 'credit_extras';
    const FINANCIAL_DATA_KEY = 'financial_data';
    const RESOLUTION_ATTEMPTS_KEY = 'resolution_attempts';

    // Statuses
    const PENDING_STATUS = 1;
    const COMPLETED_STATUS =2;

    // Credit data
    // Status
    const CREDIT_DATA_STATUS_PROBLEMATIC = 1;
    const CREDIT_DATA_STATUS_EASY = 2;
    //Currency
    const CREDIT_DATA_CURRENCY_UAH = 1;
    const CREDIT_DATA_CURRENCY_USD = 2;
    const CREDIT_DATA_CURRENCY_EUR = 3;
    const CREDIT_DATA_CURRENCY_CHF = 4;
    // Identity-Entrepreneur status
    const CREDIT_DATA_ENTREPRENEUR_STATUS_YES = 1;
    const CREDIT_DATA_ENTREPRENEUR_STATUS_NO = 2;
    // Distributed Moratorium
    const CREDIT_DATA_DISTRIBUTED_MARATORIUM_YES = 1;
    const CREDIT_DATA_DISTRIBUTED_MARATORIUM_NO = 2;

    // Custom data
    const TARIFF_CUSTOM_KEY = 'tariff';

    // Data steps
    const DATA_STEPS = array(
        1 => self::USER_DATA_KEY,
        2 => self::GOAL_DATA_KEY,
        3 => self::CREDIT_DATA_KEY,
        4 => self::CREDIT_DETAILS_KEY,
        5 => self::CREDIT_EXTRAS_KEY,
        6 => self::PENALTY_DATA_KEY,
        7 => self::FINANCIAL_DATA_KEY,
        8 => self::RESOLUTION_ATTEMPTS_KEY,
    );

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $safeId;

    /**
     * @var array
     */
    private $goalData;

    /**
     * @var array
     */
    private $creditData;

    /**
     * @var array
     */
    private $creditDetails;

    /**
     * @var array
     */
    private $creditExtras;

    /**
     * @var array
     */
    private $penaltyData;

    /**
     * @var array
     */
    private $financialData;

    /**
     * @var array
     */
    private $resolutionAttempts;

    /**
     * @var int
     */
    private $status;

    /**
     * @var int
     */
    private $user;

    /**
     * @var array
     */
    private $customData;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

     /**
     * Get SafeId
     *
     * @return string
     */
    public function getSafeId()
    {
        return $this->safeId;
    }

    /**
     * Set SafeId
     *
     * @param string $safeId
     *
     * @return Questionnaire
     */
    public function setSafeId($safeId)
    {
        $this->safeId = $safeId;

        return $this;
    }

    protected function filterDataFunction($data, $whatToLeave)
    {
        return array_filter($data, function($key) use ($whatToLeave) {
            return in_array($key, $whatToLeave);
        }, ARRAY_FILTER_USE_KEY);
    }
    /**
     * Set goalData
     *
     * @param array $goalData
     *
     * @return Questionnaire
     */
    public function setGoalData($goalData)
    {
        $dataToLeave = ['main_goal', 'client_status'];

        switch ($goalData['main_goal']) {
            case 1:// save
                $dataToLeave = array_merge($dataToLeave, [ 'exchange_to_uah', 'monthly_pay' ]);
            break;
            case 2:// pay
                $dataToLeave[] = 'property_type';
                switch ($goalData['property_type']) {
                    case 1:
                        $dataToLeave = array_merge($dataToLeave, ['property_type_1_pay_with_discount', 'property_type_1_amount']);
                    break;
                    case 2:
                        $dataToLeave = array_merge($dataToLeave, ['property_type_2_relocation']);
                    break;
                    case 3:
                        $dataToLeave = array_merge($dataToLeave, ['property_type_3_pay_with_discount', 'property_type_3_amount']);
                    break;
                }
            break;
            case 4:// bankrupt
                $dataToLeave[] = 'bankruptcy_type';
                if ($goalData['bankruptcy_type'] == 1) {
                    $dataToLeave = array_merge($dataToLeave, ['bankruptcy_pay_with_discount', 'bankruptcy_amount']);
                }
            break;
        }

        $this->goalData = $this->filterDataFunction($goalData, $dataToLeave);

        return $this;
    }

    /**
     * Get goalData
     *
     * @return array
     */
    public function getGoalData()
    {
        return $this->goalData;
    }

    /**
     * Set creditData
     *
     * @param array $creditData
     *
     * @return Questionnaire
     */
    public function setCreditData($creditData)
    {
        $this->creditData = $creditData;

        return $this;
    }

    /**
     * Get creditData
     *
     * @return array
     */
    public function getCreditData()
    {
        return $this->creditData;
    }

    /**
     * Set creditDetails
     *
     * @param array $creditDetails
     *
     * @return Questionnaire
     */
    public function setCreditDetails($creditDetails)
    {
        $dataToLeave = [];

        if ($this->getCustomDataByKey(self::TARIFF_CUSTOM_KEY) != 'standard') {
            $dataToLeave = array_merge($dataToLeave,
                [
                    'bank_name',
                    'agreement_id',
                    'credit_issue_date',
                    'credit_close_date',
                    'issued_credit_amount',
                    'credit_rate',
                    'monthly_pay',
                ]
            );
        }

        $dataToLeave = array_merge($dataToLeave, [
            'credit_left_amount',
            'credit_body_amount',
            'credit_left_rate',
            'credit_fines',
            'is_delayed',
        ]);

        if ($creditDetails['is_delayed'] == 1) {
            $dataToLeave = array_merge($dataToLeave,
                [
                    'delay_days',
                    'delay_amount',
                    'paid_to_bank_amount',
                ]
            );
        }

        $this->creditDetails = $this->filterDataFunction($creditDetails, $dataToLeave);

        return $this;
    }

    /**
     * Get creditDetails
     *
     * @return array
     */
    public function getCreditDetails()
    {
        return $this->creditDetails;
    }

    /**
     * Set creditExtras
     *
     * @param array $creditExtras
     *
     * @return Questionnaire
     */
    public function setCreditExtras($creditExtras)
    {
        $dataToLeave = [];

        if (!empty($creditExtras['mortgage']) && !empty($creditExtras['mortgage_option'])) {
            $dataToLeave = array_merge($dataToLeave, ['mortgage', 'mortgage_city', 'mortgage_county']);

            //if ($creditExtras['mortgage_apartment']) {
            if ($creditExtras['mortgage_option'] == 1) {
                $dataToLeave = array_merge($dataToLeave, ['mortgage_option', 'mortgage_apartment_area',
                    'mortgage_apartment_state', 'mortgage_apartment_cost', 'mortgage_apartment_is_registered_perps']);

                if ($creditExtras['mortgage_apartment_is_registered_perps'] == 1) {
                    $dataToLeave[] = 'mortgage_apartment_perps';
                }
            }

            //if ($creditExtras['mortgage_house']) {
            if ($creditExtras['mortgage_option'] == 2) {
                $dataToLeave = array_merge($dataToLeave, ['mortgage_option', 'mortgage_house_area',
                    'mortgage_house_state', 'mortgage_house_cost', 'mortgage_house_is_registered_perps']);

                if ($creditExtras['mortgage_house_is_registered_perps'] == 1) {
                    $dataToLeave[] = 'mortgage_house_perps';
                }
            }

            //if ($creditExtras['mortgage_estate']) {
            if ($creditExtras['mortgage_option'] == 3) {
                $dataToLeave += ['mortgage_option', 'mortgage_estate_area', 'mortgage_estate_state',
                    'mortgage_estate_cost', 'mortgage_estate_operated'];

                if ($creditExtras['mortgage_estate_operated'] == 1) {
                    $dataToLeave[] = 'mortgage_estate_profit';
                }
            }

            //if ($creditExtras['mortgage_area']) {
            if ($creditExtras['mortgage_option'] == 4) {
                $dataToLeave = array_merge($dataToLeave, ['mortgage_option', 'mortgage_area_area',
                    'mortgage_area_purpose', 'mortgage_area_cost', 'mortgage_estate_operated']);
            }

            //if ($creditExtras['mortgage_other']) {
            if ($creditExtras['mortgage_option'] == 5) {
                $dataToLeave = array_merge($dataToLeave, ['mortgage_option', 'mortgage_other_cost']);
            }
        }

        if (!empty($creditExtras['surety'])) {
            $dataToLeave = array_merge($dataToLeave, ['surety', 'surety_person_fullname', 'surety_person_phone',
                'surety_person_email', 'surety_person_contact_details']);
        }

        if (!empty($creditExtras['pledge'])) {
            $dataToLeave[] = ['pledge'];

            if ($creditExtras['pledge_car']) {
                $dataToLeave = array_merge($dataToLeave, ['pledge_car', 'pledge_car_brand',
                    'pledge_car_manufacture_year', 'pledge_car_cost', 'pledge_car_registration_year']);
            }

            if ($creditExtras['pledge_movables']) {
                $dataToLeave = array_merge($dataToLeave, ['pledge_movables', 'pledge_movables_name',
                    'pledge_movables_manufacture_year', 'pledge_movables_cost', 'pledge_movables_registration_year']);
            }

            if ($creditExtras['pledge_securities']) {
                $dataToLeave = array_merge($dataToLeave, ['pledge_securities', 'pledge_securities_cost']);
            }

            if ($creditExtras['pledge_corporate_rights']) {
                $dataToLeave = array_merge($dataToLeave, ['pledge_corporate_rights', 'pledge_corporate_rights_operated',
                    'pledge_corporate_rights_owned']);
            }

            if ($creditExtras['pledge_other']) {
                $dataToLeave = array_merge($dataToLeave, ['pledge_other', 'pledge_other_cost']);
            }
        }

        $this->creditExtras = $this->filterDataFunction($creditExtras, $dataToLeave);

        return $this;
    }

    /**
     * Get creditExtras
     *
     * @return array
     */
    public function getCreditExtras()
    {
        return $this->creditExtras;
    }

    /**
     * Set penaltyData
     *
     * @param array $penaltyData
     *
     * @return Questionnaire
     */
    public function setPenaltyData($penaltyData)
    {
        $dataToLeave = ['stage', 'creditor_type', 'city', 'creditor_name'];

        switch ($penaltyData['stage']) {
            case 1: //before the trial
                $dataToLeave = array_merge($dataToLeave, ['stage1_calls', 'stage1_mails', 'satge1_visits', 'stage1_else']);
                if ($penaltyData['stage1_else']) {
                    $dataToLeave[] = 'stage1_else_details';
                }
            break;
            case 2: // during the trial
                $dataToLeave = array_merge($dataToLeave, ['stage2_participation', 'stage2_type1', 'stage2_type2',
                    'stage2_type3', 'stage2_type4', 'stage2_court_type', 'stage2_cassation_type']);

                if ($penaltyData['stage2_court_type'] == 1) {
                    $dataToLeave = array_merge($dataToLeave, ['stage2_court_fio', 'stage2_court_stage']);
                    if ($penaltyData['stage2_court_stage'] == 2) {
                        $dataToLeave = array_merge($dataToLeave, ['stage2_court_stage2_amount1',
                            'stage2_court_stage2_amount2', 'stage2_court_stage2_date']);

                    } elseif ($penaltyData['stage2_court_stage'] == 3) {
                        $dataToLeave[] = 'stage2_court_stage3_state';
                        if ($penaltyData['stage2_court_stage3_state'] == 2) {
                            $dataToLeave = array_merge($dataToLeave, ['stage2_court_stage3_date',
                                'stage2_court_stage3_amount', 'stage2_court_stage3_type']);
                        }
                    }
                } else {
                    $dataToLeave = array_merge($dataToLeave, ['stage2_appeal_type', 'stage2_appeal_stage']);

                    if ($penaltyData['stage2_appeal_type'] == 2) {
                        $dataToLeave = array_merge($dataToLeave, ['stage2_appeal_stage2_amount1',
                            'stage2_appeal_stage2_amount2', 'stage2_appeal_stage2_date']);
                    }
                }

                if ($penaltyData['stage2_cassation_type']) {
                    $dataToLeave[] = 'stage2_cassation_type2';
                }

                if ($penaltyData['stage2_type1']) {
                    $dataToLeave[] = 'stage2_type1_amount';
                }

                if ($penaltyData['stage2_participation'] == 2) {
                    $dataToLeave = array_merge($dataToLeave, ['stage2_fio', 'stage2_phone', 'stage2_email']);
                }
            break;
            case 3: // after the trial
                $dataToLeave = array_merge($dataToLeave, ['stage3_name', 'stage3_fio', 'stage3_date', 'stage3_amount', 'stage3_stage']);

                if ($penaltyData['stage3_stage'] == 6) {
                    $dataToLeave = array_merge($dataToLeave, ['stage3_writeoff', 'stage3_queue']);
                }
            break;
        }



        $this->penaltyData = $this->filterDataFunction($penaltyData, $dataToLeave);

        return $this;
    }

    /**
     * Get penaltyData
     *
     * @return array
     */
    public function getPenaltyData()
    {
        return $this->penaltyData;
    }

    /**
     * Set financialData
     *
     * @param array $financialData
     *
     * @return Questionnaire
     */
    public function setFinancialData($financialData)
    {
        $dataToLeave = ['regular_income', 'guarantor_avg_monthly_income', 'has_children', 'has_military',
            'has_family', 'has_sick_perps', 'credit_at_different_bank', 'personal_expenses', 'other_properties'];

        if ($financialData['regular_income'] != 4) {
            $dataToLeave[] = 'avg_monthly_income';
        }

        if ($financialData['has_children']) {
            $dataToLeave = array_merge($dataToLeave, ['children_monthly_amount', 'children_age']);
            if (!empty($financialData['children_additional_expenses'])) {
                $dataToLeave += ['children_monthly_amount', 'children_additional_expenses_studies',
                    'children_additional_expenses_treatment'];
                if ($financialData['children_additional_expenses_studies']) {
                    $dataToLeave[] = 'school_monthly_expenses';
                }
                if ($financialData['children_additional_expenses_treatment']) {
                    $dataToLeave[] = 'hospital_monthly_expenses';
                }
            }
        }

        if ($financialData['has_military'] && !empty($financialData['military_additional_expenses'])) {
            $dataToLeave[] = 'military_additional_expenses';
            if ($financialData['military_additional_expenses']) {
                $dataToLeave[] = 'military_monthly_amount';
            }
        }

        if ($financialData['has_family'] && !empty($financialData['family_additional_expenses'])) {
            $dataToLeave = array_merge($dataToLeave, ['family_age', 'family_additional_expenses']);
            if ($financialData['family_additional_expenses']) {
                $dataToLeave = array_merge($dataToLeave, ['family_additional_expenses_treatment', 'family_monthly_amount']);
            }
        }

        if ($financialData['has_sick_perps']) {
            $dataToLeave = array_merge($dataToLeave, ['sick_perps_age', 'sick_perps_additional_expenses']);
            if ($financialData['sick_perps_additional_expenses']) {
                $dataToLeave = array_merge($dataToLeave, ['sick_perps_additional_expenses_treatment', 'sick_perps_monthly_amount']);
            }
        }

        if ($financialData['credit_at_different_bank']) {
            $dataToLeave = array_merge($dataToLeave, ['credit_at_different_bank_monthly_pay', 'credit_at_different_bank_status']);
        }

        if ($financialData['personal_expenses']) {
            $dataToLeave = array_merge($dataToLeave, ['personal_expenses_treatment', 'personal_expenses_treatment_cost']);
        }

        if ($financialData['other_properties']) {
            $dataToLeave = array_merge($dataToLeave, ['other_properties_estate', 'other_properties_vehicle', 'other_properties_other']);
            if ($financialData['other_properties_estate'] != 0) {
                $dataToLeave[] = 'other_properties_estate_in_pledge';
            }
            if ($financialData['other_properties_vehicle']) {
                $dataToLeave[] = 'other_properties_vehicle_in_pledge';
            }
            if ($financialData['other_properties_other']) {
                $dataToLeave[] = 'other_properties_other_in_pledge';
            }
        }

        $this->financialData = $this->filterDataFunction($financialData, $dataToLeave);

        return $this;
    }

    /**
     * Get financialData
     *
     * @return array
     */
    public function getFinancialData()
    {
        return $this->financialData;
    }

    /**
     * Set resolutionAttempts
     *
     * @param array $resolutionAttempts
     *
     * @return Questionnaire
     */
    public function setResolutionAttempts($resolutionAttempts)
    {
        $dataToLeave = [''];

        $this->resolutionAttempts = $this->filterDataFunction($resolutionAttempts, $dataToLeave);

        return $this;
    }

    /**
     * Get resolutionAttempts
     *
     * @return array
     */
    public function getResolutionAttempts()
    {
        return $this->resolutionAttempts;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Questionnaire
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return Questionnaire
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set customData
     *
     * @param array $customData
     *
     * @return Questionnaire
     */
    public function setCustomData($customData)
    {
        $this->customData = $customData;

        return $this;
    }

    /**
     * Add unique custom data. The data will not be added, if the same key already exists
     *
     * @param $key
     * @param $value
     */
    public function addUniqueCustomData($key, $value)
    {
        if (!array_key_exists(self::TARIFF_CUSTOM_KEY, $this->customData))
            $this->customData[$key] = $value;
    }

    /**
     * Get custom data by key
     *
     * @param $key
     * @return mixed
     */
    public function getCustomDataByKey($key)
    {
        return isset($this->customData[$key]) ? $this->customData[$key] : null;
    }

    /**
     * Get customData
     *
     * @return array
     */
    public function getCustomData()
    {
        return $this->customData;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Questionnaire
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Questionnaire
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    /**
     * @var \SD\Bundle\CaseBundle\Entity\DebtCase
     */
    private $case;


    /**
     * Set case
     *
     * @param \SD\Bundle\CaseBundle\Entity\DebtCase $case
     *
     * @return Questionnaire
     */
    public function setCase(\SD\Bundle\CaseBundle\Entity\DebtCase $case = null)
    {
        $this->case = $case;

        return $this;
    }

    /**
     * Get case
     *
     * @return \SD\Bundle\CaseBundle\Entity\DebtCase
     */
    public function getCase()
    {
        return $this->case;
    }
    /**
     * @var integer
     */
    private $last_step;


    /**
     * Set lastStep
     *
     * @param integer $lastStep
     *
     * @return Questionnaire
     */
    public function setLastStep($lastStep)
    {
        $this->last_step = $lastStep;

        return $this;
    }

    /**
     * Get lastStep
     *
     * @return integer
     */
    public function getLastStep()
    {
        return $this->last_step;
    }
}

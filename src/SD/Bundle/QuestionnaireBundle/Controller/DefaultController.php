<?php

namespace SD\Bundle\QuestionnaireBundle\Controller;

use SD\Bundle\QuestionnaireBundle\Entity\Questionnaire;
use SD\Bundle\QuestionnaireBundle\Model\QuestionnaireModel;
use SD\Bundle\UtilsBundle\Controller\AbstractUserController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use SD\Bundle\DocumentsBundle\Entity\Document;
use SD\Bundle\CaseBundle\Entity\DebtCase;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Validator\Constraints\NotNullValidator;

class DefaultController extends AbstractUserController
{
    // Key to fetch data from POST request
    const POST_DATA_KEY = 'data';

    const SECTION_NAME = 'questionnaire';

    public function indexAction($caseId) {
        $this->loadCaseBySafeId($caseId);
        $questionnaire = $this->activeCase->getQuestionnaire();
        $questionnaireId = $questionnaire->getSafeId();

        if (!$this->isGranted('CAN_VIEW_QUESTIONNAIRE', $this->activeCase) && !$this->isGranted('CAN_EDIT_QUESTIONNAIRE', $this->activeCase)) {
            return $this->redirectToRoute('dashboard_homepage', [
                'caseId' => $caseId
            ]);
        }

        $usr = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository('QuestionnaireBundle:Questionnaire');

        if ($this->isGranted('CAN_VIEW_QUESTIONNAIRE', $this->activeCase)) {
            $model = $this->get('sd.questionnaire_model');
            $result = $model->calculateWeights($questionnaire);
            $qarray = [];
            foreach ($model::getRelevantSections() as $key => $method) {
                $qarray[$key] = $questionnaire->$method();
            }

            return $this->render('QuestionnaireBundle:Default:edit.html.twig', [
                'questionnaire' => $questionnaire,
                'questionnaireData' => $qarray,
                'results' => $result,
                'schema' => QuestionnaireModel::getSchemaDetails(),
                'view_mode' => true
            ]);
        } elseif ($this->isGranted('CAN_EDIT_QUESTIONNAIRE', $this->activeCase)) {

            $baseData = QuestionnaireModel::getSchema();
            $baseData['questionnaireId'] = $questionnaireId;
            $baseData['caseTariff'] = $this->activeCase->getTariff();
            //$baseData['caseTariff'] = 'optimal';
            $baseData['last_step'] = $questionnaire->getLastStep();
            $baseData['send_url'] = $this->generateUrl('questionnaire_send', [
                'caseId' => $caseId
            ]);
            $baseData['redirect_on_finish'] = $this->generateUrl('questionnaire_homepage', [
                'caseId' => $caseId
            ]);
            $baseData['user'] = array_merge($baseData['user'], [
                'first_name' => $usr->getFirstName(),
                'last_name' => $usr->getLastName(),
                'patronymic' => $usr->getPatronymic(),
                'phone_number' => $usr->getPhoneNumber(),
                'locality' => $usr->getLocality(),
                'passport' => $usr->getPassport(),
                'birthdate' => $usr->getBirthdate() ? $usr->getBirthdate()->format('d-m-Y') : '',
                'identification_code' => $usr->getIdentificationCode(),
            ]);

            $baseData[Questionnaire::GOAL_DATA_KEY] = array_merge($baseData[Questionnaire::GOAL_DATA_KEY], $questionnaire->getGoalData());
            $baseData[Questionnaire::PENALTY_DATA_KEY] = array_merge($baseData[Questionnaire::PENALTY_DATA_KEY], $questionnaire->getPenaltyData());
            $baseData[Questionnaire::CREDIT_EXTRAS_KEY] = array_merge($baseData[Questionnaire::CREDIT_EXTRAS_KEY], $questionnaire->getCreditExtras());
            $baseData[Questionnaire::CREDIT_DETAILS_KEY] = array_merge($baseData[Questionnaire::CREDIT_DETAILS_KEY], $questionnaire->getCreditDetails());
            $baseData[Questionnaire::CREDIT_DATA_KEY] = array_merge($baseData[Questionnaire::CREDIT_DATA_KEY], $questionnaire->getCreditData());
            $baseData[Questionnaire::RESOLUTION_ATTEMPTS_KEY] = array_merge($baseData[Questionnaire::RESOLUTION_ATTEMPTS_KEY], $questionnaire->getResolutionAttempts());
            $baseData[Questionnaire::FINANCIAL_DATA_KEY] = array_merge($baseData[Questionnaire::FINANCIAL_DATA_KEY], $questionnaire->getFinancialData());

            return $this->render('QuestionnaireBundle:Default:edit.html.twig', [
                'questionnaire' => $questionnaire,
                'data' => json_encode($baseData),
                'view_mode' => false,
            ]);
        } else {
            throw new ServiceUnavailableHttpException(0, "Поддержка этого типа кейса / тарифа не реализована.");
        }
    }

    public function approveAction($caseId)
    {
        $this->loadCaseBySafeId($caseId, true);

        if ($this->activeCase->getTariff() == 'advanced' && $this->activeCase->getStage()  == DebtCase:: ADVANCED_TARIFF_STAGE_4_REVIEW_QUESTIONNAIRE) {
            $this->activeCase->setStage(DebtCase:: ADVANCED_TARIFF_STAGE_5_INTERVIEW);
        }

        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('dashboard_homepage', [
            'caseId' => $caseId
        ]);
    }

    public function sendAction($caseId, Request $request)
    {
        $this->loadCaseBySafeId($caseId, true);

        $usr = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository('QuestionnaireBundle:Questionnaire');

        /** @var \SD\Bundle\QuestionnaireBundle\Entity\Questionnaire $questionnaire */
        $questionnaire = $this->activeCase->getQuestionnaire();
        $questionnaireId  = $questionnaire->getSafeId();

        if ($questionnaire->getStatus() == Questionnaire::COMPLETED_STATUS) {
            return new JsonResponse(['status' => 'error', 'message'=>'already completed'], 400, ['Content-Type' => 'application/json']);
        }

        $jsonData = json_decode($request->get(self::POST_DATA_KEY), true);

        if ($jsonData) {
            if ((int)$request->get('status') == Questionnaire::COMPLETED_STATUS) {
                $questionnaire->setStatus(Questionnaire::COMPLETED_STATUS);

                if ($this->activeCase->getTariff() == 'standard') {
                    $this->activeCase->setStage(DebtCase::STANDARD_TARIFF_STAGE_2_VIEW_STRATEGY);
                } elseif ($this->activeCase->getTariff() == 'optimal') {
                    $this->activeCase->setStage(DebtCase::OPTIMAL_TARIFF_STAGE_3_SKYPE_CALL);
                }
            } else {
                $questionnaire->setStatus(Questionnaire::PENDING_STATUS);

                if ($this->activeCase->getTariff() == 'standard') {
                    $this->activeCase->setStage(DebtCase::STANDARD_TARIFF_STAGE_1_EDIT_QUESTIONNAIRE);
                } elseif ($this->activeCase->getTariff() == 'optimal') {
                    $this->activeCase->setStage(DebtCase::OPTIMAL_TARIFF_STAGE_2_EDIT_QUESTIONNAIRE);
                }
            }

            $questionnaire->setLastStep((int)$jsonData['last_step']);

            $questionnaire->setUser($usr);

            $questionnaire->addUniqueCustomData(Questionnaire::TARIFF_CUSTOM_KEY, $this->activeCase->getTariff());

            /*$user = $this->container->get('fos_user.user_manager')->findUserByEmail('andy.zaporozhets@gmail.com');
            $questionnaire->setUser($user);*/

            $userInfo = array();

            if (array_key_exists(Questionnaire::USER_DATA_KEY, $jsonData))  {
                $userInfo = $jsonData[Questionnaire::USER_DATA_KEY];

                $usr->setFirstName($userInfo['first_name']);
                $usr->setLastName($userInfo['last_name']);
                $usr->setPatronymic($userInfo['patronymic']);
                $usr->setPhoneNumber($userInfo['phone_number']);
                $usr->setLocality($userInfo['locality']);
                $usr->setPassport($userInfo['passport']);
                $usr->setIdentificationCode($userInfo['identification_code']);
                $usr->setBirthdate(isset($userInfo['birthdate']) && !empty($userInfo['birthdate']) ? \DateTime::createFromFormat('d-m-Y', $userInfo['birthdate']) : null);

                $this->get('fos_user.user_manager')->updateUser($usr);
            }

            // Fill up questionnaire obj
            if (array_key_exists(Questionnaire::GOAL_DATA_KEY, $jsonData))
                $questionnaire->setGoalData($jsonData[Questionnaire::GOAL_DATA_KEY]);

            if (array_key_exists(Questionnaire::CREDIT_DATA_KEY, $jsonData))
                $questionnaire->setCreditData($jsonData[Questionnaire::CREDIT_DATA_KEY]);

            if (array_key_exists(Questionnaire::CREDIT_DETAILS_KEY, $jsonData))
                $questionnaire->setCreditDetails($jsonData[Questionnaire::CREDIT_DETAILS_KEY]);

            if (array_key_exists(Questionnaire::PENALTY_DATA_KEY, $jsonData))
                $questionnaire->setPenaltyData($jsonData[Questionnaire::PENALTY_DATA_KEY]);

            if (array_key_exists(Questionnaire::CREDIT_EXTRAS_KEY, $jsonData))
                $questionnaire->setCreditExtras($jsonData[Questionnaire::CREDIT_EXTRAS_KEY]);

            if (array_key_exists(Questionnaire::FINANCIAL_DATA_KEY, $jsonData))
                $questionnaire->setFinancialData($jsonData[Questionnaire::FINANCIAL_DATA_KEY]);

            if (array_key_exists(Questionnaire::RESOLUTION_ATTEMPTS_KEY, $jsonData))
                $questionnaire->setResolutionAttempts($jsonData[Questionnaire::RESOLUTION_ATTEMPTS_KEY]);

            $dataToValidate = [
                Questionnaire::USER_DATA_KEY => $userInfo,
                Questionnaire::GOAL_DATA_KEY => $questionnaire->getGoalData(),
                Questionnaire::CREDIT_DATA_KEY => $questionnaire->getCreditData(),
                Questionnaire::CREDIT_DETAILS_KEY => $questionnaire->getCreditDetails(),
                Questionnaire::CREDIT_EXTRAS_KEY => $questionnaire->getCreditExtras(),
                Questionnaire::PENALTY_DATA_KEY => $questionnaire->getPenaltyData(),
                Questionnaire::FINANCIAL_DATA_KEY => $questionnaire->getFinancialData(),
                Questionnaire::RESOLUTION_ATTEMPTS_KEY => $questionnaire->getResolutionAttempts(),
            ];

            // Select what data to validate
            $dataToValidate = array(
                Questionnaire::DATA_STEPS[$questionnaire->getLastStep()] => $dataToValidate[Questionnaire::DATA_STEPS[$questionnaire->getLastStep()]]
            );

            // Link client status field to the first step
            if ($questionnaire->getLastStep() == 1)
                $dataToValidate[Questionnaire::GOAL_DATA_KEY]['client_status'] = $questionnaire->getGoalData()['client_status'];

            // Validate data
            $validationResults = QuestionnaireModel::validateData(
                QuestionnaireModel::getSchemaDetails(),
                $this->get('validator'),
                $dataToValidate
            );

            // Return validation errors if there are any
            if (!empty($validationResults))
                return new JsonResponse(
                    [
                        'status' => 'failed',
                        'validation_data' => $validationResults
                    ],
                    200, // must return 400 but when it is set to 400, data are not retrieved, check $.post JS method
                    ['Content-Type' => 'application/json']
                );


            if (!$em->contains($questionnaire)) {
                $em->persist($questionnaire);
            }
            $em->flush();


            if ($questionnaire->getStatus() == Questionnaire::COMPLETED_STATUS) {
                $this->get('assistant.notifier')->notifyQuestionnaireFinished($questionnaire);
            }

            return new JsonResponse(['status' => 'ok'], 201, ['Content-Type' => 'application/json']);
        }

        return new JsonResponse(['status' => 'error'], 400, ['Content-Type' => 'application/json']);
    }

}

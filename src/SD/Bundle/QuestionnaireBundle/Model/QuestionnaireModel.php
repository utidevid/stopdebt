<?php

namespace SD\Bundle\QuestionnaireBundle\Model;

use SD\Bundle\DocumentsBundle\Service\VATIdValidatorService;
use SD\Bundle\QuestionnaireBundle\Entity\Questionnaire;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNullValidator;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Context\ExecutionContext;

class QuestionnaireModel {

    public static function getSchema() {
        return [
            'user' => [
                'first_name' => '',
                'last_name' => '',
                'patronymic' => '',
                'phone_number' => '',
                'locality' => '',
                'passport' => '',
                'identification_code' => '',
                'birthdate' => '',
            ],
            Questionnaire::GOAL_DATA_KEY => [
                'client_status' => 1,
                'main_goal' => 1,
                'exchange_to_uah' => 1,
                'monthly_pay' => '',
                'property_type' => 1,
                'property_type_1_pay_with_discount' => 1,
                'property_type_1_amount' => '',
                'property_type_2_relocation' => 1,
                'property_type_3_pay_with_discount' => 1,
                'property_type_3_amount' => '',
                'bankruptcy_type' => 1,
                'bankruptcy_pay_with_discount' => 1,
                'bankruptcy_amount' => '',
            ],
            Questionnaire::CREDIT_DATA_KEY => [
                'status' => 1,
                'currency' => 1,
                'is_entrepreneur' => 1,
                'distributed_moratorium' => 1,
            ],
            Questionnaire::CREDIT_DETAILS_KEY => [
                'bank_name' => '',
                'agreement_id' => '',
                'credit_issue_date' => '',
                'credit_close_date' => '',
                'issued_credit_amount' => '',
                'credit_rate' => '',
                'monthly_pay' => '',
                'credit_left_amount' => '',
                'credit_body_amount' => '',
                'credit_left_rate' => '',
                'credit_fines' => '',
                'is_delayed' => 1,
                'delay_days' => '',
                'delay_amount' => '',
                'paid_to_bank_amount' => '',
            ],
            Questionnaire::CREDIT_EXTRAS_KEY => [
                'mortgage_option' => 1,
                'mortgage' => 0,
                'mortgage_city' => '',
                'mortgage_county' => '',
                'mortgage_apartment' => 0,
                'mortgage_apartment_area' => '',
                'mortgage_apartment_state' => 1,
                'mortgage_apartment_cost' => '',
                'mortgage_apartment_is_registered_perps' => 1,
                'mortgage_apartment_perps' => '',
                'mortgage_house' => 0,
                'mortgage_house_area' => '',
                'mortgage_house_state' => 1,
                'mortgage_house_cost' => '',
                'mortgage_house_is_registered_perps' => 1,
                'mortgage_house_perps' => '',
                'mortgage_estate' => 0,
                'mortgage_estate_area' => '',
                'mortgage_estate_state' => 1,
                'mortgage_estate_cost' => '',
                'mortgage_estate_operated' => 1,
                'mortgage_estate_profit' => '',
                'mortgage_area' => 0,
                'mortgage_area_area' => '',
                'mortgage_area_purpose' => 1,
                'mortgage_area_cost' => '',
                'mortgage_other' => 0,
                'mortgage_other_cost' => '',
                'surety' => 0,
                'surety_person_fullname' => '',
                'surety_person_phone' => '',
                'surety_person_email' => '',
                'surety_person_contact_details' => '',
                'pledge' => 0,
                'pledge_car' => 0,
                'pledge_car_brand' => '',
                'pledge_car_manufacture_year' => '',
                'pledge_car_cost' => '',
                'pledge_car_registration_year' => '',
                'pledge_movables' => 0,
                'pledge_movables_name' => '',
                'pledge_movables_manufacture_year' => '',
                'pledge_movables_cost' => '',
                'pledge_movables_registration_year' => '',
                'pledge_securities' => 0,
                'pledge_securities_cost' => '',
                'pledge_corporate_rights' => 0,
                'pledge_corporate_rights_operated' => 1,
                'pledge_corporate_rights_owned' => 1,
                'pledge_other' => 0,
                'pledge_other_cost' => '',
                'nothing' => 1,
            ],
            Questionnaire::PENALTY_DATA_KEY => [
                'city' => '',
                'creditor_type' => 1,
                'creditor_name' => '',
                'stage' => 1,
                'stage1_calls' => 0,
                'stage1_mails' => 0,
                'satge1_visits' => 0,
                'stage1_else' => 0,
                'stage1_else_details' => '',
                'stage2_participation' => 1,
                'stage2_fio' => '',
                'stage2_phone' => '',
                'stage2_email' => '',
                'stage2_else' => '',
                'stage2_type1' => 0,
                'stage2_type1_amount' => '',
                'stage2_type2' => 0,
                'stage2_type3' => 0,
                'stage2_type4' => 0,
                'stage2_court_type' => 1,
                'stage2_court_fio' => '',
                'stage2_court_stage' => 1,
                'stage2_court_stage2_amount1' => '',
                'stage2_court_stage2_amount2' => '',
                'stage2_court_stage2_date' => '',
                'stage2_court_stage3_state' => 1,
                'stage2_court_stage3_date' => '',
                'stage2_court_stage3_amount' => '',
                'stage2_court_stage3_type' => 1,
                'stage2_appeal_type' => 1,
                'stage2_appeal_stage' => 1,
                'stage2_appeal_stage2_amount1' => '',
                'stage2_appeal_stage2_amount2' => '',
                'stage2_appeal_stage2_date' => '',
                'stage2_cassation_type' => 1,
                'stage2_cassation_type2' => 1,
                'stage3_name' => '',
                'stage3_fio' => '',
                'stage3_date' => '',
                'stage3_amount' => '',
                'stage3_stage' => 1,
                'stage3_writeoff' => '',
                'stage3_queue' => 1
            ],
            Questionnaire::FINANCIAL_DATA_KEY => [
                'regular_income' => 1,
                'avg_monthly_income' => '',
                'guarantor_avg_monthly_income' => '',
                'has_children' => 0,
                'children_age' => '',
                'children_additional_expenses' => 0,
                'children_additional_expenses_studies' => 0,
                'school_monthly_expenses' => '',
                'children_additional_expenses_treatment' => 0,
                'hospital_monthly_expenses' => '',
                'has_military' => 0,
                'military_additional_expenses' => 0,
                'military_monthly_amount' => '',
                'has_family' => 0,
                'family_age' => '',
                'family_additional_expenses' => 0,
                'family_additional_expenses_treatment' => 0,
                'family_monthly_amount' => '',
                'has_sick_perps' => 0,
                'sick_perps_age' => '',
                'sick_perps_additional_expenses' => 0,
                'sick_perps_additional_expenses_treatment' => 0,
                'sick_perps_monthly_amount' => '',
                'credit_at_different_bank' => 0,
                'credit_at_different_bank_monthly_pay' => '',
                'credit_at_different_bank_status' => 1,
                'personal_expenses' => 0,
                'personal_expenses_treatment' => 0,
                'personal_expenses_treatment_cost' => '',
                'other_properties' => 0,
                'other_properties_estate' => 0,
                'other_properties_estate_in_pledge' => 0,
                'other_properties_vehicle' => 0,
                'other_properties_vehicle_in_pledge' => 0,
                'other_properties_other' => 0,
                'other_properties_other_in_pledge' => 0,
            ],
            Questionnaire::RESOLUTION_ATTEMPTS_KEY => [
                'no_action' => 1,
                'restructuring' => 0,
                'restructuring_happened' => 1,
                'restructuring_date' => '',
                'restructuring_conditions' => 1,
                'restructuring_condition_details' => '',
                'discount' => 0,
                'discount_conditions' => 1,
                'discount_amount_by_bank' => '',
                'discount_date_by_bank' => '',
                'discount_amount_by_you' => '',
                'discount_date_by_you' => '',
                'foreclosure_sale' => 0,
                'foreclosure_sale_conditions' => 1,
                'foreclosure_sale_date_by_bank' => '',
                'foreclosure_sale_bank_dept_conditions' => 1,
                'foreclosure_sale_date_by_you' => '',
                'foreclosure_sale_you_dept_conditions' => 1,
                'voluntary_property_transfer' => 0,
            ],
        ];
    }

    /**
     * Validate data
     *
     * @param Questionnaire $questionnaire - questionnaire data
     * @param array $userData - user data
     * @param array $schema - e.g. QuestionnaireModel::getSchemaDetails()
     * @param $validatorService - e.g. $this->get('validator');
     *
     * @return array
     */
    public static function validateData(array $schema, $validatorService, array $data = array())
    {
        $validationData = array();

        foreach ($data as $step => $stepData) {

            foreach ($stepData as $field => $fieldData) {

                // Check if validation item exists
                if (isset($schema[$step][$field]['validation']) && !empty($schema[$step][$field]['validation'])) {

                    // Validation rules for an elected field
                    $validationRules = $schema[$step][$field]['validation'];

                    $errorList = $validatorService->validateValue($fieldData, $validationRules);

                    // ToDo: return multiple error messages instead of one
                    if (count($errorList) > 0)
                        $validationData[$step][$field] = [
                            'error' => $errorList[0]->getMessage()
                        ];
                }

                // Check if validation callback items exist and no errors were previously collected
                if (isset($schema[$step][$field]['validation_callbacks']) && !empty($schema[$step][$field]['validation_callbacks']) &&
                    empty($validationData[$step][$field])) {

                    // Validation callbacks
                    $validationRules = $schema[$step][$field]['validation_callbacks'];

                    foreach ($validationRules as $callback) {

                        $params = array();

                        // Collect params from the callback
                        foreach ($callback['data'] as $param) {
                            $params[] = $data[$param[0]][$param[1]];
                        }

                        $validationCallback = call_user_func_array($callback['func'], $params);

                        if (!$validationCallback)
                            $validationData[$step][$field] = [
                                'error' => $callback['error_msg']
                            ];
                    }
                }
            }
        }

        return $validationData;
    }

    public static function isIdentificationCodeValid($identificationCode, $birthdate)
    {
        $validator = new VATIdValidatorService();

        return $validator->validateVatIdNotSex($identificationCode, $birthdate);
    }

    public static function getSchemaDetails()
    {
        return [
            'user' => [
                'first_name' => [
                    'name' => 'Имя',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(
                            ['min' => 2, 'max' => 30]
                        )
                    ]
                ],
                'last_name' => [
                    'name' => 'Фамилия',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(
                            ['min' => 2, 'max' => 30]
                        )
                    ]
                ],
                'patronymic' => [
                    'name' => 'Отчество',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(
                            ['min' => 3, 'max' => 30]
                        )
                    ]
                ],
                'phone_number' => [
                    'name' => 'Телефон',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(
                            ['min' => 10, 'max' => 19]
                        )
                    ]
                ],
                'locality' => [
                    'name' => 'Населенный пункт',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(
                            ['max' => 40]
                        )
                    ]
                ],
                'passport' => [
                    'name' => 'Паспорт',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(
                            ['min' => 8, 'max' => 8]
                        )
                    ]
                ],
                'identification_code' => [
                    'name' => 'Идентификационный код',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(
                            ['min' => 10, 'max' => 10]
                        )
                    ],
                    'validation_callbacks' => [
                        [
                            'func' => 'SD\Bundle\QuestionnaireBundle\Model\QuestionnaireModel::isIdentificationCodeValid',
                            'data' => [
                                ['user', 'identification_code'],
                                ['user', 'birthdate']
                            ],
                            'error_msg' => 'Неверный ИД код.'
                        ]
                    ]
                ],
                'birthdate' => [
                    'name' => 'Дата рождения',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(
                            ['max' => 40]
                        )
                    ]
                ]
            ],
            Questionnaire::GOAL_DATA_KEY => [
                'client_status' => [
                    'name' => 'Статус клиента',
                    'values' => [1 => 'Заемщик', 2 => 'Поручитель', 3 => 'Залогодатель', 4 => 'Ипотекодатель'],
                    'weights' => [1 => 0,2 =>0 , 3 =>0, 4 => 0],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,4),
                        )),
                    ]
                ],
                'main_goal' => [
                    'name' => 'Цель',
                    'values' => [
                        1 => 'Реструктуризация кредита',
                        2 => 'Рассчитаться по кредиту',
                        3 => 'Освободиться от поручительств',
                        4 => 'Банкротство'
                    ],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,4),
                        )),
                    ]
                ],
                'exchange_to_uah'=> [
                    'name' => 'Хотите перевести в грн?',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'monthly_pay' => [
                    'name' => 'Какую сумму сможете выплачивать ежемесячно?',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        )),
                    ]
                ],
                'property_type' => ['name' => 'Рассчитаться по кредиту',
                    'values' => [
                        1 => 'Хочу сохранить это залоговое имущество',
                        2 => 'Готов продать или расстаться с залоговым имуществом',
                        3 => 'Мой кредит беззалоговый'
                    ],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,3),
                        )),
                    ]
                ],
                'property_type_1_pay_with_discount' => [
                    'name' => 'Готовы погасить кредит с дисконтом(имущество в залоге)',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'weights' => [1 => [0,0,0,0], 2 => [2,1,0,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'property_type_1_amount'=> [
                    'name' => 'Какую сумму готовы заплатить?(с дискаунтом)',
                    'values' => [],
                    'weights' => 'discountCreditWithDepositDesiredSumFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        ))
                    ]
                ],
                'property_type_2_relocation' => [
                    'name' => 'Готовы на переезд в другой город?',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'weights' => [1 => [0,1,3,0], 2 => [0,2,2,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'property_type_3_pay_with_discount'=> [
                    'name' => 'Готовы ли погасить с дискаунтом (беззалоговый)',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'weights' => [1 => [0,0,0,0], 2 => [3,3,0,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'property_type_3_amount'=> [
                    'name' => 'Какую сумму готовы заплатить?(с дискаунтом)',
                    'values' => [],
                    'weights' => 'discountCreditNoDepositDesiredSumFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        ))
                    ]
                ],
                'bankruptcy_type'=> [
                    'name' => 'Банкротство',
                    'values' => [1 => 'Хочу сохранить залоговое имущество', 2 => 'Готов(а) расстаться с залоговым имуществом'],
                    'weights' => [1 => [0,0,0,0], 2 => [0,2,3,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'bankruptcy_pay_with_discount'=> [
                    'name' => 'Готовы ли погасить с дискаунтом (банкротство)',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'weights' => [1 => [0,0,0,0], 2 => [3,3,0,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'bankruptcy_amount'=> [
                    'name' => 'Какую сумму готовы заплатить?',
                    'values' => [],
                    'weights' => 'bankruptcyDiscountAmountFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        ))
                    ]
                ],
            ],
            Questionnaire::CREDIT_DATA_KEY => [
                'status'=> [
                    'name' => 'Статус по кредиту',
                    'values' => [1 => 'Проблемный', 2 => 'Не проблемный'],
                    'weights' => [1 => [3, 3, 3, 0], 2 => [1,2,1,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'currency'=> [
                    'name' => 'Валюта кредита',
                    'values' => [1 => 'Гривна', 2 => 'Доллар', 3 => 'Евро',4 => 'Швейцарский Франк'],
                    'weights' => [1 => [1, 1, 1, 0], 2 => [2, 3, 2, 0], 3 => [2, 3, 2, 0], 4 => [2, 3, 2, 0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,4),
                        )),
                    ]
                ],
                'is_entrepreneur'=> [
                    'name' => 'Статус Физлица-предпринимателя',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'weights' => [1 => [2, 2, 2, 3], 2 => [2, 2, 2, 0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'distributed_moratorium'=> [
                    'name' => 'Распространяется мораторий?',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'weights' => [1 => [3, 2, 2, 0], 2 => [2, 3, 2, 0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
            ],
            Questionnaire::CREDIT_DETAILS_KEY => [
                'bank_name'=> ['name' => 'Название банка', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(
                            ['max' => 60]
                        )
                    ]
                ],
                'agreement_id'=> ['name' => 'Номер кредитного договора', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(
                            ['max' => 40]
                        )
                    ]
                ],
                'credit_issue_date'=> ['name' => 'Дата выдачи кредита', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                    ]
                ],
                'credit_close_date'=> ['name' => 'Дата закрытия кредита', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                    ]
                ],
                'issued_credit_amount'=> ['name' => 'Сумма выданного кредита', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        ))
                    ]
                ],
                'credit_rate'=> ['name' => 'Процентная ставка', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 100
                        ))
                    ]
                ],
                'monthly_pay'=> ['name' => 'Ежемесячный платеж', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        ))
                    ]
                ],
                'credit_left_amount'=> ['name' => 'Сумма остатка по кредиту', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        ))
                    ]
                ],
                'credit_body_amount'=> ['name' => 'По телу кредита', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        ))
                    ]
                ],
                'credit_left_rate'=> ['name' => 'Проценты', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 100,
                        ))
                    ]
                ],
                'credit_fines'=> ['name' => 'Пени и штрафы', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        ))
                    ]
                ],
                'is_delayed'=> [
                    'name' => 'Просрочка',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'weights' => [1 => [0, 0, 0, 0], 2 => [1, 2, 1, 0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'delay_days'=> [
                    'name' => 'Просрочка, Дней',
                    'values' => [],
                    'weights' => 'delayDaysFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 20000,
                        ))
                    ]
                ],
                'delay_amount'=> [
                    'name' => 'Просрочка, Сумма',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        ))
                    ]
                ],
                'paid_to_bank_amount'=> [
                    'name' => 'Выплаченная сумма банку, включая проценты, на момент заполнения анкеты',
                    'values' => [],
                    'weights' => 'paidToBankFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        ))
                    ]
                ],
            ],
            Questionnaire::CREDIT_EXTRAS_KEY => [
                'mortgage_option'=> [
                    'name' => 'Выбраная Ипотека',
                    'values' => [
                        1 => 'Квартира',
                        2 => 'Дом',
                        3 => 'Коммерческая недвижимость',
                        4 => 'Земля',
                        5 => 'Другое',
                    ],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,5),
                        )),
                    ]
                ],
                'mortgage'=> ['name' => 'Ипотека', 'values' => [0 => 'Нет', 1 => 'Да'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'mortgage_city'=> ['name' => 'Город', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('min' => 1, 'max' => 60)),
                    ]
                ],
                'mortgage_county'=> ['name' => 'Район', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('min' => 1, 'max' => 70)),
                    ]
                ],
                'mortgage_apartment'=> ['name' => 'Квартира', 'values' => [0 => 'Нет', 1 => 'Да'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'mortgage_apartment_area'=> ['name' => 'Площадь(кв.м)', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000,
                        ))
                    ]
                ],
                'mortgage_apartment_state'=> [
                    'name' => 'Тех. состояние',
                    'values' => [1 => 'Удовлетворительное', 2 => 'Не удовлетворительное'],
                    'weights' => [1 => [0,1,3,0], 2 => [2,3,1,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'mortgage_apartment_cost'=>[
                    'name' => 'Рыночная стоимость (в валюте кредита)',
                    'values' => [],
                    'weights' => 'mortgageApartmentCostFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'mortgage_apartment_is_registered_perps'=> [
                    'name' => 'Зарегистрированы несовершеннолетние, нетрудоспособные, пенсионеры, ветераны АТО',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'weights' => [1 => [2, 2, 1, 0], 2 => [2, 2, 2, 0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'mortgage_apartment_perps'=> [
                    'name' => 'Кто из перечисленных зарегестрирован?',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 200)),
                    ]
                ],
                'mortgage_house'=> [
                    'name' => 'Дом',
                    'values' => [0 => 'Нет', 1 => 'Да'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'mortgage_house_area'=> ['name' => 'Площадь(кв.м)', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000,
                        ))
                    ]
                ],
                'mortgage_house_state'=> [
                    'name' => 'Тех. состояние',
                    'values' => [1 => 'Удовлетворительное', 2 => 'Не удовлетворительное'],
                    'weights' => [1 => [0, 1, 3, 0], 2 => [2, 3, 1, 0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'mortgage_house_cost'=> [
                    'name' => 'Рыночная стоимость (в валюте кредита)',
                    'values' => [],
                    'weights' => 'mortgageHouseCostFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'mortgage_house_is_registered_perps'=> [
                    'name' => 'Зарегистрированы несовершеннолетние, нетрудоспособные, пенсионеры, ветераны АТО',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'weights' => [1 => [2, 2, 1, 0], 2 => [2, 2, 2, 0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'mortgage_house_perps'=> [
                    'name' => 'Кто из перечисленных зарегестрирован?',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 200)),
                    ]
                ],
                'mortgage_estate'=> [
                    'name' => 'Коммерческая недвижимость',
                    'values' => [0 => 'Нет', 1 => 'Да'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'mortgage_estate_area'=> ['name' => 'Площадь(кв.м)', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000,
                        ))
                    ]
                ],
                'mortgage_estate_state'=> [
                    'name' => 'Тех. состояние',
                    'values' => [1 => 'Удовлетворительное', 2 => 'Не удовлетворительное'],
                    'weights' => [1 => [0, 1, 3, 0], 2 => [2, 3, 1, 0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'mortgage_estate_cost'=> [
                    'name' => 'Рыночная стоимость (в валюте кредита)',
                    'values' => [],
                    'weights' => 'mortgageEstateCostFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'mortgage_estate_operated'=> [
                    'name' => 'Эксплуатируется?',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'weights' => [1 => [1, 2, 3, 0], 2 => [0, 2, 2, 2]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'mortgage_estate_profit'=> [
                    'name' => 'Какой доход приносит?',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'mortgage_area'=> [
                    'name' => 'Земля',
                    'values' => [0 => 'Нет', 1 => 'Да'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'mortgage_area_area'=> ['name' => 'Площадь (соток)', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 100000,
                        ))
                    ]],
                'mortgage_area_purpose'=> [
                    'name' => 'Назначение',
                    'values' => [1 => 'Сельскохозяйственное', 2 => 'Под застройку'],
                    'weights' => [1 => [1,2,0,0], 2 => [1,1,1,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'mortgage_area_cost'=> [
                    'name' => 'Рыночная стоимость (в валюте кредита)',
                    'values' => [],
                    'weights' => 'mortgageAreaCostFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'mortgage_other'=> [
                    'name' => 'Другое',
                    'values' => [0 => 'Нет', 1 => 'Да'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'mortgage_other_cost'=> [
                    'name' => 'Рыночная стоимость (в валюте кредита)',
                    'values' => [],
                    'weights' => 'mortgageOtherCostFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'surety'=> ['name' => 'Поручительство', 'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'surety_person_fullname'=> ['name' => 'ФИО поручителя', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('min' => 5, 'max' => 100))
                    ]
                ],
                'surety_person_phone'=> ['name' => 'Телефон', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('min' => 10, 'max' => 13))
                    ]
                ],
                'surety_person_email'=> ['name' => 'E-mail', 'values' => []],
                'surety_person_contact_details'=> ['name' => 'Другой вид связи', 'values' => []],
                'pledge'=> ['name' => 'Залог', 'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'pledge_car'=> ['name' => 'Автомобиль', 'values' => [1 => 'Да', 0 => 'Нет']],
                'pledge_car_brand'=> ['name' => 'Марка', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 100)),
                    ]
                ],
                'pledge_car_manufacture_year'=> ['name' => 'Год выпуска', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 1900,
                            'max' => date('Y'),
                        )),
                    ]
                ],
                'pledge_car_cost'=> ['name' => 'Рыночная стоимость', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'pledge_car_registration_year'=> ['name' => 'Город регистрации', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 100)),
                    ]
                ],
                'pledge_movables'=> ['name' => 'Движимое имущество', 'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'pledge_movables_name'=> ['name' => 'Наименование', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 100)),
                    ]
                ],
                'pledge_movables_manufacture_year'=> ['name' => 'Год выпуска<', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 1900,
                            'max' => date('Y'),
                        )),
                    ]
                ],
                'pledge_movables_cost'=> ['name' => 'Рыночная стоимость', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'pledge_movables_registration_year'=> ['name' => 'Город регистрации', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 100)),
                    ]
                ],
                'pledge_securities'=> ['name' => 'Ценные бумаги', 'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'pledge_securities_cost'=> ['name' => 'Оценночная текущая стоимость', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'pledge_corporate_rights'=> ['name' => 'Корпоративные права', 'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'pledge_corporate_rights_operated'=> [
                    'name' => 'Предприятие работающее?',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'weights' => [1 => [2,2,0,0], 2 => [0,0,0,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]

                ],
                'pledge_corporate_rights_owned'=> [
                    'name' => 'Ваше участие в компании рещающее?',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'pledge_other'=> ['name' => 'Другое', 'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'pledge_other_cost'=> [
                    'name' => 'Оценочная текущая стоимость', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'nothing'=> [
                    'name' => 'Отсутствует',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'weights' => [1 => [0,3,0,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
            ],
            Questionnaire::PENALTY_DATA_KEY => [
                'city'=> ['name' => 'Город', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('min' => 1, 'max' => 60)),
                    ]
                ],
                'creditor_type'=> [
                    'name' => 'Кто взыскатель?',
                    'values' => [
                        1 => 'Банк',
                        2 => 'Компания от имени банка',
                        3 => 'Коллектор или фактор',
                        4 => 'Фонд гарантирования вкладов от имени банка',
                    ],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,4),
                        )),
                    ]
                ],
                'creditor_name'=> ['name' => 'Название организации', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 60)),
                    ]
                ],
                'stage'=> [
                    'name' => 'Стадия взыскания',
                    'values' => [
                        1 => 'Взыскание внесудебное / досудебное',
                        2 => 'Взыскание судебное',
                        3 => 'Взыскание послесудебное (ГИС)'
                    ],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,3),
                        )),
                    ]
                ],
                'stage1_calls'=> ['name' => 'Звонки банка / коллекторов', 'values' => [0 => 'Нет', 1 => 'Да'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'stage1_mails'=> ['name' => 'Письма / Досудебные требования', 'values' => [0 => 'Нет', 1 => 'Да'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'satge1_visits'=> ['name' => ' Визиты представителей банка / Коллекторов',
                    'values' => [0 => 'Нет', 1 => 'Да'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'stage1_else'=> ['name' => 'Другое', 'values' => [0 => 'Нет', 1 => 'Да'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'stage1_else_details'=> ['name' => 'Другое, описание', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 1000)),
                    ]
                ],
                'stage2_participation'=> [
                    'name' => 'Я учавствую в суде',
                    'values' => [1 => 'Лично', 2 => 'Через представителя / адвоката', 3 => 'Игнорирую'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,3),
                        )),
                    ]
                ],
                'stage2_fio'=> ['name' => 'Имя', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 100)),
                    ],
                ],
                'stage2_phone'=> ['name' => 'Телефон', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('min' => 10, 'max' => 13)),
                    ]
                ],
                'stage2_email'=> ['name' => 'Электронный адрес', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 200)),
                    ]
                ],
                'stage2_else'=> ['name' => 'Другие контактные данные', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 200)),
                    ]
                ],
                'stage2_type1'=> [
                    'name' => 'Взыскание суммы задолженности',
                    'values' => [0 => 'Нет', 1 => 'Да'],
                    'weights' => [1 => [1,2,3,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'stage2_type1_amount'=> [
                    'name' => 'Сумма, согласно иска',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'stage2_type2'=> [
                    'name' => 'Взыскание залога / ипотеки',
                    'values' => [0 => 'Нет', 1 => 'Да'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'stage2_type3'=> [
                    'name' => 'Путем принятия кредитором залога/ипотеки в собственность',
                    'values' => [0 => 'Нет', 1 => 'Да'],
                    'weights' => [1 => [3,2,3,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'stage2_type4'=> [
                    'name' => 'Путем получения кредитором права продажи залога/ипотеки',
                    'values' => [0 => 'Нет', 1 => 'Да'],
                    'weights' => [1 => [2,2,2,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'stage2_court_type'=> ['name' => 'Наименование суда',
                    'values' => [1 => 'Название суда 1ой инстанции', 2 => 'Апелляционная инстанция'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'stage2_court_fio'=> ['name' => 'ФИО судьи', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 100)),
                    ]
                ],
                'stage2_court_stage'=> [
                    'name' => 'Стадия судебного производства',
                    'values' => [
                        1 => 'Назначено первое заседание',
                        2 => 'Судебное рассмотрение дела',
                        3 => 'Вынесено судебное решение'],
                    'weights' => [1 => [2,1,2,0], 2 => [0,0,0,0], 3 => [1,2,3,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,3),
                        )),
                    ]
                ],
                'stage2_court_stage2_amount1'=> [
                    'name' => 'Сколько заседаний прошло?',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 100,
                        )),
                    ]
                ],
                'stage2_court_stage2_amount2'=> [
                    'name' => 'Сколько заседаний перенесено?',
                    'values' => [],
                    'weights' => 'courtMovedAmountFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 100,
                        )),
                    ]
                ],
                'stage2_court_stage2_date'=> [
                    'name' => 'Когда следующее заседание?',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 40)),
                    ]
                ],
                'stage2_court_stage3_state'=> [
                    'name' => 'Исполнительный лист выдан?',
                    'values' => [1 => 'Нет или Не знаю', 2 => 'Да'],
                    'weights' => [1 => [1,1,1,0], 2 => [0,0,0,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'stage2_court_stage3_date'=> ['name' => 'Дата выдачи исполнительного листа', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 40)),
                    ]
                ],
                'stage2_court_stage3_amount'=> ['name' => 'Сумма взыскания', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'stage2_court_stage3_type'=> [
                    'name' => 'Тип взыскания',
                    'values' => [
                        1 => 'Деньги за счет всего имущества',
                        2 => 'Принятие залога / ипотеки в собственность',
                        3 => 'Право продажи залога / ипотеки',
                        4 => 'Наличие решения о выселении'],
                    'weights' => [1 => [2,2,3,0], 2 => [0,1,3,0], 3 => [2,1,2,0], 4 => [0,1,0,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,4),
                        )),
                    ]
                ],
                'stage2_appeal_type'=> [
                    'name' => 'Кто апеллянт?',
                    'values' => [1 => 'Должник', 2 => 'Кредитор(банк)', 3 => 'Третья сторона'],
                    'weights' => [2 => [0,3,1,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,3),
                        )),
                    ]
                ],
                'stage2_appeal_stage'=> [
                    'name' => 'Стадия обжалования',
                    'values' => [
                        1 => 'Назначено первое засидание',
                        2 => 'Рассмотрение апелляционной жалобы',
                        3 => 'Вынесено судебное решение'
                    ],
                    'weights' => [1 => [1,2,1,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,3),
                        )),
                    ]
                ],
                'stage2_appeal_stage2_amount1'=> [
                    'name' => 'Сколько заседаний прошло?',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 100,
                        )),
                    ]
                ],
                'stage2_appeal_stage2_amount2'=> [
                    'name' => 'Сколько заседаний перенесено?',
                    'values' => [],
                    'weights' => 'appealStage2AmountFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 100,
                        )),
                    ]
                ],
                'stage2_appeal_stage2_date'=> ['name' => 'Когда следующее заседание?', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 40)),
                    ]
                ],
                'stage2_cassation_type'=> [
                    'name' => 'Касационное обжалование:',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'weights' => [1 => [1,3,3,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'stage2_cassation_type2'=> [
                    'name' => 'Кто кассатор?',
                    'values' => [
                        1 => 'Должник',
                        2 => 'Кредитор(банк)',
                        3 => 'Третья сторона',
                    ],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,3),
                        )),
                    ]
                ],
                'stage3_name'=> ['name' => 'Наименование ОГИС', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 100)),
                    ]
                ],
                'stage3_fio'=> ['name' => 'ФИО госисполнителя', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 100)),
                    ]
                ],
                'stage3_date'=> ['name' => 'Дата открытия', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 40)),
                    ]
                ],
                'stage3_amount'=> ['name' => 'Сумма взыскания', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'stage3_stage'=> [
                    'name' => 'Стадия исполнительного производства?',
                    'values' => [
                        1 => 'Срок на добровольное исполнение судебного решения',
                        2 => 'Арест имущества',
                        3 => 'Опись имущества',
                        4 => 'Оценка имущества',
                        5 => 'Передача имущества на торги',
                        6 => 'Публичные торги',
                    ],
                    'weights' => [6 => [0,0,2,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,6),
                        )),
                    ]
                ],
                'stage3_writeoff'=> [
                    'name' => 'Будет списан остаток долга?',
                    'values' => [1 => 'Да', 2 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'stage3_queue'=> [
                    'name' => 'Очередность торгов',
                    'values' => [1 => 'Первые', 2 => 'Вторые', 3 => 'Третьи'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,3),
                        )),
                    ]
                ],
            ],
            Questionnaire::FINANCIAL_DATA_KEY => [
                'regular_income'=> [
                    'name' => 'Вы имеете постоянный доход?',
                    'values' => [
                        1 => 'Работа по найму',
                        2 => 'Бизнесс или предприниматель',
                        3 => 'Пенсия',
                        4 => 'Не имею',
                    ],
                    'weights' => [1 => [2,1,0,0], 2 => [1,2,0,0], 3 => [2,1,2,0], 4 => [0,1,3,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,4),
                        )),
                    ]
                ],
                'avg_monthly_income'=> [
                    'name' => 'Среднемесячный доход',
                    'values' => [],
                    'weights' => 'avgMonthIncomeFunction',
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        )),
                    ]
                 ],
                'guarantor_avg_monthly_income'=> ['name' => 'Среднемесячный доход поручителя', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        )),
                    ]
                ],
                'has_children'=> [
                    'name' => 'Дети до 18 лет',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    // if set to 0, studies or hospital options should not be taken into account
                    'weights' => 'childrenAdaptiveValueFunction',
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'children_monthly_amount'=> ['name' => 'Ежемесячная сумма расходов, дети', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        )),
                    ]
                ],
                'children_studies'=> [
                    'name' => 'Учеба',
                ],
                'children_age'=> [
                    'name' => 'Возраст детей, через запятую',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 100)),
                    ]
                ],
                'children_additional_expenses' => [
                    'name' => 'Дети: Необходимость дополнительных расходов',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    // if set to 0, studies or hospital options should not be taken into account
                    'weights' => 'childrenAdaptiveValueFunction',
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'children_additional_expenses_studies' => [
                    'name' => 'Учеба',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'weights' => [1 => [1,1,0,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'school_monthly_expenses' => [
                    'name' => 'Ежемесячная сумма за учебу',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        )),
                    ]
                ],
                'children_additional_expenses_treatment' => [
                    'name' => 'Лечение',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'weights' => [1 => [1,2,1,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'hospital_monthly_expenses' => ['name' => 'Ежемесячная сумма за лечение', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        )),
                    ]
                ],
                'has_military'=> [
                    'name' => 'Воины или ветераны АТО',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'weights' => [1 => [1,1,1,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'military_additional_expenses' => [
                    'name' => 'Воины или ветераны АТО: Необходимость дополнительных расходов',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'military_monthly_amount'=> [
                    'name' => 'Ежемесячная сумма',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        )),
                    ]
                ],
                'has_family'=> [
                    'name' => 'Родители, пенсионеры',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    // if set to 0, family additional expenses should not be taken into account
                    'weights' => 'familyAdaptiveValueFunction',
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'family_age'=> [
                    'name' => 'Возраст родителей или пенсионеров, через запятую',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 100)),
                    ]
                ],
                'family_additional_expenses' => [
                    'name' => 'Родителей или пенсионеры: Необходимость дополнительных расходов',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'weights' => [1 => [1,1,1,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'family_additional_expenses_treatment'=> [
                    'name' => 'Уход и лечение',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'family_monthly_amount'=> [
                    'name' => 'Ежемесячная сумма за уход и лечение',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        )),
                    ]
                ],
                'has_sick_perps'=> [
                    'name' => 'Больные и другие варианты',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'weights' => [1 => [1,1,1,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'sick_perps_age'=> [
                    'name' => 'Возраст, через запятую',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 100)),
                    ]
                ],
                'sick_perps_additional_expenses' => [
                    'name' => 'Больные: Необходимость дополнительных расходов',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'sick_perps_additional_expenses_treatment' => [
                    'name' => 'Уход и лечение',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'sick_perps_monthly_amount' => [
                    'name' => 'Ежемесячная сумма за уход и лечение',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        )),
                    ]
                ],
                'credit_at_different_bank' => [
                    'name' => 'Есть кредиты в других банках?',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    // if set to 0, this field is estimated as 1 1 1 0 and credit_at_different_bank_status field should not be taken into account
                    'weights' => 'creditAtDifferebtBankAdaptiveValueFunction',
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'credit_at_different_bank_monthly_pay' => [
                    'name' => 'Ежемесячный платеж (в грн.)',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'credit_at_different_bank_status' => [
                    'name' => 'Кредит проблемный?',
                    'values' => [
                        1 => 'ДА (НЕ обслуживаю, или ЧАСТИЧНО обслуживаю)',
                        2 => 'НЕТ (обслуживаю)'
                    ],
                    'weights' => [
                        1 => [1,2,3,0],
                        2 => [1,1,2,0],
                    ],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'personal_expenses' => [
                    'name' => 'У Вас лично есть необходимость БЕЗУСЛОВНЫХ дополнительных расходов?',
                    'values' => [0 => 'Нет', 1 => 'Да'],
                    // if set to 0, personal_expenses_treatment is estimated as 1 1 1 0
                    'weights' => 'personalExpensesFunction',
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'personal_expenses_treatment' => [
                    'name' => 'Уход и лечение',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'weights' => [
                        0 => [1,1,1,0],
                        1 => [1,2,3,0],
                    ],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'personal_expenses_treatment_cost' => [
                    'name' => 'Ежемесячная сумма',
                    'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 1000000, // a million
                        )),
                    ]
                ],
                'other_properties' => [
                    'name' => 'Наличие другого имущества?',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    // if set to 0, other_properties_ estate,vehicle,other _in_pledge fields should not be taken into account
                    'weights' => 'otherPropertiesAdaptiveFunction',
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'other_properties_estate' => [
                    'name' => 'Недвижимость',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    // if set to 0, other_properties_estate_in_pledge field should not be taken into account
                    'weights' => 'otherPropertiesEstateAdaptiveFunction',
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'other_properties_estate_in_pledge' => [
                    'name' => 'Заложена',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'weights' => [
                        0 => [1,3,3,0],
                        1 => [2,2,2,0],
                    ],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'other_properties_vehicle' => [
                    'name' => 'Автомобиль',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    // if set to 0, other_properties_vehicle_in_pledge field should not be taken into account
                    'weights' => 'otherPropertiesVehicleAdaptiveFunction',
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'other_properties_vehicle_in_pledge' => [
                    'name' => 'Заложен',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'weights' => [
                        0 => [1,2,2,0],
                        1 => [2,2,2,0],
                    ],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'other_properties_other' => [
                    'name' => 'Другое',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    // if set to 0, other_properties_other_in_pledge field should not be taken into account
                    'weights' => 'otherPropertiesOtherAdaptiveFunction',
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'other_properties_other_in_pledge' => [
                    'name' => 'Заложено',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'weights' => [
                        0 => [1,2,2,0],
                        1 => [1,1,1,0],
                    ],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
            ],
            Questionnaire::RESOLUTION_ATTEMPTS_KEY => [
                'no_action'=> [
                    'name' => 'Не предпринимались',
                    'values' => [0 => 'Нет', 1 => 'Да'],
                    'weights' => [1 => [3,0,1,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'restructuring'=> [
                    'name' => 'Реструктуризация',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'restructuring_happened'=> [
                    'name' => 'Реструктуризация',
                    'values' => [1 => 'Проведена', 2 => 'Не проведена'],
                    'weights' => [1 => [1,1,1,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'restructuring_date'=> ['name' => 'Дата реструктуризации', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 40)),
                    ]
                ],
                'restructuring_conditions'=> [
                    'name' => 'Реструктуризация, причина',
                    'values' => [1 => 'Отказано банком', 2 => 'Не подходящее для Вас условия'],
                    'weights' => [1 => [0,2,2,0], 2 => [2,2,2,0]],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,2),
                        )),
                    ]
                ],
                'restructuring_condition_details'=> [
                    'name' => 'Какие условия предлагались?', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 500)),
                    ]
                ],
                'discount'=> [
                    'name' => 'Погашение кредита с дисконтом',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'discount_conditions'=> [
                    'name' => 'Погашение кредита с дисконтом',
                    'values' => [1 => 'Нет', 2 => 'Предлагал банк', 3 => 'Предлагали Вы'],
                    'weights' => [],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,3),
                        )),
                    ]
                ],
                'discount_amount_by_bank'=> ['name' => 'Сумма, предложенная банком', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'discount_date_by_bank'=> ['name' => 'Дата предложения(банк)', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 40)),
                    ]
                ],
                'discount_amount_by_you'=> ['name' => 'Сумма, предложенная клиентом', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Type(array(
                            'type' => 'numeric'
                        )),
                        new Range(array(
                            'min' => 0,
                            'max' => 10000000, // 10 million
                        )),
                    ]
                ],
                'discount_date_by_you'=> ['name' => 'Дата предложения(клиент)', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 40)),
                    ]
                ],
                'foreclosure_sale'=> ['name' => 'Продажа залогового имущества', 'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ],
                'foreclosure_sale_conditions'=> [
                    'name' => 'Продажа залогового имущества',
                    'values' => [1 => 'Нет', 2 => 'Предлагал банк', 3 => 'Предлагали Вы'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,3),
                        )),
                    ]
                ],
                'foreclosure_sale_date_by_bank'=> ['name' => 'Дата предложения(банк)', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 40)),
                    ]
                ],
                'foreclosure_sale_bank_dept_conditions'=> [
                    'name' => 'Банк списывает остаток долга после продажи?',
                    'values' => [1 => 'Да', 2 => 'Нет', 3 => 'Не знаю'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,3),
                        )),
                    ]
                ],
                'foreclosure_sale_date_by_you'=> ['name' => 'Дата предложения(клиент)', 'values' => [],
                    'validation' => [
                        new NotBlank(),
                        new Length(array('max' => 40)),
                    ]
                ],
                'foreclosure_sale_you_dept_conditions'=> [
                    'name' => 'Банк списывает остаток долга после продажи?',
                    'values' => [1 => 'Да', 2 => 'Нет', 3 => 'Не знаю'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(1,3),
                        )),
                    ]
                ],
                'voluntary_property_transfer'=> [
                    'name' => 'Добровольная передача имущества в счет погашения обязательств',
                    'values' => [1 => 'Да', 0 => 'Нет'],
                    'validation' => [
                        new Choice(array(
                            'choices' => range(0,1),
                        )),
                    ]
                ]
            ],
        ];
    }

    public static function processSpecialCases(Questionnaire $questionnaire)
    {
        $result = [];
        $goalData = $questionnaire->getGoalData();
        $extras = $questionnaire->getCreditExtras();
        $penalty = $questionnaire->getPenaltyData();
        $financial = $questionnaire->getFinancialData();
        $resolution = $questionnaire->getResolutionAttempts();

        if (!empty($goalData['client_status']) && $goalData['client_status'] == 2) {
            $result[] = 'Освободить поручителя - Пакет "Стопдолг защита"';
        }

        if (!empty($extras['mortgage_house_is_registered_perps']) ||
            !empty($extras['mortgage_apartment_is_registered_perps'])
        ) {
            $result[] = 'прописаны несовершеннолетние, нетрудоспособные, пенсионеры или ветераны АТО';
        }

        if (!empty($extras['mortgage_estate_operated'])) {
            $result[] = 'Риск! Коммерческая недвижимость эксплуатируется';
        }

        if (!empty($extras['pledge_corporate_rights_operated'])) {
            $result[] = 'Риск! Предприятие работающее';
        }

        if (!empty($penalty['stage'])) {
            if ($penalty['stage'] == 1) {
                $result[] = 'Внесудебное\досудебное разбирательство - необходимость юр. защиты. монитиринг';
            } elseif ($penalty['stage'] == 2) {
                $result[] = 'Суд - необходимость юр защиты';
            }
        }

        if (!empty($penalty['stage2_type3'])) {
            $result[] = 'Принятия кредитором залога/ипотеки в собственность';
        }

        if (!empty($penalty['stage3_stage'])) {
            if ($penalty['stage3_stage'] == 4) {
                $result[] = 'возможна подача иска об оспаривании оценки';
            } elseif ($penalty['stage3_stage'] == 5) {
                $result[] = 'возможна рецензия на оценку ( не согласована - иск )';
            } elseif ($penalty['stage3_stage'] == 6) {
                $result[] = 'возможен выкуп с торгов в интересах клиента';
            }
        }

        if (!empty($penalty['stage3_writeoff'])) {
            if ($penalty['stage3_writeoff'] == 1) {
                $result[] = 'Риск! закон о налогообложении прощения долга';
            } elseif ($penalty['stage3_writeoff'] == 2) {
                $result[] = 'Риск! на вас будет подан иск о взыскании оставшейся суммы задолженности';
            }
        }

        if (!empty($financial['other_properties_estate'])) {
            $result[] = 'Недвижимость, от которой рекомендуется освободиться';
        }

        if (!empty($resolution['restructuring_happened']) && $resolution['restructuring_happened'] == 1) {
            $result[] = 'Реструктуризация проведена';
        }

        if (!empty($resolution['discount_conditions']) && $resolution['discount_conditions'] == 3) {
            $result[] = 'Погашение с дисконтом - предлагали вы';
        }

        if (!empty($resolution['foreclosure_sale_conditions']) && $resolution['foreclosure_sale_conditions'] != 1) {
            $result[] = 'Попытки продажи залогового имущества.';
        }

        // ** - отдельная рекомендация
        // * - признак отдельного влияния на рекомендацию
        // Ю1 - необходимость юрзащиты
        // ! - Раскрыть риски!

        return $result;
    }

    public static function getRelevantSections($method = 'get')
    {
        if (!in_array($method, ['get', 'set'])) {
            throw new \LogicException("Wrong method passed (should be either set or get)");
        }

        return array_map(function ($el) use ($method) {
            return $method . $el;
        }, [
            Questionnaire::GOAL_DATA_KEY => 'GoalData',
            Questionnaire::CREDIT_DATA_KEY => 'CreditData',
            Questionnaire::CREDIT_DETAILS_KEY => 'CreditDetails',
            Questionnaire::PENALTY_DATA_KEY => 'PenaltyData',
            Questionnaire::CREDIT_EXTRAS_KEY => 'CreditExtras',
            Questionnaire::FINANCIAL_DATA_KEY => 'FinancialData',
            Questionnaire::RESOLUTION_ATTEMPTS_KEY => 'ResolutionAttempts'
        ]);
    }

    public function calculateWeights($questionnaire)
    {
        $weights = [0,0,0,0];
        $separate = [];

        foreach (static::getRelevantSections() as $section => $accessMethod) {
            /**
             * Accessing object data for the section
             */
            $realData = $questionnaire->$accessMethod();

            foreach ($realData as $realKey => $value) {
                if (empty(static::getSchemaDetails()[$section][$realKey])) {
                    continue;
                }
                $schema = static::getSchemaDetails()[$section][$realKey];
                if (!empty($schema['weights'])) {
                    if (!empty($schema['weights'][$value]) && is_array($schema['weights'][$value])) {

                        $separate[$section][$realKey] = $schema['weights'][$value];

                        $weights = array_map(function () {
                            return array_sum(func_get_args());
                        }, $schema['weights'][$value], $weights);
                    } elseif (is_string($schema['weights']) && method_exists($this, $schema['weights'])) {

                        $currentWeights = $this->{$schema['weights']}($questionnaire);
                        $separate[$section][$realKey] = $currentWeights;

                        $weights = array_map(function () {
                            return array_sum(func_get_args());
                        }, $weights, $currentWeights);
                    }
                }
            }
        }

        return ['total' => $weights, 'separate' => $separate];
    }

    /**
     *
     * @param Questionnaire $questionnaire
     * @return array
     */
    protected function delayDaysFunction(Questionnaire $questionnaire)
    {
        $sectionData = $questionnaire->getCreditDetails();
        if (!empty($sectionData['delay_days']) && $sectionData['delay_days'] > 90 ) {
            return [3, 3, 3, 0];
        }
        return [1, 2, 1, 0];
    }

    protected function paidToBankFunction(Questionnaire $questionnaire)
    {
        $sectionData = $questionnaire->getCreditDetails();

        if (!empty($sectionData['delay_days']) && $sectionData['delay_days'] > $sectionData['issued_credit_amount']) {
            return [1, 2, 1, 0];
        }

        return [2, 1, 1, 0];
    }

    protected function propertyCostEstimationFunction(Questionnaire $questionnaire, $field)
    {
        $creditExtrasData = $questionnaire->getCreditExtras();
        $creditDetailsData = $questionnaire->getCreditDetails();

        if (!empty($creditExtrasData[$field])) {
            if ($creditExtrasData[$field] > $creditDetailsData['issued_credit_amount']) {
                return [1, 2, 3, 0];
            } elseif ($creditExtrasData[$field] < $creditDetailsData['credit_left_amount']) {
                return [2, 2, 0, 0];
            } else {
                return [2, 2, 2, 0];
            }
        }

        return [0, 0, 0, 0];
    }

    protected function mortgageApartmentCostFunction(Questionnaire $questionnaire)
    {
        return $this->propertyCostEstimationFunction($questionnaire, 'mortgage_apartment_cost');
    }

    protected function mortgageHouseCostFunction(Questionnaire $questionnaire)
    {
        return $this->propertyCostEstimationFunction($questionnaire, 'mortgage_house_cost');
    }

    protected function mortgageEstateCostFunction(Questionnaire $questionnaire)
    {
        return $this->propertyCostEstimationFunction($questionnaire, 'mortgage_estate_cost');
    }

    protected function mortgageAreaCostFunction(Questionnaire $questionnaire)
    {
        return $this->propertyCostEstimationFunction($questionnaire, 'mortgage_area_cost');
    }

    protected function mortgageOtherCostFunction(Questionnaire $questionnaire)
    {
        return $this->propertyCostEstimationFunction($questionnaire, 'mortgage_other_cost');
    }





    /**
     * Amount of courts moved to other date
     * @param Questionnaire $questionnaire
     * @return array
     */
    protected function courtMovedAmountFunction(Questionnaire $questionnaire)
    {
        $penaltyData = $questionnaire->getPenaltyData();

        // amount of courts moved
        if ($penaltyData['stage2_court_stage2_amount2'] > 3) {
            return [1, 2, 2, 0];
        }

        return [1, 1, 1, 0];
    }

    /**
     * Amount of appeal courts moved to other date
     * @param Questionnaire $questionnaire
     * @return array
     */
    protected function appealStage2AmountFunction(Questionnaire $questionnaire)
    {
        $penaltyData = $questionnaire->getPenaltyData();
        if ($penaltyData['stage2_appeal_stage2_amount2'] > 2) {
            return [1, 1, 1, 0];
        }

        return [1, 2, 3, 0];
    }

    protected function avgMonthIncomeFunction(Questionnaire $questionnaire)
    {
        $finData = $questionnaire->getFinancialData();
        $creditDetails = $questionnaire->getCreditDetails();

        if (!empty($finData['avg_monthly_income']) && $finData['avg_monthly_income'] > $creditDetails['monthly_pay']) {
            return [2, 2, 1, 0];
        }

        return [3, 1, 3, 0];
    }

//    protected function familyCostAmountFunction(Questionnaire $questionnaire)
//    {
//
//    }

    protected function discountCreditNoDepositDesiredSumFunction(Questionnaire $questionnaire)
    {
        $goalData = $questionnaire->getGoalData();
        $creditDetails = $questionnaire->getCreditDetails();

        if (!empty($goalData['property_type_3_pay_with_discount'])) {
            if ($goalData['property_type_3_pay_with_discount'] >= 0.3 * $creditDetails['credit_left_amount']) {
                return [0, 3, 0, 0];
            } else {
                return [3, 3, 0, 0];
            }
        }

        return [0, 0, 0, 0];
    }

    protected function discountCreditWithDepositDesiredSumFunction(Questionnaire $questionnaire)
    {
        $goalData = $questionnaire->getGoalData();
        $creditDetails = $questionnaire->getCreditDetails();

        if (!empty($goalData['property_type_3_pay_with_discount'])) {
            if ($goalData['property_type_3_pay_with_discount'] >= 0.4 * $creditDetails['credit_left_amount']) {
                return [0, 3, 0, 0];
            } else {
                return [3, 2, 0, 0];
            }
        }

        return [0, 0, 0, 0];
    }

    protected function bankruptcyDiscountAmountFunction(Questionnaire $questionnaire)
    {
        $goalData = $questionnaire->getGoalData();
        $creditDetails = $questionnaire->getCreditDetails();

        if (!empty($goalData['bankruptcy_amount'])) {
            if ($goalData['bankruptcy_amount'] >= 0.4 * $creditDetails['credit_left_amount']) {
                return [0, 3, 0, 0];
            } else {
                return [3, 2, 0, 0];
            }
        }

        return [0, 0, 0, 0];
    }
}
<?php

namespace SD\Bundle\PaymentsBundle\Entity;

/**
 * AutopayOrder
 */
class AutopayOrder
{
    const STATUS_NEW = 1;
    const STATUS_VERIFIED = 2;

    // TEMPORARY - move to db or sync with api
    public static function getProductsTarifs()
    {
        return [
            220252 => 'optimal',
            179664 => 'optimal',
            220253 => 'advanced',
            181017 => 'advanced',
            220038 => 'standard',
            181006 => 'standard'
        ];
    }

    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $paymentDate;

    /**
     * @var \DateTime
     */
    private $orderDate;

    /**
     * @var int
     */
    private $ip;

    /**
     * @var string
     */
    private $paymentMethod;

    /**
     * @var int
     */
    private $autopayStatus;

    /**
     * @var string
     */
    private $details;

    /**
     * @var string
     */
    private $commentsClient;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paymentDate
     *
     * @param \DateTime $paymentDate
     *
     * @return AutopayOrder
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    /**
     * Get paymentDate
     *
     * @return \DateTime
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * Set orderDate
     *
     * @param \DateTime $orderDate
     *
     * @return AutopayOrder
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;

        return $this;
    }

    /**
     * Get orderDate
     *
     * @return \DateTime
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * Set ip
     *
     * @param integer $ip
     *
     * @return AutopayOrder
     */
    public function setIp($ip)
    {
        $this->ip = ip2long($ip);

        return $this;
    }

    /**
     * Get ip
     *
     * @return int
     */
    public function getIp()
    {
        return long2ip($this->ip);
    }

    /**
     * Set paymentMethod
     *
     * @param string $paymentMethod
     *
     * @return AutopayOrder
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set autopayStatus
     *
     * @param integer $autopayStatus
     *
     * @return AutopayOrder
     */
    public function setAutopayStatus($autopayStatus)
    {
        $this->autopayStatus = $autopayStatus;

        return $this;
    }

    /**
     * Get autopayStatus
     *
     * @return int
     */
    public function getAutopayStatus()
    {
        return $this->autopayStatus;
    }

    /**
     * Set details
     *
     * @param array $details
     *
     * @return AutopayOrder
     */
    public function setDetails($details)
    {
        $this->details = json_encode($details);

        return $this;
    }

    /**
     * Get details
     *
     * @return array
     */
    public function getDetails()
    {
        return json_decode($this->details, true);
    }

    /**
     * Set commentsClient
     *
     * @param string $commentsClient
     *
     * @return AutopayOrder
     */
    public function setCommentsClient($commentsClient)
    {
        $this->commentsClient = $commentsClient;

        return $this;
    }

    /**
     * Get commentsClient
     *
     * @return string
     */
    public function getCommentsClient()
    {
        return $this->commentsClient;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return AutopayOrder
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    /**
     * @var \SD\Bundle\UserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \SD\Bundle\UserBundle\Entity\User $user
     *
     * @return AutopayOrder
     */
    public function setUser(\SD\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \SD\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}

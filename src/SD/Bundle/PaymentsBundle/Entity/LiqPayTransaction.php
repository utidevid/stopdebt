<?php

namespace SD\Bundle\PaymentsBundle\Entity;
use SD\Bundle\QuestionnaireBundle\Entity\Questionnaire;
use Symfony\Component\Security\Core\User\User;

/**
 * LiqPay
 */
class LiqPayTransaction
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $transactionId;

    /**
     * @var int
     */
    private $paymentId;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $paytype;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var int
     */
    private $ip;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var float
     */
    private $senderCommission;

    /**
     * @var float
     */
    private $receiverCommission;

    /**
     * @var array
     */
    private $customData;

    /**
     * @var string
     */
    private $liqpayOrderId;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var User
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set transactionId
     *
     * @param integer $transactionId
     *
     * @return LiqPay
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Get transactionId
     *
     * @return int
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set paymentId
     *
     * @param integer $paymentId
     *
     * @return LiqPay
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Get paymentId
     *
     * @return int
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return LiqPay
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return LiqPay
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set paytype
     *
     * @param string $paytype
     *
     * @return LiqPay
     */
    public function setPaytype($paytype)
    {
        $this->paytype = $paytype;

        return $this;
    }

    /**
     * Get paytype
     *
     * @return string
     */
    public function getPaytype()
    {
        return $this->paytype;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     *
     * @return LiqPay
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return LiqPay
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return LiqPay
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set ip
     *
     * @param integer $ip
     *
     * @return LiqPay
     */
    public function setIp($ip)
    {
        $this->ip = ip2long($ip);

        return $this;
    }

    /**
     * Get ip
     *
     * @return int
     */
    public function getIp()
    {
        return long2ip($this->ip);
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return LiqPay
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set senderCommission
     *
     * @param float $senderCommission
     *
     * @return LiqPay
     */
    public function setSenderCommission($senderCommission)
    {
        $this->senderCommission = $senderCommission;

        return $this;
    }

    /**
     * Get senderCommission
     *
     * @return float
     */
    public function getSenderCommission()
    {
        return $this->senderCommission;
    }

    /**
     * Set receiverCommission
     *
     * @param float $receiverCommission
     *
     * @return LiqPay
     */
    public function setReceiverCommission($receiverCommission)
    {
        $this->receiverCommission = $receiverCommission;

        return $this;
    }

    /**
     * Get receiverCommission
     *
     * @return float
     */
    public function getReceiverCommission()
    {
        return $this->receiverCommission;
    }

    /**
     * Set customData
     *
     * @param array $customData
     *
     * @return LiqPay
     */
    public function setCustomData(array $customData)
    {
        $this->customData = $customData;

        return $this;
    }

    /**
     * Get customData
     *
     * @return array
     */
    public function getCustomData()
    {
        return $this->customData;
    }

    /**
     * Set liqpayOrderId
     *
     * @param string $liqpayOrderId
     *
     * @return LiqPay
     */
    public function setLiqpayOrderId($liqpayOrderId)
    {
        $this->liqpayOrderId = $liqpayOrderId;

        return $this;
    }

    /**
     * Get liqpayOrderId
     *
     * @return string
     */
    public function getLiqpayOrderId()
    {
        return $this->liqpayOrderId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return LiqPay
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return Questionnaire
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var \SD\Bundle\CaseBundle\Entity\DebtCase
     */
    private $case;


    /**
     * Set case
     *
     * @param \SD\Bundle\CaseBundle\Entity\DebtCase $case
     *
     * @return LiqPayTransaction
     */
    public function setCase(\SD\Bundle\CaseBundle\Entity\DebtCase $case = null)
    {
        $this->case = $case;

        return $this;
    }

    /**
     * Get case
     *
     * @return \SD\Bundle\CaseBundle\Entity\DebtCase
     */
    public function getCase()
    {
        return $this->case;
    }
    /**
     * @var \SD\Bundle\CaseBundle\Entity\CustomService
     */
    private $custom_service;


    /**
     * Set customService
     *
     * @param \SD\Bundle\CaseBundle\Entity\CustomService $customService
     *
     * @return LiqPayTransaction
     */
    public function setCustomService(\SD\Bundle\CaseBundle\Entity\CustomService $customService = null)
    {
        $this->custom_service = $customService;

        return $this;
    }

    /**
     * Get customService
     *
     * @return \SD\Bundle\CaseBundle\Entity\CustomService
     */
    public function getCustomService()
    {
        return $this->custom_service;
    }
}

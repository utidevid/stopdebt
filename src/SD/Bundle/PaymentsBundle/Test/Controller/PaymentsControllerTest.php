<?php

namespace SD\Bundle\PaymentsBundle\Tests\Controller;

use SD\Bundle\UtilsBundle\Tests\WebTestCase;

class PaymentsControllerTest extends WebTestCase
{

    public function testIndex()
    {
        $this->client->loginRealUser();
        $crawler = $this->client->request('GET', '/payments/list');
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

}

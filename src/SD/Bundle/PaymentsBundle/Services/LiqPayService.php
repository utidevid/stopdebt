<?php

namespace SD\Bundle\PaymentsBundle\Services;

use SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction;

class LiqPayService {

    protected $doctrine;
    protected $em;
    protected $userManager;
    protected $router;
    protected $mailer;
    protected $parameters;


    public function __construct($doctrine, $userManager, $router, $mailer, $parameters) {
        $this->doctrine = $doctrine;
        $this->em = $doctrine->getManager();
        $this->userManager = $userManager;
        $this->router = $router;
        $this->mailer = $mailer;
        $this->parameters = $parameters;
    }

    public function getPublicKey() {
        return $this->parameters['public_key'];
    }

    public function getPrivateKey() {
        return $this->parameters['private_key'];
    }

    public function createLiqPay() {
        return new \LiqPay($this->getPublicKey(), $this->getPrivateKey());
    }


    public function processCallback($jsonData) {
        $liqPayTransaction = new LiqPayTransaction();

        $liqPayTransaction->setTransactionId($jsonData['transaction_id']);
        $liqPayTransaction->setPaymentId($jsonData['payment_id']);
        $liqPayTransaction->setStatus($jsonData['status']);
        $liqPayTransaction->setType($jsonData['type']);
        $liqPayTransaction->setPaytype($jsonData['paytype']);
        $liqPayTransaction->setOrderId($jsonData['order_id']);
        $liqPayTransaction->setDescription($jsonData['description']);
        $liqPayTransaction->setAmount($jsonData['amount']);
        $liqPayTransaction->setIp($jsonData['ip']);
        $liqPayTransaction->setCurrency($jsonData['currency']);
        $liqPayTransaction->setSenderCommission($jsonData['sender_commission']);
        $liqPayTransaction->setReceiverCommission($jsonData['receiver_commission']);
        $liqPayTransaction->setLiqpayOrderId($jsonData['liqpay_order_id']);

        $liqPayTransaction->setCustomData(
            [
                'action'    =>  $jsonData['action'],
                'version'    =>  $jsonData['version'],
                'public_key'    =>  $jsonData['public_key'],
                'acq_id'    =>  $jsonData['acq_id'],
                'sender_card_mask2'    =>  $jsonData['sender_card_mask2'],
                'sender_card_bank'    =>  $jsonData['sender_card_bank'],
                'sender_card_country'    =>  $jsonData['sender_card_country'],
                'agent_commission'    =>  $jsonData['agent_commission'],
                'amount_debit'    =>  $jsonData['amount_debit'],
                'amount_credit'    =>  $jsonData['amount_credit'],
                'commission_debit'    =>  $jsonData['commission_debit'],
                'commission_credit'    =>  $jsonData['commission_credit'],
                'currency_debit'    =>  $jsonData['currency_debit'],
                'currency_credit'    =>  $jsonData['currency_credit'],
                'sender_bonus'    =>  $jsonData['sender_bonus'],
                'amount_bonus'    =>  $jsonData['amount_bonus'],
                'mpi_eci'    =>  $jsonData['mpi_eci'],
                'is_3ds'    =>  $jsonData['is_3ds'],
            ]
        );

        $orderIdInfo = explode('#', $jsonData['order_id']);

        if ($orderIdInfo[0] == 1) {
            $repo = $this->doctrine->getRepository('CaseBundle:DebtCase');
            $case =  $repo->findOneBy([
                'safeId' => $orderIdInfo[1]
            ]);

            $liqPayTransaction->setCase($case);
            $liqPayTransaction->setUser($case->getUser());

            if (in_array($liqPayTransaction->getStatus(), ['success', 'sandbox']) && !$case->getBilled()) {
                $case->setBilled(1);
                $this->mailer->sendCasePaidMessage($case->getUser(), $case);
            }
        } elseif ($orderIdInfo[0] == 2) {
            $repo = $this->doctrine->getRepository('CaseBundle:CustomService');
            $service =  $repo->findOneBy([
                'safeId' => $orderIdInfo[1]
            ]);

            $liqPayTransaction->setCustomService($service);
            $liqPayTransaction->setUser($service->getCase()->getUser());

            if (in_array($liqPayTransaction->getStatus(), ['success', 'sandbox']) && !$service->getBilled()) {
                $service->setBilled(1);
                $this->mailer->sendServicePaidMessage($service->getUser(), $service);
            }
        }

        $this->em->persist($liqPayTransaction);
        $this->em->flush();
    }

    public function getParamsForCasePayment($case) {
        $amounts = [
            'standard' => 398,
            'optimal' => 598,
            'advanced' => 998
        ];

        $amount = $case->getFirstPayment() > 0 ? $case->getFirstPayment() : $amounts[$case->getTariff()];

        $params =  [
            'action'     => 'pay',
            'amount'   => $amount,
            'currency' => 'UAH',
            'product_name' => $case->getTariff().' - тариф.',
            'product_description' => 'This is a basic description of '.$case->getTariff().' tariff',
            'description' => 'Оплата кейса '.$case->getSafeId().' ('.$case->getTariff().' тариф)',
            'order_id'      => '1#'.$case->getSafeId().'#'.$this->generateRandomString(10),
            'version'       => 3,
            'public_key'    =>  $this->getPublicKey(),
            'customer'      => $case->getUser()->getEmail(),
            'sandbox'       => 1
        ];

        return $params;
    }

    public function getParamsForServicePayment($service) {
        $params =  [
            'action'     => 'pay',
            'amount'   => $service->getAmount(),
            'currency' => 'UAH',
            'product_name' => $service->getTitle(),
            'product_description' => $service->getDescription(),
            'description' => $service->getDescription(),
            'order_id'      => '2#'.$service->getSafeId().'#'.$this->generateRandomString(10),
            'version'       => 3,
            'public_key'    =>  $this->getPublicKey(),
            'customer'      => $service->getCase()->getUser()->getEmail(),
            'sandbox'       => 1
        ];

        return $params;
    }

    protected function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
<?php
namespace SD\Bundle\PaymentsBundle\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;

class CallbackApiUser implements UserInterface
{
    protected $name;
    protected $roles;

    public function __construct($name, $roles)
    {
        $this->name = $name;
        $this->roles = $roles;
    }

    public function getUsername()
    {
        return $this->name;
    }

    public function getSalt()
    {
        return null;
    }

    public function getPassword()
    {
        return null;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function eraseCredentials()
    {
        return true;
    }
} 
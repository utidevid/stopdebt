<?php
namespace SD\Bundle\PaymentsBundle\Security;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use SD\Bundle\PaymentsBundle\Security\User\CallbackApiUser;

class AutopayAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var string api secret key
     */
    protected $secret;


    public function __construct($secret)
    {
        $this->secret = $secret;
    }

    public function getCredentials(Request $request)
    {
        $keys = ['id', 'email', 'phone', 'hash'];
        $credentials = [];
        foreach ($keys as $key) {
            $credentials[$key] = $request->get($key);
            if ($credentials[$key] === null) {
                return null;
            }
        }

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return new CallbackApiUser('autopay', ['ROLE_AUTOPAY']);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return md5(join('', array_slice($credentials, 0, 3)) . $this->secret) == $credentials['hash'];
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = ['message' => 'failure'];

        return new JsonResponse($data, 403);
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = ['message' => 'Authentication Required'];

        return new JsonResponse($data, 401);
    }

    public function supportsRememberMe()
    {
        return false;
    }
} 
<?php

namespace SD\Bundle\PaymentsBundle\Controller;

use SD\Bundle\PaymentsBundle\Entity\AutopayOrder;
use SD\Bundle\UserBundle\Model\SDUserManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class AutopayController extends Controller
{
    /**
     * @param AutopayOrder $payment
     * @return JsonResponse
     * @ParamConverter(converter="autopay", name="payment")
     */
    public function callbackAction(AutopayOrder $payment)
    {
        // todo add duplicate check, etc
        $details = $payment->getDetails();
        $email = $details['email'];

        /** @var SDUserManager $manager */
        $manager = $this->get('sd_user_manager');

        $user = $manager->findUserByEmail($email);

        if (!$user) {
            $user = $manager->autoSignupUser($email, $details['first_name'], $details['last_name'],
                $details['phone'], $details['middle_name'], $details['city']);
        }

        $payment->setUser($user);
        $this->getDoctrine()->getManager()->persist($payment);
        $this->getDoctrine()->getManager()->flush($payment);


        $tarrifs = AutopayOrder::getProductsTarifs();

        if (!empty($tarrifs[$details['product_id']])) {
            $this->get('sd_debt_case_service')->createCase($user, $tarrifs[$details['product_id']], 1);
        }

        return new JsonResponse('success');
    }
}

<?php
namespace SD\Bundle\PaymentsBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractUserController;

/**
 * User operations with payments
 * Class PaymentsController
 * @package SD\Bundle\PaymentsBundle\Controller
 */
class PaymentsController extends AbstractUserController
{
    const SECTION_NAME = 'payments';

    public function indexAction()
    {
        $payments = $this->getDoctrine()->getRepository('SDPaymentsBundle:AutopayOrder')->findBy(['user' =>
            $this->getUser()]);


        return $this->render('SDPaymentsBundle::payments_list.html.twig', ['payments' => $payments]);
    }
}
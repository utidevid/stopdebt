<?php

namespace SD\Bundle\PaymentsBundle\Controller;

use SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use SD\Bundle\UtilsBundle\Controller\AbstractUserController;

class LiqPayController extends AbstractUserController
{
    const SECTION_NAME = 'payments';

    public function indexAction(Request $request)
    {
        $data = $request->get('data');

        $sign = base64_encode(sha1($this->get('payments.liqpay')->getPrivateKey().$data.$this->get('payments.liqpay')->getPrivateKey(),1));

        if ($request->get('signature') == $sign) {
            $jsonData = json_decode(base64_decode($data), true);
            $this->get('payments.liqpay')->processCallback($jsonData);
            return new Response('ok');
        }

        return new Response('invalid signature: '.$request->get('signature').' != '.$sign);
    }

    public function ignoreAction($caseId) {
        // WARNING!!!!
        // ЭТОТ МЕТОД ДЛЯ ТЕСТИРОВАНИЯ... ОБХОД ОПЛАТЫ ДЛЯ РАЗРАБОТЧИКОВ

        $this->loadCaseBySafeId($caseId);

        $this->activeCase->setBilled(1);
        $this->getDoctrine()->getManager()->flush($this->activeCase);

        return $this->redirectToRoute('dashboard_homepage', [
            'caseId' => $caseId
        ]);
    }

    public function payAction($caseId)
    {
        $this->loadCaseBySafeId($caseId);

        if ($this->activeCase->getBilled()) {
            return $this->redirectToRoute('dashboard_homepage', [
                'caseId' => $caseId
            ]);
        }

        $params = $this->get('payments.liqpay')->getParamsForCasePayment($this->activeCase);
        $params['result_url'] = $this->generateUrl('payments_pay_liqpay_success', [
            'caseId' => $caseId
        ], true);
        $params['server_url'] = $this->generateUrl('payments_liqpay_callback', [], true);

        $liqPay = $this->get('payments.liqpay')->createLiqPay();

        $resultUrl = $this->generateUrl('payments_pay_liqpay_success', [
            'caseId' => $caseId
        ], true);

        return $this->render('SDPaymentsBundle::pay.html.twig', [
            'resultUrl' => $resultUrl,
            'sign' => $liqPay->cnb_signature($params),
            'data' => base64_encode(json_encode($params))
        ]);
    }

    public function servicePayAction($serviceId)
    {
        $service = $this->loadServiceBySafeId($serviceId);
        $this->activeCase = $service->getCase();

        if ($service->getBilled()) {
            return $this->redirectToRoute('dashboard_homepage', [
                'caseId' => $caseId
            ]);
        }

        $params = $this->get('payments.liqpay')->getParamsForServicePayment($service);
        $params['result_url'] = $this->generateUrl('payments_pay_liqpay_service_success', [
            'serviceId' => $serviceId
        ], true);
        $params['server_url'] = $this->generateUrl('payments_liqpay_callback', [], true);

        $liqPay = $this->get('payments.liqpay')->createLiqPay();

        $resultUrl = $this->generateUrl('payments_pay_liqpay_service_success', [
            'serviceId' => $serviceId
        ], true);

        return $this->render('SDPaymentsBundle::service_pay.html.twig', [
            'service' => $service,
            'resultUrl' => $resultUrl,
            'sign' => $liqPay->cnb_signature($params),
            'data' => base64_encode(json_encode($params))
        ]);
    }

    public function successAction($caseId) {
        $this->loadCaseBySafeId($caseId);

        return $this->render('SDPaymentsBundle::success.html.twig',[
        ]);
    }

    public function serviceSuccessAction($serviceId) {
        $service = $this->loadServiceBySafeId($serviceId);
        $this->activeCase = $service->getCase();

        return $this->render('SDPaymentsBundle::service_success.html.twig',[
            'service' => $service
        ]);
    }
}

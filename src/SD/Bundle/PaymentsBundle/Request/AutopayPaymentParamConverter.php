<?php
namespace SD\Bundle\PaymentsBundle\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use SD\Bundle\PaymentsBundle\Entity\AutopayOrder;


class AutopayPaymentParamConverter implements ParamConverterInterface
{
    function apply(Request $request, ParamConverter $configuration)
    {
        $parameters = $request->request->all();
        $order = new AutopayOrder();
        $orderDate = new \DateTime();
        $payDate = new \DateTime();
        $orderDate->setTimestamp($parameters['order_date']);
        $payDate->setTimestamp($parameters['pay_date']);

        $order->setOrderDate($orderDate);
        $order->setPaymentDate($payDate);

        $order->setId($parameters['id']);
        $order->setAutopayStatus($parameters['status']);
        $order->setPaymentMethod($parameters['pay_method']);
        $order->setIp($parameters['ip']);

        foreach(['id', 'status', 'pay_method', 'ip', 'paymentDate', 'orderDate'] as $key) {
            unset($parameters[$key]);
        }

        $order->setDetails($parameters);

        $request->attributes->set($configuration->getName(), $order);
    }

    function supports(ParamConverter $configuration)
    {
        return $configuration->getConverter() == 'autopay';
    }
} 
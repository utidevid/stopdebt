<?php

namespace SD\Bundle\NotificationsBundle\Tests\Controller;

use SD\Bundle\UtilsBundle\Tests\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $this->client->loginRealUser();
        $crawler = $this->client->request('GET', '/notifications');
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
}

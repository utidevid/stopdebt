<?php

namespace SD\Bundle\NotificationsBundle\Service;

use Doctrine\ORM\EntityManager;
use SD\Bundle\UserBundle\Entity\User;

class NotificationsService
{
    protected $em;

    public function __construct(EntityManager $manager)
    {
        $this->em = $manager;
    }

    protected function getGenericNotViewedCountQuery()
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select($qb->expr()->count('m'))
            ->from('SD\Bundle\UtilsBundle\Entity\Notification', 'm')
            ->where('m.viewed = :viewed')
            ->setParameter('viewed', 0);

        return $qb;
    }

    public function getUserNotViewedCount($user)
    {
        return $this->getGenericNotViewedCountQuery()
            ->andWhere('m.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

}
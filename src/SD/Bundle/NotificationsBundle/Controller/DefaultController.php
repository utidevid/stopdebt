<?php

namespace SD\Bundle\NotificationsBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractUserController;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AbstractUserController
{
    const SECTION_NAME = 'notifications';

    public function indexAction()
    {
        $data = [];

        foreach ($this->getUser()->getNotifications() as $n) {
            $key = $n->getCreatedAt()->format('d M, Y');
            $data[$key][] = $n;
        }

        return $this->render('NotificationsBundle:Default:index.html.twig', [
            'data' => $data
        ]);
    }

    public function setAllNotificationsViewedAction()
    {
        $repository = $this->getDoctrine()->getRepository('UtilsBundle:Notification');

        $repository->createQueryBuilder('m')
            ->update()->set('m.viewed', 1)
            ->where('m.viewed = 0')
            ->andWhere('m.user = :user')
            ->setParameter('user', $this->getUser())
            ->getQuery()->execute();

        return new JsonResponse(['status' => true]);
    }
}

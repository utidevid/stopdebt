<?php
namespace SD\Bundle\StrategyBundle\Service;

use SD\Bundle\QuestionnaireBundle\Entity\Questionnaire;
use SD\Bundle\QuestionnaireBundle\Model\QuestionnaireModel;


class StrategyService
{
    const STRATEGY_RESTRUCTURING = 'restructuring';
    const STRATEGY_DISCOUNT = 'discount';
    const STRATEGY_PLEDGE_SELL = 'pledge_sell';
    const STRATEGY_BANKRUPTCY = 'bankruptcy';

    /**
     * @var QuestionnaireModel
     */
    protected $questionnaireModel;

    /**
     * @param QuestionnaireModel $model
     */
    public function setQuestionnaireModel(QuestionnaireModel $model)
    {
        $this->questionnaireModel = $model;
    }

    /**
     * @return QuestionnaireModel
     */
    public function getQuestionnaireModel()
    {
        return $this->questionnaireModel;
    }

    public function getStrategies()
    {
        return [
            self::STRATEGY_RESTRUCTURING,
            self::STRATEGY_DISCOUNT,
            self::STRATEGY_PLEDGE_SELL,
            self::STRATEGY_BANKRUPTCY
        ];
    }

    /**
     * Calculates recommendation
     * @param Questionnaire $questionnaire
     * @return string recommendation name
     */
    public function getRecommendation(Questionnaire $questionnaire)
    {
        #todo make saving to database + later get from there
        $weights =
            array_combine(self::getStrategies(), $this->getQuestionnaireModel()->calculateWeights($questionnaire)
            ['total']);

        return current(array_keys($weights));
    }
}
<?php

namespace SD\Bundle\StrategyBundle\Controller;

use SD\Bundle\StrategyBundle\Service\StrategyService;
use SD\Bundle\UtilsBundle\Controller\AbstractUserController;

class DefaultController extends AbstractUserController
{
    const SECTION_NAME = 'strategy';

    public function indexAction($caseId)
    {
        $case = $this->loadCaseBySafeId($caseId);
        $this->denyAccessUnlessGranted('HAS_STATEGY_ACCESS', $this->activeCase);


        /** @var StrategyService $service */
        $service = $this->get('sd.strategy.service');
        $strategy = $service->getRecommendation($case->getQuestionnaire());

        #todo propose additional services which correspond to user revenue\strategy

        return $this->render('StrategyBundle:Default:index.html.twig', [
            'recommended_strategy' => $strategy,
            'case' =>$case
        ]);
    }
}

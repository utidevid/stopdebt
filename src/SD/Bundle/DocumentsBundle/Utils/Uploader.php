<?php

namespace SD\Bundle\DocumentsBundle\Utils;

use SD\Bundle\DocumentsBundle\Entity\Document;

class Uploader {

    protected $em;
    protected $uploadsDir;

    public function __construct($doctrine, $uploadsDir) {
        $this->doctrine = $doctrine;
        $this->uploadsDir = $uploadsDir;
    }

    public function attachUpload($doc, $file, $uploadedByUser = true) {
        $repository = $this->doctrine->getRepository('DocumentsBundle:Document');

        $uploads = $doc->getUploads();
        array_push($uploads, [
                'filename' => $file->getClientOriginalName(),
                'uploadedByUser' => $uploadedByUser,
                'uploaded' => date("Y-m-d H:i:s")
        ]);
        $doc->setUploads($uploads);

        $file->move($this->uploadsDir.'/'.$doc->getSafeId(), $file->getClientOriginalName());
    }
}
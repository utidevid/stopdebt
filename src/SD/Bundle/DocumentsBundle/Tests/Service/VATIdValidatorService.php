<?php
namespace SD\Bundle\DocumentsBundle\Tests\Service;

use SD\Bundle\DocumentsBundle\Service\VATIdValidatorService;

class VATIdValidatorServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var VATIdValidatorService
     */
    private static $service;

    public static function setUpBeforeClass()
    {
        self::$service = new VATIdValidatorService();
    }

    public function testValidateCheckNumProvider()
    {
        return [
            ['1231231231', false],
            ['5345345345', false],
            ['3879300003', true],
            ['3879100006', true]
        ];
    }

    public function testValidateSexProvider()
    {
        return [
            ['0000000000', 0, true],
            ['0000000010', 1, true],
            ['0000000020', 1, false],
            ['0000000030', 0, false]
        ];
    }

    public function testValidateBDayProvider()
    {
        return [
            ['3689100009', '2000-12-31', true],
            ['3879300004', '2006-03-17', true],
            ['0000000006', '1899-12-30', true],
            ['0000000006', '1900-01-01', false],
            ['0000213235', '1900-01-01', true],
            ['0000299994', '1900-01-01', true],
            ['0000199996', '1900-01-01', false],
            ['3879200001', '1900-01-01', false],
        ];
    }

    public function testValidateVatIdProvider()
    {
        return [
            ['3689100003', '2000-12-31', 0, true],
            ['3689100026', '2000-12-31', 0, true],
            ['3689100007', '2000-12-31', 0, false],
            ['3689100009', '2000-12-31', 0, false],
            ['3689100076', '2000-12-31', 0, false],
            ['3689100006', '2002-12-31', 0, false],
            ['3689100006', '2002-12-31', 0, false],
            ['0000000000', '1899-12-30', 0, true],
        ];
    }

    /**
     * @param $vatId
     * @param $bDay
     * @param $correctness
     * @dataProvider testValidateBDayProvider
     */
    public function testValidateBDay($vatId, $bDay, $correctness)
    {
        $this->assertEquals($correctness, self::$service->validateBDay($bDay, $vatId),
            "Birth date was validated wrong");
    }

    /**
     * @param $vatId
     * @param $sex
     * @param $correctness
     * @dataProvider testValidateSexProvider
     */
    public function testValidateSex($vatId, $sex, $correctness)
    {
        $this->assertEquals($correctness, self::$service->validateSex($sex, $vatId), 'Sex was validated wrong');
    }

    /**
     * @param string $vatId
     * @param bool $correctness
     * @dataProvider testValidateCheckNumProvider
     */
    public function testValidateCheckNum($vatId, $correctness)
    {
        $result = self::$service->validateCheckNum($vatId);
        $this->assertEquals($correctness, $result, "Checksum was validated wrong");
    }

    /**
     * @param $vatId
     * @param $bDay
     * @param $sex
     * @param $correctness
     * @dataProvider testValidateVatIdProvider
     */
    public function testValidateVatId($vatId, $bDay, $sex, $correctness)
    {
        $this->assertEquals($correctness, self::$service->validateVatId($vatId, $bDay, $sex),
            "VAT was validated wrong");
    }
} 
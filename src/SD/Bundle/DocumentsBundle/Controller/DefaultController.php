<?php

namespace SD\Bundle\DocumentsBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractUserController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use SD\Bundle\DocumentsBundle\Entity\Document;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use SD\Bundle\CaseBundle\Entity\DebtCase;

class DefaultController extends AbstractUserController
{
    const SECTION_NAME = 'documents';

    public function indexAction($caseId, Request $request)
    {
        $this->loadCaseBySafeId($caseId, true);
        $this->denyAccessUnlessGranted('HAS_DOCUMENTS_ACCESS', $this->activeCase);

        $repo = $this->getDoctrine()->getRepository('DocumentsBundle:Document');

        $form = $this->createFormBuilder()
            ->add('documentTypes', ChoiceType::class, [
                'choices' => Document::getDocumentTypeOptions(),
                'multiple' => true,
                'expanded' => true
            ])
            ->add('filter', SubmitType::class, ['label' => 'Фильтровать', 'attr' => ['class' => 'btn btn-sm btn-primary']])
            ->setMethod('GET')
            ->getForm();

        $form->handleRequest($request);

        $documents = $this->activeCase->getDocuments()->toArray();

        $filterVals = $form['documentTypes']->getData();
        if (!empty($filterVals)) {
            $documents = array_filter($documents, function($i) use ($filterVals) {
                return !empty(
                    array_intersect($i->getDocumentTypes(), $filterVals)
                );
            });
        }

        return $this->render('DocumentsBundle:Default:index.html.twig', [
            'documents' => $documents,
            'form' => $form->createView(),
        ]);
    }

    public function sendManuallyAction($caseId) {
        $this->loadCaseBySafeId($caseId, true);
        $this->denyAccessUnlessGranted('HAS_DOCUMENTS_ACCESS', $this->activeCase);

        if (($this->activeCase->getTariff() == 'optimal' && $this->activeCase->getStage() == DebtCase::OPTIMAL_TARIFF_STAGE_1_UPLOAD_DOCUMENTS) or ($this->activeCase->getTariff() == 'advanced' && $this->activeCase->getStage() == DebtCase::ADVANCED_TARIFF_STAGE_1_UPLOAD_DOCUMENTS)) {
            $this->activeCase->setStage(2);

            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('documents_homepage', [
            'caseId' => $caseId
        ]);
    }

    public function uploadAction($caseId, Request $request)
    {
        $this->loadCaseBySafeId($caseId, true);
        $this->denyAccessUnlessGranted('HAS_DOCUMENTS_ACCESS', $this->activeCase);

        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('DocumentsBundle:Document');
        $uploader = $this->get('documents.uploader');

        $this->loadCaseBySafeId($caseId, true);

        $form = $this->createFormBuilder()
            ->add('documentTypes', ChoiceType::class, [
                'choices' => Document::getDocumentTypeOptions(),
                'multiple' => false,
                'expanded' => false
            ])
            ->add('title', TextType::class)
            ->add('attachment', FileType::class)
            ->add('upload', SubmitType::class, ['label' => 'Загрузить', 'attr' => ['class' => 'btn btn-primary']])
            ->getForm();

        $showFormErrors = false;
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $document = new Document();
                $document->setSafeId($repository->generateSafeId());
                $document->setTitle($form['title']->getData());
                $document->setDocumentTypes([$form['documentTypes']->getData()]);
                $document->setUser($this->getUser());
                $document->setIsApproved(0);
                $document->setCase($this->activeCase);

                $uploader->attachUpload($document, $form['attachment']->getData());

                $em->persist($document);
                $em->flush();

                $this->container->get('assistant.notifier')->notifyNewDocumentUpload($document);

                return $this->redirectToRoute('documents_view', ['caseId' => $caseId, 'documentId' => $document->getSafeId()]);
            } else {
                $showFormErrors = true;
            }
        }

        return $this->render('DocumentsBundle:Default:upload.html.twig', [
            'form' => $form->createView(),
            'showFormErrors' => $showFormErrors
        ]);
    }

    public function quickUploadAction($caseId, $documentId, Request $request) {
        $this->loadCaseBySafeId($caseId, true);
        $this->denyAccessUnlessGranted('HAS_DOCUMENTS_ACCESS', $this->activeCase);

        $document = $this->loadDocumentBySafeId($documentId);

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('attachment', FileType::class)
            ->add('upload', SubmitType::class, ['label' => 'Загрузить', 'attr' => ['class' => 'btn btn-primary']])
            ->getForm();

        $form->handleRequest($request);
        $uploader = $this->get('documents.uploader');
        $uploader->attachUpload($document, $form['attachment']->getData());
        $em->flush();

        $this->container->get('assistant.notifier')->notifyNewDocumentUpload($document);

        return $this->redirectToRoute('documents_view', [
            'caseId' => $caseId,
            'documentId' => $documentId
        ]);
    }

    public function viewAction($caseId, $documentId)
    {
        $this->loadCaseBySafeId($caseId, true);
        $this->denyAccessUnlessGranted('HAS_DOCUMENTS_ACCESS', $this->activeCase);

        $document = $this->loadDocumentBySafeId($documentId);

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('documents_quick_upload', [
                    'caseId' => $caseId,
                    'documentId' => $documentId
                ]))
            ->add('attachment', FileType::class)
            ->add('upload', SubmitType::class, ['label' => 'Загрузить', 'attr' => ['class' => 'btn btn-primary']])
            ->getForm();

        return $this->render('DocumentsBundle:Default:view.html.twig', [
            'document' => $document,
            'form' => $form->createView(),
        ]);
    }

    public function rawAction($caseId, $documentId, $index)
    {
        $this->loadCaseBySafeId($caseId, true);
        $this->denyAccessUnlessGranted('HAS_DOCUMENTS_ACCESS', $this->activeCase);

        $repo = $this->getDoctrine()->getRepository('DocumentsBundle:Document');

        $document = $this->loadDocumentBySafeId($documentId);

        $fileInfo = $document->getUploads()[$index];

        $fullpath = $this->getParameter('documents.uploads_dir').'/'.$document->getSafeId().'/'.$fileInfo['filename'];
        #todo let nginx do this
        if (file_exists($fullpath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($fullpath).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($fullpath));
            readfile($fullpath);
            exit;
        }
    }
}

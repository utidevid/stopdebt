<?php

namespace SD\Bundle\DocumentsBundle\Entity;

/**
 * Document
 */
class Document
{

    public static function getDocumentTypeOptions() {
        return [
         1 => 'Паспорт',
         2 => 'Идентификационный код',
         3 => 'Соглашения',
         4 => 'Консультации',
         5 => 'Кредитные',
         6 => 'Переговоры',
         7 => 'Судебные'
        ];
    }

    public function getDocumentTypesStr() {
        $options = self::getDocumentTypeOptions();

        return join(array_map(function($i) use ($options) {
            return $options[$i];
        }, $this->getDocumentTypes()), ', ');
    }

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $safeId;

    /**
     * @var \DateTime
     */
    private $uploadedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set safeId
     *
     * @param string $safeId
     *
     * @return Document
     */
    public function setSafeId($safeId)
    {
        $this->safeId = $safeId;

        return $this;
    }

    /**
     * Get safeId
     *
     * @return string
     */
    public function getSafeId()
    {
        return $this->safeId;
    }


     /**
     * Get uploadedAt
     *
     * @return \DateTime
     */
    public function getUploadedAt()
    {
        return $this->uploadedAt;
    }

    /**
     * Set uploadedAt
     *
     * @param \DateTime $uploadedAt
     *
     * @return Questionnaire
     */
    public function setUploadedAt()
    {
        $this->uploadedAt = new \DateTime();

        return $this;
    }

    /**
     * @var \SD\Bundle\UserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \SD\Bundle\UserBundle\Entity\User $user
     *
     * @return Document
     */
    public function setUser(\SD\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \SD\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var array
     */
    private $documentTypes;


    /**
     * Set documentTypes
     *
     * @param array $documentTypes
     *
     * @return Document
     */
    public function setDocumentTypes($documentTypes)
    {
        $this->documentTypes = $documentTypes;

        return $this;
    }

    /**
     * Get documentTypes
     *
     * @return array
     */
    public function getDocumentTypes()
    {
        return $this->documentTypes ? $this->documentTypes : [];
    }
    /**
     * @var string
     */
    private $title;

    /**
     * @var integer
     */
    private $isApproved;


    /**
     * Set title
     *
     * @param string $title
     *
     * @return Document
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set isApproved
     *
     * @param integer $isApproved
     *
     * @return Document
     */
    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;

        return $this;
    }

    /**
     * Get isApproved
     *
     * @return integer
     */
    public function getIsApproved()
    {
        return $this->isApproved;
    }


    /**
     * @var array
     */
    private $uploads;

    /**
     * @var \SD\Bundle\CaseBundle\Entity\DebtCase
     */
    private $case;


    /**
     * Set uploads
     *
     * @param array $uploads
     *
     * @return Document
     */
    public function setUploads($uploads)
    {
        $this->uploads = $uploads;

        return $this;
    }

    /**
     * Get uploads
     *
     * @return array
     */
    public function getUploads()
    {
        return empty($this->uploads) ? [] : $this->uploads;
    }

    /**
     * Set case
     *
     * @param \SD\Bundle\CaseBundle\Entity\DebtCase $case
     *
     * @return Document
     */
    public function setCase(\SD\Bundle\CaseBundle\Entity\DebtCase $case = null)
    {
        $this->case = $case;

        return $this;
    }

    /**
     * Get case
     *
     * @return \SD\Bundle\CaseBundle\Entity\DebtCase
     */
    public function getCase()
    {
        return $this->case;
    }
}

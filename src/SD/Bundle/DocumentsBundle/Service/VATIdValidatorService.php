<?php
namespace SD\Bundle\DocumentsBundle\Service;

/**
 * Ukrainian VAT ID validation functions.
 * <i> If the algorithm doesn't validate ID,it doesn't mean the ID is wrong. Some
 * non strictly defined exceptions can happen during real life VAT ID generation. </i>
 * Class VATIdValidatorService
 * @package SD\Bundle\DocumentsBundle\Service
 */
class VATIdValidatorService
{
    /**
     * Validates check number (last one)
     * @param string $vatId VAT identification code
     * @return bool valid or not
     */
    public function validateCheckNum($vatId)
    {
        $checksum = array_reduce(
            array_map(function($a, $b){
                return $a * $b;
            }, str_split($vatId), [-1, 5, 7, 9, 4, 6, 10, 5, 7]),
            function($carry, $item) {
                return $carry + $item;
            },
            0
        );

        $checkNum = $checksum % 11 % 10;

        return $checkNum == $vatId[strlen($vatId) - 1];
    }

    /**
     * Returns true if birthday corresponds to the encoded in VAT number, false otherwise
     * @param string $bDay 'yyyy-mm-dd'
     * @param string $vatId VAT ID
     * @return bool
     */
    public function validateBday($bDay, $vatId)
    {
        $standard = new \DateTime('1899-12-30');
        $bDayDt = new \DateTime($bDay);
        $diff = date_diff($standard, $bDayDt);

        return $diff->days == join('', array_slice(str_split($vatId), 0, 5));
    }

    /**
     * Validates vat id sex. Returns true if encoded sex is correct
     * @param int $sex 1 - male , 0 - female
     * @param string $vatId VAT ID
     * @return bool
     */
    public function validateSex($sex, $vatId)
    {
        $sexCode = (int)str_split($vatId)[8];
        return ($sexCode & 1) == $sex;// odd - male, even - female
    }

    /**
     * Validates VAT ID
     * @param string $vatId
     * @param string $bDay 'yyyy-mm-dd'
     * @param $sex 1 - male, 0 - female
     * @return bool
     */
    public function validateVatId($vatId, $bDay, $sex)
    {
        return $this->validateCheckNum($vatId) && $this->validateSex($sex, $vatId) && $this->validateBday($bDay,
            $vatId);
    }

    public function validateVatIdNotSex($vatId, $bday)
    {
        return $this->validateCheckNum($vatId) && $this->validateBday($bday, $vatId);
    }
} 
<?php

namespace SD\Bundle\CaseBundle\Service;

use Doctrine\ORM\EntityManager;
use SD\Bundle\CaseBundle\Entity\DebtCase;
use SD\Bundle\DocumentsBundle\Entity\Document;
use SD\Bundle\QuestionnaireBundle\Entity\Questionnaire;
use SD\Bundle\UserBundle\Entity\User;
use SD\Bundle\UtilsBundle\Services\Notifier;
use Symfony\Component\Routing\Router;

class DebtCaseService
{
    /** @var EntityManager  */
    protected $objectManager;
    protected $router;
    protected $notifier;

    public function __construct(EntityManager $objectManager, Router $router, Notifier $notifier)
    {
        $this->objectManager = $objectManager;
        $this->notifier = $notifier;
        $this->router = $router;
    }

    /**
     * Creates a case
     * @param User $user
     * @param $tariff
     * @param $billed
     * @return DebtCase
     */
    public function createCase(User $user, $tariff, $billed)
    {
        if (!in_array($tariff, DebtCase::availableTarrifs())) {
            throw new \InvalidArgumentException($tariff . ' tariff doesn\'t exist');
        }

        $case = new DebtCase();
        $case->setUser($user);
        $case->setTariff($tariff);
        $case->setBilled($billed);

        $bonusCaseMode = count(array_filter($user->getCases()->toArray(), function($c) {
            return $c->getBilled() && in_array($c->getTariff(), ['optimal', 'advanced']);
        })) > 0;

        if  ($bonusCaseMode) {
            $case->setBilled(1);
        }

        if ($tariff == 'standard') {
            $case->setStage(DebtCase::STANDARD_TARIFF_STAGE_1_EDIT_QUESTIONNAIRE);
        } elseif ($tariff == 'optimal') {
            $case->setStage(DebtCase::OPTIMAL_TARIFF_STAGE_1_UPLOAD_DOCUMENTS);
        } elseif ($tariff == 'advanced') {
            $case->setStage(DebtCase::ADVANCED_TARIFF_STAGE_1_UPLOAD_DOCUMENTS);
        }

        $case->setSafeId($this->objectManager->getRepository('CaseBundle:DebtCase')->generateSafeId());
        $case->setTitle($case->getSafeId());
        $case->setStatus(0); // todo constant
        $case->setCreatedAt(new \DateTime());

        $questionnaire = new Questionnaire();

        $questionnaire->setSafeId($this->objectManager->getRepository('QuestionnaireBundle:Questionnaire')
            ->generateSafeId());
        $questionnaire->setStatus(Questionnaire::PENDING_STATUS);
        $questionnaire->setUser($user);
        $questionnaire->setCase($case);
        $questionnaire->setLastStep(1);

        $case->setQuestionnaire($questionnaire);

        $this->objectManager->persist($case);
        $this->objectManager->persist($questionnaire);

        if ($tariff == 'optimal' || $tariff == 'advanced') {
            //todo captions
            $necessaryDocs = [
                'Паспорт' => [1],
                'Идентификационный код' => [2]
            ];

            $shift = 0;
            foreach ($necessaryDocs as $docTitle => $docTypes) {
                $document = new Document();
                $document->setSafeId($this->objectManager->getRepository('DocumentsBundle:Document')->generateSafeId($shift++));
                $document->setTitle($docTitle);
                $document->setDocumentTypes($docTypes);
                $document->setUser($user);
                $document->setIsApproved(0);
                $document->setCase($case);

                $this->objectManager->persist($document);
            }

            // todo: can't give the link before payment
            // $this->notifier->pushNotification($user, 'В кейсе '.$case->getSafeId().' нужно загрузить документы!', $this->router->generate('documents_homepage', ['caseId' => $case->getSafeId()], true), false);
        }

        $this->objectManager->flush();
        return $case;
    }
}
<?php

namespace SD\Bundle\CaseBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractUserController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use SD\Bundle\CaseBundle\Entity\DebtCase;
use SD\Bundle\QuestionnaireBundle\Entity\Questionnaire;
use SD\Bundle\DocumentsBundle\Entity\Document;

class DefaultController extends AbstractUserController
{
    const SECTION_NAME = 'case';

    public function createAction(Request $request)
    {

        $form = $this->createFormBuilder()->setAction($this->generateUrl('case_create_new'))
            ->add('tariff', ChoiceType::class, [
                'choices' => DebtCase::tariffsOptions(),
                'multiple' => false,
                'expanded' => false
            ])
            ->add('add', SubmitType::class, ['label' => 'Выбрать', 'attr' => ['class' => 'btn btn-primary']])->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            $case = $this->get('sd_debt_case_service')->createCase($this->getUser(), $form['tariff']->getData(), 0);

            $this->addFlash('notice','Ваш кейс был создан успешно!');

            return $this->redirectToRoute('questionnaire_homepage', [
                'caseId' => $case->getSafeId()
            ]);
        }

        return $this->render('CaseBundle:Default:create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function switchCaseAction($caseId) {
        $this->loadCaseBySafeId($caseId);

        return $this->redirectToRoute('dashboard_homepage', [
            'caseId' => $caseId
        ]);
    }
}

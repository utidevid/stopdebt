<?php

namespace SD\Bundle\CaseBundle\Tests\Controller;

use SD\Bundle\UtilsBundle\Tests\WebTestCase;

class DefaultControllerTest extends WebTestCase
{

    public function testCreate()
    {
        $this->client->loginRealUser();

        $crawler = $this->client->request('GET', '/cases/create');
        $this->assertTrue($this->client->getResponse()->isSuccessful());

        $form = $crawler->selectButton('Выбрать')->form(array(
            'form[tariff]' => 'standard',
        ));

        $this->client->submit($form);
        $this->client->followRedirect();
        $this->client->followRedirect();
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertContains('Кейс', $this->client->getResponse()->getContent());
    }


}

<?php

namespace SD\Bundle\CaseBundle\Entity;

/**
 * Cases
 */
class DebtCase
{

    const STANDARD_TARIFF_STAGE_1_EDIT_QUESTIONNAIRE =  1;
    const STANDARD_TARIFF_STAGE_2_VIEW_STRATEGY =  2;

    const OPTIMAL_TARIFF_STAGE_1_UPLOAD_DOCUMENTS =  1;
    const OPTIMAL_TARIFF_STAGE_2_EDIT_QUESTIONNAIRE =  2;
    const OPTIMAL_TARIFF_STAGE_3_SKYPE_CALL =  3;
    const OPTIMAL_TARIFF_STAGE_4_PREPARING_STRATEGY =  4;
    const OPTIMAL_TARIFF_STAGE_5_VIEW_STRATEGY =  5;

    const ADVANCED_TARIFF_STAGE_1_UPLOAD_DOCUMENTS =  1;
    const ADVANCED_TARIFF_STAGE_2_ASSISTANT_EDIT_QUESTIONNAIRE =  2;
    const ADVANCED_TARIFF_STAGE_3_SKYPE_CALL =  3;
    const ADVANCED_TARIFF_STAGE_4_REVIEW_QUESTIONNAIRE =  4;
    const ADVANCED_TARIFF_STAGE_5_INTERVIEW =  5;
    const ADVANCED_TARIFF_STAGE_6_PREPARING_STRATEGY =  6;
    const ADVANCED_TARIFF_STAGE_7_VIEW_STRATEGY =  7;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Cases
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Cases
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Cases
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * @var string
     */
    private $safeId;


    /**
     * Set safeId
     *
     * @param string $safeId
     *
     * @return DebtCase
     */
    public function setSafeId($safeId)
    {
        $this->safeId = $safeId;

        return $this;
    }

    /**
     * Get safeId
     *
     * @return string
     */
    public function getSafeId()
    {
        return $this->safeId;
    }
    /**
     * @var \SD\Bundle\UserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \SD\Bundle\UserBundle\Entity\User $user
     *
     * @return DebtCase
     */
    public function setUser(\SD\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \SD\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var \SD\Bundle\QuestionnaireBundle\Entity\Questionnaire
     */
    private $questionnaire;


    /**
     * Set questionnaire
     *
     * @param \SD\Bundle\QuestionnaireBundle\Entity\Questionnaire $questionnaire
     *
     * @return DebtCase
     */
    public function setQuestionnaire(\SD\Bundle\QuestionnaireBundle\Entity\Questionnaire $questionnaire = null)
    {
        $this->questionnaire = $questionnaire;

        return $this;
    }

    /**
     * Get questionnaire
     *
     * @return \SD\Bundle\QuestionnaireBundle\Entity\Questionnaire
     */
    public function getQuestionnaire()
    {
        return $this->questionnaire;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $strategy_items;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->strategy_items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add strategyItem
     *
     * @param \SD\Bundle\AssistantBundle\Entity\StrategyItem $strategyItem
     *
     * @return DebtCase
     */
    public function addStrategyItem(\SD\Bundle\AssistantBundle\Entity\StrategyItem $strategyItem)
    {
        $this->strategy_items[] = $strategyItem;

        return $this;
    }

    /**
     * Remove strategyItem
     *
     * @param \SD\Bundle\AssistantBundle\Entity\StrategyItem $strategyItem
     */
    public function removeStrategyItem(\SD\Bundle\AssistantBundle\Entity\StrategyItem $strategyItem)
    {
        $this->strategy_items->removeElement($strategyItem);
    }

    /**
     * Get strategyItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStrategyItems()
    {
        return $this->strategy_items;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $documents;


    /**
     * Add document
     *
     * @param \SD\Bundle\DocumentsBundle\Entity\Document $document
     *
     * @return DebtCase
     */
    public function addDocument(\SD\Bundle\DocumentsBundle\Entity\Document $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \SD\Bundle\DocumentsBundle\Entity\Document $document
     */
    public function removeDocument(\SD\Bundle\DocumentsBundle\Entity\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $reminders;


    /**
     * Add reminder
     *
     * @param \SD\Bundle\AssistantBundle\Entity\Reminder $reminder
     *
     * @return DebtCase
     */
    public function addReminder(\SD\Bundle\AssistantBundle\Entity\Reminder $reminder)
    {
        $this->reminders[] = $reminder;

        return $this;
    }

    /**
     * Remove reminder
     *
     * @param \SD\Bundle\AssistantBundle\Entity\Reminder $reminder
     */
    public function removeReminder(\SD\Bundle\AssistantBundle\Entity\Reminder $reminder)
    {
        $this->reminders->removeElement($reminder);
    }

    /**
     * Get reminders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReminders()
    {
        return $this->reminders;
    }

    /**
     * Set stage
     *
     * @param integer $stage
     *
     * @return DebtCase
     */
    public function setStage($stage)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage
     *
     * @return integer
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * @var integer
     */
    private $stage;

    /**
     * @var string
     */
    private $tariff;

    public static function availableTarrifs() {
        return [
            'standard', 'optimal', 'advanced'
        ];
    }

    public static function tariffsOptions() {
        return [
            'standard' => 'Standard',
            'optimal' => 'Optimal',
            'advanced' => 'Advanced'
        ];
    }


    /**
     * Set tariff
     *
     * @param string $tariff
     *
     * @return DebtCase
     */
    public function setTariff($tariff)
    {
        $this->tariff = $tariff;

        return $this;
    }

    /**
     * Get tariff
     *
     * @return string
     */
    public function getTariff()
    {
        return $this->tariff;
    }
    /**
     * @var string
     */
    private $custom_strategy = '';


    /**
     * Set customStrategy
     *
     * @param string $customStrategy
     *
     * @return DebtCase
     */
    public function setCustomStrategy($customStrategy)
    {
        $this->custom_strategy = $customStrategy;

        return $this;
    }

    /**
     * Get customStrategy
     *
     * @return string
     */
    public function getCustomStrategy()
    {
        return $this->custom_strategy;
    }
    /**
     * @var boolean
     */
    private $billed = 0;


    /**
     * Set billed
     *
     * @param boolean $billed
     *
     * @return DebtCase
     */
    public function setBilled($billed)
    {
        $this->billed = $billed;

        return $this;
    }

    /**
     * Get billed
     *
     * @return boolean
     */
    public function getBilled()
    {
        return $this->billed;
    }
    /**
     * @var \SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction
     */
    private $liqpay_transactions;


    /**
     * Set liqpayTransactions
     *
     * @param \SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction $liqpayTransactions
     *
     * @return DebtCase
     */
    public function setLiqpayTransactions(\SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction $liqpayTransactions = null)
    {
        $this->liqpay_transactions = $liqpayTransactions;

        return $this;
    }

    /**
     * Get liqpayTransactions
     *
     * @return \SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction
     */
    public function getLiqpayTransactions()
    {
        return $this->liqpay_transactions;
    }

    public function getUniqueLiqpayTransactions()
    {
        $list = $this->getLiqpayTransactions()->toArray();
        $buffer = [];


        return array_filter($list, function($i) use (&$buffer) {
            $key = $i->getTransactionId().'_'.$i->getPaymentId().'_'.$i->getAmount().'_'.$i->getStatus();
            $isThere = isset($buffer[$key]);
            $buffer[$key] = 1;
            return !$isThere;
        });
    }
    /**
     * @var integer
     */
    private $first_payment = 0;


    /**
     * Set firstPayment
     *
     * @param integer $firstPayment
     *
     * @return DebtCase
     */
    public function setFirstPayment($firstPayment)
    {
        $this->first_payment = $firstPayment;

        return $this;
    }

    /**
     * Get firstPayment
     *
     * @return integer
     */
    public function getFirstPayment()
    {
        return $this->first_payment;
    }

    /**
     * Add liqpayTransaction
     *
     * @param \SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction $liqpayTransaction
     *
     * @return DebtCase
     */
    public function addLiqpayTransaction(\SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction $liqpayTransaction)
    {
        $this->liqpay_transactions[] = $liqpayTransaction;

        return $this;
    }

    /**
     * Remove liqpayTransaction
     *
     * @param \SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction $liqpayTransaction
     */
    public function removeLiqpayTransaction(\SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction $liqpayTransaction)
    {
        $this->liqpay_transactions->removeElement($liqpayTransaction);
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $custom_services;


    /**
     * Add customService
     *
     * @param \SD\Bundle\CaseBundle\Entity\CustomService $customService
     *
     * @return DebtCase
     */
    public function addCustomService(\SD\Bundle\CaseBundle\Entity\CustomService $customService)
    {
        $this->custom_services[] = $customService;

        return $this;
    }

    /**
     * Remove customService
     *
     * @param \SD\Bundle\CaseBundle\Entity\CustomService $customService
     */
    public function removeCustomService(\SD\Bundle\CaseBundle\Entity\CustomService $customService)
    {
        $this->custom_services->removeElement($customService);
    }

    /**
     * Get customServices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomServices()
    {
        return $this->custom_services;
    }
}

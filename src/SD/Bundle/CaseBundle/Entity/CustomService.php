<?php

namespace SD\Bundle\CaseBundle\Entity;

/**
 * CustomService
 */
class CustomService
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $safeId;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var bool
     */
    private $billed;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CustomService
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CustomService
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set safeId
     *
     * @param string $safeId
     *
     * @return CustomService
     */
    public function setSafeId($safeId)
    {
        $this->safeId = $safeId;

        return $this;
    }

    /**
     * Get safeId
     *
     * @return string
     */
    public function getSafeId()
    {
        return $this->safeId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CustomService
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set billed
     *
     * @param boolean $billed
     *
     * @return CustomService
     */
    public function setBilled($billed)
    {
        $this->billed = $billed;

        return $this;
    }

    /**
     * Get billed
     *
     * @return bool
     */
    public function getBilled()
    {
        return $this->billed;
    }
    /**
     * @var \SD\Bundle\UserBundle\Entity\User
     */
    private $user;

    /**
     * @var \SD\Bundle\CaseBundle\Entity\DebtCase
     */
    private $case;


    /**
     * Set user
     *
     * @param \SD\Bundle\UserBundle\Entity\User $user
     *
     * @return CustomService
     */
    public function setUser(\SD\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \SD\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set case
     *
     * @param \SD\Bundle\CaseBundle\Entity\DebtCase $case
     *
     * @return CustomService
     */
    public function setCase(\SD\Bundle\CaseBundle\Entity\DebtCase $case = null)
    {
        $this->case = $case;

        return $this;
    }

    /**
     * Get case
     *
     * @return \SD\Bundle\CaseBundle\Entity\DebtCase
     */
    public function getCase()
    {
        return $this->case;
    }
    /**
     * @var integer
     */
    private $amount;


    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return CustomService
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $liqpay_transactions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->liqpay_transactions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add liqpayTransaction
     *
     * @param \SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction $liqpayTransaction
     *
     * @return CustomService
     */
    public function addLiqpayTransaction(\SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction $liqpayTransaction)
    {
        $this->liqpay_transactions[] = $liqpayTransaction;

        return $this;
    }

    /**
     * Remove liqpayTransaction
     *
     * @param \SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction $liqpayTransaction
     */
    public function removeLiqpayTransaction(\SD\Bundle\PaymentsBundle\Entity\LiqPayTransaction $liqpayTransaction)
    {
        $this->liqpay_transactions->removeElement($liqpayTransaction);
    }

    /**
     * Get liqpayTransactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLiqpayTransactions()
    {
        return $this->liqpay_transactions;
    }

    public function getUniqueLiqpayTransactions()
    {
        $list = $this->getLiqpayTransactions()->toArray();
        $buffer = [];

        return array_filter($list, function($i) use (&$buffer) {
            $key = $i->getTransactionId().'_'.$i->getPaymentId().'_'.$i->getAmount().'_'.$i->getStatus();
            $isThere = isset($buffer[$key]);
            $buffer[$key] = 1;
            return !$isThere;
        });
    }
}

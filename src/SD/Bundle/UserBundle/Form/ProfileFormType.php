<?php
namespace SD\Bundle\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;

class ProfileFormType extends \FOS\UserBundle\Form\Type\ProfileFormType
{

    /**
     * Builds the embedded form representing the user.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array('label' => 'form.email', 'translation_domain' => 'UserBundle',
                'disabled' => true))
            ->add('firstName', 'text', array('label' => 'form.firstName', 'translation_domain' => 'UserBundle'))
            ->add('lastName', 'text', array('label' => 'form.lastName', 'translation_domain' => 'UserBundle'))
            ->add('patronymic', 'text', array('label' => 'form.patronymic', 'translation_domain' => 'UserBundle'))
            ->add('locality', 'text', array('label' => 'form.locality', 'translation_domain' => 'UserBundle'))
            ->add('phoneNumber', 'text', array('label' => 'form.phoneNumber', 'translation_domain' => 'UserBundle',
                'attr' => ['placeholder' => '+380 (00) 000 00 00', 'class' => 'bfh-phone',
                    'data-format' => '+38 (ddd) ddd dd dd']));
    }
}
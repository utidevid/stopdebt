<?php
namespace SD\Bundle\UserBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

use SD\Bundle\UserBundle\Entity\User;
use SD\Bundle\CaseBundle\Entity\DebtCase;
use SD\Bundle\QuestionnaireBundle\Entity\Questionnaire;

class AppVoter extends Voter
{
    const CAN_CREATE_NEW_CASE = 'CAN_CREATE_NEW_CASE';
    const CAN_EDIT_QUESTIONNAIRE = 'CAN_EDIT_QUESTIONNAIRE';
    const CAN_VIEW_QUESTIONNAIRE = 'CAN_VIEW_QUESTIONNAIRE';
    const HAS_DOCUMENTS_ACCESS = 'HAS_DOCUMENTS_ACCESS';
    const HAS_STATEGY_ACCESS = 'HAS_STATEGY_ACCESS';
    const HAS_REMINDERS_ACCESS = 'HAS_REMINDERS_ACCESS';

    const HAS_REGULAR_ACCESS = 'HAS_REGULAR_ACCESS';
    const HAS_ASSISTANT_ACCESS = 'HAS_ASSISTANT_ACCESS';


    protected function getAvailableAttributes() {
        return [
            self::CAN_CREATE_NEW_CASE,
            self::CAN_EDIT_QUESTIONNAIRE,
            self::CAN_VIEW_QUESTIONNAIRE,
            self::HAS_DOCUMENTS_ACCESS,
            self::HAS_STATEGY_ACCESS,
            self::HAS_REMINDERS_ACCESS,
            self::HAS_REGULAR_ACCESS,
            self::HAS_ASSISTANT_ACCESS
        ];
    }

    protected function supports($attribute, $subject = NULL)
    {
        if (!in_array($attribute, $this->getAvailableAttributes())) {
            return false;
        }

        if (is_null($subject)) {
            return true;
        } elseif ($subject instanceof DebtCase) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute($attribute, $subject = NULL, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch($attribute) {
            case self::CAN_CREATE_NEW_CASE:
                return $this->canCreateNewCase($user);
            case self::CAN_EDIT_QUESTIONNAIRE:
                return $this->canEditQuestionnaire($user, $subject);
            case self::CAN_VIEW_QUESTIONNAIRE:
                return $this->canViewQuestionnaire($user, $subject);
            case self::HAS_DOCUMENTS_ACCESS:
                return $this->hasDocumentsAccess($user, $subject);
            case self::HAS_STATEGY_ACCESS:
                return $this->hasStrategyAccess($user, $subject);
            case self::HAS_REMINDERS_ACCESS:
                return $this->hasRemindersAccess($user, $subject);
            case self::HAS_REGULAR_ACCESS:
                return $this->hasRegularAccess($user);
            case self::HAS_ASSISTANT_ACCESS:
                return $this->hasAssistantAccess($user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function hasRegularAccess(User $user)
    {
        return $user->hasRole('ROLE_USER');
    }

    private function hasAssistantAccess(User $user)
    {
        return $user->hasRole('ROLE_ASSISTANT');
    }


    private function canCreateNewCase(User $user)
    {
        return $user->getCases()->count() == 0;
    }

    private function canEditQuestionnaire(User $user, DebtCase $case)
    {
        if (!$user->getIsOfferAccepted()) {
            return false;
        }

        if (!$case->getBilled()) {
            return false;
        }

        if ($case->getTariff() == 'standard') {
            return $case->getStage() == DebtCase::STANDARD_TARIFF_STAGE_1_EDIT_QUESTIONNAIRE;
        }  elseif ($case->getTariff() == 'optimal') {
            return $case->getStage() == DebtCase::OPTIMAL_TARIFF_STAGE_2_EDIT_QUESTIONNAIRE;
        } else {
            return false;
        }
    }

    private function canViewQuestionnaire(User $user, DebtCase $case)
    {
        if (!$user->getIsOfferAccepted()) {
            return false;
        }

        if (!$case->getBilled()) {
            return false;
        }

        if ($case->getTariff() == 'standard') {
            return $case->getStage() == DebtCase::STANDARD_TARIFF_STAGE_2_VIEW_STRATEGY;
        }  elseif ($case->getTariff() == 'optimal') {
            return $case->getStage() == DebtCase::OPTIMAL_TARIFF_STAGE_3_SKYPE_CALL;
        }  elseif ($case->getTariff() == 'advanced') {
            return $case->getStage() >= DebtCase::ADVANCED_TARIFF_STAGE_4_REVIEW_QUESTIONNAIRE;
        }
        return false;
    }

    private function hasDocumentsAccess(User $user, DebtCase $case)
    {
        if (!$user->getIsOfferAccepted()) {
            return false;
        }

        if (!$case->getBilled()) {
            return false;
        }

        if ($case->getTariff() == 'standard') {
            return false;
        }  elseif ($case->getTariff() == 'optimal') {
            return true;
        }  elseif ($case->getTariff() == 'advanced') {
            return true;
        }
        return false;
    }

    private function hasStrategyAccess(User $user, DebtCase $case)
    {
        if (!$user->getIsOfferAccepted()) {
            return false;
        }

        if (!$case->getBilled()) {
            return false;
        }

        if ($case->getTariff() == 'standard') {
            return $case->getStage() == DebtCase::STANDARD_TARIFF_STAGE_2_VIEW_STRATEGY;
        }  elseif ($case->getTariff() == 'optimal') {
            return $case->getStage() == DebtCase::OPTIMAL_TARIFF_STAGE_5_VIEW_STRATEGY;
        }  elseif ($case->getTariff() == 'advanced') {
            return $case->getStage() == DebtCase::ADVANCED_TARIFF_STAGE_7_VIEW_STRATEGY;
        }
        return false;
    }

    private function hasRemindersAccess(User $user, DebtCase $case)
    {
        if (!$user->getIsOfferAccepted()) {
            return false;
        }

        if (!$case->getBilled()) {
            return false;
        }

        if ($case->getReminders()->count() > 0) {
            return true;
        }

        if ($case->getTariff() == 'standard') {
            return false;
        } elseif ($case->getTariff() == 'optimal') {
            return $case->getStage() >= DebtCase::OPTIMAL_TARIFF_STAGE_3_SKYPE_CALL && $case->getReminders()->count() > 0;
        } elseif ($case->getTariff() == 'advanced') {
            return $case->getStage() >= DebtCase::ADVANCED_TARIFF_STAGE_2_ASSISTANT_EDIT_QUESTIONNAIRE && $case->getReminders()->count() > 0;
        }
        return false;
    }

}
<?php
namespace SD\Bundle\UserBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\User\UserInterface;
use SD\Bundle\UserBundle\Entity\User;

class FOSUBUserProvider extends BaseClass
{
    const TWITTER_RESOURCE_OWNER = 'twitter';
    const FACEBOOK_RESOURCE_OWNER = 'facebook';
    const VKONTAKTE_RESOURCE_OWNER = 'vkontakte';

    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $username = $response->getUsername();

        //on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();

        $setter = 'set'.ucfirst($service);
        $setter_id = $setter.'Id';
        $setter_token = $setter.'AccessToken';

        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }

        //we connect current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());

        $this->userManager->updateUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $email = $response->getEmail();

        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));

        // when user is registering
        if (null === $user) {
            // Retrieve specific social network functions
            $service = $response->getResourceOwner()->getName();
            $setter = 'set'.ucfirst($service);
            $setter_id = $setter.'Id';
            $setter_token = $setter.'AccessToken';

            // Check if user with such email already exists
            if ($user === null && !empty($response->getEmail()))
                $user = $this->userManager->findUserByEmail($response->getEmail());

            // If the user is not found by retrieved email address, create a new one
            if ($user === null)
                $user = $this->userManager->createUser();

            // Set social network's ID and token
            $user->$setter_id($username);
            $user->$setter_token($response->getAccessToken());

            // Set user data
            // Retrieve first and last names from twitter
            if ($response->getResourceOwner()->getName() == self::TWITTER_RESOURCE_OWNER) {
                $fullname = $response->getRealName();

                if (!empty($fullname)) {
                    $fullname = explode(' ', trim($fullname));

                    if (empty($user->getFirstName()) && isset($fullname[0]) && !empty($fullname[0]))
                        $user->setFirstName($fullname[0]);

                    if (empty($user->getLastName()) && isset($fullname[1]) && !empty($fullname[1]))
                        $user->setLastName($fullname[1]);
                }
            }
            // Retrieve first and last names by default
            else {
                if (empty($user->getFirstName()))
                    $user->setFirstName($response->getFirstName());

                if (empty($user->getLastName()))
                    $user->setLastName($response->getLastName());
            }

            if (empty($user->getEmail()))
                $user->setEmail($email);
            
            $user->setPlainPassword('no pass');
            $user->setEnabled(true);

            // Save user
            $this->userManager->updateUser($user);

            return $user;
        }

        //if user exists - go with the HWIOAuth way
        $user = parent::loadUserByOAuthUserResponse($response);

        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';

        //update access token
        $user->$setter($response->getAccessToken());

        return $user;
    }

}

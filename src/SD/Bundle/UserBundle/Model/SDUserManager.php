<?php
namespace SD\Bundle\UserBundle\Model;

use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\CanonicalizerInterface;
use FOS\UserBundle\Util\TokenGenerator;
use SD\Bundle\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class SDUserManager extends UserManager
{
    /** @var  TokenGenerator */
    protected $tokenGenerator;

    public function __construct(EncoderFactoryInterface $encoderFactory,
                                CanonicalizerInterface $usernameCanonicalizer,
                                CanonicalizerInterface $emailCanonicalizer, ObjectManager $om, $class,
                                TokenGenerator $tokenGenerator)
    {
        $this->tokenGenerator = $tokenGenerator;
        parent::__construct($encoderFactory, $usernameCanonicalizer, $emailCanonicalizer, $om, $class);
    }

    protected function canonicalizeUsername($username)
    {
        if (empty($username)) {
            return null;
        } else {
            parent::canonicalizeUsername($username);
        }
    }

    /**
     * Finds a user by email
     *
     * @param string $username
     *
     * @return UserInterface
     */
    public function findUserByUsername($username)
    {
        return $this->findUserBy(array('emailCanonical' => $this->canonicalizeUsername($username)));
    }

    /**
     * @return UserInterface|User
     */
    public function createUser()
    {
        return new User();
    }

    public function autoSignUpUser($email, $firstName = null, $lastName = null, $phoneNumber = null, $patronymic = null,
                                   $locality = null)
    {
        $user = $this->createUser();
        $user->setEmail($email);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setPhoneNumber($phoneNumber);
        $user->setPatronymic($patronymic);
        $user->setLocality($locality);
        $user->getPlainPassword(substr($this->tokenGenerator->generateToken(), 0, 8));

        $this->objectManager->persist($user);
        $this->objectManager->flush($user);

        return $user;
    }
}
<?php

namespace SD\Bundle\UserBundle\Tests\Controller;

use SD\Bundle\UtilsBundle\Tests\WebTestCase;

class ProfileControllerTest extends WebTestCase
{

    public function testIndex()
    {
        $this->client->loginRealUser();
        $crawler = $this->client->request('GET', '/profile/');
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

}

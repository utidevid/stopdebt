<?php

namespace SD\Bundle\UserBundle\Tests\Controller;

use SD\Bundle\UtilsBundle\Tests\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testLogin()
    {
        $crawler = $this->client->request('GET', '/login');
        $this->assertContains('Вход', $this->client->getResponse()->getContent());
        $this->assertContains('Войти', $this->client->getResponse()->getContent());

        $form = $crawler->selectButton('Войти')->form(array(
            '_username' => 'test@test.com',
            '_password' => 'test',
        ));

        $this->client->submit($form);

        $this->client->followRedirect(); // redirects to "/""
        $this->client->followRedirect(); // redirects to "/dashboard"

        $this->assertContains('Кейс', $this->client->getResponse()->getContent());
        $this->assertContains('C-000-1', $this->client->getResponse()->getContent());
    }

    public function testRegularUser() {
        $this->client->loginRealUser();

        $crawler = $this->client->request('GET', '/cases/C-000-1/dashboard');
        $this->assertTrue($this->client->getResponse()->isSuccessful());

        $crawler = $this->client->request('GET', '/assistant/users');
        $this->assertFalse($this->client->getResponse()->isSuccessful(), "Regular user shouldn't have access to assistant area!!!");
    }
    public function testAssistantUser() {
        $this->client->loginAssistant();

        $crawler = $this->client->request('GET', '/assistant/users');
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

}

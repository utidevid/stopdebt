<?php

namespace SD\Bundle\UserBundle\Tests\Controller;

use SD\Bundle\UtilsBundle\Tests\Traits\SDDatabaseTestCaseTrait;
use SD\Bundle\UtilsBundle\Tests\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    use SDDatabaseTestCaseTrait;

    public function testIndex()
    {
        /** @var \SD\Bundle\UtilsBundle\Tests\SDClient $client */
        $client = static::createClient();
        $this->purgeDatabaseAndRollFixtures(self::$kernel);

        $client->loginRealUser();

        $client->request('GET', '/');

        $this->assertTrue($client->getResponse()->isRedirect('/cases/C-000-1/dashboard'), "user isn't redirected to dashboard");
    }

    public function testDashboard()
    {
        /** @var \SD\Bundle\UtilsBundle\Tests\SDClient $client */
        $client = static::createClient();
        $client->loginRealUser();
        $client->request('GET', '/cases/C-000-1/dashboard');

        $this->assertTrue($client->getResponse()->isSuccessful());
    }
}

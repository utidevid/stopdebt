<?php

namespace SD\Bundle\UserBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractUserController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DefaultController extends AbstractUserController
{
    const SECTION_NAME = 'offer';

    public function indexAction()
    {
        if ($this->isGranted('HAS_ASSISTANT_ACCESS')) {
            return $this->redirectToRoute('assistant_homepage');
        }

        if (!$this->getUser()->getIsOfferAccepted()) {
            return $this->redirectToRoute('user_accept_offer');
        }

        if ($this->isGranted('CAN_CREATE_NEW_CASE')) {
            return $this->redirectToRoute('case_create_new');
        } else {

            $case = $this->getUser()->getCases()->first();
            return $this->redirectToRoute('dashboard_homepage', [
                'caseId' => $case->getSafeId()
            ]);
        }
    }

    public function loginToPaymentAction($q) {
        $qObj = json_decode(base64_decode($q), true);

        if (!empty($qObj)) {
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->findUserBy(array('email' => $qObj['email']));

            if (!empty($user)) {
                $secret = 'f6f28a9a-07ac-11e6-b512-3e1d05defe78';
                $lastLoginTime = $user->getLastLogin() ? $user->getLastLogin()->format('dMY') : 0;
                $checkSign = md5($secret.$qObj['url'].$user->getEmail().$lastLoginTime);

                if ($checkSign == $qObj['sign']) {
                    $response = new RedirectResponse($qObj['url']);

                    $session = $this->get('session');
                    $token = new UsernamePasswordToken($user, null, 'fos_userbundle', $user->getRoles());
                    $session->set('_security_main', serialize($token));
                    $session->save();

                    $user->setLastLogin(new \DateTime());
                    $userManager->updateUser($user);
                    $this->getDoctrine()->getManager()->flush($user);

                    return $response;
                }
            }
        }

        return $this->redirectToRoute('login');
    }

    public function acceptOfferAction(Request $request) {
        if ($this->getUser()->getIsOfferAccepted()) {
            return $this->redirectToRoute('user_homepage');
        }

        $follow = $_GET['follow'];

        $form = $this->createFormBuilder()->setAction($this->generateUrl('user_accept_offer', [
            'follow' => $follow
        ]))
            ->add('add', SubmitType::class, ['label' => 'Я принимаю условия!', 'attr' => ['class' => 'btn btn-lg btn-primary']])->getForm();

        if ($request->isMethod('POST')) {
            $this->getUser()->setIsOfferAccepted(1);
            $this->getDoctrine()->getManager()->flush($this->getUser());

            if (!empty($follow)) {
                return $this->redirect($follow);
            } else {
                return $this->redirectToRoute('user_homepage');
            }
        }

        return $this->render('UserBundle:Default:accept_offer.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}

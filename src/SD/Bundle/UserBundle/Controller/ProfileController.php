<?php
namespace SD\Bundle\UserBundle\Controller;


use SD\Bundle\UtilsBundle\Controller\AbstractUserController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProfileController extends AbstractUserController
{
    const SECTION_NAME = 'profile';

    public function editAction()
    {
        $form = $this->get('fos_user.profile.form');
        $formHandler = $this->get('fos_user.profile.form.handler');

        $process = $formHandler->process($this->getUser());
        if ($process) {
            $this->setFlash('fos_user_success', 'profile.flash.updated');

            return new RedirectResponse($this->get('router')->generate('fos_user_profile_edit'));
        }

        $this->loadCaseFromSession();

        $passForm = $this->getPasswordForm($this->getUser());

        return $this->render('UserBundle:Profile:edit.html.twig', [
            'form' => $form->createView(),
            'passForm' => $passForm->createView(),
            'showFormErrors' => false
        ]);
    }

    public function setPasswordAction(Request $request)
    {
        $passForm = $this->getPasswordForm($this->getUser());
        $passForm->handleRequest($request);

        if ($passForm->isSubmitted() && $passForm->isValid()) {
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($this->getUser());
            $this->getDoctrine()->getManager()->flush();

            $this->container->get('utils.mailer')->sendPasswordChangedMessage($this->getUser());

            return $this->redirectToRoute('profile');
        } else {
            $form = $this->get('fos_user.profile.form');
            $form->setData($this->getUser());
            $formHandler = $this->get('fos_user.profile.form.handler');

            $this->loadCaseFromSession();

            return $this->render('UserBundle:Profile:edit.html.twig', [
                'form' => $form->createView(),
                'passForm' => $passForm->createView(),
                'showFormErrors' => true
            ]);
        }
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->set($action, $value);
    }

    protected  function getPasswordForm($user) {
        return $this->createFormBuilder($user)->setAction($this->generateUrl('profile_set_password'))
            ->add('plainPassword',  RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match',
                'required' => true,
                'first_options'  => ['label' => 'Password'],
                'second_options' => ['label' => 'Repeat Password']
            ])
            ->add('save', SubmitType::class, ['label' => 'Set new password', 'attr' => ['class' => 'btn btn-primary']])
            ->getForm();
    }
}
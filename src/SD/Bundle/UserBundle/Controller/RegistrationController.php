<?php

namespace SD\Bundle\UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use SD\Bundle\UtilsBundle\Entity\LandingVisitLog;

class RegistrationController extends \FOS\UserBundle\Controller\RegistrationController
{
    public function registerAction() {

        if ($this->container->get('request_stack')->getCurrentRequest()->isMethod('GET') && $this->container->get('kernel')->getEnvironment() != 'test') {
            $log = new LandingVisitLog();
            $log->setParams(isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : "");
            $log->setAddr($this->container->get('request_stack')->getCurrentRequest()->getClientIp());
            $log->setHeaders(getallheaders());

            $this->container->get('doctrine')->getManager()->persist($log);
            $this->container->get('doctrine')->getManager()->flush($log);
        }

        return parent::registerAction();
    }

    public function confirmedAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $pass = $this->generateRandomPassword(7);
        $user->setPlainPassword($pass);

        $this->container->get('fos_user.user_manager')->updateUser($user);
        $this->container->get('doctrine')->getManager()->flush();

        $this->container->get('utils.mailer')->sendWelcomeMessage($user, $pass);
        $this->container->get('assistant.notifier')->notifyHasNewUser($user);
        return new RedirectResponse($this->container->get('router')->generate('user_homepage'));
    }

    protected function generateRandomPassword($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
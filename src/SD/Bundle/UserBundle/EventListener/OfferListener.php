<?php
namespace SD\Bundle\UserBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;

class OfferListener
{
    protected $container;
    protected $router;

    public function __construct($container, Router $router)
    {
        $this->container = $container;
        $this->router = $router;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        $safeControllers = [
            'SD\Bundle\UserBundle\Controller\SecurityController',
            'SD\Bundle\UserBundle\Controller\RegistrationController'
        ];

        if ($controller[1] == 'acceptOfferAction' || in_array(get_class($controller[0]), $safeControllers) ) {
            return;
        }

        $user = $this->container->get('security.context')->getToken() ?  $this->container->get('security.context')->getToken()->getUser() : null;

        if (empty($user) || $user == 'anon.') {
            return;
        }

        if (!$user->hasRole('ROLE_ASSISTANT') &&  !$user->getIsOfferAccepted())  {
            $fUrl = $event->getRequest()->getUri();
            $redirectUrl = $this->router->generate('user_accept_offer', [
                'follow' => $fUrl
            ]);

            $event->setController(function() use ($redirectUrl) {
                return new RedirectResponse($redirectUrl);
            });
        }
    }

}
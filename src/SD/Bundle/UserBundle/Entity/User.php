<?php

namespace SD\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 */
class User extends \FOS\UserBundle\Model\User
{
    public function __construct()
    {
        parent::__construct();
        $this->roles = array('ROLE_USER');
    }

    /**
     * @var int
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var string
     */
    protected $facebookId;

    /**
     * @var string
     */
    protected $facebookAccessToken;

    /**
     * @var string
     */
    protected $vkontakteId;

    /**
     * @var string
     */
    protected $vkontakteAccessToken;

    /**
     * @var string
     */
    protected $twitterId;

    /**
     * @var string
     */
    protected $twitterAccessToken;


    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $patronymic;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var string
     */
    private $locality;

    /**
     *  @var array
     */
    private $questionnaires;

    /**
     * @var string
     */
    private $passport;

    /**
     * @var string
     */
    private $identification_code;

    /**
     * @var \DateTime
     */
    private $birthdate;

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set patronymic
     *
     * @param string $patronymic
     *
     * @return User
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    /**
     * Get patronymic
     *
     * @return string
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return User
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set locality
     *
     * @param string $locality
     *
     * @return User
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;

        return $this;
    }

    /**
     * Get locality
     *
     * @return string
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Get expired
     *
     * @return boolean
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Get credentialsExpired
     *
     * @return boolean
     */
    public function getCredentialsExpired()
    {
        return $this->credentialsExpired;
    }

    /**
     * Get credentialsExpireAt
     *
     * @return \DateTime
     */
    public function getCredentialsExpireAt()
    {
        return $this->credentialsExpireAt;
    }

    /**
     * Add questionnaire
     *
     * @param \SD\Bundle\QuestionnaireBundle\Entity\Questionnaire $questionnaire
     *
     * @return User
     */
    public function addQuestionnaire(\SD\Bundle\QuestionnaireBundle\Entity\Questionnaire $questionnaire)
    {
        $this->questionnaires[] = $questionnaire;

        return $this;
    }

    /**
     * Remove questionnaire
     *
     * @param \SD\Bundle\QuestionnaireBundle\Entity\Questionnaire $questionnaire
     */
    public function removeQuestionnaire(\SD\Bundle\QuestionnaireBundle\Entity\Questionnaire $questionnaire)
    {
        $this->questionnaires->removeElement($questionnaire);
    }

    /**
     * Get questionnaires
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionnaires()
    {
        return $this->questionnaires;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $strategy_items;


    /**
     * Add strategyItem
     *
     * @param \SD\Bundle\AssistantBundle\Entity\StrategyItem $strategyItem
     *
     * @return User
     */
    public function addStrategyItem(\SD\Bundle\AssistantBundle\Entity\StrategyItem $strategyItem)
    {
        $this->strategy_items[] = $strategyItem;

        return $this;
    }

    /**
     * Remove strategyItem
     *
     * @param \SD\Bundle\AssistantBundle\Entity\StrategyItem $strategyItem
     */
    public function removeStrategyItem(\SD\Bundle\AssistantBundle\Entity\StrategyItem $strategyItem)
    {
        $this->strategy_items->removeElement($strategyItem);
    }

    /**
     * Get strategyItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStrategyItems()
    {
        return $this->strategy_items;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cases;


    /**
     * Add case
     *
     * @param \SD\Bundle\CaseBundle\Entity\DebtCase $case
     *
     * @return User
     */
    public function addCase(\SD\Bundle\CaseBundle\Entity\DebtCase $case)
    {
        $this->cases[] = $case;

        return $this;
    }

    /**
     * Remove case
     *
     * @param \SD\Bundle\CaseBundle\Entity\DebtCase $case
     */
    public function removeCase(\SD\Bundle\CaseBundle\Entity\DebtCase $case)
    {
        $this->cases->removeElement($case);
    }

    /**
     * Get cases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCases()
    {
        return $this->cases;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     *
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     *
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebookAccessToken
     *
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * Set vkontakteId
     *
     * @param string $vkontakteId
     *
     * @return User
     */
    public function setVkontakteId($vkontakteId)
    {
        $this->vkontakteId = $vkontakteId;

        return $this;
    }

    /**
     * Get vkontakteId
     *
     * @return string
     */
    public function getVkontakteId()
    {
        return $this->vkontakteId;
    }

    /**
     * Set vkontakteAccessToken
     *
     * @param string $vkontakteAccessToken
     *
     * @return User
     */
    public function setVkontakteAccessToken($vkontakteAccessToken)
    {
        $this->vkontakteAccessToken = $vkontakteAccessToken;

        return $this;
    }

    /**
     * Get vkontakteAccessToken
     *
     * @return string
     */
    public function getVkontakteAccessToken()
    {
        return $this->vkontakteAccessToken;
    }

    /**
     * Set twitterId
     *
     * @param string $twitterId
     *
     * @return User
     */
    public function setTwitterId($twitterId)
    {
        $this->twitterId = $twitterId;

        return $this;
    }

    /**
     * Get twitterId
     *
     * @return string
     */
    public function getTwitterId()
    {
        return $this->twitterId;
    }

    /**
     * Set twitterAccessToken
     *
     * @param string $twitterAccessToken
     *
     * @return User
     */
    public function setTwitterAccessToken($twitterAccessToken)
    {
        $this->twitterAccessToken = $twitterAccessToken;

        return $this;
    }

    /**
     * Get twitterAccessToken
     *
     * @return string
     */
    public function getTwitterAccessToken()
    {
        return $this->twitterAccessToken;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $related_messages;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messages;


    /**
     * Add relatedMessage
     *
     * @param \SD\Bundle\MessagingBundle\Entity\Message $relatedMessage
     *
     * @return User
     */
    public function addRelatedMessage(\SD\Bundle\MessagingBundle\Entity\Message $relatedMessage)
    {
        $this->related_messages[] = $relatedMessage;

        return $this;
    }

    /**
     * Remove relatedMessage
     *
     * @param \SD\Bundle\MessagingBundle\Entity\Message $relatedMessage
     */
    public function removeRelatedMessage(\SD\Bundle\MessagingBundle\Entity\Message $relatedMessage)
    {
        $this->related_messages->removeElement($relatedMessage);
    }

    /**
     * Get relatedMessages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRelatedMessages()
    {
        return $this->related_messages;
    }

    /**
     * Add message
     *
     * @param \SD\Bundle\MessagingBundle\Entity\Message $message
     *
     * @return User
     */
    public function addMessage(\SD\Bundle\MessagingBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \SD\Bundle\MessagingBundle\Entity\Message $message
     */
    public function removeMessage(\SD\Bundle\MessagingBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $notifications;


    /**
     * Add notification
     *
     * @param \SD\Bundle\UtilsBundle\Entity\Notification $notification
     *
     * @return User
     */
    public function addNotification(\SD\Bundle\UtilsBundle\Entity\Notification $notification)
    {
        $this->notifications[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     *
     * @param \SD\Bundle\UtilsBundle\Entity\Notification $notification
     */
    public function removeNotification(\SD\Bundle\UtilsBundle\Entity\Notification $notification)
    {
        $this->notifications->removeElement($notification);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * Set passport
     *
     * @param string $passport
     *
     * @return User
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;

        return $this;
    }

    /**
     * Get passport
     *
     * @return string
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     *
     * @return User
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set identificationCode
     *
     * @param string $identificationCode
     *
     * @return User
     */
    public function setIdentificationCode($identificationCode)
    {
        $this->identification_code = $identificationCode;

        return $this;
    }

    /**
     * Get identificationCode
     *
     * @return string
     */
    public function getIdentificationCode()
    {
        return $this->identification_code;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $assigned_messages;


    /**
     * Add assignedMessage
     *
     * @param \SD\Bundle\MessagingBundle\Entity\Message $assignedMessage
     *
     * @return User
     */
    public function addAssignedMessage(\SD\Bundle\MessagingBundle\Entity\Message $assignedMessage)
    {
        $this->assigned_messages[] = $assignedMessage;

        return $this;
    }

    /**
     * Remove assignedMessage
     *
     * @param \SD\Bundle\MessagingBundle\Entity\Message $assignedMessage
     */
    public function removeAssignedMessage(\SD\Bundle\MessagingBundle\Entity\Message $assignedMessage)
    {
        $this->assigned_messages->removeElement($assignedMessage);
    }

    /**
     * Get assignedMessages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignedMessages()
    {
        return $this->assigned_messages;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $assistant_comments;


    /**
     * Add assistantComment
     *
     * @param \SD\Bundle\AssistantBundle\Entity\UserComment $assistantComment
     *
     * @return User
     */
    public function addAssistantComment(\SD\Bundle\AssistantBundle\Entity\UserComment $assistantComment)
    {
        $this->assistant_comments[] = $assistantComment;

        return $this;
    }

    /**
     * Remove assistantComment
     *
     * @param \SD\Bundle\AssistantBundle\Entity\UserComment $assistantComment
     */
    public function removeAssistantComment(\SD\Bundle\AssistantBundle\Entity\UserComment $assistantComment)
    {
        $this->assistant_comments->removeElement($assistantComment);
    }

    /**
     * Get assistantComments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssistantComments()
    {
        return $this->assistant_comments;
    }
    /**
     * @var boolean
     */
    private $isOfferAccepted = 0;


    /**
     * Set isOfferAccepted
     *
     * @param boolean $isOfferAccepted
     *
     * @return User
     */
    public function setIsOfferAccepted($isOfferAccepted)
    {
        $this->isOfferAccepted = $isOfferAccepted;

        return $this;
    }

    /**
     * Get isOfferAccepted
     *
     * @return boolean
     */
    public function getIsOfferAccepted()
    {
        return $this->isOfferAccepted;
    }

    public function addRole($role)
    {
        $role = strtoupper($role);

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $custom_services;


    /**
     * Add customService
     *
     * @param \SD\Bundle\CaseBundle\Entity\CustomService $customService
     *
     * @return User
     */
    public function addCustomService(\SD\Bundle\CaseBundle\Entity\CustomService $customService)
    {
        $this->custom_services[] = $customService;

        return $this;
    }

    /**
     * Remove customService
     *
     * @param \SD\Bundle\CaseBundle\Entity\CustomService $customService
     */
    public function removeCustomService(\SD\Bundle\CaseBundle\Entity\CustomService $customService)
    {
        $this->custom_services->removeElement($customService);
    }

    /**
     * Get customServices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomServices()
    {
        return $this->custom_services;
    }
}

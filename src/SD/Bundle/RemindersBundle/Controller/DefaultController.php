<?php

namespace SD\Bundle\RemindersBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractUserController;

class DefaultController extends AbstractUserController
{
    const SECTION_NAME = 'reminders';

    public function indexAction($caseId)
    {
        $this->loadCaseBySafeId($caseId);
        $this->denyAccessUnlessGranted('HAS_REMINDERS_ACCESS', $this->activeCase);

        $previous = [];
        $list = [];

        $now = new \DateTime();

        foreach ($this->activeCase->getReminders() as $reminder) {
            if ($reminder->getStartAt() >= $now) {
                $list[] = $reminder;
            } else {
                $previous[] = $reminder;
            }
        }

        return $this->render('RemindersBundle:Default:index.html.twig', [
            'previous' => $previous,
            'list' => $list
        ]);
    }

    public function calendarAction($caseId, $day)
    {
        $this->loadCaseBySafeId($caseId);
        $this->denyAccessUnlessGranted('HAS_REMINDERS_ACCESS', $this->activeCase);

        $listByDay = [];

        for ($i=1; $i <= 31; $i++) {
            $listByDay[$i] = [];
        }

        if (empty($day)) {
            $time = time();
        } else {
            $time = strtotime($day);
        }

        $time = strtotime('first day of this month', $time);
        $nextTime = strtotime('first day of next month', $time);
        $prevTime = strtotime('first day of previous month', $time);

        foreach ($this->activeCase->getReminders() as $reminder) {
            if (date('mY', $time) == $reminder->getStartAt()->format('mY')) {
                $listByDay[(int)$reminder->getStartAt()->format('d')][] = $reminder;
            }
        }

        return $this->render('RemindersBundle:Default:calendar.html.twig', [
            'time' => $time,
            'nextTime' => $nextTime,
            'prevTime' => $prevTime,
            'listByDay' => $listByDay
        ]);
    }


}

$(function() {
    if ($('#documentUploadWrapper #form_documentTypes').length) {
        $('#documentUploadWrapper #form_documentTypes').chosen({});
    }

    $(document).on('click', 'a[data-method]', function(e) {
        e.preventDefault();
        var $form = $('<form action="' + $(this).attr('href') + '" method="'+ $(this).attr('data-method')  +'"></form>');
        $('body').append($form);
        $form.submit();
    });

    $('.show-payments').click(function(e) {
        e.preventDefault();
        $($(this).attr('data-id')).toggle();
    });
});

$(document).on("page:change", function() {
    $(".navbar .dropdown").hover((function() {
         $(this).find(".dropdown-menu").first().stop(true, true).delay(150).slideDown();
    }), function() {
         $(this).find(".dropdown-menu").first().stop(true, true).delay(50).slideUp();
    });
});
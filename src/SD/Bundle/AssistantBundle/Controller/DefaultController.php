<?php

namespace SD\Bundle\AssistantBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractAssistantController;

class DefaultController extends AbstractAssistantController
{
    const SECTION_NAME = 'dashboard';

    public function indexAction()
    {
        return $this->redirectToRoute('assistant_users_index');
    }
}

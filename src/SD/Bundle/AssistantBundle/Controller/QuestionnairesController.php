<?php

namespace SD\Bundle\AssistantBundle\Controller;

use SD\Bundle\QuestionnaireBundle\Model\QuestionnaireModel;
use SD\Bundle\QuestionnaireBundle\Entity\Questionnaire;
use SD\Bundle\UtilsBundle\Controller\AbstractAssistantController;
use SD\Bundle\CaseBundle\Entity\DebtCase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class QuestionnairesController extends AbstractAssistantController
{
    const SECTION_NAME = 'users';

    public function viewAction($questionnaireId)
    {
        $questionnaire = $this->loadQuestionnaireBySafeId($questionnaireId);

        /** @var QuestionnaireModel $model */
        $model = $this->get('sd.questionnaire_model');

        $result = $model->calculateWeights($questionnaire);

        $qarray = [];
        foreach ($model::getRelevantSections() as $key => $method) {
            $qarray[$key] = $questionnaire->$method();
        }

        $specialRecommendations = $model::processSpecialCases($questionnaire);

        return $this->render('AssistantBundle:Questionnaires:view.html.twig', [
            'questionnaireData' => $qarray,
            'questionnaire' => $questionnaire,
            'results' => $result,
            'schema' => QuestionnaireModel::getSchemaDetails(),
            'specialRecommendations' => $specialRecommendations
        ]);
    }

    public function editAction($questionnaireId)
    {
        $questionnaire = $this->loadQuestionnaireBySafeId($questionnaireId);

        $usr = $questionnaire->getUser();

        $baseData = QuestionnaireModel::getSchema();
        $baseData['questionnaireId'] = $questionnaireId;
        $baseData['last_step'] = $questionnaire->getLastStep();
        $baseData['send_url'] = $this->generateUrl('assistant_questionnaires_save', [
            'questionnaireId' => $questionnaireId
        ]);
        $baseData['redirect_on_finish'] = $this->generateUrl('assistant_questionnaires_view', [
            'questionnaireId' => $questionnaireId
        ]);
        $baseData['user'] = array_merge($baseData['user'], [
            'first_name' => $usr->getFirstName(),
            'last_name' => $usr->getLastName(),
            'patronymic' => $usr->getPatronymic(),
            'phone_number' => $usr->getPhoneNumber(),
            'locality' => $usr->getLocality(),
        ]);

        $baseData[Questionnaire::GOAL_DATA_KEY] = array_merge($baseData[Questionnaire::GOAL_DATA_KEY], $questionnaire->getGoalData());
        $baseData[Questionnaire::PENALTY_DATA_KEY] = array_merge($baseData[Questionnaire::PENALTY_DATA_KEY], $questionnaire->getPenaltyData());
        $baseData[Questionnaire::CREDIT_EXTRAS_KEY] = array_merge($baseData[Questionnaire::CREDIT_EXTRAS_KEY], $questionnaire->getCreditExtras());
        $baseData[Questionnaire::CREDIT_DETAILS_KEY] = array_merge($baseData[Questionnaire::CREDIT_DETAILS_KEY], $questionnaire->getCreditDetails());
        $baseData[Questionnaire::CREDIT_DATA_KEY] = array_merge($baseData[Questionnaire::CREDIT_DATA_KEY], $questionnaire->getCreditData());
        $baseData[Questionnaire::RESOLUTION_ATTEMPTS_KEY] = array_merge($baseData[Questionnaire::RESOLUTION_ATTEMPTS_KEY], $questionnaire->getResolutionAttempts());
        $baseData[Questionnaire::FINANCIAL_DATA_KEY] = array_merge($baseData[Questionnaire::FINANCIAL_DATA_KEY], $questionnaire->getFinancialData());

         return $this->render('AssistantBundle:Questionnaires:edit.html.twig', [
            'questionnaire' => $questionnaire,
            'data' => json_encode($baseData),
        ]);
    }

    public function saveAction($questionnaireId, Request $request)
    {
        $questionnaire = $this->loadQuestionnaireBySafeId($questionnaireId);

        $jsonData = json_decode($request->get('data'), true);

        if ((int)$request->get('status') == Questionnaire::COMPLETED_STATUS) {
            $questionnaire->setStatus(Questionnaire::COMPLETED_STATUS);

            $case = $questionnaire->getCase();

            if ($case->getTariff() == 'standard') {
                $case->setStage(DebtCase::STANDARD_TARIFF_STAGE_2_VIEW_STRATEGY);
            } elseif ($case->getTariff() == 'optimal') {
                $case->setStage(DebtCase::OPTIMAL_TARIFF_STAGE_3_SKYPE_CALL);
            } elseif ($case->getTariff() == 'advanced') {
                $case->setStage(DebtCase::ADVANCED_TARIFF_STAGE_3_SKYPE_CALL);
            }
        }

        $questionnaire->setLastStep((int)$jsonData['last_step']);

        $questionnaire->setGoalData($jsonData[Questionnaire::GOAL_DATA_KEY]);
        $questionnaire->setCreditData($jsonData[Questionnaire::CREDIT_DATA_KEY]);
        $questionnaire->setCreditDetails($jsonData[Questionnaire::CREDIT_DETAILS_KEY]);
        $questionnaire->setPenaltyData($jsonData[Questionnaire::PENALTY_DATA_KEY]);
        $questionnaire->setCreditExtras($jsonData[Questionnaire::CREDIT_EXTRAS_KEY]);
        $questionnaire->setFinancialData($jsonData[Questionnaire::FINANCIAL_DATA_KEY]);
        $questionnaire->setResolutionAttempts($jsonData[Questionnaire::RESOLUTION_ATTEMPTS_KEY]);

        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(['status' => 'ok'], 201, ['Content-Type' => 'application/json']);
    }
}

<?php

namespace SD\Bundle\AssistantBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractAssistantController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PaymentsController extends AbstractAssistantController
{
    const SECTION_NAME = 'payments';

    public function listAction()
    {
        $repo = $this->getDoctrine()->getRepository('CaseBundle:DebtCase');
        $list = $repo->createQueryBuilder('c')->addOrderBy('c.billed', 'ASC')->addOrderBy('c.createdAt', 'DESC')->getQuery()->getResult();

        $repo2 = $this->getDoctrine()->getRepository('CaseBundle:CustomService');
        $listServices = $repo2->createQueryBuilder('s')->addOrderBy('s.billed', 'ASC')->addOrderBy('s.createdAt', 'DESC')->getQuery()->getResult();

        return $this->render('AssistantBundle:Payments:list.html.twig', [
            'list' => $list,
            'listServices' => $listServices,
        ]);
    }

}

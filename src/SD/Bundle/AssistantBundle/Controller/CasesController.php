<?php

namespace SD\Bundle\AssistantBundle\Controller;

use SD\Bundle\QuestionnaireBundle\Model\QuestionnaireModel;
use SD\Bundle\UtilsBundle\Controller\AbstractAssistantController;
use SD\Bundle\CaseBundle\Entity\DebtCase;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class CasesController extends AbstractAssistantController
{
    const SECTION_NAME = 'users';

    public function viewAction($caseId)
    {
        $case = $this->loadCaseBySafeId($caseId);

        return $this->render('AssistantBundle:Cases:view.html.twig', [
            'case' => $case,
            'flowInfo'  => $this->getFlowInfo($case)
        ]);
    }

    public function finishStageAction($caseId)
    {
        $case = $this->loadCaseBySafeId($caseId);

        if ($case->getTariff() == 'optimal' && in_array($case->getStage(), [DebtCase::OPTIMAL_TARIFF_STAGE_3_SKYPE_CALL, DebtCase::OPTIMAL_TARIFF_STAGE_4_PREPARING_STRATEGY])) {
            $case->setStage($case->getStage() + 1);
        } elseif ($case->getTariff() == 'advanced' && in_array($case->getStage(), [DebtCase::ADVANCED_TARIFF_STAGE_3_SKYPE_CALL, DebtCase::ADVANCED_TARIFF_STAGE_5_INTERVIEW, DebtCase::ADVANCED_TARIFF_STAGE_6_PREPARING_STRATEGY])) {
            $case->setStage($case->getStage() + 1);
        }

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('assistant_cases_view', [
            'caseId' => $caseId
        ]);
    }

    protected function getFlowInfo($case) {
        $steps = [
            'standard' => ['Заполнение анкеты клиентом', 'Отображение отчёта и стратегии'],
            'optimal' => ['Загрузка документов', 'Заполнение анкеты клиентом', 'Skype звонок', 'Подготовка стратегии асcистентом', 'Отображение отчёта и стратегии'],
            'advanced' => ['Загрузка документов', 'Заполнение анкеты асcистентом', 'Skype звонок', 'Ревью анкеты клиентом', 'Собеседование с клиентом', 'Подготовка стратегии асcистентом', 'Отображение отчёта и стратегии']
        ];

        $data = [];

        foreach ($steps[$case->getTariff()] as $i => $title) {
            $automatic = true;
            if ($case->getTariff() == 'optimal') {
                $automatic = ! in_array($case->getStage(), [DebtCase::OPTIMAL_TARIFF_STAGE_3_SKYPE_CALL, DebtCase::OPTIMAL_TARIFF_STAGE_4_PREPARING_STRATEGY]);
            }
            if ($case->getTariff() == 'advanced') {
                $automatic = ! in_array($case->getStage(), [DebtCase::ADVANCED_TARIFF_STAGE_3_SKYPE_CALL, DebtCase::ADVANCED_TARIFF_STAGE_5_INTERVIEW, DebtCase::ADVANCED_TARIFF_STAGE_6_PREPARING_STRATEGY]);
            }

            $data[] = [
                'label' => $title,
                'active' => $i + 1 == $case->getStage(),
                'done' => $i + 1 < $case->getStage(),
                'future' => $i + 1 > $case->getStage(),
                'automatic' => $automatic
            ];
        }

        return $data;
    }

}

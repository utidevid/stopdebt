<?php

namespace SD\Bundle\AssistantBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractAssistantController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use SD\Bundle\DocumentsBundle\Entity\Document;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use SD\Bundle\CaseBundle\Entity\DebtCase;

class DocumentsController extends AbstractAssistantController
{
    const SECTION_NAME = 'users';

    public function viewAction($caseId, $documentId)
    {
        $case = $this->loadCaseBySafeId($caseId);
        $document = $this->loadDocumentBySafeId($documentId);

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('assistant_documents_quick_upload', [
                    'caseId' => $caseId,
                    'documentId' => $documentId
                ]))
            ->add('attachment', FileType::class)
            ->add('upload', SubmitType::class, ['label' => 'Загрузить', 'attr' => ['class' => 'btn btn-primary']])
            ->getForm();

        return $this->render('AssistantBundle:Documents:view.html.twig', [
            'case' => $case,
            'document' => $document,
            'form' => $form->createView()
        ]);
    }

    public function setApprovedAction($caseId, $documentId, $status, Request $request) {
        $case = $this->loadCaseBySafeId($caseId);
        $document = $this->loadDocumentBySafeId($documentId);
        $em = $this->getDoctrine()->getManager();

        if ($status == 'yes') {
            $document->setIsApproved(1);
        } elseif ($status == 'no') {
            $document->setIsApproved(0);
        }
        $em->flush();

        if ($case->getTariff() == 'optimal' || $case->getTariff() == 'advanced') {
            $allApproved = empty(array_filter($case->getDocuments()->toArray(), function($d) {
                return $d->getIsApproved() == 0;
            }));

            if ($case->getStage() == DebtCase::OPTIMAL_TARIFF_STAGE_1_UPLOAD_DOCUMENTS && $allApproved) {
                $case->setStage(DebtCase::OPTIMAL_TARIFF_STAGE_2_EDIT_QUESTIONNAIRE);
            } elseif ($case->getStage() == DebtCase::OPTIMAL_TARIFF_STAGE_2_EDIT_QUESTIONNAIRE && !$allApproved) {
                $case->setStage(DebtCase::OPTIMAL_TARIFF_STAGE_1_UPLOAD_DOCUMENTS);
            }

        }

        $em->flush();

        return $this->redirectToRoute('assistant_documents_view', [
            'caseId' => $caseId,
            'documentId' => $documentId
        ]);
    }

    public function quickUploadAction($caseId, $documentId, Request $request) {
        $this->loadCaseBySafeId($caseId);
        $document = $this->loadDocumentBySafeId($documentId);

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('attachment', FileType::class)
            ->add('upload', SubmitType::class, ['label' => 'Загрузить', 'attr' => ['class' => 'btn btn-primary']])
            ->getForm();

        $form->handleRequest($request);
        $uploader = $this->get('documents.uploader');
        $uploader->attachUpload($document, $form['attachment']->getData(), false);
        $em->flush();

        return $this->redirectToRoute('assistant_documents_view', [
            'caseId' => $caseId,
            'documentId' => $documentId
        ]);
    }

    public function uploadAction($caseId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('DocumentsBundle:Document');
        $uploader = $this->get('documents.uploader');

        $case = $this->loadCaseBySafeId($caseId);

        $form = $this->createFormBuilder()
            ->add('documentTypes', ChoiceType::class, [
                'choices' => Document::getDocumentTypeOptions(),
                'multiple' => false,
                'expanded' => false
            ])
            ->add('title', TextType::class)
            ->add('upload', SubmitType::class, ['label' => 'Загрузить', 'attr' => ['class' => 'btn btn-primary']])
            ->getForm();

        $showFormErrors = false;
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $document = new Document();
                $document->setSafeId($repository->generateSafeId());
                $document->setTitle($form['title']->getData());
                $document->setDocumentTypes([$form['documentTypes']->getData()]);
                $document->setUser($this->getUser());
                $document->setIsApproved(0);
                $document->setCase($case);

                $em->persist($document);
                $em->flush();

                return $this->redirectToRoute('assistant_documents_view', ['caseId' => $caseId, 'documentId' => $document->getSafeId()]);
            } else {
                $showFormErrors = true;
            }
        }


        return $this->render('AssistantBundle:Documents:upload.html.twig', [
            'case' => $case,
            'form' => $form->createView(),
            'showFormErrors' => $showFormErrors
        ]);
    }

    public function rawAction($caseId, $documentId, $index)
    {
        $case = $this->loadCaseBySafeId($caseId);
        $document = $this->loadDocumentBySafeId($documentId);
        $fileInfo = $document->getUploads()[$index];

        $fullpath = $this->getParameter('documents.uploads_dir').'/'.$document->getSafeId().'/'.$fileInfo['filename'];

        #todo rewrite. let nginx do this work
        if (file_exists($fullpath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($fullpath).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($fullpath));
            readfile($fullpath);
            exit;
        }
    }

    protected function getSectionName() {
        return 'users';
    }
}

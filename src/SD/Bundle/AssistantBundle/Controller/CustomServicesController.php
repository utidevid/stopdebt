<?php

namespace SD\Bundle\AssistantBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractAssistantController;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;

use SD\Bundle\CaseBundle\Entity\CustomService;

class CustomServicesController extends AbstractAssistantController
{
    public function pushAction($caseId, Request $request)
    {
        $case = $this->loadCaseBySafeId($caseId);

        $service = new CustomService();
        $service->setCase($case);
        $service->setUser($case->getUser());
        $service->setBilled(0);
        $service->setCreatedAt(new \DateTime());
        $service->setSafeId($this->getDoctrine()->getManager()->getRepository('CaseBundle:CustomService')->generateSafeId());

        $form = $this->getForm($service);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $this->get('utils.mailer')->sendServiceInvoiceMessage($service->getUser(), $service);

                $this->getDoctrine()->getManager()->persist($service);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('assistant_cases_view', [
                    'caseId' => $caseId
                ]);
            }
        }

        return $this->render('AssistantBundle:CustomServices:push.html.twig', [
            'case' => $case,
            'form' => $form->createView(),
        ]);
    }

    protected function getForm($model)
    {

        $form = $this->createFormBuilder($model);

        $form->add('title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('amount', TextType::class)

            ->add('add', SubmitType::class, ['label' => 'Создать и выслать счёт', 'attr' => ['class' => 'btn btn-primary']]);

        return $form->getForm();
    }

}

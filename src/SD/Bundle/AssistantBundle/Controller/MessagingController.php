<?php

namespace SD\Bundle\AssistantBundle\Controller;

use SD\Bundle\MessagingBundle\Repository\MessageRepository;
use SD\Bundle\UtilsBundle\Controller\AbstractAssistantController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use SD\Bundle\MessagingBundle\Entity\Message;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MessagingController extends AbstractAssistantController
{
    const SECTION_NAME = 'messaging';

    public function indexAction()
    {
        /** @var MessageRepository $repository */
        $repository = $this->getDoctrine()->getRepository('MessagingBundle:Message');

        $messages = $repository->createQueryBuilder('p')
            ->select('p as message', 'count(m.id) as cnt')
            ->where('p.isTopMessage = 1')
            ->orderBy('cnt', 'DESC')
            ->addOrderBy('p.threadStatus','ASC')
            ->addOrderBy('p.createdAt', 'DESC')
            ->leftJoin('p.threadMessages', 'm', 'WITH', 'm.status = 0 and m.userMessage = 1')
            ->groupBy('p.id')
            ->getQuery()->getResult();

        foreach ($messages as &$message) {
            $message['cnt'] = $message['cnt'] + ($message['message']->getStatus() == 0);
        }

        return $this->render('AssistantBundle:Messaging:index.html.twig', [
            'messages' => $messages
        ]);
    }

    public function assignSelfAction($messageId, $assign) {
        $repository = $this->getDoctrine()->getRepository('MessagingBundle:Message');
        $mainMessage = $repository->findOneBy([
            'id' => $messageId
        ]);

        if ($assign == 1) {
            if ($mainMessage->getAssignedAssistant() == NULL) {
                $mainMessage->setAssignedAssistant($this->getUser());
            }
        }elseif ($assign == 0) {
            $mainMessage->setAssignedAssistant(NULL);
        }

        $this->getDoctrine()->getManager()->flush($mainMessage);

        return $this->redirectToRoute('assistant_messages', [
            'messageId' => $messageId
        ]);
    }

    public function changeLevelAction($messageId, $level) {
        $repository = $this->getDoctrine()->getRepository('MessagingBundle:Message');
        $mainMessage = $repository->findOneBy([
            'id' => $messageId
        ]);

        $mainMessage->setLevel((int)$level);
        $this->getDoctrine()->getManager()->flush($mainMessage);

        return $this->redirectToRoute('assistant_messages', [
            'messageId' => $messageId
        ]);
    }

    public function changeStatusAction($messageId, $status) {
        $repository = $this->getDoctrine()->getRepository('MessagingBundle:Message');
        $mainMessage = $repository->findOneBy([
            'id' => $messageId
        ]);

        $mainMessage->setThreadStatus((int)$status);
        $this->getDoctrine()->getManager()->flush($mainMessage);

        return $this->redirectToRoute('assistant_messages', [
            'messageId' => $messageId
        ]);
    }

    public function messagesAction($messageId)
    {
        $repository = $this->getDoctrine()->getRepository('MessagingBundle:Message');
        $mainMessage = $repository->findOneBy([
            'id' => $messageId
        ]);
        $messages = $mainMessage->getThreadMessages();
        $messages = array_merge([$mainMessage], $messages->toArray());

        $form = $this->createFormBuilder()->setAction($this->generateUrl('assistant_messages_push', [
                'messageId' => $messageId
            ]))
            ->add('message', TextareaType::class)
            ->add('add', SubmitType::class, ['label' => 'Отправить', 'attr' => ['class' => 'btn btn-primary']])->getForm();

        return $this->render('AssistantBundle:Messaging:messages.html.twig', [
            'form' => $form->createView(),
            'mainMessage' => $mainMessage,
            'messages' => $messages
        ]);
    }

    public function pushAction($messageId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('MessagingBundle:Message');
        $mainMessage = $repository->findOneBy([
            'id' => $messageId
        ]);

        $message = new Message();

        $form = $this->createFormBuilder()->add('message', TextareaType::class)->add('add', SubmitType::class)->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $message->setMessage($form['message']->getData());
                $message->setAuthor($this->getUser());
                $message->setRelatedUser($mainMessage->getAuthor());
                $message->setUserMessage(0);
                $message->setIsTopMessage(0);
                $message->setThreadStatus(0);
                $message->setLevel(0);
                $message->setStatus(Message::STATUS_UNREAD);
                $message->setParentMessage($mainMessage);
                $em->persist($message);
                $em->flush();

                // TODO: uncomment this later, when notifications are necessary
                // $this->get('utils.mailer')->sendNewResponseFromAssistant($user, $message);
            }
        }

        return $this->redirectToRoute('assistant_messages', [
            'messageId' => $messageId
        ]);
    }

    /**
     * @param $mainMessage
     * @return JsonResponse
     * @ParamConverter("mainMessage", class="MessagingBundle:Message")
     */
    public function setAssistantThreadReadAction($mainMessage)
    {
        $this->denyAccessUnlessGranted('assistant_read');

        /** @var MessageRepository $repository */
        $repository = $this->getDoctrine()->getRepository('MessagingBundle:Message');

        $repository->createQueryBuilder('p')
            ->update('Message', 'm')
            ->where('m.parentMessage = :message AND p.userMessage = 1')
            ->setParameter('message', $mainMessage)
            ->getQuery()->execute();

        return new JsonResponse(['status' => true]);
    }

}

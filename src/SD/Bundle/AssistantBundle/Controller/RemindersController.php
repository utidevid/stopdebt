<?php

namespace SD\Bundle\AssistantBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractAssistantController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

use SD\Bundle\AssistantBundle\Entity\Reminder;

class RemindersController extends AbstractAssistantController
{
    const SECTION_NAME = 'users';

    public function indexAction($caseId)
    {
        $case = $this->loadCaseBySafeId($caseId);
        $form = $this->getReminderForm($caseId);

        return $this->render('AssistantBundle:Reminders:index.html.twig', [
            'case' => $case,
            'form' => $form->createView(),
        ]);
    }

    public function pushAction($caseId, Request $request)
    {
        $case = $this->loadCaseBySafeId($caseId);
        $form = $this->getReminderForm($caseId);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $item = new Reminder();
                $item->setCase($case);
                $item->setTitle($form['title']->getData());
                $item->setDescription($form['description']->getData());
                $item->setStartAt($form['startAt']->getData());
                $case->addReminder($item);
                $em->persist($item);
                $em->flush();
            }
        }

        return $this->redirectToRoute('assistant_reminders', ['caseId' => $caseId]);
    }

    public function editAction($caseId, $reminderId, Request $request)
    {
        $case = $this->loadCaseBySafeId($caseId);
        $reminder = $this->loadReminderById($reminderId);

        $form = $this->getReminderForm($caseId, $reminder);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                return $this->redirectToRoute('assistant_reminders', ['caseId' => $caseId]);
            }
        }

        return $this->render('AssistantBundle:Reminders:edit.html.twig', [
            'case' => $case,
            'reminder' => $reminder,
            'form' => $form->createView(),
        ]);
    }

    public function deleteAction($caseId, $reminderId)
    {
        $reminder = $this->loadReminderById($reminderId);
        $em = $this->getDoctrine()->getManager();
        $em->remove($reminder);
        $em->flush();
        return $this->redirectToRoute('assistant_reminders', ['caseId' => $caseId]);
    }

    protected function getReminderForm($caseId, $model = null)
    {

        if (!empty($model)) {
            $form = $this->createFormBuilder($model);
        } else {
            $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('assistant_reminders_push', ['caseId' => $caseId]));
        }

        $form->add('title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('startAt', DateTimeType::class)
            ->add('add', SubmitType::class, ['label' => empty($model) ? 'Добавить' : 'Сохранить', 'attr' => ['class' => 'btn btn-primary']]);

        return $form->getForm();
    }

}

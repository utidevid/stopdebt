<?php

namespace SD\Bundle\AssistantBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractAssistantController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\HttpFoundation\Request;

use SD\Bundle\CaseBundle\Entity\DebtCase;

use SD\Bundle\AssistantBundle\Entity\UserComment;

class UsersController extends AbstractAssistantController
{
    const SECTION_NAME = 'users';

    public function indexAction()
    {
        $userManager = $this->get('fos_user.user_manager');

        $list = [];
        $assistants = [];

        foreach ($userManager->findUsers() as $u) {
            if ($u->hasRole('ROLE_ASSISTANT')) {
                $assistants[] = $u;
            } else {
                $list[] = $u;
            }
        }

        return $this->render('AssistantBundle:Users:index.html.twig', [
            'list' => $list,
            'assistants' => $assistants
        ]);
    }

    public function simpleInvoiceAction($userId, Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $userId));

        if (empty($user)) {
            throw new NotFoundHttpException("Пользователь №{userId} не найден!");
        }

        $form = $this->getSimpleInvoiceForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->sendSimpleInvoice($user, $form);
                return $this->redirectToRoute('assistant_users_view', [
                    'userId' => $userId
                ]);
            }
        }

        return $this->render('AssistantBundle:Users:simple_invoice.html.twig', [
            'viewed_user' => $user,
            'form' => $form->createView()
        ]);
    }

    public function inviteAction(Request $request)
    {
        $badEmail = false;
        $form = $this->getInviteForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $email = $form['email']->getData();
                $userManager = $this->get('fos_user.user_manager');
                $user = $userManager->findUserBy(array('email' => $email));

                if (is_null($user)) {
                    $this->inviteNewUser($form);
                    return $this->redirectToRoute('assistant_users_index');
                } else {
                    $badEmail = true;
                }

            }
        }

        return $this->render('AssistantBundle:Users:invite.html.twig', [
            'form' => $form->createView(),
            'badEmail' => $badEmail
        ]);
    }

    public function viewAction($userId)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $userId));

        if (empty($user)) {
            throw new NotFoundHttpException("Пользователь №{userId} не найден!");
        }

        $form = $this->createFormBuilder()->setAction($this->generateUrl('assistant_users_push_comment', [
                'userId' => $userId
            ]))->add('message', TextareaType::class)
            ->add('add', SubmitType::class, ['label' => 'Отправить', 'attr' => ['class' => 'btn btn-primary']])->getForm();

        $notification_form = $this->createFormBuilder()->setAction($this->generateUrl('assistant_users_push_notification', [
                'userId' => $userId
            ]))->add('message', TextareaType::class)
            ->add('link', TextType::class, ['required' => false])->add('sendEmail', CheckboxType::class, [
                    'label' => 'Отправить уведомление по email',
                    'required' => false
                ])
            ->add('add', SubmitType::class, ['label' => 'Отправить', 'attr' => ['class' => 'btn btn-primary']])->getForm();

        return $this->render('AssistantBundle:Users:view.html.twig', [
            'viewed_user' => $user,
            'form' => $form->createView(),
            'notification_form' => $notification_form->createView()
        ]);
    }

    public function pushCommentAction($userId, Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $userId));

        if (empty($user)) {
            throw new NotFoundHttpException("Пользователь №{userId} не найден!");
        }

        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('MessagingBundle:Message');

        $form = $this->createFormBuilder()->add('message', TextareaType::class)
            ->add('add', SubmitType::class, ['label' => 'Отправить', 'attr' => ['class' => 'btn btn-primary']])->getForm();

        $comment = new UserComment();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $comment->setMessage($form['message']->getData());
                $comment->setUser($user);
                $comment->setAssistant($this->getUser());

                $em->persist($comment);
                $em->flush();
            }
        }

        return $this->redirectToRoute('assistant_users_view', [
            'userId' => $userId
        ]);
    }

    public function pushNotificationAction($userId, Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $userId));

        if (empty($user)) {
            throw new NotFoundHttpException("Пользователь №{userId} не найден!");
        }

        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('MessagingBundle:Message');

        $form = $this->createFormBuilder()->add('message', TextareaType::class)
            ->add('link', TextType::class, ['required' => false])->add('sendEmail', CheckboxType::class, [
                    'label' => 'Отправить уведомление по email',
                    'required' => false
                ])
            ->add('add', SubmitType::class, ['label' => 'Отправить', 'attr' => ['class' => 'btn btn-primary']])->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $linkData = $form['link']->getData();
                $this->get('utils.notifier')->pushNotification($user, $form['message']->getData(), $linkData ? $linkData : '', $form['sendEmail']->getData());
            }
        }

        return $this->redirectToRoute('assistant_users_view', [
            'userId' => $userId
        ]);
    }

    protected function getSimpleInvoiceForm()
    {

        $form = $this->createFormBuilder();

        $form->add('tariff', ChoiceType::class, [
                'choices' => DebtCase::tariffsOptions(),
                'multiple' => false,
                'expanded' => false
            ])
            ->add('invoiceAmount', TextType::class)

            ->add('add', SubmitType::class, ['label' => 'Создать кейс и выслать счёт', 'attr' => ['class' => 'btn btn-primary']]);

        return $form->getForm();
    }

    protected function getInviteForm()
    {

        $form = $this->createFormBuilder();

        $form->add('email', EmailType::class)
            ->add('firstName', TextType::class, ['required' => false])
            ->add('lastName', TextType::class, ['required' => false])
            ->add('patronymic', TextType::class, ['required' => false])
            ->add('tariff', ChoiceType::class, [
                'choices' => DebtCase::tariffsOptions(),
                'multiple' => false,
                'expanded' => false
            ])
            ->add('invoiceAmount', TextType::class)

            ->add('add', SubmitType::class, ['label' => 'Пригласить и выслать счёт', 'attr' => ['class' => 'btn btn-primary']]);

        return $form->getForm();
    }

    protected function sendSimpleInvoice($user, $form) {
        $case = $this->get('sd_debt_case_service')->createCase($user, $form['tariff']->getData(), 0);
        $case->setFirstPayment($form['invoiceAmount']->getData());

        $this->getDoctrine()->getManager()->flush($case);

        $this->get('utils.mailer')->sendInvoiceMessage($user, $case);

        return $case;
    }

    protected function inviteNewUser($form) {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setEmail($form['email']->getData());
        $user->setEnabled(true);

        $user->setFirstName($form['firstName']->getData());
        $user->setLastName($form['lastName']->getData());
        $user->setPatronymic($form['patronymic']->getData());

        $pass = $this->generateRandomPassword(7);
        $user->setPlainPassword($pass);

        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush($user);

        $this->get('utils.mailer')->sendInviteWelcomeMessage($user, $pass);

        $case = $this->get('sd_debt_case_service')->createCase($user, $form['tariff']->getData(), 0);
        $case->setFirstPayment($form['invoiceAmount']->getData());

        $this->getDoctrine()->getManager()->flush($case);

        return $user;
    }


    protected function generateRandomPassword($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}

<?php

namespace SD\Bundle\AssistantBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractAssistantController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use SD\Bundle\AssistantBundle\Entity\StrategyItem;

class StrategyController extends AbstractAssistantController
{
    const SECTION_NAME = 'users';

    public function viewAction($caseId)
    {
        $case = $this->loadCaseBySafeId($caseId);
        // $form = $this->getStrategyItemForm($caseId);

        $form = $this->createFormBuilder($case)->setAction($this->generateUrl('assistant_strategy_set_custom', [
                'caseId' => $caseId
            ]))
            ->add('custom_strategy', TextareaType::class)
            ->add('add', SubmitType::class, ['label' => 'Сохранить', 'attr' => ['class' => 'btn btn-primary']])->getForm();

        return $this->render('AssistantBundle:Strategy:view.html.twig', [
            'case' => $case,
            'form' => $form->createView(),
        ]);
    }

    public function setCustomStrategyAction($caseId, Request $request) {
        $case = $this->loadCaseBySafeId($caseId);

        $form = $this->createFormBuilder($case)->add('custom_strategy', TextareaType::class)
            ->add('add', SubmitType::class, ['label' => 'Сохранить', 'attr' => ['class' => 'btn btn-primary']])->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->getDoctrine()->getManager()->flush($case);
            }
        }

        return $this->redirectToRoute('assistant_strategy_view', ['caseId' => $caseId]);
    }


    public function pushAction($caseId, Request $request)
    {
        $case = $this->loadCaseBySafeId($caseId);
        $form = $this->getStrategyItemForm($caseId);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $item = new StrategyItem();
                $item->setUser($case->getUser());
                $item->setCase($case);
                $item->setTitle($form['title']->getData());
                $item->setStatus($form['status']->getData());
                $item->setOrderNumber(count($case->getStrategyItems()) + 1);
                $em->persist($item);
                $em->flush();
            }
        }

        return $this->redirectToRoute('assistant_strategy_view', ['caseId' => $caseId]);
    }

    public function editAction($caseId, $strategyItemId, Request $request)
    {
        $case = $this->loadCaseBySafeId($caseId);

        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository('AssistantBundle:StrategyItem');

        $item = $repo->findOneBy([
            'id' => $strategyItemId
        ]);

        $form = $this->getStrategyItemForm($caseId, $item);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em->flush();
                return $this->redirectToRoute('assistant_strategy_view', ['caseId' => $caseId]);
            }
        }

        return $this->render('AssistantBundle:Strategy:edit.html.twig', [
            'case' => $case,
            'item' => $item,
            'form' => $form->createView(),
        ]);
    }

    public function reorderAction($caseId, $strategyItemId, $way, Request $request)
    {
        $case = $this->loadCaseBySafeId($caseId);

        $em = $this->getDoctrine()->getManager();
        $list = $case->getStrategyItems()->toArray();
        foreach ($list as $i => $item) {
            if ($item->getId() == (int) $strategyItemId) {
                if ($way == 'up') {
                    list($a, $b) = [$item->getOrderNumber(), $list[$i - 1]->getOrderNumber()];
                    $item->setOrderNumber($b);
                    $list[$i - 1]->setOrderNumber($a);
                    break;
                } elseif ($way == 'down') {
                    list($a, $b) = [$item->getOrderNumber(), $list[$i + 1]->getOrderNumber()];
                    $item->setOrderNumber($b);
                    $list[$i + 1]->setOrderNumber($a);
                    break;
                }
            }
        }
        $em->flush();

        return $this->redirectToRoute('assistant_strategy_view', ['caseId' => $caseId]);
    }

    public function deleteAction($caseId, $strategyItemId, Request $request)
    {
        $case = $this->loadCaseBySafeId($caseId);
        $em = $this->getDoctrine()->getManager();
        $i = 1;
        foreach ($case->getStrategyItems() as $item) {
            if ($item->getId() == $strategyItemId) {
                $em->remove($item);
            } else {
                $item->setOrderNumber($i++);
            }
        }
        $em->flush();

        return $this->redirectToRoute('assistant_strategy_view', ['caseId' => $caseId]);
    }

    protected function getStrategyItemForm($caseId, $model = null)
    {

        if (!empty($model)) {
            $form = $this->createFormBuilder($model);
        } else {
            $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('assistant_strategy_push', ['caseId' => $caseId]));
        }

        $form->add('status', ChoiceType::class, [
            'choices' => [StrategyItem::STATUS_ACTIVE => 'Активный', StrategyItem::STATUS_DONE => 'Выполнен'],
            'multiple' => false,
            'expanded' => false
        ])
            ->add('title', TextType::class)
            ->add('add', SubmitType::class, ['label' => empty($model) ? 'Добавить' : 'Сохранить', 'attr' => ['class' => 'btn btn-primary']]);

        return $form->getForm();
    }

}

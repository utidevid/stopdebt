<?php
namespace SD\Bundle\AssistantBundle\Services;

class Notifier  {

    protected $router;
    protected $utilsNotifier;
    protected $assistants = [];

    public function __construct($doctrine, $utilsNotifier, $router) {
        $this->router = $router;
        $this->utilsNotifier = $utilsNotifier;

        $repo = $doctrine->getRepository('UserBundle:User');
        $this->assistants = $repo->findByRole('ROLE_ASSISTANT');
    }

    public function notifyHasNewUser($newUser) {
        $message = 'Новый пользователь в системе: '.$newUser->getEmail();
        $link = $this->router->generate('assistant_users_view', [
            'userId' => $newUser->getId()
        ], true);

        foreach ($this->assistants as $assistant) {
            $this->utilsNotifier->pushNotification($assistant, $message, $link, false);
        }
    }

    public function notifyNewDocumentUpload($document) {
        $message = 'Новая загрузка в документе '.$document->getSafeId();
        $link = $this->router->generate('assistant_documents_view', [
            'caseId' => $document->getCase()->getSafeId(),
            'documentId' => $document->getSafeId()
        ], true);

        foreach ($this->assistants as $assistant) {
            $this->utilsNotifier->pushNotification($assistant, $message, $link, false);
        }
    }

    public function notifyQuestionnaireFinished($questionnaire) {
        $message = 'Клиент заполнил анкету '.$questionnaire->getSafeId();
        $link = $this->router->generate('assistant_questionnaires_view', [
            'questionnaireId' => $questionnaire->getSafeId(),
        ], true);

        foreach ($this->assistants as $assistant) {
            $this->utilsNotifier->pushNotification($assistant, $message, $link, false);
        }
    }

}
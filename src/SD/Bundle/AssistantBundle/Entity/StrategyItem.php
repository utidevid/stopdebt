<?php

namespace SD\Bundle\AssistantBundle\Entity;

/**
 * StrategyItem
 */
class StrategyItem
{

    const STATUS_ACTIVE = 0;
    const STATUS_DONE = 1;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $status;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return StrategyItem
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return StrategyItem
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var \SD\Bundle\UserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \SD\Bundle\UserBundle\Entity\User $user
     *
     * @return StrategyItem
     */
    public function setUser(\SD\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \SD\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var integer
     */
    private $orderNumber;


    /**
     * Set orderNumber
     *
     * @param integer $orderNumber
     *
     * @return StrategyItem
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * Get orderNumber
     *
     * @return integer
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }
    /**
     * @var \SD\Bundle\CaseBundle\Entity\DebtCase
     */
    private $case;


    /**
     * Set case
     *
     * @param \SD\Bundle\CaseBundle\Entity\DebtCase $case
     *
     * @return StrategyItem
     */
    public function setCase(\SD\Bundle\CaseBundle\Entity\DebtCase $case = null)
    {
        $this->case = $case;

        return $this;
    }

    /**
     * Get case
     *
     * @return \SD\Bundle\CaseBundle\Entity\DebtCase
     */
    public function getCase()
    {
        return $this->case;
    }
}

<?php

namespace SD\Bundle\AssistantBundle\Entity;

/**
 * UserComment
 */
class UserComment
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return UserComment
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return UserComment
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * @var \SD\Bundle\UserBundle\Entity\User
     */
    private $user;

    /**
     * @var \SD\Bundle\UserBundle\Entity\User
     */
    private $assistant;


    /**
     * Set user
     *
     * @param \SD\Bundle\UserBundle\Entity\User $user
     *
     * @return UserComment
     */
    public function setUser(\SD\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \SD\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set assistant
     *
     * @param \SD\Bundle\UserBundle\Entity\User $assistant
     *
     * @return UserComment
     */
    public function setAssistant(\SD\Bundle\UserBundle\Entity\User $assistant = null)
    {
        $this->assistant = $assistant;

        return $this;
    }

    /**
     * Get assistant
     *
     * @return \SD\Bundle\UserBundle\Entity\User
     */
    public function getAssistant()
    {
        return $this->assistant;
    }
}

<?php

namespace SD\Bundle\AssistantBundle\Entity;

/**
 * Reminder
 */
class Reminder
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $startAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Reminder
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Reminder
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set startAt
     *
     * @param \DateTime $startAt
     *
     * @return Reminder
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt
     *
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }
    /**
     * @var \SD\Bundle\CaseBundle\Entity\DebtCase
     */
    private $case;


    /**
     * Set case
     *
     * @param \SD\Bundle\CaseBundle\Entity\DebtCase $case
     *
     * @return Reminder
     */
    public function setCase(\SD\Bundle\CaseBundle\Entity\DebtCase $case = null)
    {
        $this->case = $case;

        return $this;
    }

    /**
     * Get case
     *
     * @return \SD\Bundle\CaseBundle\Entity\DebtCase
     */
    public function getCase()
    {
        return $this->case;
    }
}

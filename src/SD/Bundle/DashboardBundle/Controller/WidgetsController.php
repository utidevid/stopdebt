<?php

namespace SD\Bundle\DashboardBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractUserController;
use SD\Bundle\CaseBundle\Entity\DebtCase;

class WidgetsController extends AbstractUserController
{
    public function flowAction($caseId)
    {
        $this->loadCaseBySafeId($caseId);

        return $this->render('DashboardBundle:Widgets:flow.html.twig', array(
            'flowInfo' => $this->getFlowInfo($this->activeCase),
        ));
    }

    public function billingAction($caseId)
    {
        $this->loadCaseBySafeId($caseId);

        return $this->render('DashboardBundle:Widgets:billing.html.twig', array(
            'flowInfo' => $this->getFlowInfo($this->activeCase),
        ));
    }

    public function remindersAction($caseId)
    {
        $this->loadCaseBySafeId($caseId);

        $previous = [];
        $list = [];

        $now = new \DateTime();

        foreach ($this->activeCase->getReminders() as $reminder) {
            if ($reminder->getStartAt() >= $now) {
                $list[] = $reminder;
            } else {
                $previous[] = $reminder;
            }
        }

        return $this->render('DashboardBundle:Widgets:reminders.html.twig', array(
            'previous' => $previous,
            'list' => $list
        ));
    }

    protected function getFlowInfo($case) {
        $steps = [
            'standard' => ['Заполнение анкеты клиентом', 'Отображение отчёта и стратегии'],
            'optimal' => ['Загрузка документов', 'Заполнение анкеты клиентом', 'Skype звонок', 'Подготовка стратегии асcистентом', 'Отображение отчёта и стратегии'],
            'advanced' => ['Загрузка документов', 'Заполнение анкеты асcистентом', 'Skype звонок', 'Ревью анкеты клиентом', 'Собеседование с клиентом', 'Подготовка стратегии асcистентом', 'Отображение отчёта и стратегии']
        ];

        $data = [];

        foreach ($steps[$case->getTariff()] as $i => $title) {
            $automatic = true;
            if ($case->getTariff() == 'optimal') {
                $automatic = ! in_array($case->getStage(), [DebtCase::OPTIMAL_TARIFF_STAGE_3_SKYPE_CALL, DebtCase::OPTIMAL_TARIFF_STAGE_4_PREPARING_STRATEGY]);
            }
            if ($case->getTariff() == 'advanced') {
                $automatic = ! in_array($case->getStage(), [DebtCase::ADVANCED_TARIFF_STAGE_3_SKYPE_CALL, DebtCase::ADVANCED_TARIFF_STAGE_5_INTERVIEW, DebtCase::ADVANCED_TARIFF_STAGE_6_PREPARING_STRATEGY]);
            }

            $data[] = [
                'label' => $title,
                'active' => $i + 1 == $case->getStage(),
                'done' => $i + 1 < $case->getStage(),
                'future' => $i + 1 > $case->getStage(),
                'automatic' => $automatic
            ];
        }

        return $data;
    }

}

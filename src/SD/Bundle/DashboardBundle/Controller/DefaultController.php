<?php

namespace SD\Bundle\DashboardBundle\Controller;

use SD\Bundle\UtilsBundle\Controller\AbstractUserController;

class DefaultController extends AbstractUserController
{
    const SECTION_NAME = 'dashboard';

    public function indexAction($caseId)
    {
        $this->loadCaseBySafeId($caseId, true);
        return $this->render('DashboardBundle:Default:index.html.twig');
    }
}
